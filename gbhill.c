/*
  gbhill (ver. 5.6) -- Hill Maximum Likelihhod estimation
  Copyright (C) 2012-2018 Giulio Bottazzi

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  (version 2) as published by the Free Software Foundation;
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#include "gbhill.h"
#include "multimin.h"

/* 
   In C ordering

   x[0] <= x[1] <= ... <= X[N-1]

   so the (relative) negative log-likelihood is written

method:      def:
0    -LL[up,unc]/k = C[up,unc] -1/k\sum_{j=1}^{k} log f(x[j]) - (N/k-1) log(F(x[k]))
1    -LL[up,con]/(k+1) = C[up,con2] -1/(k+1)\sum_{j=1}^{k+1} log f(x[j]) -(N/(k+1) -1) log(F(d))
2    -LL[lo,unc]/k = C[lo,unc] -1/k\sum_{j=N-k+1}^{N} log f(x[j])  - (N/k-1) log(1-F(x[N-k-1]))
3    -LL[lo,con]/(k+1) = C[lo,con2] -1/(k+1)\sum_{j=N-k}^{N} log f(x[j])  - (N/(k+1) -1) log(1-F(d))

*/


int main(int argc,char* argv[]){

  char *splitstring = strdup(" \t");

  int o_verbose=0;
  int o_output=0;
  int o_printall=0;
  int o_varcovar=1;
  
  /* binary options */
  int o_binary_in=0;

  /* name of the distribution to fit */
  char *name;

  /* store the amount of data to use */
  double used=1;

  /* minimization parameters */
  size_t xnum;
  double llmin;
  double *x;
  unsigned *xtype;
  double *xmin;
  double *xmax;
  char **xname;

  /* definition of the covariance matrix */
  gsl_matrix *Pcovar=NULL;  /* covariance matrix */
 
  
  /* pointer to functions to use */
  double (*f)  (const double,const size_t, const double *);
  double (*F)  (const double,const size_t, const double *);

  void (*obj_f) (const size_t, const double *,void *,double *);
  void (*obj_df) (const size_t, const double *,void *,double *);
  void (*obj_fdf) (const size_t, const double *,void *,double *,double *);

  gsl_matrix * (*varcovar) (const int, double *, void *);

  /* log-likelihood parameters */
  struct loglike_params llparams={0,NULL,0,0,0,0};

  /* minimization parameters */
  struct multimin_params mmparams={.01,.01,100,1e-6,1e-6,5,0};

  /* variables for reading command line options */
  /* ------------------------------------------ */
  int opt;
  /* ------------------------------------------ */

  /* COMMAND LINE PROCESSING */
  while((opt=getopt_long(argc,argv,"v:hF:O:A:u:M:V:ab:",gb_long_options, &gb_option_index))!=EOF){
    if(opt==0){
      gbutils_header(argv[0],stdout);
      exit(0);
    }
    else if(opt=='?'){
      fprintf(stderr,"option %c not recognized\n",optopt);
      exit(-1);
    }
    else if(opt=='h'){
      /*print help*/
      fprintf(stdout,"Maximum Likelihood estimation of distribution based on extremal\n");
      fprintf(stdout,"(tail) observations. The distributions included are: exponential,\n");
      fprintf(stdout,"pareto1, pareto3, gaussian. Provide the name of the distribution\n");
      fprintf(stdout,"and the initial values of the parameters in the command line.\n");
      fprintf(stdout,"\nUsage: %s [options] <function definition> \n\n",argv[0]);
      fprintf(stdout,"Options:\n");
      fprintf(stdout," -O  type of output (default 0) \n");
      fprintf(stdout,"      0  parameters and min NLL       \n");
      fprintf(stdout,"      1  parameters and errors        \n");
      fprintf(stdout,"      2  the distribution function    \n");
      fprintf(stdout,"      3  the density function         \n");
      fprintf(stdout,"      4  transformed observations: uniform in [0.1] under the null\n");
      fprintf(stdout,"      5  log Renyi residuals: iid uniform in [0.1] under the null\n");
      fprintf(stdout,"      6  Renyi residuals: iid exponential with mean 1 under the null\n");
      fprintf(stdout," -M  method used (default 0) \n");
      fprintf(stdout,"      0  unconditional, upper tail    \n");
      fprintf(stdout,"      1  threshold, upper tail        \n");
      fprintf(stdout,"      2  unconditional, lower tail    \n");
      fprintf(stdout,"      3  threshold, lower tail        \n");
      fprintf(stdout," -V  variance matrix estimation (default2) \n");
      fprintf(stdout,"      0  < J^{-1} >                  \n");
      fprintf(stdout,"      1  < H^{-1} >                  \n");
      fprintf(stdout,"      2  < H^{-1} J H^{-1} >         \n");
      fprintf(stdout," -v  verbosity level (default0) \n");
      fprintf(stdout,"      0  just results                \n");
      fprintf(stdout,"      1  comment headers             \n");
      fprintf(stdout,"      2  summary statistics          \n");
      fprintf(stdout,"      3+ minimization steps          \n");
      fprintf(stdout," -a  print entire set for -O 1,2      \n");
      fprintf(stdout," -u  observations or threshold (default 1)\n");
      fprintf(stdout," -F  input fields separators (default \" \\t\")\n");
      fprintf(stdout," -h  this help\n");
      fprintf(stdout," -A  comma separated MLL optimization options step,tol,iter,eps,msize,\n");
      fprintf(stdout,"     algo. Use empty fields for default (default 0.01,0.01,100,1e-6,1e-6,5)\n");
      fprintf(stdout,"      step  initial step size of the searching algorithm               \n");
      fprintf(stdout,"      tol   line search tolerance iter: maximum number of iterations   \n");
      fprintf(stdout,"      eps   gradient tolerance : stopping criteria ||gradient||<eps    \n");
      fprintf(stdout,"      algo  optimization methods: 0 Fletcher-Reeves, 1 Polak-Ribiere,  \n");
      fprintf(stdout,"            2 Broyden-Fletcher-Goldfarb-Shanno, 3 Steepest descent,\n");
      fprintf(stdout,"            4 simplex, 5 Broyden-Fletcher-Goldfarb-Shanno2.\n");
      fprintf(stdout,"\n");
      fprintf(stdout,"Examples:\n");
      fprintf(stdout,"  gbhill pareto1 1 1 < file.dat  estimate the Pareto type 1 distribution, \n");
      fprintf(stdout,"                                 initial values are gamma=1 and b=1 \n");
      fprintf(stdout,"  gbhill -u .2 pareto1 1 1 < file.dat  the same using only top 20%% observations\n");
      exit(0);
    }
    else if(opt=='u'){
      /* set the fields separator string */
      used = atof(optarg);
    }
    else if(opt=='a'){
      /* set the fields separator string */
      o_printall = 1;
    }
    else if(opt=='F'){
      /* set the fields separator string */
      free(splitstring);
      splitstring = strdup(optarg);
    }
    else if(opt=='O'){
      /* set the type of output */
      o_output = atoi(optarg);
      if(o_output<0 || o_output>6){
	fprintf(stderr,"ERROR (%s): output option '%d' not recognized. Try option -h.\n",
		GB_PROGNAME,o_output);
	exit(-1);
      }
    }
     else if(opt=='V'){
      /* set the type of covariance */
      o_varcovar = atoi(optarg);
      if(o_varcovar<0 || o_varcovar>2){
	fprintf(stderr,"ERROR (%s): variance option '%d' not recognized. Try option -h.\n",
		GB_PROGNAME,o_varcovar);
	exit(-1);
      }
     }
     else if(opt=='M'){
      /* set the type of method */
      llparams.method = atoi(optarg);
      if(llparams.method<0 || llparams.method>3){
	fprintf(stderr,"ERROR (%s): method '%d' not recognized. Try option -h.\n",
		GB_PROGNAME,llparams.method);
	exit(-1);
      }
    }
    else if(opt=='A'){
      char *stmp1=strdup (optarg);
      char *stmp2;
      char *stmp3=stmp1;
      if( (stmp2=strsep (&stmp1,",")) != NULL){
	if(strlen(stmp2)>0) 
	  mmparams.step_size=atof(stmp2);
      }
      if( (stmp2=strsep (&stmp1,",")) != NULL){
	if(strlen(stmp2)>0) 
	  mmparams.tol=atof(stmp2);
      }
      if( (stmp2=strsep (&stmp1,",")) != NULL){
	if(strlen(stmp2)>0) 
	  mmparams.maxiter=(unsigned) atoi(stmp2);
      }
      if( (stmp2=strsep (&stmp1,",")) != NULL){
	if(strlen(stmp2)>0) 
	  mmparams.epsabs=atof(stmp2);
      }
      if( (stmp2=strsep (&stmp1,",")) != NULL){
	if(strlen(stmp2)>0) 
	  mmparams.maxsize=atof(stmp2);
      }
      if( (stmp2=strsep (&stmp1,",")) != NULL){
	if(strlen(stmp2)>0) 
	  mmparams.method= (unsigned) atoi(stmp2);
      }
      free(stmp3);
    }
    else if(opt=='v'){
      o_verbose = atoi(optarg);
      mmparams.verbosity=(o_verbose>2?o_verbose-2:0);
    }
    else if(opt=='b'){
      /*set binary input or output*/

      switch(atoi(optarg)){
      case 0 :
	break;
      case 1 :
	/* binary input */
	o_binary_in=1;
	break;
      case 2 :
	/* binary output */
	PRINTCOL=printcol_bin;
	break;
      case 3 :
	/* binary input and output */
	o_binary_in=1;
	PRINTCOL=printcol_bin;
	break;
      default:
	fprintf(stderr,"unclear binary specification %d, option ignored.\n",atoi(optarg));
	break;
      }
    }
  }
  /* END OF COMMAND LINE PROCESSING */

  /* initialize global variables */
  initialize_program(argv[0]);

  /* load data */
  /* ========= */
  if(o_binary_in==1)
    load_bin(&llparams.data,&llparams.size,0);
  else
    load(&llparams.data,&llparams.size,0,splitstring);


  /* sort data in ascending order */
  /* ============================ */
  qsort(llparams.data,llparams.size,sizeof(double),sort_by_value);

  
  /* decide the number of observations used */
  /* ====================================== */
  if(llparams.method == 1)
    {
      size_t i;

      if(llparams.data[llparams.size-1]<=used){
	fprintf(stderr,"ERROR (%s): wrong value, too large threshold: %g\n",
		GB_PROGNAME,used);
	exit(-1);
      }

      llparams.d=used;
      for(i=0;i<llparams.size;i++)
	if(llparams.data[i]>used) break;
      llparams.used=llparams.size-i;
    }
  else if(llparams.method == 3)
    {
      size_t i;

      if(llparams.data[0]>=used){
	fprintf(stderr,"ERROR (%s): wrong value, too small threshold: %g\n",
		GB_PROGNAME,used);
	exit(-1);
      }

      llparams.d= used;
      for(i=0;i<llparams.size;i++)
	if(llparams.data[i]>used) break;
      llparams.used=i;
    }
  else
    if (used>0 && used<=1)
      llparams.used=llparams.size*used;
    else if(  used>0 && floor(used) == used && used <= llparams.size )
      llparams.used=(size_t) used;
    else{
      fprintf(stderr,"ERROR (%s): wrong value, used observations: %g\n",
	      GB_PROGNAME,used);
      exit(-1);
    }
  

  /* set minimum and maximum index */
  /* ============================= */
  switch(llparams.method){
  case 0:
    llparams.min=llparams.size-llparams.used;
    llparams.max=llparams.size-1;
    break;
  case 1:
    llparams.min=llparams.size-llparams.used;
    llparams.max=llparams.size-1;
    break;
  case 2:
    llparams.min=0;
    llparams.max=llparams.used-1;
    break;
  case 3:
    llparams.min=0;
    llparams.max=llparams.used-1;
    break;
  }


  /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
  if(o_verbose){
    fprintf(stderr,"# total observations %zd; xmin=%g xmax=%g\n",
	    llparams.size,llparams.data[0],llparams.data[llparams.size-1]);

    switch(llparams.method){
    case 0:
      fprintf(stderr,"# tail:       upper\n");
      fprintf(stderr,"# condition:  unconditional\n");
      fprintf(stderr,"# num obs:    %zd\n",llparams.used);
      break;
    case 1:
      fprintf(stderr,"# tail:       upper\n");
      fprintf(stderr,"# condition:  x[%zd]>%g\n",
	      llparams.min,llparams.d);
      fprintf(stderr,"# num obs:    %zd\n",llparams.used);
      break;
    case 2:
      fprintf(stderr,"# tail:       lower\n");
      fprintf(stderr,"# condition:  unconditional\n");
      fprintf(stderr,"# num obs:    %zd\n",llparams.used);
      break;
    case 3:
      fprintf(stderr,"# tail:       lower\n");
      fprintf(stderr,"# condition:  x[%zd]<%g\n",
	      llparams.max,llparams.d);
      fprintf(stderr,"# num obs:    %zd\n",llparams.used);
      break;
    }
    fprintf(stderr,"# indexes:    from %zd to %zd\n",
	    llparams.min,llparams.max);
    fprintf(stderr,"# values:     from %g to %g\n",
	    llparams.data[llparams.min],llparams.data[llparams.max]);

  }
  /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */


  /* estimate parameters */
  /* =================== */

#define NAME(x) !strcmp(name,(x))
#define PERROR(x) {fprintf(stderr,"ERROR (%s): parameters needed: %s\n",GB_PROGNAME,x); exit(-1);}
#define DBL_ARG(x) if (argc-optind) { x=atof(argv[optind]);optind++;} else {PERROR( #x);};

  if(optind<argc){
    name = argv[optind]; 
    optind++;
  }
  else{
    fprintf(stderr,"ERROR (%s): please provide a distribution, available choices:\n",GB_PROGNAME);
    fprintf(stderr,"ERROR (%s):   exponential pareto1 gaussian pareto3\n",GB_PROGNAME);
    exit(-1);
  }


  if (NAME("exponential"))
    {


      /* parameters initial settings */
      /* --------------------------- */

      xnum=2;
      
      x     = (double *)   my_alloc(xnum*sizeof(double));
      xmin  = (double *)   my_alloc(xnum*sizeof(double));
      xmax  = (double *)   my_alloc(xnum*sizeof(double));
      xtype = (unsigned *) my_alloc(xnum*sizeof(unsigned));
      xname = (char **) my_alloc(xnum*sizeof(char *));

      
      /* mu */
      xname[0]=strdup("mu (tail index)");
      x[0]=1.0;
      xmin[0]=0.0;
      xmax[0]=NAN;
      xtype[0]=4;
      
      /* b */
      xname[1]=strdup("b (position)");
      x[1]=0.0;
      xmin[1]=NAN;

      /* upper bound on b */
      switch(llparams.method){
	/* upper tail */
      case 0:
	xmax[1]=llparams.data[llparams.size-llparams.used];
	break;
      case 1:
	xmax[1]=llparams.d;
	break;
	/* lower tail */
      case 2:
      case 3:
	xmax[1]=llparams.data[0];
	break;
      }

      /* minimization method for b */
      switch(llparams.method){
	/* upper tail */
      case 0:
      case 1:
	/* for method=0,1 solution is internal */
	xtype[1]=9;
	break;
	/* lower tail */
      case 2:
      case 3:
	/* for method=2,3 solution is on the boundary */
	xtype[1]=2;
	break;
      }

      /* check number of parameters and load their values */
      if (argc < optind+2){
	fprintf(stderr,"ERROR (%s): exponential density f(x)=exp(-(1/mu)*(x-b))/mu\n",GB_PROGNAME);
	fprintf(stderr,"ERROR (%s): provide initial values of mu (exponential tail index), b (position)\n",GB_PROGNAME);
	exit(-1);
      }
      DBL_ARG(*x);
      DBL_ARG(*(x+1));


      /* check parameters initial values */

      if(x[0]<=0.0){
	x[0]=1.0;
	if(o_verbose){
	  fprintf(stderr,
		"WARNING (%s): parameter mu out of range; set to %g\n",
		GB_PROGNAME,x[0]);
	  fprintf(stderr,"WARNING (%s): mu must be positive\n",
		GB_PROGNAME);
	}
      }

      if(x[1] > xmax[1] ){
	
	x[1] = xmax[1] - mmparams.step_size;
	if(o_verbose){
	  fprintf(stderr,"WARNING (%s): parameter b out of range; set to %g\n",
		GB_PROGNAME,x[1]);
	  fprintf(stderr,"WARNING (%s): b must be smaller equal than the smallest observation used\n",
		GB_PROGNAME);
	 }
      }

      /* set the functions to use */
      F        = exponential_F;
      f        = exponential_f;
      obj_f    = exponential_nll;
      obj_df   = exponential_dnll;
      obj_fdf  = exponential_nlldnll;
      varcovar = exponential_varcovar;

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      if(o_verbose){
	if (llparams.method>1)
	  fprintf(stderr,"WARNING (%s): likelihood is monotonic in b: boundary solution\n",
		  GB_PROGNAME);
      }
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

    }
  else if (NAME("pareto1"))
    {
      
      xnum=2;
      
      x     = (double *)   my_alloc(xnum*sizeof(double));
      xmin  = (double *)   my_alloc(xnum*sizeof(double));
      xmax  = (double *)   my_alloc(xnum*sizeof(double));
      xtype = (unsigned *) my_alloc(xnum*sizeof(unsigned));
      xname = (char **) my_alloc(xnum*sizeof(char *));
      
      /* gamma */
      xname[0]=strdup("gamma (tail index)");
      x[0]=1.0;
      xmin[0]=0.0;
      xmax[0]=NAN;
      xtype[0]=4;
      
      /* b */
      xname[1]=strdup("b (position)");
      x[1]=0.0;
      xmax[1]=NAN;

      /* lower bound on b */
      switch(llparams.method){
	/* upper tail */
      case 0:
	xmin[1]=1./llparams.data[llparams.size-llparams.used];
	break;
      case 1:
	xmin[1]=1./llparams.d;
	break;
	/* lower tail */
      case 2:
      case 3:
	xmin[1]=1./llparams.data[0];
	break;
      }

      /* minimization method for b */
      switch(llparams.method){
	/* upper tail */
      case 0:
      case 1:
	/* for method=0,1 solution is internal */
	xtype[1]=4;
	break;
	/* lower tail */
      case 2:
      case 3:
	/* for method=2,3 solution is on the boundary */
	xtype[1]=4;
	break;
      }

      /* check number of parameters and load their values */
      if (argc < optind+2){
	fprintf(stderr,"ERROR (%s): Pareto Type 1 distribution F(x)=1-(b x[j])^(-1/gamma)\n",GB_PROGNAME);
	fprintf(stderr,"ERROR (%s): provide initial values of gamma (tail index) and b (position)\n",GB_PROGNAME);
	exit(-1);
      }
      DBL_ARG(*x);
      DBL_ARG(*(x+1));
      /* check parameters initial values */
      if(x[0]<= 0){
	x[0]=1.0;
	if(o_verbose){
	  fprintf(stderr,
		  "WARNING (%s): parameter gamma out of range; set to %g\n",
		  GB_PROGNAME,x[0]);
	  fprintf(stderr,
		  "WARNING (%s): gamma must be positive\n",
		  GB_PROGNAME);
	}
      }
      
      if(x[1] <= xmin[1] ){
	/* N.B.: when llparams.method==1 the likelihood DOES NOT
	   depend on b; so in principle one could set x[1] to
	   xmin[1]. For consistencey with other cases, we retain this
	   setting. */
	x[1] = xmin[1]+ mmparams.step_size;
	if(o_verbose){
	  fprintf(stderr,"WARNING (%s): parameter b out of range; set to %g\n",
		  GB_PROGNAME,x[1]);
	  fprintf(stderr,"WARNING (%s): b must be larger than the inverse of the smallest observation used\n",
		  GB_PROGNAME);
	}
      }

      /* set the functions to use */
      F        = pareto1_F;
      f        = pareto1_f;
      obj_f    = pareto1_nll;
      obj_df   = pareto1_dnll;
      obj_fdf  = pareto1_nlldnll;
      varcovar = pareto1_varcovar;

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      if(o_verbose){
	if (llparams.method>1)
	  fprintf(stderr,"WARNING (%s): likelihood is monotonic in b: boundary solution\n",
		  GB_PROGNAME);
      }
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */


    }
  else if (NAME("pareto3"))
    {
      
      xnum=3;
      
      x     = (double *)   my_alloc(xnum*sizeof(double));
      xmin  = (double *)   my_alloc(xnum*sizeof(double));
      xmax  = (double *)   my_alloc(xnum*sizeof(double));
      xtype = (unsigned *) my_alloc(xnum*sizeof(unsigned));
      xname = (char **) my_alloc(xnum*sizeof(char *));
      
      /* alpha */
      xname[0]=strdup("alpha (power decay index)");
      x[0]=1.0;
      xmin[0]=0.0;
      xmax[0]=NAN;
      xtype[0]=4;
      
      /* beta */
      xname[1]=strdup("beta (exponential decay index)");
      x[1]=1.0;
      xmin[1]=0.0;
      xmax[1]=NAN;
      xtype[1]=4;

      /* s */
      xname[2]=strdup("s (scale)");
      x[2]=1.0;      
      xmin[2]=0.0;
      xmax[2]=NAN;
      xtype[2]=4;


      if (argc < optind+3){
	fprintf(stderr,"ERROR (%s): Pareto Type III distribution F(x)=1-(x/s +1)^(-alpha) exp(-beta x/s)\n",GB_PROGNAME);
	fprintf(stderr,"ERROR (%s): provide initial values of alpha (power decay index), beta (exponential decay index), s (scale)\n",GB_PROGNAME);
	exit(-1);
      }
      DBL_ARG(*x);
      DBL_ARG(*(x+1));
      DBL_ARG(*(x+2));
      
      /* check parameters initial values */
      if(x[0]<=0){
	x[0]=1.0;
	if(o_verbose){
	  fprintf(stderr,
		  "WARNING (%s): parameter alpha out of range; set to %g\n",
		  GB_PROGNAME,x[0]);
	  fprintf(stderr,"WARNING (%s): alpha must be positive\n",GB_PROGNAME);
	}
      }

      if(x[1] <=0 ){
	x[1] = 1.0 ;
	if(o_verbose){
	  fprintf(stderr,"WARNING (%s): parameter beta out of range; set to %g\n",
		  GB_PROGNAME,x[1]);
	  fprintf(stderr,"WARNING (%s): beta must be positive\n",GB_PROGNAME);
	}
      }
      

      if(x[2] <= 0 ){
	x[2] = 1.0 ;
	if(o_verbose){
	  fprintf(stderr,"WARNING (%s): parameter s out of range; set to %g\n",
		  GB_PROGNAME,x[2]);
	  fprintf(stderr,"WARNING (%s): s must be positive\n",GB_PROGNAME);
	}
      }


      /* set the functions to use */
      F        = pareto3_F;
      f        = pareto3_f;
      obj_f    = pareto3_nll;
      obj_df   = pareto3_dnll;
      obj_fdf  = pareto3_nlldnll;
      varcovar = pareto3_varcovar;

    }
  else if(NAME("gaussian"))
    {
      xnum=2;
      
      x     = (double *)   my_alloc(xnum*sizeof(double));
      xmin  = (double *)   my_alloc(xnum*sizeof(double));
      xmax  = (double *)   my_alloc(xnum*sizeof(double));
      xtype = (unsigned *) my_alloc(xnum*sizeof(unsigned));
      xname = (char **) my_alloc(xnum*sizeof(char *));
      
      /* Mean */
      xname[0]=strdup("m (mean)");
      x[0]=0.0;
      xmin[0]=NAN;
      xmax[0]=NAN;
      xtype[0]=0;
      
      /* Standard Deviation */
      xname[1]=strdup("s (standard deviation)");
      x[0]=1.0;
      xmin[1]=0.0;
      xmax[1]=NAN;
      xtype[1]=4;
      if (argc < optind+2){
	fprintf(stderr,"ERROR (%s): Gaussian density f(x)=exp(-1/2 (x-m)^2/s^2)/sqrt(2 pi s^2)\n",GB_PROGNAME);
	fprintf(stderr,"ERROR (%s): provide initial values of m (mean) and s (standard deviation)\n",GB_PROGNAME);
	exit(-1);
      }
      DBL_ARG(*x);
      DBL_ARG(*(x+1));


      /* check parameters initial values */
      if(x[1] <= 0 ){
	x[1] = 1.0 ;
	if(o_verbose){
	  fprintf(stderr,"WARNING (%s): parameter s out of range; set to %g\n",
		GB_PROGNAME,x[1]);
	  fprintf(stderr,"WARNING (%s): s must be positive\n",
		GB_PROGNAME);
	}
      }

      /* set the functions to use */
      F        = gaussian_F;
      f        = gaussian_f;
      obj_f    = gaussian_nll;
      obj_df   = gaussian_dnll;
      obj_fdf  = gaussian_nlldnll;
      varcovar = gaussian_varcovar;
  
    }
  else
    {
      fprintf(stderr,"ERROR (%s): unrecognized distribution: %s\n",GB_PROGNAME,name);
      fprintf(stderr,"ERROR (%s): available choices:\n",GB_PROGNAME);
      fprintf(stderr,"ERROR (%s):  exponential pareto1 gaussian pareto3\n",GB_PROGNAME);
      exit(-1);
    }
  

  /* minimize negative log likelihood */
  /* ================================ */
  multimin(xnum,x,&llmin,xtype,xmin,xmax,obj_f,obj_df,obj_fdf,(void *) &llparams,mmparams);
  

  /* compute variance-covariance matrix */
  /* ================================== */
  if(o_output==1)
    Pcovar = varcovar(o_varcovar,x,&llparams);
 

  /* output */
  /* ===== */

  switch(o_output){
  case 0: /* print parameters*/
    {
      size_t i;
   
      /* ++++++++++++++++++++++++d+++++++++++ */
      if(o_verbose>=1){
	for(i=0;i<xnum;i++)
	  fprintf(stderr,EMPTY_SEP,xname[i]);
	fprintf(stderr,EMPTY_NL,"min NLL/obs");
      }
      /* +++++++++++++++++++++++++++++++++++ */
      
      for(i=0;i<xnum;i++)
	fprintf(stdout,FLOAT_SEP,x[i]);
      fprintf(stdout,FLOAT_NL,llmin);
    }
    break;
  case 1:
    {
      size_t i;
      
      /* +++++++++++++++++++++++++++++++++++ */
      if(o_verbose>=1){
	for(i=0;i<xnum-1;i++){
	  fprintf(stderr,EMPTY_SEP,xname[i]);
	  fprintf(stderr,EMPTY_SEP,"+/-");
	}
	i=xnum-1;
	fprintf(stderr,EMPTY_SEP,xname[i]);
	fprintf(stderr,EMPTY_NL,"+/-");
      }
      /* +++++++++++++++++++++++++++++++++++ */
      for(i=0;i<xnum-1;i++){
	fprintf(stdout,FLOAT_SEP,x[i]);
	fprintf(stdout,FLOAT_SEP,sqrt(gsl_matrix_get(Pcovar,i,i)));
      }
      i=xnum-1;
      fprintf(stdout,FLOAT_SEP,x[i]);
      fprintf(stdout,FLOAT_NL,sqrt(gsl_matrix_get(Pcovar,i,i)));
    }
    break;

  case 2: /* print distribution function */
    {
      size_t i;

      /* +++++++++++++++++++++++++++++++++++ */
      if(o_verbose>=1){
	for(i=0;i<xnum;i++)
	  fprintf(stderr,EMPTY_SEP,xname[i]);
	fprintf(stderr,EMPTY_NL,"min NLL/obs");
	
	for(i=0;i<xnum;i++)
	  fprintf(stderr,FLOAT_SEP,x[i]);
	fprintf(stderr,FLOAT_NL,llmin);
      }
      /* +++++++++++++++++++++++++++++++++++ */
      
      if(o_printall)      
	for(i=0;i<llparams.size;i++){
	  fprintf(stdout,FLOAT_SEP,llparams.data[i]);
	  fprintf(stdout,FLOAT_NL,F (llparams.data[i],xnum,x) );
	}
      else
	for(i=llparams.min;i<=llparams.max;i++){
	  fprintf(stdout,FLOAT_SEP,llparams.data[i]);
	  fprintf(stdout,FLOAT_NL,F (llparams.data[i],xnum,x) );
	}
      
    }
    break;
  case 3: /* print density */
    {
      size_t i;

      /* +++++++++++++++++++++++++++++++++++ */
      if(o_verbose>=1){
	for(i=0;i<xnum;i++)
	  fprintf(stderr,EMPTY_SEP,xname[i]);
	fprintf(stderr,EMPTY_NL,"min NLL/obs");
	
	for(i=0;i<xnum;i++)
	  fprintf(stderr,FLOAT_SEP,x[i]);
	fprintf(stderr,FLOAT_NL,llmin);
      }
      /* +++++++++++++++++++++++++++++++++++ */
      
      if(o_printall)      
	for(i=0;i<=llparams.size;i++){
	  fprintf(stdout,FLOAT_SEP,llparams.data[i]);
	  fprintf(stdout,FLOAT_NL, f(llparams.data[i],xnum,x));
	}
      else
	for(i=llparams.min;i<=llparams.max;i++){
	  fprintf(stdout,FLOAT_SEP,llparams.data[i]);
	  fprintf(stdout,FLOAT_NL, f(llparams.data[i],xnum,x));
	}
    }
    break;
  case 4: /* 
	     Consider the order statistics x[0] < x[1] < x[2] < ... <
	     x[N-1] we use a transform to obtain variables which,
	     under the null, are uniformly distributed in [0,1].

	     upper tail, k observations used
	     ===============================

	     x[N-k] < x[N-k+1] <... < x[N-1]


	     Define the distribution in [x[N-k], +ifty)

	     G(x) = ( F(x)-F(x[N-k]) )/(1-F(x[N-k]))

	     then if the estimated model were the true model, the set

	     G(x[h]) with h in [N-k+1,N-1]

	     would be made of random variables distributed in [0,1].


	     lower tail, k observations used
	     ===============================

	     x[0] < x[1] < .... < x[k-1]

	     Define the distribution in ( -infty , x[k-1] ]

	     G(x) = F(x)/F(x[k-1])

	     then if the estimated model were the true model, the set

	     G(x[h]) with h in [0,k-2]

	     would be made of random variables distributed in [0,1].


	     Notice that in both cases we use k-1 observations, that
	     is one observation less than the sample used for the
	     estimation. Indeed one observation is used as threshold
	     value of the distribution G.

	  */
    {
      size_t i;
      double * residual = (double *)  my_alloc((llparams.used-1)*sizeof(double));


      /* +++++++++++++++++++++++++++++++++++ */
      if(o_verbose>=1){
	for(i=0;i<xnum;i++)
	  fprintf(stderr,EMPTY_SEP,xname[i]);
	fprintf(stderr,EMPTY_NL,"min NLL/obs");
	
	for(i=0;i<xnum;i++)
	  fprintf(stderr,FLOAT_SEP,x[i]);
	fprintf(stderr,FLOAT_NL,llmin);
      }
      /* +++++++++++++++++++++++++++++++++++ */

      switch(llparams.method){
      case 0: 			/* upper tail */
      case 1:
	{
	  const size_t indexmin= llparams.size-llparams.used;
	  const double Flower = F(llparams.data[indexmin],xnum,x);

	  for(i=0;i<llparams.used-1;i++){
	    residual[i] = (F(llparams.data[indexmin+i+1],xnum,x)- Flower)/(1-Flower);
	    /* fprintf(stdout,FLOAT_NL,(F(llparams.data[indexmin+i+1],xnum,x)- Flower)/(1-Flower)); */
	  }
	}
	break;

      case 2: 			/* lower tail */
      case 3:

	{
	  const size_t indexmax = llparams.used-1;
	  const double Fupper = F(llparams.data[indexmax],xnum,x);

	  for(i=0;i<indexmax;i++){
	    residual[i] = F(llparams.data[i],xnum,x)/Fupper;
	    /* fprintf(stdout,FLOAT_NL,F(llparams.data[i],xnum,x)/Fupper); */
	  }
	}
	break;

      }

      PRINTCOL(stdout,residual,llparams.used-1);
      free(residual);

    }
    break;
  case 5: /* 
	     from the order statistics x[0] < x[1] < x[2] < ... <
	     x[N-1] we use a Renji transform to obtain variables which
	     are independently uniformly distributed in [0,1].

	     upper tail, k observations used
	     ===============================

	     x[N-k] < x[N-k+1] <... < x[N-1]

	     consider
	     
	     {-ln F(x[N-1]),-ln F(x[N-2])+ln F(x[N-1]),...,-ln F(x[N-k])+ln F(x[N-k+1])}

	     these are independent exponential variables with mean
	     1/N,1/(N-1),..,1/(N-k+1). Thus consider

	     {-N ln F(x[N-1]),(N-1) (-ln F(x[N-2])+ln F(x[N-1]) ),...,
	     (N-k+1) (-ln F(x[N-k])+ln F(x[N-k+1]))}

	     these are independent exponential r.v. with mean 1. Apply
	     the density G(x)=1-exp(-x) to obtain independent
	     variables uniformly distributed in [0,1]:

	     {1-F(x[N-1])^N,1-(F(x[N-2])/F(x[N-1]))^{N-1},...,
	     1-(F(x[N-k])/F(x[N-k+1]))^{N-k+1} }


	     lower tail, k observations used
	     ===============================

	     x[0] < x[1] < .... < x[k-1]

	     consider
	     
	     {-ln (1-F(x[0])),-ln (1-F(x[1]))+ln (1-F(x[0]),...,-ln (1-F(x[k-1]))+ln (1-F(x[k-2]))}

	     these are independent exponential variables with mean
	     1/N,1/(N-1),..,1/(N-k+1). Thus consider

	     {-N ln (1-F(x[0])),(N-1) (-ln (1-F(x[1]))+ln (1-F(x[0])),...,
	     (N-k+1) (-ln (1-F(x[k-1]))+ln (1-F(x[k-2])))}

	     these are independent exponential r.v. with mean 1. Apply
	     the density G(x)=1-exp(-x) to obtain independent
	     variables uniformly distributed in [0,1]:

	     { 1-(1-F(x[0]))^N,1-((1-F(x[1]))/(1-F(x[0])))^{N-1},...,
	     1-((1-F(x[k-1]))/(1-F(x[k-2])))^{N-k+1} }


	  */
    {
      size_t i;
      double * residual = (double *)  my_alloc(llparams.used*sizeof(double));

      /* +++++++++++++++++++++++++++++++++++ */
      if(o_verbose>=1){
	for(i=0;i<xnum;i++)
	  fprintf(stderr,EMPTY_SEP,xname[i]);
	fprintf(stderr,EMPTY_NL,"min NLL/obs");
	
	for(i=0;i<xnum;i++)
	  fprintf(stderr,FLOAT_SEP,x[i]);
	fprintf(stderr,FLOAT_NL,llmin);
      }
      /* +++++++++++++++++++++++++++++++++++ */

      switch(llparams.method){
      case 0: 			/* upper tail */
      case 1:
	{

	  const size_t indexmin= llparams.size-llparams.used;

	  residual[llparams.used-1]=1-pow(F(llparams.data[llparams.size-1],xnum,x),llparams.size);
	  /* fprintf(stdout,FLOAT_NL,1-pow(F(llparams.data[llparams.size-1],xnum,x),llparams.size)); */
	  for(i=0;i<llparams.used-1;i++){
	    const double dtmp1 = F(llparams.data[indexmin+i],xnum,x);
	    const double dtmp2 = F(llparams.data[indexmin+i+1],xnum,x);
	    residual[i] = 1-pow(dtmp1/dtmp2,indexmin+i+1);
	    /* fprintf(stdout,FLOAT_NL,1-pow(dtmp1/dtmp2,i+1)); */
	  }
	}
	break;

      case 2: 			/* lower tail */
      case 3:

	{

	  residual[0]=1-pow(F(llparams.data[0],xnum,x),llparams.size);
	  /* fprintf(stdout,FLOAT_NL,1-pow(1-F(llparams.data[0],xnum,x),llparams.size)); */
	  for(i=1;i<llparams.used;i++){
	    const double dtmp1 = 1-F(llparams.data[i],xnum,x);
	    const double dtmp2 = 1-F(llparams.data[i-1],xnum,x);
	    residual[i] = 1-pow(dtmp1/dtmp2,llparams.size-i);
	    /* fprintf(stdout,FLOAT_NL,1-pow(dtmp1/dtmp2,llparams.size-i)); */
	  }

	}
	break;

      }

      PRINTCOL(stdout,residual,llparams.used);
      free(residual);
      
    }
    break;
  case 6: /* 
	     from the order statistics x[0] < x[1] < x[2] < ... <
	     x[N-1] we use a Renji transform to obtain variables which
	     are independently uniformly distributed in [0,1].

	     upper tail, k observations used
	     ===============================

	     x[N-k] < x[N-k+1] <... < x[N-1]

	     consider
	     
	     {-ln F(x[N-1]),-ln F(x[N-2])+ln F(x[N-1]),...,-ln F(x[N-k])+ln F(x[N-k+1])}

	     these are independent exponential variables with mean
	     1/N,1/(N-1),..,1/(N-k+1). Thus consider

	     {-N ln F(x[N-1]),(N-1) (-ln F(x[N-2])+ln F(x[N-1]) ),...,
	     (N-k+1) (-ln F(x[N-k])+ln F(x[N-k+1]))}

	     these are independent exponential r.v. with mean 1.


	     lower tail, k observations used
	     ===============================

	     x[0] < x[1] < .... < x[k-1]

	     consider
	     
	     {-ln (1-F(x[0])),-ln (1-F(x[1]))+ln (1-F(x[0]),...,-ln (1-F(x[k-1]))+ln (1-F(x[k-2]))}

	     these are independent exponential variables with mean
	     1/N,1/(N-1),..,1/(N-k+1). Thus consider

	     {-N ln (1-F(x[0])),(N-1) (-ln (1-F(x[1]))+ln (1-F(x[0])),...,
	     (N-k+1) (-ln (1-F(x[k-1]))+ln (1-F(x[k-2])))}

	     these are independent exponential r.v. with mean 1.
	  */
    {
      size_t i;
      double * residual = (double *)  my_alloc(llparams.used*sizeof(double));

      /* +++++++++++++++++++++++++++++++++++ */
      if(o_verbose>=1){
	for(i=0;i<xnum;i++)
	  fprintf(stderr,EMPTY_SEP,xname[i]);
	fprintf(stderr,EMPTY_NL,"min NLL/obs");
	
	for(i=0;i<xnum;i++)
	  fprintf(stderr,FLOAT_SEP,x[i]);
	fprintf(stderr,FLOAT_NL,llmin);
      }
      /* +++++++++++++++++++++++++++++++++++ */

      switch(llparams.method){
      case 0: 			/* upper tail */
      case 1:
	{

	  const size_t indexmin= llparams.size-llparams.used;

	  residual[llparams.used-1] = -( (double) llparams.size)*log(F(llparams.data[llparams.size-1],xnum,x));
	  /* fprintf(stdout,FLOAT_NL,1-pow(F(llparams.data[llparams.size-1],xnum,x),llparams.size)); */
	  for(i=0;i<llparams.used-1;i++){
	    const double dtmp1 = F(llparams.data[indexmin+i],xnum,x);
	    const double dtmp2 = F(llparams.data[indexmin+i+1],xnum,x);
	    residual[i] = -((double) indexmin+i+1)*log(dtmp1/dtmp2);
	    /* fprintf(stdout,FLOAT_NL,1-pow(dtmp1/dtmp2,i+1)); */
	  }
	}
	break;

      case 2: 			/* lower tail */
      case 3:

	{

	  residual[0]=-( (double) llparams.size)*log(F(llparams.data[0],xnum,x));
	  /* fprintf(stdout,FLOAT_NL,1-pow(1-F(llparams.data[0],xnum,x),llparams.size)); */
	  for(i=1;i<llparams.used;i++){
	    const double dtmp1 = 1-F(llparams.data[i],xnum,x);
	    const double dtmp2 = 1-F(llparams.data[i-1],xnum,x);
	    residual[i] = -((double) llparams.size-i)*log(dtmp1/dtmp2);
	    /* fprintf(stdout,FLOAT_NL,1-pow(dtmp1/dtmp2,llparams.size-i)); */
	  }

	}
	break;

      }

      PRINTCOL(stdout,residual,llparams.used);
      free(residual);
      
    }
    break;
  }
  
  return 0;

}
