/*
  gbdist (ver. 5.6) -- Produce cumulative distribution from data
  Copyright (C) 2000-20014 Giulio Bottazzi

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  (version 2) as published by the Free Software Foundation;
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#include "tools.h"

/* this is the unbiased version of the empirical distribution
   function. Indeed if x_k is the k-th largest observation, that is
   k-1 are smaller and n-k larger, then E[F(x_k)]=k/(N+1)
 */
void printdist(double const *data,int const size,int const o_reverse, int const o_identical){

  size_t i;

  if(o_identical){
    /* print one point for any observed value */

    if(!o_reverse){
      for(i=0;i<size;i++){
	printf(FLOAT_SEP,data[i]);
	printf(FLOAT_NL,(double) (i+1)/size);
      }
    }
    else{
      for(i=0;i<size;i++){
	printf(FLOAT_SEP,data[size-i-1]);
	printf(FLOAT_NL,(double) (i+1)/size);
      }
    }
    
  }
  else{
    /* print one point for any DISTINCT observed value */

    if(!o_reverse){
      for(i=0;i<size;i++){
	if(i<size-1 && data[i+1] == data[i]) continue;
	printf(FLOAT_SEP,data[i]);
	printf(FLOAT_NL,(double) (i+1)/size);
      }
    }
    else{
      for(i=0;i<size;i++){
	if(i<size-1 && data[size-i-1] == data[size-i-2]) continue;
	printf(FLOAT_SEP,data[size-i-1]);
	printf(FLOAT_NL,(double) (i+1)/size);
      }
    }
    
  }

}



int main(int argc,char* argv[]){

  int o_reverse=0;
  int o_identical=0;

  int o_table=0;

  char *splitstring = strdup(" \t");

  int o_verbose=0;

  /* COMMAND LINE PROCESSING */
    
  /* variables for reading command line options */
  /* ------------------------------------------ */
  int opt;
  /* ------------------------------------------ */
    
    
  /* read the command line */    
  while((opt=getopt_long(argc,argv,"rhtviF:",gb_long_options, &gb_option_index))!=EOF){
    if(opt==0){
      gbutils_header(argv[0],stdout);
      exit(0);
    }
    else if(opt=='?'){
      fprintf(stderr,"option %c not recognized\n",optopt);
      exit(-1);
    }
    else if(opt=='r'){
      /*reverse the order*/
      o_reverse=1;
    }
    else if(opt=='t'){
      /*set the table form*/
      o_table=1;
    }
    else if(opt=='v'){
      /*set verbose output*/
      o_verbose=1;
    }
    else if(opt=='i'){
      /*set verbose output*/
      o_identical=1;
    }
    else if(opt=='F'){
      /*set the fields separator string*/
      free(splitstring);
      splitstring = strdup(optarg);
    }
    else if(opt=='h'){
      /*print short help*/
      fprintf(stdout,"Compute the empirical probability distribution function as\n");
      fprintf(stdout,"F(x_k) = k/N where x_k is the k-th ordered observation,\n");
      fprintf(stdout,"that is k-1 observations are smaller then x_k and n-k are\n");
      fprintf(stdout,"larger, and N is the sample size. \n");
      fprintf(stdout,"\nUsage: %s [options]\n\n",argv[0]);
      fprintf(stdout,"Options:\n");
      fprintf(stdout," -r  print right cumulated distribution function, i.e. 1-F(x)\n");
      fprintf(stdout," -i  print multiple points for identical observations \n");
      fprintf(stdout," -t  print the distribution for each column of input\n");
      fprintf(stdout," -F  specify the input fields separators (default \" \\t\")  \n");
      fprintf(stdout," -v  verbose mode\n");
      fprintf(stdout," -h  this help\n");
      exit(0);
    }
  }
  /* END OF COMMAND LINE PROCESSING */

  /* initialize global variables */
  initialize_program(argv[0]);

  /* ++++++++++++++++++++++++++++ */
  if(o_verbose){
    fprintf(stdout,"#output type:\t");
    if(o_reverse)
      fprintf(stdout,"right cumulated distribution function");
    else
      fprintf(stdout,"distribution function");
    fprintf(stdout,"\n");

    fprintf(stdout,"#data statistics:\n");
    fprintf(stdout,
	    "# mean      stdev     skewness  kurtosis  ave.dev.  min       max\n");
  }
  /* ++++++++++++++++++++++++++++ */


  
  if(o_table){/*TABLE MODE*/
    size_t rows=0,columns=0,i;
    double **data=NULL;
      
   loadtable(&data,&rows,&columns,0,splitstring);

    for(i=0;i<columns;i++){
      size_t size=rows;
      denan(&(data[i]),&size);
      
      /* ++++++++++++++++++++++++++++ */
      if(o_verbose){
	double xmean,xvar,xsd,xskew,xkurt,xadev,xmin,xmax;
	moment(data[i],size,&xmean,&xadev,&xsd,&xvar,&xskew,&xkurt,&xmin,&xmax);
	fprintf(stdout,"# %+.2e %+.2e %+.2e %+.2e %+.2e %+.2e %+.2e %zd\n",
		xmean,xsd,xskew,xkurt,xadev,xmin,xmax,size);
      }
      /* ++++++++++++++++++++++++++++ */

      qsort(data[i],size,sizeof(double),sort_by_value);
      printdist(data[i],size,o_reverse,o_identical);
      if(i<columns-1) printf("\n\n");
    }
  }
  else{/*NORMAL MODE*/
    double *data=NULL;
    size_t size;

   load(&data,&size,0,splitstring);
    denan(&data,&size);

    /* ++++++++++++++++++++++++++++ */
    if(o_verbose){
      double xmean,xvar,xsd,xskew,xkurt,xadev,xmin,xmax;
      moment(data,size,&xmean,&xadev,&xsd,&xvar,&xskew,&xkurt,&xmin,&xmax);
      fprintf(stdout,"# %+.2e %+.2e %+.2e %+.2e %+.2e %+.2e %+.2e %zd\n",
	      xmean,xsd,xskew,xkurt,xadev,xmin,xmax,size);
    }
    /* ++++++++++++++++++++++++++++ */

    /*sort in ascending numerical order*/
    qsort(data,size,sizeof(double),sort_by_value);
	
    printdist(data,size,o_reverse,o_identical);
  }
    
  exit(0);
}
