/*
  gbnlpolyit (ver. 5.6) -- Non linear polyit regression

  Copyright (C) 2010-2018 Giulio Bottazzi

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  (version 2) as published by the Free Software Foundation;
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#include "tools.h"
#include "matheval.h"
#include "assert.h"
#include "multimin.h"

#include <gsl/gsl_sf_gamma.h>

/* handy structure used in computation */
typedef struct function {
  void *f;
  size_t argnum;
  char **argname;
  void **df;
  void **dfdx;
} Function;


struct objdata {
  Function * F;
  size_t rows;
  size_t columns;
  size_t *datan;
  double **datax;
  size_t N;
};


/* ========================== */
/* Zeroth-order approximation */
/* ========================== */

/* This is the negative log-likelihood per observation (that is,
   divided by the total number of observations) computed with no
   independent variables: the agglomeration parameter is set to zero
   and no dependence on regressors is introduced. Under this
   hypothesis ALL locations have the same attractiveness, equal to 1/L
   where L is the number of locations:

   NLL = -log(N!) + N*log(L) + \sum_l log(n_l!)

   This function is used for the
   computation of the generalized R^2. */

double
zeromll (size_t *datan,const size_t rows,const size_t N){

  size_t i;
  double res= -lgamma(N+1) + N*log(rows);
  
  for(i=0;i<rows;i++)
    res += lgamma(datan[i]+1);

  return res/rows;
}

/* ======================== */
/* Multinomial distribution */
/* ======================== */

/* marginal Multinomial distribution given a and A */
/* =============================================== */


double
marginal_multi(const size_t n, const double a, const double A, const size_t N){

  return exp(gsl_sf_lnchoose(N,n) + n*log(a)+(N-n)*log(A-a)-N*log(A));

}


/* Likelihood and gradients */
/* ======================== */


void
objmulti_f (const size_t pnum, const double *x,void *params,double *fval){
  
  const Function *F = ((struct objdata *)params)->F;
  const size_t rows=((struct objdata *)params)->rows;
  const size_t columns=((struct objdata *)params)->columns;
  const size_t N=((struct objdata *)params)->N;  
  size_t *datan=((struct objdata *)params)->datan;
  double **datax=((struct objdata *)params)->datax;
  double values[pnum+columns];
  size_t i,j;
  double res=0;			/* negative log likelihood */
  double a,A=0;
  
  /* set the parameter values */
  for(i=columns;i<columns+pnum;i++)
    values[i] = x[i-columns];

  /* cycle on rows */
  for(i=0;i<rows;i++){

    /* set the variables values */
    for(j=0;j<columns;j++)
      values[j] = datax[j][i];

    a=evaluator_evaluate (F->f,columns+pnum,F->argname,values);

    A += a;

    if( a < 0 ){
      fprintf(stderr,">>   x:");   
      for(i=0;i<pnum;i++)  
	fprintf(stderr," %+12.6e",x[i]);
      fprintf(stderr,">> n=%zd a=%f col=%f\n",datan[i],a,datax[0][i]);

      fprintf(stderr,">> f(x) = %s\n",evaluator_get_string (F->f));
    }

    res += lgamma(datan[i]+1) - datan[i]*log(a);

  }


  res += N*log(A)-lgamma(N+1);


  *fval=res/rows;

  /* fprintf(stderr,"  f: %+12.6e\n",*fval); */
  /* fprintf(stderr,"    x:");      */
  /* for(i=0;i<pnum;i++)     */
  /*   fprintf(stderr," %+12.6e",x[i]);      */
  /* fprintf(stderr,"\n");      */

}



void
objmulti_df (const size_t pnum, const double *x,void *params,double *grad){


  const Function *F = ((struct objdata *)params)->F;
  const size_t rows=((struct objdata *)params)->rows;
  const size_t columns=((struct objdata *)params)->columns;
  const size_t N=((struct objdata *)params)->N;  
  size_t *datan=((struct objdata *)params)->datan;
  double **datax=((struct objdata *)params)->datax;
  double values[pnum+columns];
  size_t i,j;
  double a,A=0;

  /* initialize gradient */
  for(i=0;i<pnum;i++)
    grad[i]=0.0;

  /* set the parameter values */
  for(i=columns;i<columns+pnum;i++)
    values[i] = x[i-columns];

  /* compute A */
  for(i=0;i<rows;i++){

    /* set the variables values */
    for(j=0;j<columns;j++)
      values[j] = datax[j][i];

    a=evaluator_evaluate (F->f,columns+pnum,F->argname,values);

    A += a;

  }

  /* cycle on rows */
  for(i=0;i<rows;i++){
    
    /* set the variables values */
    for(j=0;j<columns;j++)
      values[j] = datax[j][i];

    a=evaluator_evaluate (F->f,columns+pnum,F->argname,values);

    for(j=0;j<pnum;j++)
      grad[j] += (N/A-datan[i]/a)*evaluator_evaluate ((F->df)[j],columns+pnum,F->argname,values);

  }

  for(j=0;j<pnum;j++)
    grad[j] /=rows;

  /* fprintf(stderr," df:",grad[j]);      */
  /* for(j=0;j<pnum;j++)      */
  /*   fprintf(stderr," %+12.6e",grad[j]);      */
  /* fprintf(stderr,"\n");      */
  /* fprintf(stderr,"   x:");      */
  /* for(i=0;i<pnum;i++)     */
  /*   fprintf(stderr," %+12.6e",x[i]);      */
  /* fprintf(stderr,"\n");      */

}


void
objmulti_fdf (const size_t pnum, const double *x,void *params,double *fval,double *grad){

  const Function *F = ((struct objdata *)params)->F;
  const size_t rows=((struct objdata *)params)->rows;
  const size_t columns=((struct objdata *)params)->columns;
  const size_t N=((struct objdata *)params)->N;  
  size_t *datan=((struct objdata *)params)->datan;
  double **datax=((struct objdata *)params)->datax;
  double values[pnum+columns];
  size_t i,j;
  double res=0;			/* negative log likelihood */
  double a,A=0;

  /* initialize gradient */
  for(i=0;i<pnum;i++)
    grad[i]=0.0;

  /* set the parameter values */
  for(i=columns;i<columns+pnum;i++)
    values[i] = x[i-columns];

  /* cycle on rows */
  for(i=0;i<rows;i++){

    /* set the variables values */
    for(j=0;j<columns;j++)
      values[j] = datax[j][i];

    a=evaluator_evaluate (F->f,columns+pnum,F->argname,values);

    A += a;

    res += lgamma(datan[i]+1)-datan[i]*log(a);

  }

  res += N*log(A)-lgamma(N);

  /* cycle on rows */
  for(i=0;i<rows;i++){
    
    /* set the variables values */
    for(j=0;j<columns;j++)
      values[j] = datax[j][i];

    a=evaluator_evaluate (F->f,columns+pnum,F->argname,values);
    
    for(j=0;j<pnum;j++)
      grad[j] += (N/A-datan[i]/a)*evaluator_evaluate ((F->df)[j],columns+pnum,F->argname,values);

  }

  for(j=0;j<pnum;j++)
    grad[j] /=rows;

  *fval=res/rows; 


  /* fprintf(stderr,"fdf:",grad[j]); */
  /* for(j=0;j<pnum;j++) */
  /*   fprintf(stderr," %+12.6e",grad[j]); */
  /* fprintf(stderr," %+12.6e\n",*fval); */
  /* fprintf(stderr,"   x:"); */
  /* for(i=0;i<pnum;i++) */
  /*   fprintf(stderr," %+12.6e",x[i]); */
  /* fprintf(stderr,"\n"); */

}


/* function to compute the marginal effects */
/* ======================================== */

void
marginals_multi(const size_t Pnum,double * const Pval,const Function F,
		const size_t columns,const size_t rows,double ** const datax,
		size_t * const datan, double *meff){

  size_t i,j,h;
  double A=0;

  double values[Pnum+columns];
  
  /* set the parameter values */
  for(i=columns;i<columns+Pnum;i++)
    values[i] = Pval[i-columns];
  
  
  /* compute A */
  for(i=0;i<rows;i++){
    
    /* set the variables values */
    for(j=0;j<columns;j++)
      values[j] = datax[j][i];
    A += evaluator_evaluate (F.f,columns+Pnum,F.argname,values);

  }

  /* reset covariates */
  for(h=0;h<columns;h++)
    meff[h]=0;

  /* compute covariates effects */
  for(i=0;i<rows;i++){
    
    /* set the variables values */
    for(j=0;j<columns;j++)
      values[j] = datax[j][i];
    
    for(h=0;h<columns;h++)
      meff[h] += evaluator_evaluate (F.dfdx[h],columns+Pnum,F.argname,values);
  }

  for(h=0;h<columns;h++)
    meff[h]/=A;

}

void
margelast_multi(const size_t Pnum,double * const Pval,const Function F,
		const size_t columns,const size_t rows,double ** const datax,
		size_t * const datan, double *meff){
  
  size_t i,j,h;
  double A=0;

  double values[Pnum+columns];

  /* set the parameter values */
  for(i=columns;i<columns+Pnum;i++)
    values[i] = Pval[i-columns];
  
  
  /* compute A */
  for(i=0;i<rows;i++){
    
    /* set the variables values */
    for(j=0;j<columns;j++)
      values[j] = datax[j][i];
    A += evaluator_evaluate (F.f,columns+Pnum,F.argname,values);
  }

  /* reset covariates */
  for(h=0;h<columns;h++)
    meff[h]=0;

  /* compute covariates effects */
  for(i=0;i<rows;i++){
    
    /* set the variables values */
    for(j=0;j<columns;j++)
      values[j] = datax[j][i];
    
    for(h=0;h<columns;h++)
      meff[h] += datax[h][i]*
	evaluator_evaluate (F.dfdx[h],columns+Pnum,F.argname,values);
    
  }

  for(h=0;h<columns;h++)
    meff[h]/=A;

}



/* ================== */
/* Polya distribution */
/* ================== */


/* marginal Polya distribution given a and A */
/* ========================================= */

/* 
   gsl_sf_lnpoch (double A, double X) = 
   \log((a)_x) = \log(\Gamma(a + x)/\Gamma(a)) for a > 0, a+x > 0. */

double
marginal_polya(const size_t n, const double a, const double A, const size_t N){

  return exp( gsl_sf_lnpoch(a,n) + gsl_sf_lnpoch(A-a,N-n)-
	      gsl_sf_lnpoch(A,N) + gsl_sf_lnchoose(N,n));

}


/* Likelihood and gradients */
/* ======================== */

void
objpolya_f (const size_t pnum, const double *x,void *params,double *fval){
  
  const Function *F = ((struct objdata *)params)->F;
  const size_t rows=((struct objdata *)params)->rows;
  const size_t columns=((struct objdata *)params)->columns;
  const size_t N=((struct objdata *)params)->N;  
  size_t *datan=((struct objdata *)params)->datan;
  double **datax=((struct objdata *)params)->datax;
  double values[pnum+columns];
  size_t i,j;
  double res=0;			/* negative log likelihood */
  double a,A=0;
  
  /* set the parameter values */
  for(i=columns;i<columns+pnum;i++)
    values[i] = x[i-columns];

  /* cycle on rows */
  for(i=0;i<rows;i++){

    /* set the variables values */
    for(j=0;j<columns;j++)
      values[j] = datax[j][i];

    a=evaluator_evaluate (F->f,columns+pnum,F->argname,values);

    A += a;

    if( a < 0 ){
      fprintf(stderr,">>   x:");   
      for(i=0;i<pnum;i++)  
	fprintf(stderr," %+12.6e",x[i]);
      fprintf(stderr,">> n=%zd a=%f col=%f\n",datan[i],a,datax[0][i]);

      fprintf(stderr,">> f(x) = %s\n",evaluator_get_string (F->f));
    }

    if(datan[i]>0)
      for(j=0;j<datan[i];j++)
	res -= log(a+j);

    res += lgamma(datan[i]+1);

  }

  for(j=0;j<N;j++)
    res += log(A+j);

  res -= lgamma(N+1);


  *fval=res/rows;


      /* fprintf(stderr,"  f: %+12.6e\n",res);     */
      /* fprintf(stderr,"   x:");     */
      /* for(i=0;i<pnum;i++)    */
      /*   fprintf(stderr," %+12.6e",x[i]);     */
      /* fprintf(stderr,"\n");     */


}


void
objpolya_df (const size_t pnum, const double *x,void *params,double *grad){


  const Function *F = ((struct objdata *)params)->F;
  const size_t rows=((struct objdata *)params)->rows;
  const size_t columns=((struct objdata *)params)->columns;
  const size_t N=((struct objdata *)params)->N;  
  size_t *datan=((struct objdata *)params)->datan;
  double **datax=((struct objdata *)params)->datax;
  double values[pnum+columns];
  size_t i,j;
  double a,A=0,sumA=0;

  /* variables used in computation */
  double y[rows];

  /* initialize gradient */
  for(i=0;i<pnum;i++)
    grad[i]=0.0;

  /* set the parameter values */
  for(i=columns;i<columns+pnum;i++)
    values[i] = x[i-columns];

  /* compute A and y */
  for(i=0;i<rows;i++){

    y[i]=0;

    /* set the variables values */
    for(j=0;j<columns;j++)
      values[j] = datax[j][i];

    a=evaluator_evaluate (F->f,columns+pnum,F->argname,values);

    A += a;

    if(datan[i]>0)
      for(j=0;j<datan[i];j++)
	y[i] += 1/(a+j);
  }

  /* compute sumA */
  for(j=0;j<N;j++)
    sumA += 1/(A+j);

  /* cycle on rows */
  for(i=0;i<rows;i++){
    
    /* set the variables values */
    for(j=0;j<columns;j++)
      values[j] = datax[j][i];

    for(j=0;j<pnum;j++)
      grad[j] += (sumA-y[i])*evaluator_evaluate ((F->df)[j],columns+pnum,F->argname,values);

  }

  for(j=0;j<pnum;j++)
    grad[j] /=rows;

      /* fprintf(stderr," df:",grad[j]);     */
      /* for(j=0;j<pnum;j++)     */
      /*   fprintf(stderr," %+12.6e",grad[j]);     */
      /* fprintf(stderr,"\n");     */
      /* fprintf(stderr,"   x:");     */
      /* for(i=0;i<pnum;i++)    */
      /*   fprintf(stderr," %+12.6e",x[i]);     */
      /* fprintf(stderr,"\n");     */


}


void
objpolya_fdf (const size_t pnum, const double *x,void *params,double *fval,double *grad){

  const Function *F = ((struct objdata *)params)->F;
  const size_t rows=((struct objdata *)params)->rows;
  const size_t columns=((struct objdata *)params)->columns;
  const size_t N=((struct objdata *)params)->N;  
  size_t *datan=((struct objdata *)params)->datan;
  double **datax=((struct objdata *)params)->datax;
  double values[pnum+columns];
  size_t i,j;
  double res=0;			/* negative log likelihood */
  double a,A=0,sumA=0;

  /* variables used in computation */
  double y[rows];

  /* initialize gradient */
  for(i=0;i<pnum;i++)
    grad[i]=0.0;

  /* set the parameter values */
  for(i=columns;i<columns+pnum;i++)
    values[i] = x[i-columns];


  /* cycle on rows */
  for(i=0;i<rows;i++){

    y[i]=0;

    /* set the variables values */
    for(j=0;j<columns;j++)
      values[j] = datax[j][i];

    a=evaluator_evaluate (F->f,columns+pnum,F->argname,values);

    A += a;

    if(datan[i]>0)
      for(j=0;j<datan[i];j++){
	res -= log(a+j);
	y[i] += 1/(a+j);
      }

    res += lgamma(datan[i]+1);

  }

  for(j=0;j<N;j++){
    res += log(A+j);
    sumA += 1/(A+j);
  }

  res -= lgamma(N+1);

  /* cycle on rows */
  for(i=0;i<rows;i++){
    
    /* set the variables values */
    for(j=0;j<columns;j++)
      values[j] = datax[j][i];

    for(j=0;j<pnum;j++)
      grad[j] += (sumA-y[i])*evaluator_evaluate ((F->df)[j],columns+pnum,F->argname,values);

  }

  for(j=0;j<pnum;j++)
    grad[j] /=rows;

  *fval=res/rows; 


      /* fprintf(stderr,"fdf:",grad[j]);     */
      /* for(j=0;j<pnum;j++)     */
      /*   fprintf(stderr," %+12.6e",grad[j]);     */
      /* fprintf(stderr," %+12.6e\n",res);     */
      /* fprintf(stderr,"   x:");     */
      /* for(i=0;i<pnum;i++)    */
      /*   fprintf(stderr," %+12.6e",x[i]);     */
      /* fprintf(stderr,"\n");     */


}


/* function to compute the marginal effects */
/* ======================================== */

void
marginals_polya(const size_t Pnum,double * const Pval,const Function F,
	  const size_t columns,const size_t rows,double ** const datax,
	  size_t * const datan, double *meff){

  size_t i,j,h;
  double A=0,N=0;

  double values[Pnum+columns];
  
  /* set the parameter values */
  for(i=columns;i<columns+Pnum;i++)
    values[i] = Pval[i-columns];
  
  
  /* compute A and N */
  for(i=0;i<rows;i++){
    
    /* set the variables values */
    for(j=0;j<columns;j++)
      values[j] = datax[j][i];
    A += evaluator_evaluate (F.f,columns+Pnum,F.argname,values);

    N += datan[i];
  }

  /* reset covariates */
  for(h=0;h<columns+1;h++)
    meff[h]=0;

  /* compute covariates effects */
  for(i=0;i<rows;i++){
    
    /* set the variables values */
    for(j=0;j<columns;j++)
      values[j] = datax[j][i];
    
    for(h=0;h<columns;h++)
      meff[h] += evaluator_evaluate (F.dfdx[h],columns+Pnum,F.argname,values);
    
  }

  for(h=0;h<columns;h++)
    meff[h]/=A+N;

  /* compute agglomeration effect */
  meff[columns]=rows/(A+N);

}


void
margelast_polya(const size_t Pnum,double * const Pval,const Function F,
	  const size_t columns,const size_t rows,double ** const datax,
	  size_t * const datan, double *meff){

  size_t i,j,h;
  double A=0,N=0;

  double values[Pnum+columns];

  /* set the parameter values */
  for(i=columns;i<columns+Pnum;i++)
    values[i] = Pval[i-columns];
  
  
  /* compute A and N */
  for(i=0;i<rows;i++){
    
    /* set the variables values */
    for(j=0;j<columns;j++)
      values[j] = datax[j][i];
    A += evaluator_evaluate (F.f,columns+Pnum,F.argname,values);

    N += datan[i];
  }

  /* reset covariates */
  for(h=0;h<columns+1;h++)
    meff[h]=0;

  /* compute covariates effects */
  for(i=0;i<rows;i++){
    
    /* set the variables values */
    for(j=0;j<columns;j++)
      values[j] = datax[j][i];
    
    for(h=0;h<columns;h++)
      meff[h] += datax[h][i]*
	evaluator_evaluate (F.dfdx[h],columns+Pnum,F.argname,values);
    
  }

  for(h=0;h<columns;h++)
    meff[h]/=A+N;

  /* compute agglomeration effect */
  meff[columns]=N/(A+N);

}



int main(int argc,char* argv[]){

  int i;

  /* options and default values */
  char *splitstring = strdup(" \t");
  int o_verbose=0;
  int o_output=0;
  int o_stderr=0;
  int o_model=0;

  /* data from stdin */
  size_t rows=0,columns=0;

  size_t *datan=NULL; 	/* occupancy vector */
  double **datax=NULL;		/* independent variables matrix */

  Function F;                   /* definition of the function */
  char **Param=NULL;		/* parameter names */
  double *Pval=NULL;		/* parameter values */
  size_t Pnum=0;		/* parameters number */
  double *Pstd=NULL;            /* parameters standard error */
  double *Pscr=NULL;            /* parameters p-score (prob. different from zero) */ 
  double llmin;

  double *meff=NULL; 		/* marginal effects  */
  double *meff_std=NULL;	/* marginal effects standard deviation */
  double *meff_scr=NULL;	/* marginal effects p-score */
  size_t meffnum=0;		/* number of marginal effects */

  /* model-specific functions */
  double (* marginal) (const size_t, const double, const double, const size_t)=NULL;
  void (* obj_f) (const size_t, const double *,void *,double *)=NULL;
  void (* obj_df) (const size_t, const double *,void *,double *)=NULL;
  void (* obj_fdf) (const size_t, const double *,void *,double *,double *)=NULL;
  void (* marginals) (const size_t,double * const,const Function, const size_t,
		      const size_t,double ** const,size_t * const, double *)=NULL;
  void (* margelast) (const size_t,double * const,const Function, const size_t,
		      const size_t,double ** const,size_t * const, double *)=NULL;

  /* minimization parameters */
  struct multimin_params mmparams={.01,.1,100,1e-6,1e-6,5,0};

  /* error handler */
  gsl_error_handler_t old_handler;

  /* parameters for the likelihood expression */
  struct objdata llparams;

  /* bootstrap parameters */
  unsigned seed=0;
  size_t replicas=20;



  /* variables for reading command line options */
  /* ------------------------------------------ */
  int opt;
  /* ------------------------------------------ */

  /* COMMAND LINE PROCESSING */
  while((opt=getopt_long(argc,argv,"v:hF:O:VA:R:r:M:",gb_long_options, &gb_option_index))!=EOF){
    if(opt==0){
      gbutils_header(argv[0],stdout);
      exit(0);
    }
    else if(opt=='?'){
      fprintf(stderr,"option %c not recognized\n",optopt);
      exit(-1);
    }
    else if(opt=='h'){
      /*print help*/
      fprintf(stdout,"Non linear polyit estimation. Minimize the negative log-likelihood\n\n");
      fprintf(stdout," sum_{h=0}^{L-1} log(A+h) - sum_{l=1}^L sum_{h=0}^{n_l-1} log(a_l+h)\n\n");
      fprintf(stdout,"for the Polya specification or\n\n");
      fprintf(stdout," L log(A) - sum_{l=1}^L n_l log(a_l) \n\n");
      fprintf(stdout,"for the multinomial specification, where A = sum_{l=1}^L a_l and L is the\n");
      fprintf(stdout,"number of alternatives. The input data file should contain L rows, one for\n");
      fprintf(stdout,"each alternative, of the type n x1 ... XN. The first column contains the\n");
      fprintf(stdout,"dependent variable (# of observations) and the other columns the independent\n");
      fprintf(stdout,"variables. The model is specified by a function a_l=g(x1,x2...) where x1,.. XN\n");
      fprintf(stdout,"stands for the first, second .. N-th column of independent variables.\n");
      fprintf(stdout,"\nUsage: %s [options] <function definition> \n\n",argv[0]);
      fprintf(stdout,"Options:\n");
      fprintf(stdout," -O  type of output (default 0)\n");
      fprintf(stdout,"      0  parameters and log-like (ll)   \n");
      fprintf(stdout,"      1  marginal effects               \n");
      fprintf(stdout,"      2  marginal elasticities          \n");
      fprintf(stdout,"      3  n_l n*_l a*_l  *=estimated     \n");
      fprintf(stdout,"      4  occupancies classes            \n");
      fprintf(stdout," -F  input fields separators (default \" \\t\")\n");
      fprintf(stdout," -V  standard errors and p-scores of diff. from zero using bootstrap \n");
      fprintf(stdout," -r  number of replicas (default 20)\n");
      fprintf(stdout," -v  verbosity level (default 0)\n");
      fprintf(stdout,"      0  just results                    \n");
      fprintf(stdout,"      1  comment headers                 \n");
      fprintf(stdout,"      2  summary statistics              \n");
      fprintf(stdout,"      3  covariance matrix               \n");
      fprintf(stdout,"      4  minimization steps             \n");
      fprintf(stdout,"      5  model definition               \n");
      fprintf(stdout," -R  set the rng seed (default 0)\n");
      fprintf(stdout," -M  set the model to use (default 0) |\n");
      fprintf(stdout,"      0  Polya\n");
      fprintf(stdout,"      1  multinomial\n");
      fprintf(stdout," -A  MLL optimization options (default 0.01,0.1,100,1e-6,1e-6,5)\n");
      fprintf(stdout,"     fields are step,tol,iter,eps,msize,algo. Empty fields for default\n");
      fprintf(stdout,"      step  initial step size of the searching algorithm               \n");
      fprintf(stdout,"      tol   line search tolerance iter: maximum number of iterations   \n");
      fprintf(stdout,"      eps   gradient tolerance : stopping criteria ||gradient||<eps    \n");
      fprintf(stdout,"      algo  optimization methods: 0 Fletcher-Reeves, 1 Polak-Ribiere,  \n");
      fprintf(stdout,"            2 Broyden-Fletcher-Goldfarb-Shanno, 3 Steepest descent, 4 simplex,\n");
      fprintf(stdout,"            5 Broyden-Fletcher-Goldfarb-Shanno-2\n");
      exit(0);
    }
    else if(opt=='M'){
      /* set the model */
      o_model=atoi(optarg);
    }
    else if(opt=='F'){
      /*set the fields separator string*/
      free(splitstring);
      splitstring = strdup(optarg);
    }
    else if(opt=='O'){
      /* set the type of output */
      o_output = atoi(optarg);
      if(o_output<0 || o_output>5){
	fprintf(stderr,"ERROR (%s): output option '%d' not recognized. Try option -h.\n",
		GB_PROGNAME,o_output);
	exit(-1);
      }
    }
    else if(opt=='V'){
      /* compute standard errors */
      o_stderr=1;
    }
    else if(opt=='R'){
      /* RNG seed */
      seed = (unsigned) atol(optarg);
    }
    else if(opt=='r'){
      /* number of replicas */
      replicas = (size_t) atoi(optarg);
    }
    else if(opt=='A'){
      char *stmp1=strdup (optarg);
      char *stmp2;
      char *stmp3=stmp1;
      if( (stmp2=strsep (&stmp1,",")) != NULL){
	if(strlen(stmp2)>0) 
	  mmparams.step_size=atof(stmp2);
      }
      if( (stmp2=strsep (&stmp1,",")) != NULL){
	if(strlen(stmp2)>0) 
	  mmparams.tol=atof(stmp2);
      }
      if( (stmp2=strsep (&stmp1,",")) != NULL){
	if(strlen(stmp2)>0) 
	  mmparams.maxiter=(unsigned) atoi(stmp2);
      }
      if( (stmp2=strsep (&stmp1,",")) != NULL){
	if(strlen(stmp2)>0) 
	  mmparams.epsabs=atof(stmp2);
      }
      if( (stmp2=strsep (&stmp1,",")) != NULL){
	if(strlen(stmp2)>0) 
	  mmparams.maxsize=atof(stmp2);
      }
      if( (stmp2=strsep (&stmp1,",")) != NULL){
	if(strlen(stmp2)>0) 
	  mmparams.method= (unsigned) atoi(stmp2);
      }
      free(stmp3);
    }
    else if(opt=='v'){
      o_verbose = atoi(optarg);
      mmparams.verbosity=(o_verbose>2?o_verbose-2:0);
    }
  }
  /* END OF COMMAND LINE PROCESSING */

  /* initialize global variables */
  initialize_program(argv[0]);


  /* define the dataset */
  {
    size_t i,j;
    double **data=NULL;

    /* load data; the first column contains occupancy numbers */
    loadtable(&data,&rows,&columns,0,splitstring);

    /* split the occupancies from the variables */
    datan = (size_t *) my_alloc(rows*sizeof(size_t));
    datax = (double **) my_alloc((columns-1)*sizeof(double *));
    for(i=0;i<columns-1;i++){
      datax[i] = (double *) my_alloc(rows*sizeof(double));
    }

    for(i=0;i<rows;i++){
      datan[i]= (size_t) data[0][i];
      for(j=0;j<columns-1;j++)
	datax[j][i] = data[j+1][i];
    }
    
    /* free loaded data */
    for(i=0;i<columns;i++)
      free(data[i]);
    free(data);

    /* reduce the number of columns by one, since first column is
       absent */
    columns--;
  }




  /* parse line for functions and variables specification */
  /* ==================================================== */

  if(optind == argc){
    fprintf(stderr,"ERROR (%s): please provide a function to fit.\n",
	    GB_PROGNAME);
    exit(-1);
  }
    
  for(i=optind;i<argc;i++){
    char *piece=strdup (argv[i]);
    char *stmp1=piece;
    char *stmp2;
    
    while( (stmp2=strsep (&stmp1,",")) != NULL ){/*take a piece*/
      char *stmp3 = strdup(stmp2);
      char *stmp4 = stmp3;
      char *stmp5;
      /* 	fprintf(stderr,"token:%s\n",stmp3); */

      /* initial condition */
      if( (stmp5=strsep(&stmp4,"=")) != NULL && stmp4 != NULL ){
	if( strlen(stmp5)>0 && strlen(stmp4)>0){
	  Pnum++;
	  Param=(char **) my_realloc((void *)  Param,Pnum*sizeof(char *));
	  Pval=(double *) my_realloc((void *) Pval,Pnum*sizeof(double));
	  
	  Param[Pnum-1] = strdup(stmp5);
	  Pval[Pnum-1] = atof(stmp4);
	}
      }
      else{ /* allocate new function */
	F.f = evaluator_create (stmp3);
	assert(F.f);
      }
      free(stmp3);
    }
    free(piece);
  }


  /* check that everything is correct */
  if(Pnum==0){
    fprintf(stderr,"ERROR (%s): please provide a parameter to estimate.\n",
	    GB_PROGNAME);
    exit(-1);
  }


  {
    size_t i,j;
    char **NewParam=NULL;
    double *NewPval=NULL;
    size_t NewPnum=0;

    char **storedname=NULL;
    size_t storednum=0;

    /* retrieve list of arguments and their number */
    /* notice that storedname is not allocated */
    {
      int argnum;
      evaluator_get_variables (F.f,&storedname,&argnum);
      storednum = (size_t) argnum;
    }

    /* check the definition of the function: column specifications
       refer to existing columns and parameters are properly
       initialized */
    for(i=0;i<storednum;i++){
      char *stmp1 = storedname[i];
      if(*stmp1 == 'x'){
	size_t index = (atoi(stmp1+1)>0?atoi(stmp1+1):0);
	if(index>columns){
	  fprintf(stderr,"ERROR (%s): column %zu not present in data\n",
		  GB_PROGNAME,index);
	  exit(-1);
	}
      }
      else {
	for(j=0;j<Pnum;j++)
	  if( strcmp(Param[j],stmp1)==0 ) break;
	if(j==Pnum){
	  fprintf(stderr,"ERROR (%s): parameter %s without initial value\n",
		  GB_PROGNAME,stmp1);
	  exit(-1);
	}
      }
    }

    /* remove unnecessary parameters */
    for(i=0;i<Pnum;i++){
      for(j=0;j<storednum;j++)
	if( strcmp(Param[i],storedname[j])==0 ) break;
      if(j==storednum){
	fprintf(stderr,"WARNING (%s): irrelevant parameter %s removed\n",
		GB_PROGNAME,Param[i]);
	continue;
      }
      NewPnum++;
      NewParam=(char **) my_realloc((void *)  NewParam,NewPnum*sizeof(char *));
      NewPval=(double *) my_realloc((void *) NewPval,NewPnum*sizeof(double));
      NewParam[NewPnum-1] = strdup(Param[i]);
      NewPval[NewPnum-1] = Pval[i];
    }

    for(i=0;i<Pnum;i++)
      free(Param[i]);
    free(Param);
    free(Pval);
    
    Param = NewParam;
    Pval = NewPval;
    Pnum= NewPnum;


    /* prepare the new list of argument names */
    F.argnum=columns+Pnum;
    F.argname = (char **) my_alloc((columns+Pnum)*sizeof(char *));
    
    for(i=0;i<columns;i++){

      int length;
      length = snprintf(NULL,0,"x%zu",i+1);
      F.argname[i] = (char *) my_alloc((length+1)*sizeof(char));
       snprintf(F.argname[i],length+1,"x%zu",i+1);

    }
    for(i=columns;i<columns+Pnum;i++){

      int length;
      length = snprintf(NULL,0,"%s",Param[i-columns]);
      F.argname[i] = (char *) my_alloc((length+1)*sizeof(char));
      snprintf(F.argname[i],length+1,"%s",Param[i-columns]);

    }

    /* define first order derivatives w.r.t. the parameters */
    F.df = (void **) my_alloc(Pnum*sizeof(void *));
    for(i=0;i<Pnum;i++){
      F.df[i] = evaluator_derivative (F.f,Param[i]);
      assert(F.df[i]);
    }

    /* define first order derivatives w.r.t. the co-variates */
    F.dfdx = (void **) my_alloc(columns*sizeof(void *));
    for(i=0;i<columns;i++){
      F.dfdx[i] = evaluator_derivative (F.f,F.argname[i]);
      assert(F.dfdx[i]);
    }

  }


  /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
  /* print builded functions and variables */
  if(o_verbose>3){
    size_t i;

    fprintf(stderr," ------------------------------------------------------------\n");

    fprintf (stderr,"\n  Parameters and initial conditions:\n");
    for(i=0;i<Pnum;i++)
      fprintf (stderr,"      %s = %f\n", Param[i],Pval[i]);

    fprintf (stderr,"\n  Model first derivatives:\n");
    for(i=0;i<Pnum;i++)
      fprintf (stderr,"     d f(x) / d%s = %s\n",Param[i],evaluator_get_string ((F.df)[i]));
    for(i=0;i<columns;i++)
      fprintf (stderr,"     d f(x) / d%s = %s\n",F.argname[i],evaluator_get_string ((F.dfdx)[i]));

  }
  /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */


  switch(o_model){
  case 0:			/* Polya model */
    obj_f=objpolya_f;
    obj_df=objpolya_df;
    obj_fdf=objpolya_fdf;
    marginals=marginals_polya;
    margelast=margelast_polya;
    marginal=marginal_polya;
    meffnum=columns+1;
    break;
  case 1:			/* multinomial model */
    obj_f=objmulti_f;
    obj_df=objmulti_df;
    obj_fdf=objmulti_fdf;
    marginals=marginals_multi;
    margelast=margelast_multi;
    marginal=marginal_multi;
    meffnum=columns;
    break;
  }



  /* parameters estimation */
  /* ===================== */

  /* set the parameter for the function */
  /* ---------------------------------- */
  llparams.F = &F;
  llparams.rows = rows;
  llparams.columns = columns;
  llparams.datan = datan;
  llparams.datax = datax;

  {
    size_t i;
    llparams.N=0;
    for(i=0;i<rows;i++)
      llparams.N+=datan[i];
  }


  /* negative log-likelihood minimization */
  {


    /* ADD types and xmin if constraints are desired */
    /* set the boundaries */
    /* size_t i; */
    /* unsigned *types= (unsigned *) my_alloc(Pnum*sizeof(unsigned)); */
    /* double *xmins= (double *) my_alloc(Pnum*sizeof(double)); */

    /* for(i=0;i<Pnum;i++){ */
    /*   types[i]=1; */
    /*   xmins[i]=0.0; */
    /* } */

    /* multimin(Pnum,Pval,&llmin,types,xmins,NULL,obj_f,obj_df,obj_fdf,(void *) &llparams,mmparams); */

    multimin(Pnum,Pval,&llmin,NULL,NULL,NULL,obj_f,obj_df,obj_fdf,(void *) &llparams,mmparams);


  }


  /* compute marginal effects */
  if(o_output==1){
    meff = (double *) my_alloc(meffnum*sizeof(double));
    marginals(Pnum,Pval,F,columns,rows,datax,datan,meff);
  }
  else if (o_output==2 ){
    meff = (double *) my_alloc(meffnum*sizeof(double));
    margelast(Pnum,Pval,F,columns,rows,datax,datan,meff);
  }

  /* allocate standard error and p-score */
  Pstd = (double *) my_calloc(Pnum,sizeof(double));
  Pscr = (double *) my_calloc(Pnum,sizeof(double));
  meff_std = (double *) my_calloc(meffnum,sizeof(double));
  meff_scr = (double *) my_calloc(meffnum,sizeof(double));

  /* bootstrap estimation of errors */
  if(o_stderr==1){

    size_t i,j,t;

    double res[Pnum],newmeff[meffnum];
    double newllmin;

    /* parameters for the likelihood expression */
    struct objdata newllparams;

    /* allocate new data */
    size_t * newdatan = (size_t *) my_alloc(rows*sizeof(size_t));
    double ** newdatax = (double **) my_alloc(columns*sizeof(double *));
    for(i=0;i<columns;i++)
      newdatax[i] = (double *) my_alloc(rows*sizeof(double));

    /* set appropriate references */
    newllparams.F = &F;
    newllparams.rows = rows;
    newllparams.columns = columns;
    newllparams.datan = newdatan;
    newllparams.datax = newdatax;


    /* set the RNG */
    srandom(seed);

    /* fprintf(stderr,"run %d: ",0); */
    /* for(i=0;i<Pnum;i++) */
    /*   fprintf(stderr,"%f ",Pval[i]); */
    /* fprintf(stderr,"\n"); */


    for(t=0;t<replicas;t++){

      /* data sampling with replacement */
      for(i=0;i<rows;i++){
	size_t index= (size_t) (rows*( (double) random())/RAND_MAX );
	newdatan[i] = datan[index];
	for(j=0;j<columns;j++)
	  newdatax[j][i]=datax[j][index];
      }
      
      /* compute the total number of observations */
      newllparams.N=0;
      for(i=0;i<rows;i++)
	newllparams.N+=newdatan[i];

      /* initialize the values of the parameters */
      for(i=0;i<Pnum;i++)
	res[i]=Pval[i];

      /* estimation of the model */
      multimin(Pnum,res,&newllmin,NULL,NULL,NULL,obj_f,obj_df,obj_fdf,(void *) &newllparams,mmparams);

      /* fprintf(stderr,"run %d: ",t+1); */
      /* for(i=0;i<Pnum;i++) */
      /* 	fprintf(stderr,"%f ",res[i]); */
      /* fprintf(stderr,"\n"); */

      /* computation of new marginal effect */
      if(o_output==1)
	marginals(Pnum,res,F,columns,rows,newdatax,newdatan,newmeff);
      else if (o_output==2 )
	margelast(Pnum,res,F,columns,rows,newdatax,newdatan,newmeff);

      /* update standard error on estimates and p-scores */
      for(i=0;i<Pnum;i++){
	Pstd[i]+=pow(res[i]-Pval[i],2);
	if(Pval[i]*res[i]<0) 	/* if original estimate and replica
	 			   estimate have different signs */ 
	  Pscr[i]+=1;
      }

      /* update standard error on marginal effects */
      if(o_output==1 || o_output==2 )
	for(i=0;i<meffnum;i++){
	  meff_std[i]+=pow(newmeff[i]-meff[i],2);
	  if(newmeff[i]*meff[i]<0) /* if original estimate and replica
	 			   estimate have different signs */ 
	    meff_scr[i]+=1;
	}

    }

    /* rescale standard errors and p-scores */
    for(i=0;i<Pnum;i++){
      Pstd[i]= sqrt(Pstd[i]/(replicas-1));
      Pscr[i]= Pscr[i]/replicas;
    }

    /* rescale standard errors and p-scores on marginal effects */
    if(o_output==1 || o_output==2 )
      for(i=0;i<meffnum;i++){
	meff_std[i]=sqrt(meff_std[i]/(replicas-1));
	meff_scr[i]=meff_scr[i]/replicas;
      }

    /* deallocate new data */
    for(i=0;i<columns;i++)
      free(newdatax[i]);
    free(newdatax);
    free(newdatan);
  }


  /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
  if(o_verbose>1){

    size_t i,j;
    double A=0;
    double MFpR2=0; 		/* McFadden adjusted pseudo R^2 */
    double CpR2=0;		/* Effron pseudo R^2 */
    double CSpR2=0;		/* Cox&Snell rescaled pseudo R^2 */
    double values[Pnum+columns];

    fprintf(stderr,"#\n");
    fprintf(stderr,"# Summary statistics       \n");
    fprintf(stderr,"# number of rows       : %zd\n",rows);
    fprintf(stderr,"# number of variables  : %zd\n",Pnum);
    fprintf(stderr,"# number of regressors : %zd\n",columns);
    fprintf(stderr,"# number of events     : %zd\n",llparams.N);

    /* set the parameter values */
    for(i=columns;i<columns+Pnum;i++)
      values[i] = Pval[i-columns];
      
    /* compute A */
    for(i=0;i<rows;i++){
      
      /* set the variables values */
      for(j=0;j<columns;j++)
	values[j] = datax[j][i];
      A += evaluator_evaluate (F.f,columns+Pnum,F.argname,values);
      
    }

    /* compute the Effron pseudo R^2

                  1/L \sum_l (n_l -n^*_l)^2
       R^2 = 1 - --------------------
                    Var[n_l]

       where n_l is the observed value of the l-th realization and
       n^*_l the prediction of the model. Var[n_l] is the variance of
       the observed realizations
     */
    {
      double dtmp1=0;
      
      /* squared deviation between predicted and observed */
      for(i=0;i<rows;i++){ 
	
     	for(j=0;j<columns;j++) 
     	  values[j] = datax[j][i]; 
	
     	CpR2 += pow(datan[i] - evaluator_evaluate(F.f,columns+Pnum,F.argname,values)*llparams.N/A,2); 
      } 
      CpR2/=rows;

      /* variance of observed; the mean is llparams.N/rows */
      for(i=0;i<rows;i++){
	dtmp1 += datan[i]*datan[i];
      }      
      dtmp1 = dtmp1/rows - (llparams.N/rows)*(llparams.N/rows);

      CpR2 = 1-CpR2/dtmp1;

    }



    /* compute the McFadden adjusted pseudo R^2 


                   log(L)-K         NLL + K/N
       R^2 = 1 - ------------ = 1- ----------------
                   log(L_0)         NLL_0

       where L is the log-likelihood of the estimated model, L_0 the
       log-likelihood of the zeroth-order model and K the number of
       independent variables . In the second expression NLL is the
       negative log-likelihood of the estimated model per observation,
       NLL_0 is the negative log-likelihood of the zeroth-order model
       per observation and N is the number of observations.

     */
    {
      const double dtmp1=zeromll(datan,rows,llparams.N);
      MFpR2 = 1 -(llmin+Pnum/rows)/dtmp1 ;
    }



    /* compute the Cox & Snell rescaled pseudo R^2:


             1-(L_0/L)^(2/N)    1-exp( 2*( NLL-NLL_0 ) ) 
       R^2 = --------------- = -----------------------------
              1-(L_0)^(2/N)         1-exp( -2*NLL_0 ) 

       where L is the log-likelihood of the estimated model, L_0 the
       log-likelihood of the zeroth-order model and N the number of
       observations. In the second expression NLL is the negative
       log-likelihood of the estimated model per observation, NLL_0 is
       the negative log-likelihood of the zeroth-order model per
       observation.
     */
    {
      const double dtmp1=zeromll(datan,rows,llparams.N);
      CSpR2 = ( 1-exp( 2*(llmin-dtmp1) ))/( 1-exp(-2*dtmp1) );
    }


    fprintf(stderr,"# total agglomeration        : %f\n",A);
    fprintf(stderr,"# null-model ll per obs      : %f\n",-zeromll(datan,rows,llparams.N));
    fprintf(stderr,"# max log-like per obs       : %f\n",-llmin);
    fprintf(stderr,"# Effron pseudo R^2          : %f\n",CpR2);
    fprintf(stderr,"# McFadden adj. pseudo R^2   : %f\n",MFpR2);
    fprintf(stderr,"# Cox&Snell resc. pseudo R^2 : %f\n",CSpR2);
    fprintf(stderr,"# Akaike inf. crit.          : %f\n",2*llmin*rows+2*Pnum);
    fprintf(stderr,"#\n");
  }
  /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
  
  /* output */
  /* ====== */

  /* +++++++++++++++++++++++++++++++++++ */
  if(o_verbose > 0 && o_output > 0){
    size_t i;

    fprintf(stderr,"# Estimated parameters:\n");
    fprintf(stderr,"#");
    if(o_stderr==0){
      for(i=0;i<Pnum-1;i++){
	fprintf(stderr,EMPTY_SEP,Param[i]);
      }
      i=Pnum-1;
      fprintf(stderr,EMPTY_NL,Param[i]);
      
      fprintf(stderr,"#");
      for(i=0;i<Pnum-1;i++){
	fprintf(stderr,FLOAT_SEP,Pval[i]);
      }
      /* i=Pnum-1; */
      fprintf(stderr,FLOAT_NL,Pval[i]);
    }
    else if (o_stderr==1){
      for(i=0;i<Pnum-1;i++){
	fprintf(stderr,EMPTY_SEP,Param[i]);
	fprintf(stderr,EMPTY_SEP,"+/-");
	fprintf(stderr,EMPTY_SEP,"p-score");
      }
      fprintf(stderr,EMPTY_SEP,Param[i]);
      fprintf(stderr,EMPTY_SEP,"+/-");
      fprintf(stderr,EMPTY_NL,"p-score");
      
      fprintf(stderr,"#");
      for(i=0;i<Pnum-1;i++){
	fprintf(stderr,FLOAT_SEP,Pval[i]);
	fprintf(stderr,FLOAT_SEP,Pstd[i]);
	fprintf(stderr,FLOAT_SEP,Pscr[i]);
      }
      fprintf(stderr,FLOAT_SEP,Pval[i]);
      fprintf(stderr,FLOAT_SEP,Pstd[i]);
      fprintf(stderr,FLOAT_NL,Pscr[i]);
    }

    fprintf(stderr,"#\n");

  }
  /* +++++++++++++++++++++++++++++++++++ */
  
  switch(o_output){
  case 0:  /* point estimates and log-likelihood */
    {
      size_t i;
      
      if(o_stderr==0){
	/* +++++++++++++++++++++++++++++++++++ */
	if(o_verbose>0){
	  fprintf(stdout,"#");
	  for(i=0;i<Pnum;i++)
	    fprintf(stdout,EMPTY_SEP,Param[i]);
	  fprintf(stdout,EMPTY_NL,"-log-like");
	}
	/* +++++++++++++++++++++++++++++++++++ */

	for(i=0;i<Pnum;i++){
	  fprintf(stdout,FLOAT_SEP,Pval[i]);
	}
	fprintf(stdout,FLOAT_NL,llmin);
      }      
      else{

	/* +++++++++++++++++++++++++++++++++++ */
	if(o_verbose>0){
	  fprintf(stdout,"#");
	  for(i=0;i<Pnum;i++){
	    fprintf(stdout,EMPTY_SEP,Param[i]);
	    fprintf(stdout,EMPTY_SEP,"+/-");
	  }
	  fprintf(stdout,EMPTY_NL,"-log-like");
	}
	/* +++++++++++++++++++++++++++++++++++ */

	for(i=0;i<Pnum;i++){
	  fprintf(stdout,FLOAT_SEP,Pval[i]);
	  fprintf(stdout,FLOAT_SEP,Pstd[i]);
	}
	fprintf(stdout,FLOAT_NL,llmin);
      }

    }
    break;
  case 1:  /* marginal effects */
  case 2:
    {
      size_t i;
      
      /* +++++++++++++++++++++++++++++++++++ */
      if(o_verbose>0){
	if(o_output==1)
	  fprintf(stdout,"# Marginal effects\n");
	else if (o_output==2 )
	  fprintf(stdout,"# Marginal elasticities\n");
      }
      /* +++++++++++++++++++++++++++++++++++ */

      
      if(o_stderr==0){
	/* +++++++++++++++++++++++++++++++++++ */
	if(o_verbose>0){
	  fprintf(stdout,"#");
	  for(i=0;i<columns;i++)
	    fprintf(stdout,EMPTY_SEP,F.argname[i]);
	  if(meffnum>columns)
	    fprintf(stdout,EMPTY_NL,"n");
	  else
	    fprintf(stdout,"\n");
	}
	/* +++++++++++++++++++++++++++++++++++ */

	for(i=0;i<meffnum-1;i++){
	  fprintf(stdout,FLOAT_SEP,meff[i]);
	}
	fprintf(stdout,FLOAT_NL,meff[meffnum-1]);
      }
      else{

	/* +++++++++++++++++++++++++++++++++++ */
	if(o_verbose>0){
	  fprintf(stdout,"#");
	  for(i=0;i<columns;i++){
	    fprintf(stdout,EMPTY_SEP,F.argname[i]);
	    fprintf(stdout,EMPTY_SEP,"+/-");
	    fprintf(stdout,EMPTY_SEP,"p-scr");
	  }
	  if(meffnum>columns){
	    fprintf(stdout,EMPTY_SEP,"n");
	    fprintf(stdout,EMPTY_SEP,"+/-");
	    fprintf(stdout,EMPTY_NL,"p-scr");
	  }
	  else
	    fprintf(stdout,"\n");
	}
	/* +++++++++++++++++++++++++++++++++++ */

	for(i=0;i<meffnum-1;i++){
	  fprintf(stdout,FLOAT_SEP,meff[i]);
	  fprintf(stdout,FLOAT_SEP,meff_std[i]);
	  fprintf(stdout,FLOAT_SEP,meff_scr[i]);
	}
	fprintf(stdout,FLOAT_SEP,meff[meffnum-1]);
	fprintf(stdout,FLOAT_SEP,meff_std[meffnum-1]);
	fprintf(stdout,FLOAT_NL,meff_scr[meffnum-1]);
      }

    }
    break;
  case 3:
    {
      
      size_t i,j;
      double A=0,a;
      double values[Pnum+columns];
      
      /* set the parameter values */
      for(i=columns;i<columns+Pnum;i++)
	values[i] = Pval[i-columns];

      /* compute A */
      for(i=0;i<rows;i++){
	
	/* set the variables values */
	for(j=0;j<columns;j++)
	  values[j] = datax[j][i];
	A += evaluator_evaluate (F.f,columns+Pnum,F.argname,values);

      }

      /* +++++++++++++++++++++++++++++++++++ */
      if(o_verbose>0){
	fprintf(stdout,"#observed vs. predicted occupancy\n");
	fprintf(stdout,"#");
	fprintf(stdout,EMPTY_SEP,"obs.");
	fprintf(stdout,EMPTY_SEP,"pred.");
	fprintf(stdout,EMPTY_NL,"a/b");
      }
      /* +++++++++++++++++++++++++++++++++++ */


      for(i=0;i<rows;i++){

	fprintf(stdout,INT_SEP,datan[i]); 

	/* set the variables values */
	for(j=0;j<columns;j++)
	  values[j] = datax[j][i];

	a= evaluator_evaluate (F.f,columns+Pnum,F.argname,values);

	fprintf(stdout,FLOAT_SEP,llparams.N*a/A);

	fprintf(stdout,FLOAT_NL,a);
      }

    }
    break;
  case 4:
    {
      
      size_t bins,i,j,k;
      double A=0,a,chisq;
      double values[Pnum+columns];
      double *emp,*the;

      /* decide the number of bins */
      bins=0;
      for(i=0;i<rows;i++){
	size_t itmp1 = (size_t)	floor(log(datan[i]+1)/log(2));
	if(itmp1>bins)
	  bins=itmp1;
      }
      bins+=1;

      /* allocate necessary space */
      emp = (double *) my_calloc(bins,sizeof(double));
      the = (double *) my_calloc(bins,sizeof(double));

      /* set the parameter values */
      for(i=columns;i<columns+Pnum;i++)
	values[i] = Pval[i-columns];
      
      /* compute A */
      for(i=0;i<rows;i++){
	
	/* set the variables values */
	for(j=0;j<columns;j++)
	  values[j] = datax[j][i];
	A += evaluator_evaluate (F.f,columns+Pnum,F.argname,values);

      }

      /* build the empirical occupancy */
      for(i=0;i<rows;i++)
	emp[(size_t ) floor(log(datan[i]+1)/log(2))]++;

      /* build the theoretical occupancy */
      for(i=0;i<rows;i++){
	unsigned bound;

	/* set the variables values */
	for(j=0;j<columns;j++)
	  values[j] = datax[j][i];

	a= evaluator_evaluate (F.f,columns+Pnum,F.argname,values);
	
	bound=1;
	for(k=0;k<bins;k++){
 	  for(j=bound-1;j<2*bound-1;j++){
	    /* fprintf(stderr,"ciao %d %d %d\n",bound-1,2*bound-1,j); */
	    the[k]+=marginal(j,a,A,llparams.N);
	  }
	  bound *=2;
	}
      }

      /* compute chi-square */
      chisq=0.0;
      for(k=0;k<bins;k++){
	chisq+=pow(the[k]-emp[k],2)/the[k];
      }

      /* +++++++++++++++++++++++++++++++++++ */
      if(o_verbose>0){
	fprintf(stderr,"# chi-square: %f\n",chisq);
	fprintf(stdout,"# occupancy by classes\n");
	fprintf(stdout,"#");
	fprintf(stdout,EMPTY_SEP,"cmin");
	fprintf(stdout,EMPTY_SEP,"cmax");
	fprintf(stdout,EMPTY_SEP,"emp");
	fprintf(stdout,EMPTY_NL,"theo");
      }
      /* +++++++++++++++++++++++++++++++++++ */


      /* print the classes occupancy */
      for(i=0;i<bins;i++){
	fprintf(stdout,INT_SEP,(int) ( pow(2,i)-1   ) );
	fprintf(stdout,INT_SEP,(int) ( pow(2,i+1)-2 ) );
	fprintf(stdout,FLOAT_SEP,emp[i]);
	fprintf(stdout,FLOAT_NL,the[i]);
      }

      free(emp);
      free(the);

    }
    break;

  }


  /* deallocate function and data */
  /* ============================ */
  {
    size_t i;


    /* deallocate data storage */
    for(i=0;i<columns;i++){
      free(datax[i]);
    }
    free(datax);
    free(datan);

    /* deallocate point estimates and standard errors */
    for(i=0;i<Pnum;i++)
      free(Param[i]);
    free(Param);
    free(Pval);

    /* deallocate standard error and p-score of parameter and marginal
       effects */
    free(Pstd);
    free(Pscr);
    free(meff_std);
    free(meff_scr);
    
    /* deallocate functions */
    evaluator_destroy(F.f);

    for(i=0;i<Pnum;i++)
      evaluator_destroy(F.df[i]);
    free(F.df);

    for(i=0;i<columns;i++){
      evaluator_destroy(F.dfdx[i]);
    }
    free(F.dfdx);

    for(i=0;i<F.argnum;i++)
      free(F.argname[i]);
    free(F.argname);

    free(splitstring);
  }

  return 0;

}
