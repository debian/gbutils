#!/bin/bash

# created:  2022-03-10 giulio
# Time-stamp: <2022-04-20 18:42:06 giulio>

export LANG=C
export LC_ALL=C

res=`echo "
10
11
12
13
14
20
21
22
23
24
30
31
32
33
34
" | ./gbhill exponential 1 0 -u .3 | ./gbfun 'sqrt((x1-1.5)^2+(x2-29.01737)^2+(x3-3.58014)^2 )*10000' -o %d`

status=$?

[[ $status == 0 && $res -lt 1 ]]
