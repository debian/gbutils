/*
  gblreg  (ver. 5.6) -- Estimate linear regression model
  Copyright (C) 2005-2018 Giulio Bottazzi

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  (version 2) as published by the Free Software Foundation;
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#include "tools.h"

#include <gsl/gsl_fit.h>
#include <gsl/gsl_cdf.h>


void print_linear_verbose(FILE *file,const int o_weight, const int o_model,
			  const size_t size,
			  const double c0,const double c1, 
			  const double cov00,const double cov01, const double cov11,
			  const double sumsq){

  const int ndf = size-(o_model==0?2:1);

  fprintf(file,"\n");
  fprintf(file," ------------------------------------------------------------\n");
  fprintf(stderr,"  number of observations                  = %zd\n",size);
  fprintf(stderr,"  number of indep. variables              = %d\n",ndf);
  fprintf(file,"\n");
  fprintf(stderr,"  model (C[i]: i-th column; eps: residual):\n\n");
  fprintf(stderr,"    C[2] ~ ");
  if(o_model==0) fprintf(stderr," c0 + ");
  fprintf(stderr,"c1*C[1] + ");
  if(o_weight)
    fprintf(stderr,"eps*C[3]\n");
  else
    fprintf(stderr,"eps\n");
  
  fprintf(file,"\n");
  fprintf(file," ------------------------------------------------------------\n");
  if(!o_weight){
    fprintf(file,"  sum of squared residual           (SSR) = %f\n",sumsq);
    fprintf(file,"  number degrees of freedom         (ndf) = %d\n",ndf);
    fprintf(file,"                            sqrt(SSR/ndf) = %f\n",sqrt(sumsq/ndf));
    fprintf(file,"  chi-square test           P(>SSR | ndf) = %e\n",
	    gsl_cdf_chisq_Q (sumsq,ndf));
  }
  else{
    fprintf(file,"  sum of weighted squared residual (WSSR) = %f\n",sumsq);
    fprintf(file,"  number degrees of freedom         (ndf) = %d\n",ndf);
    fprintf(file,"                           sqrt(WSSR/ndf) = %f\n",sqrt(sumsq/ndf));
    fprintf(file,"  chi-square test          P(>WSSR | ndf) = %e\n",
	    gsl_cdf_chisq_Q (sumsq,ndf));
    
  }
  fprintf(file," ------------------------------------------------------------\n");
  fprintf(file,"\n");
  
    /* Normal approximation to chi^2 */
/*     fprintf(file,"            chisq. test Q(ndf/2,WSSR/2) = %e\n", */
/* 	    gsl_cdf_gaussian_Q(sumsq-(size-cov->size1),sqrt(2*(size-cov->size1)))); */

  if(o_model==0){
    fprintf(file," c0= %+f +/- %f (%5.1f%%)  | % f % f |\n",c0,sqrt(cov00),100*sqrt(cov00)/fabs(c0),1.,cov01/sqrt(cov00*cov11));
    fprintf(file," c1= %+f +/- %f (%5.1f%%)  | % f % f |\n",c1,sqrt(cov11),100*sqrt(cov11)/fabs(c1),cov01/sqrt(cov00*cov11),1.);
  }
  else {
    fprintf(file," c1= %+f +/- %f (%5.1f%%)\n",c1,sqrt(cov11),100*sqrt(cov11)/fabs(c1));
  }
  fprintf(file," ------------------------------------------------------------\n");
  fprintf(file,"\n");

}

int main(int argc,char* argv[]){


  double **data=NULL; /* array of values */
  size_t size=0; /* length of array vals */

  char *splitstring = strdup(" \t");

  int o_verbose=0;
  int o_model=0;
  int o_output=0;
  int o_weight=0;



  /* COMMAND LINE PROCESSING */
  
  /* variables for reading command line options */
  /* ------------------------------------------ */
  int opt;
  /* ------------------------------------------ */
  
  /* read the command line */    
  while((opt=getopt_long(argc,argv,"v:hF:M:O:w",gb_long_options, &gb_option_index))!=EOF){
    if(opt==0){
      gbutils_header(argv[0],stdout);
      exit(0);
    }
    else if(opt=='?'){
      fprintf(stderr,"option %c not recognized\n",optopt);
      exit(-1);
    }
    else if(opt=='F'){
      /* set the fields separator string*/
      free(splitstring);
      splitstring = strdup(optarg);
    }
    else if(opt=='M'){
      /* set the method to use*/
      o_model = atoi(optarg);
    }
    else if(opt=='O'){
      /* set the type of output */
      o_output = atoi(optarg);
    }
    else if(opt=='w'){
      /* set third columns as weight */
      o_weight = 1;
    }
    else if(opt=='v'){
      /* set verbosity level*/
      o_verbose = atoi(optarg);
    }
    else if(opt=='h'){
      /* print short help */
      fprintf(stdout,"Linear regression. Data provided should have independent variable on the\n");
      fprintf(stdout,"1st column and dependent variable on the 2nd. Standard error on dependent\n");
      fprintf(stdout,"variable can be provided on the 3rd column.\n");
      fprintf(stdout,"\nUsage: %s [options]\n\n",argv[0]);
      fprintf(stdout,"Options:\n");
      fprintf(stdout," -M  the linear regression model (default 0)\n");
      fprintf(stdout,"      0  with estimated intercept                        \n");
      fprintf(stdout,"      1  with zero intercept                             \n");
      fprintf(stdout," -O  the type of output (default 0)\n");
      fprintf(stdout,"      0  regression coefficients                         \n");
      fprintf(stdout,"      1  regression coefficients and errors              \n");
      fprintf(stdout,"      2  x, fitted y, error on y, residual               \n");
      fprintf(stdout," -w  consider provided standard errors on y             \n");
      fprintf(stdout," -v  verbosity level (default 0)\n");
      fprintf(stdout,"      0  just output                                     \n");
      fprintf(stdout,"      1  commented headings                              \n");
      fprintf(stdout,"      2  model details                                   \n");
      fprintf(stdout," -F  specify the input fields separators (default \" \\t\")\n");
      fprintf(stdout," -h  print this help\n");
      exit(0);
    }
  }
  /* END OF COMMAND LINE PROCESSING */

  /* initialize global variables */
  initialize_program(argv[0]);

  switch(o_model){
  case 0 :
    {
      double c0,c1,cov00,cov01,cov11,sumsq;
      
      if(!o_weight){
	load2(&data,&size,0,splitstring);
	
	gsl_fit_linear (data[0],1, data[1],1, size,
			&c0,&c1,&cov00,&cov01,&cov11,&sumsq);
      }
      else {
	size_t i;
	load3(&data,&size,0,splitstring);
	for(i=0;i<size;i++){	/* build weights from std. dev. */
	  const double dtmp1=data[2][i];
	  data[2][i]=1./(dtmp1*dtmp1);
	}
	gsl_fit_wlinear (data[0],1,data[2],1,data[1],1, size,
			 &c0,&c1,&cov00,&cov01,&cov11,&sumsq);
      }
      
      /* in case, print verbose output */
      if(o_verbose>1)
	print_linear_verbose(stdout,o_weight,o_model,size,c0,c1,cov00,cov01,cov11,sumsq);
      
      switch(o_output){
      case 0:
	/* +++++++++++++++++++++++++++++++ */
	if(o_verbose>0){
	  fprintf(stdout,EMPTY_SEP,"#c0");
	  fprintf(stdout,EMPTY_NL,"c1");
	}
	/* +++++++++++++++++++++++++++++++ */
	fprintf(stdout,FLOAT_SEP,c0);
	fprintf(stdout,FLOAT_NL,c1);
	break;
      case 1:
	/* +++++++++++++++++++++++++++++++ */
	if(o_verbose>0){
	  fprintf(stdout,EMPTY_SEP,"#c0");
	  fprintf(stdout,EMPTY_SEP,"+/-");
	  fprintf(stdout,EMPTY_SEP,"c1");
	  fprintf(stdout,EMPTY_NL,"+/-");
	}
	/* +++++++++++++++++++++++++++++++ */
	fprintf(stdout,FLOAT_SEP,c0);
	fprintf(stdout,FLOAT_SEP,sqrt(cov00));
	fprintf(stdout,FLOAT_SEP,c1);
	fprintf(stdout,FLOAT_NL,sqrt(cov11));
	break;
      case 2:
	{
	  size_t i;
	  double y,y_err;
	  /* +++++++++++++++++++++++++++++++ */
	  if(o_verbose>0){
	  fprintf(stdout,EMPTY_SEP,"#c0");
	  fprintf(stdout,EMPTY_SEP,"+/-");
	  fprintf(stdout,EMPTY_SEP,"c1");
	  fprintf(stdout,EMPTY_NL,"+/-");
	  fprintf(stdout,"#");
	  fprintf(stdout,FLOAT_SEP,c0);
	  fprintf(stdout,FLOAT_SEP,sqrt(cov00));
	  fprintf(stdout,FLOAT_SEP,c1);
	  fprintf(stdout,FLOAT_NL,sqrt(cov11));
	  }
	  /* +++++++++++++++++++++++++++++++ */
	  for(i=0;i<size;i++){
	    gsl_fit_linear_est (data[0][i], c0,c1,cov00,cov01,cov11,&y,&y_err);
	    fprintf(stdout,FLOAT_SEP,data[0][i]);
	    fprintf(stdout,FLOAT_SEP,y);
	    fprintf(stdout,FLOAT_SEP,y_err);
	    fprintf(stdout,FLOAT_NL,data[1][i]-y);
	  }
	}
      }
    }
    break;

  case 1 :
    {
      double c1,cov11,sumsq;
      
      if(!o_weight){
	load2(&data,&size,0,splitstring);       
	gsl_fit_mul (data[0],1,data[1],1, size,&c1,&cov11,&sumsq);
      }
      else {
	size_t i;
	load3(&data,&size,0,splitstring);
	for(i=0;i<size;i++){	/* build weights from std. dev. */
	  const double dtmp1=data[2][i];
	  data[2][i]=1./(dtmp1*dtmp1);
	}
	gsl_fit_wmul (data[0],1,data[2],1,data[1],1, size,&c1,&cov11,&sumsq);
      }
    
      /* in case, print verbose output */
      if(o_verbose>1)
	print_linear_verbose(stdout,o_weight,o_model,size,0.0,c1,0.0,0.0,cov11,sumsq);
      
    
      switch(o_output){
      case 0:
	/* +++++++++++++++++++++++++++++++ */
	if(o_verbose>0)
	  fprintf(stdout,EMPTY_NL,"#c1");
	/* +++++++++++++++++++++++++++++++ */
	fprintf(stdout,FLOAT_NL,c1);
	break;
	
      case 1:
	/* +++++++++++++++++++++++++++++++ */
	if(o_verbose>0){
	  fprintf(stdout,EMPTY_SEP,"#c1");
	  fprintf(stdout,EMPTY_NL,"+/-");
	}
	/* +++++++++++++++++++++++++++++++ */
	fprintf(stdout,FLOAT_SEP,c1);
	fprintf(stdout,FLOAT_NL,sqrt(cov11));
	break;
	
      case 2:
	{
	  size_t i;
	  double y,y_err;
	  /* +++++++++++++++++++++++++++++++ */
	  if(o_verbose>0){
	  fprintf(stdout,EMPTY_SEP,"#c1");
	  fprintf(stdout,EMPTY_NL,"+/-");
	  }
	  /* +++++++++++++++++++++++++++++++ */
	  fprintf(stdout,FLOAT_SEP,c1);
	  fprintf(stdout,FLOAT_NL,sqrt(cov11));
	  for(i=0;i<size;i++){
	    gsl_fit_mul_est (data[0][i],c1,cov11,&y,&y_err);
	    fprintf(stdout,FLOAT_SEP,data[0][i]);
	    fprintf(stdout,FLOAT_SEP,y);
	    fprintf(stdout,FLOAT_SEP,y_err);
	    fprintf(stdout,FLOAT_NL,data[1][i]-y);
	  }
	}
      }
    }
    break;
  }


  return 0;
}
