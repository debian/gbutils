#!/bin/bash

# created: 2021-08-06 giulio
# Time-stamp: <2022-04-11 14:03:28 giulio>

res=`echo "10 20 30" | ./gbfun -r0 x+x1 -o %d`

status=$?

[[ $status == 0 && $res  == "60" ]]
