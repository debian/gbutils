#include "gbhill.h"



/* Gaussian Distribution */
/* ------------------------ */

/* x[0]=m x[1]=s */

/* F[j] =  */
/* -log(f[j]) = log(s) + (1.0/(2*pow(s,2)))*pow((x[j]-m),2)   */

/* N: sample size k: number of observations used */


void
gaussian_nll (const size_t n, const double *x,void *params,double *fval){

  size_t i;

  /* log-likelihood parameters */
  const size_t min    = ((struct loglike_params *) params)->min;
  const size_t max    = ((struct loglike_params *) params)->max;
  const double N      = (double) ((struct loglike_params *) params)->size;
  const double d      = (double) ((struct loglike_params *) params)->d;
  const int    method = ((struct loglike_params *) params)->method;
  const double *data  = ((struct loglike_params *) params)->data;

  /* distribution parameters */
  const double m=x[0];
  const double s=x[1];

  const double k = ((double) max-min+1);
  
  *fval=0.0;
  for(i=min;i<=max;i++) *fval += pow((data[i]-m),2);
  *fval = log(s) + (*fval)/(2*k*s*s);

  switch(method){

  case 0:
    if(N>k){ 
      const double F =gsl_cdf_gaussian_P(data[min]-m, s) ;
      *fval += -(N/k-1.0)*log(F);
    }
    break;
  case 1:
    if(N>k){ 
      const double F =gsl_cdf_gaussian_P(d-m, s) ;
      *fval += -(N/k-1.0)*log(F);
    }
    break;
  case 2:
    if(N>k){
      const double F =gsl_cdf_gaussian_P(data[max]-m, s);      
      *fval += -(N/k-1.0)*log(1.0-F);
    }    
    break;
  case 3:
    if(N>k){ 
      const double F =gsl_cdf_gaussian_P(d-m, s) ;
      *fval += -(N/k-1.0)*log(1.0-F);
    }
    break;
  }

/*   fprintf(stderr,"[ f ] x1=%g x2=%g f=%g\n",x[0],x[1],*fval); */

}


void	
gaussian_dnll (const size_t n, const double *x,void *params, double *grad){
	
  size_t i;

  /* log-likelihood parameters */
  const size_t min    = ((struct loglike_params *) params)->min;
  const size_t max    = ((struct loglike_params *) params)->max;
  const double N      = (double) ((struct loglike_params *) params)->size;
  const double d      = (double) ((struct loglike_params *) params)->d;
  const int    method = ((struct loglike_params *) params)->method;
  const double *data  = ((struct loglike_params *) params)->data;

  /* distribution parameters */
  const double m=x[0];
  const double s=x[1];

  const double k = ((double) max-min+1);

  grad[0]=0.0;
  for(i=min;i<=max;i++) grad[0] += data[i];
  grad[0]= -(grad[0]/k-m)/(s*s);
  
  grad[1]=0.0;
  for(i=min;i<=max;i++) grad[1] += pow((data[i]-m),2);
  grad[1]= - grad[1]/(k*pow(s,3)) + 1/s;


  switch(method){

  case 0:
    if(N>k) {
      const double F = gsl_cdf_gaussian_P(data[min]-m, s);
      const double f = gsl_ran_gaussian_pdf(data[min]-m,s);
      const double dFdm = - f;
      const double dFds = - f*(data[min]-m)/s;

      grad[0] += - (N/k-1.0)*dFdm/F;
      grad[1] += - (N/k-1.0)*dFds/F;
      
    }
    break;
  case 1:
    if(N>k) {
      const double F = gsl_cdf_gaussian_P(d-m, s);
      const double f = gsl_ran_gaussian_pdf(d-m,s);
      const double dFdm = - f;
      const double dFds = - f*(d-m)/s;

      grad[0] += - (N/k-1.0)*dFdm/F;
      grad[1] += - (N/k-1.0)*dFds/F;
      
    }
    break;
  case 2:
    if(N>k){
      const double F = gsl_cdf_gaussian_P(data[max]-m,s);
      const double f = gsl_ran_gaussian_pdf(data[max]-m,s);
      const double dFdm = - f;
      const double dFds = - f*(data[max]-m)/s;

      grad[0] += (N/k-1.0)*dFdm/(1.0-F);
      grad[1] += (N/k-1.0)*dFds/(1.0-F);
    }
    break;
  case 3:
    if(N>k){
      const double F = gsl_cdf_gaussian_P(d-m,s);
      const double f = gsl_ran_gaussian_pdf(d-m,s);
      const double dFdm = - f;
      const double dFds = - f*(d-m)/s;

      grad[0] += (N/k-1.0)*dFdm/(1.0-F);
      grad[1] += (N/k-1.0)*dFds/(1.0-F);
    }
    break;
  }


/*   fprintf(stderr,"[ df] x1=%g x2=%g df/dx1=%g df/dx2=%g\n", */
/* 	  x[0],x[1],grad[0],grad[1]); */

}


void
gaussian_nlldnll (const size_t n, const double *x,void *params,double *fval,double *grad){


  size_t i;

  /* log-likelihood parameters */
  const size_t min    = ((struct loglike_params *) params)->min;
  const size_t max    = ((struct loglike_params *) params)->max;
  const double N      = (double) ((struct loglike_params *) params)->size;
  const double d      = (double) ((struct loglike_params *) params)->d;
  const int    method = ((struct loglike_params *) params)->method;
  const double *data  = ((struct loglike_params *) params)->data;

  /* distribution parameters */
  const double m=x[0];
  const double s=x[1];

  const double k = ((double) max-min+1);
  
  *fval=0.0;
  for(i=min;i<=max;i++) *fval += pow((data[i]-m),2); 
  *fval = log(s) + (*fval)/(2*k*s*s);
  
  grad[0]=0.0;
  for(i=min;i<=max;i++) grad[0] += data[i];
  grad[0]= -(grad[0]/k-m)/(s*s);
  
  grad[1]=0.0;
  for(i=min;i<=max;i++) grad[1] += pow((data[i]-m),2);
  grad[1]= - grad[1]/(k*pow(s,3)) + 1/s;
  
  switch(method){

  case 0:
    if(N>k) {
      const double F = gsl_cdf_gaussian_P(data[min]-m,s);
      const double f = gsl_ran_gaussian_pdf(data[min]-m,s);
      const double dFdm = - f;
      const double dFds = - f*(data[min]-m)/s;
      
      *fval += -(N/k-1.0)*log(F);			    
      grad[0] += - (N/k-1.0)*dFdm/F;
      grad[1] += - (N/k-1.0)*dFds/F;
     
    }
    break;
  case 1:
    if(N>k) {
      
      const double F = gsl_cdf_gaussian_P(d-m,s);
      const double f = gsl_ran_gaussian_pdf(d-m,s);
      const double dFdm = - f;
      const double dFds = - f*(d-m)/s;
     
      *fval += -(N/k-1.0)*log(F);			    
      grad[0] += - (N/k-1.0)*dFdm/F;
      grad[1] += - (N/k-1.0)*dFds/F;
  
    }
    break;
  case 2:
    if(N>k){
      
      const double F = gsl_cdf_gaussian_P(data[max]-m,s);
      const double f = gsl_ran_gaussian_pdf(data[max]-m,s);
      const double dFdm = - f;
      const double dFds = - f*(data[max]-m)/s;

      *fval += -(N/k-1.0)*log(1.0-(F));
      grad[0] += -(N/k-1.0)*(-dFdm)/(1.0-F);
      grad[1] += -(N/k-1.0)*(-dFds)/(1.0-F);

    }
    break;
  case 3:
    if(N>k){
      const double F = gsl_cdf_gaussian_P(d-m,s);
      const double f = gsl_ran_gaussian_pdf(d-m,s);
      const double dFdm = - f;
      const double dFds = - f*(d-m)/s;

      *fval += -(N/k-1.0)*log(1.0-(F));
      grad[0] += -(N/k-1.0)*(-dFdm)/(1.0-F);
      grad[1] += -(N/k-1.0)*(-dFds)/(1.0-F);
     
    }
    break;
  }


/*   fprintf(stderr,"[fdf] x1=%g x2=%g f=%g df/dx1=%g df/dx2=%g\n", */
/* 	  x[0],x[1],*fval,grad[0],grad[1]); */

}



double
gaussian_f (const double x,const size_t n, const double *par){

  return gsl_ran_gaussian_pdf(x-par[0], par[1]);

}

double
gaussian_F (const double x,const size_t n, const double *par){

  return gsl_cdf_gaussian_P(x-par[0], par[1]);

}


gsl_matrix *gaussian_varcovar(const int o_varcovar, double *x, void *params){

  size_t i;

  /* log-likelihood parameters */
  const size_t min    = ((struct loglike_params *) params)->min;
  const size_t max    = ((struct loglike_params *) params)->max;
  const double N      = (double) ((struct loglike_params *) params)->size;
  const double d      = (double) ((struct loglike_params *) params)->d;
  const int    method = ((struct loglike_params *) params)->method;
  const double *data  = ((struct loglike_params *) params)->data;

  /* distribution parameters */
  const double m=x[0];
  const double s=x[1];

  double k = ((double) max-min+1);
  
  double sum = 0.0;
  for(i=min;i<=max;i++) sum += data[i]-m;

  double sum2 = 0.0;
  for(i=min;i<=max;i++) sum2 += pow((data[i]-m),2);
 
       
  double dldm    = 0.0;
  double d2ldm2  = 0.0;
  double d2ldmds = 0.0;
  double dlds    = 0.0;
  double d2lds2  = 0.0;
  gsl_matrix *covar;
  covar = gsl_matrix_alloc (2,2);
  double F;
  double f;
  switch(method){
  case 0:
  
    /* GSL allocation of memory for the covar matrix */
  
    F = gsl_cdf_gaussian_P(data[min]-m, s) ;
    f = gsl_ran_gaussian_pdf(data[min]-m, s) ;
    
    /* Compute ll partial derivatives. */
    dldm    = (1./pow(s,2.))*sum-(N-k)*f/F;
    d2ldm2  = (-k/pow(s,2.))-(N-k)*(f/F)*(((data[min]-m)/pow(s,2.))+f/F);
    d2ldmds = (-2./pow(s,3.))*sum-(N-k)*(f/F)*(1./s)*(-1.+(data[min]-m)*(f/F)+pow((data[min]-m)/s,2.));
    dlds = -(k/s)+(1./pow(s,3.))*sum2-(N-k)*(f/F)*((data[min]-m)/s);
    d2lds2 = (k/pow(s,2.0))-(3./pow(s,4.))*sum2-(N-k)*((data[min]-m)/(F*s))*(-(f/s)+(f/pow(s,3.))*pow(data[min]-m,2.))-(N-k)*pow(f/F,2.)*pow((data[min]-m)/s,2.)+(N-k)*(f/F)*((data[min]-m)/pow(s,2.));

    break;
  case 1:
    /* GSL allocation of memory for the covar matrix */
 
    F = gsl_cdf_gaussian_P(d-m, s) ;
    f = gsl_ran_gaussian_pdf(d-m, s) ;
   
    /* Compute ll partial derivatives. */
    dldm    = (1./pow(s,2.))*sum-(N-k)*f/F;
    d2ldm2  = (-k/pow(s,2.))-(N-k)*(f/F)*(((data[min]-m)/pow(s,2.))+f/F);
    d2ldmds = (-2./pow(s,3.))*sum-(N-k)*(f/F)*(1./s)*(-1.+(data[min]-m)*(f/F)+pow((data[min]-m)/s,2.));
    dlds = -(k/s)+(1./pow(s,3.))*sum2-(N-k)*(f/F)*((data[min]-m)/s);
    d2lds2 = (k/pow(s,2.0))-(3./pow(s,4.))*sum2-(N-k)*((data[min]-m)/(F*s))*(-(f/s)+(f/pow(s,3.))*pow(data[min]-m,2.))-(N-k)*pow(f/F,2.)*pow((data[min]-m)/s,2.)+(N-k)*(f/F)*((data[min]-m)/pow(s,2.));

    break;
  case 2:
    {
      F = gsl_cdf_gaussian_P(data[max]-m,s);
      f = gsl_ran_gaussian_pdf(data[max]-m,s);
      const double dFdm = - f;
      const double dFds = - f*(data[max]-m)/s;
      const double dfdm =  f*((data[max]-m)/pow(s,2.0));
      const double dfds = (f/s)*(pow((data[max]-m)/s,2.0)-1);
    
      dldm    = (1./pow(s,2.0))*sum+(N-k)*f/(1-F);
      d2ldm2  = -k/pow(s,2.0)+(N-k)*dfdm*(1/(1-F))+(N-k)*(f/pow(1-F,2.0))*dFdm;
      d2ldmds = -(2./pow(s,3.0))*sum+(N-k)*(dfds/(1.-F))+(N-k)*(f/pow(1.-F,2.0))*dFds;
      dlds    = -k/s+sum2/pow(s,3.)+(N-k)*(f/(1.-F))*((data[max]-m)/s);
      d2lds2  = k/pow(s,2.)-(3./pow(s,4.))*sum2+(N-k)*(dfds/(1.-F))*((data[max]-m)/s)+(N-k)*(f/pow(1.-F,2.))*dFds*((data[max]-m)/s)-(N-k)*(f/(1.-F))*((data[max]-m)/pow(s,2.));
      
      break;
    }
  case 3:
    {
      F = gsl_cdf_gaussian_P(d-m,s);
      f = gsl_ran_gaussian_pdf(d-m,s);
      const double dFdm = - f;
      const double dFds = - f*(d-m)/s;
      const double dfdm =  f*((d-m)/pow(s,2.0));
      const double dfds = (f/s)*(pow((d-m)/s,2.0)-1);
      
      dldm    = (1./pow(s,2.0))*sum+(N-k)*f/(1-F);
      d2ldm2  = -k/pow(s,2.0)+(N-k)*dfdm*(1/(1-F))+(N-k)*(f/pow(1-F,2.0))*dFdm;
      d2ldmds = -(2./pow(s,3.0))*sum+(N-k)*(dfds/(1.-F))+(N-k)*(f/pow(1.-F,2.0))*dFds;
      dlds    = -k/s+sum2/pow(s,3.)+(N-k)*(f/(1.-F))*((d-m)/s);
      d2lds2  = k/pow(s,2.)-(3./pow(s,4.))*sum2+(N-k)*(dfds/(1.-F))*((d-m)/s)+(N-k)*(f/pow(1.-F,2.))*dFds*((d-m)/s)-(N-k)*(f/(1.-F))*((d-m)/pow(s,2.));
      
    }
    break;
  }
  
  gsl_permutation * P = gsl_permutation_alloc (2);
  int signum;
  gsl_matrix *J = gsl_matrix_calloc (2,2);
  gsl_matrix *H = gsl_matrix_calloc (2,2);
  gsl_matrix *tmp = gsl_matrix_calloc (2,2);
  
  switch(o_varcovar){
  case 0:   
    
    gsl_matrix_set (J,0,0,dldm*dldm);
    gsl_matrix_set (J,0,1,dldm*dlds);
    gsl_matrix_set (J,1,0,dlds*dldm);
    gsl_matrix_set (J,1,1,dlds*dlds);
    gsl_linalg_LU_decomp (J,P,&signum);
    gsl_linalg_LU_invert (J,P,covar);
    
    break;
  case 1:
 
    gsl_matrix_set(H,0,0,d2ldm2);
    gsl_matrix_set(H,0,1,d2ldmds);
    gsl_matrix_set(H,1,0,d2ldmds);
    gsl_matrix_set(H,1,1,d2lds2);
    gsl_matrix_scale (H,-1.);
    gsl_linalg_LU_decomp (H,P,&signum);
    gsl_linalg_LU_invert (H,P,covar);
  
    break;
  case 2:
    
    gsl_matrix_set(H,0,0,d2ldm2);
    gsl_matrix_set(H,0,1,d2ldmds);
    gsl_matrix_set(H,1,0,d2ldmds);
    gsl_matrix_set(H,1,1,d2lds2);
    gsl_matrix_set(J,0,0,dldm*dldm);
    gsl_matrix_set(J,0,1,dldm*dlds);
    gsl_matrix_set(J,1,0,dlds*dldm);
    gsl_matrix_set(J,1,1,dlds*dlds);    
    gsl_linalg_LU_decomp (H,P,&signum);
    gsl_linalg_LU_invert (H,P,tmp);

    gsl_blas_dgemm (CblasNoTrans,
		    CblasNoTrans,1.0,tmp,J,0.0,H);
    gsl_blas_dgemm (CblasNoTrans,
		    CblasNoTrans,1.0,H,tmp,0.0,covar);
    
    break;
  }
  
  gsl_matrix_free(H);
  gsl_matrix_free(J);
  gsl_matrix_free(tmp);
  gsl_permutation_free(P);
  
  return covar;
}
