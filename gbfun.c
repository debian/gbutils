/*
  gbfun (ver. 5.6) -- Apply functions to table of data
  Copyright (C) 2008-2018 Giulio Bottazzi

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  (version 2) as published by the Free Software Foundation;
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/


#include "tools.h"

#if defined HAVE_LIBGSL
#include "matheval.h"
#include "assert.h"
#endif


/* handy structure used in computation */
typedef struct function {
  void *f;
  size_t argnum;
/*   char *args[] = { "x" }; */
  char **args;			/* list of arguments names */
  size_t *indeces;	        /* the index (shift) for each variable */
  size_t *lags;	                /* the lag for each variable */
  double *values;		/* values used in computation */
  size_t maxindex;		/* the largest index */
  size_t maxlag;		/* the largest lag */
} Function;


void data_transpose(double *** vals,size_t *rows,size_t *columns){

  size_t i,j;

  /* allocate space */
  double **newvals = (double **) my_alloc((*rows)*sizeof(double *));
  for(i=0;i<*rows;i++){
    newvals[i] = (double *) my_alloc((*columns)*sizeof(double));
  }

  /* fill space */
  for(i=0;i<*rows;i++)
    for(j=0;j<*columns;j++)
      newvals[i][j] = (*vals)[j][i];

  /* deallocate old space */
  if(*columns>0)
    for(i=0;i<*columns;i++)
      free((*vals)[i]);
  free(*vals);
  *vals = newvals;

  /* swap axes */
  j=*columns;
  *columns=*rows;
  *rows=j;

}


int main(int argc,char* argv[]){

  size_t rows=0,columns=0;
  double **data=NULL;
  
  char *splitstring = strdup(" \t");

  Function *F=NULL;
  size_t Fnum=0;

  /* function spec */
  size_t spec;


  /* OPTIONS */
  int o_table=0;
  int o_transpose=0;
  int o_recursive=0;
  int o_recursive_printall=0;
  int o_verbose=0;
  int o_binary_in=0;
  double recursive_start=0;
  
  
  /* variables for reading command line options */
  /* ------------------------------------------ */
  int opt;
  /* ------------------------------------------ */
  
  /* START COMMAND LINE PROCESSING */
  while((opt=getopt_long(argc,argv,"htvr:R:F:o:s:b:T",gb_long_options, &gb_option_index))!=EOF){
    if(opt==0){
      gbutils_header(argv[0],stdout);
      exit(0);
    }
    else if(opt=='?'){
      fprintf(stderr,"option %c not recognized\n",optopt);
      exit(-1);
    }
    else if(opt=='h'){
      /*print help*/
      fprintf(stdout,"Compute specified functions on data read from standard input. The default\n");
      fprintf(stdout,"is to perform the computation column-wise, on each row of data separately.\n");
      fprintf(stdout,"Variable 'xi' stands for the i-th column while 'x0' stands for the row\n");
      fprintf(stdout,"number, e.g. a function f(x1,x2) operates on the first and secod column.\n");
      fprintf(stdout,"With the option -t the function is computed, in turn, on every column. In\n");
      fprintf(stdout,"this case f(x1,x2) stands for a function of the column itself and of the \n");
      fprintf(stdout,"following column (the index being a lead operator). In these cases 'x' is\n");
      fprintf(stdout,"equivalent to 'x1'. With -r or -R the function is recursevely computed\n");
      fprintf(stdout,"\"columwise\" on each row. In this case the variable 'x' identifies the\n");
      fprintf(stdout,"result of the previous evaluation. A lag operator can be specified with the\n");
      fprintf(stdout,"letter l, like in 'x1l2', which means the first column two steps before.\n");
      fprintf(stdout,"More functions can be specified and will be considered in turn.\n");
      fprintf(stdout,"\nUsage: %s [options] <function definition> ...\n\n",argv[0]);
      fprintf(stdout,"Options:\n");
      fprintf(stdout," -T  compute the function row-wise, on each column separately.\n");
      fprintf(stdout," -t  compute on each column\n");
      fprintf(stdout," -r  set initial value and compute recursively\n");
      fprintf(stdout," -R  set initial value, compute recursively and print intermediary results\n");
      fprintf(stdout," -v  verbose mode\n");
      fprintf(stdout," -F  specify the input fields separators (default \" \\t\")\n");
      fprintf(stdout," -o  set the output format (default '%%12.6e')  \n");
      fprintf(stdout," -s  set the output separation string (default ' ')  \n");
      fprintf(stdout," -h  this help\n");
      fprintf(stdout,"Examples:\n");
      fprintf(stdout," gbfun 'x0+log(x2)' < file  print the log of the second column of 'file'\n");
      fprintf(stdout,"                            adding the progressive number of the row \n");
      fprintf(stdout," gbfun -T 'x0+log(x2)' < file  print the log of the second row of 'file'\n");
      fprintf(stdout,"                               adding the progressive number of the column \n");
      fprintf(stdout," gbfun -r 0 'x+sqrt(x1)' < file  print the sum of the square root of the\n");
      fprintf(stdout,"                                 elements of the first column of 'file'\n");
      exit(0);
    }
    else if(opt=='F'){
      /*set the fields separator string*/
      free(splitstring);
      splitstring = strdup(optarg);
    }
    else if(opt=='b'){
      /*set binary input or output*/

      switch(atoi(optarg)){
      case 0 :
	break;
      case 1 :
	o_binary_in=1;
	break;
      case 2 :
	PRINTMATRIXBYCOLS=printmatrixbycols_bin;
	break;
      case 3 :
	o_binary_in=1;
	PRINTMATRIXBYCOLS=printmatrixbycols_bin;
	break;
      default:
	fprintf(stderr,"unclear binary specification %d, option ignored.\n",atoi(optarg));
	break;
      }
    }
    else if(opt=='o'){
      /*set the ouptustring */
      FLOAT = strdup(optarg);
    }
    else if(opt=='s'){
      /*set the separator*/
      SEP = strdup(optarg);
    }
    else if(opt=='t'){
      o_table=1;
    }
    else if(opt=='T'){
      o_transpose=1;
    }
    else if(opt=='r'){
      o_recursive=1;
      recursive_start=mystrtod(optarg);
    }
    else if(opt=='R'){
      o_recursive_printall=1;
      recursive_start=mystrtod(optarg);
    }
    else if(opt=='v'){
      o_verbose=1;
    }
  }
  /* END OF COMMAND LINE PROCESSING */

  /* initialize global variables */
  initialize_program(argv[0]);

  /* load data */
  if(o_binary_in==1)
    loadtable_bin(&data,&rows,&columns,0);
  else{
    loadtable(&data,&rows,&columns,0,splitstring);
    free(splitstring);
  }

  /* transpose if required */
  if(o_transpose == 1) {
    data_transpose(&data,&rows,&columns);
  }
  
  /* parse specs for functions specification */
  for(spec=(size_t) optind;spec< (size_t) argc;spec++){
    char *piece=strdup (argv[spec]);
    char *stmp1=piece;
    char *stmp2;

    size_t j;
    
    while( (stmp2=strsep (&stmp1,",")) != NULL ){/*take a piece*/
      char *stmp3 = strdup(stmp2);
      /* 	fprintf(stderr,"token:%s\n",stmp3); */
      
      /* allocate new function */
      Fnum++;
      F=(Function *) my_realloc((void *) F,Fnum*sizeof(Function));
      
      /* generate the formal expression */
      F[Fnum-1].f = evaluator_create (stmp3);
      assert(F[Fnum-1].f);
      free(stmp3);
      
      /* retrive list of argument */
      {
	/* hack necessary due to libmatheval propotypes */
	int argnum;
	evaluator_get_variables (F[Fnum-1].f, &(F[Fnum-1].args), &argnum);
	F[Fnum-1].argnum = (size_t) argnum;
      }
      /* compute list of indeces */
      F[Fnum-1].indeces = (size_t *) my_alloc(F[Fnum-1].argnum*sizeof(size_t));
      F[Fnum-1].lags = (size_t *) my_alloc(F[Fnum-1].argnum*sizeof(size_t));
      F[Fnum-1].maxindex=0;
      F[Fnum-1].maxlag=0;
      for(j=0;j<F[Fnum-1].argnum;j++){
	stmp3 = (F[Fnum-1].args)[j];

	if(*stmp3 != 'x'){
	  fprintf(stderr,"ERROR (%s): variable name %s should begin with x\n",
		  GB_PROGNAME,stmp3);
	  exit(-1);
	}

	if( *(stmp3+1) == '\0' ){
	  (F[Fnum-1].indeces)[j] = 1;
	  (F[Fnum-1].lags)[j] = 0;

	}
	else {
	  char *endptr;

	  if( strchr(stmp3,'l') == NULL ){
	  
	    (F[Fnum-1].indeces)[j] = strtol(stmp3+1,&endptr,10);
	    (F[Fnum-1].lags)[j] = 0;
	    
	    if(*endptr != '\0'){
	      fprintf(stderr,"ERROR (%s): variable name %s must be of the form x#l#\n",
		    GB_PROGNAME,stmp3);
	      exit(-1);
	    }

	  }
	  else{
	    (F[Fnum-1].indeces)[j] = strtol(stmp3+1,&endptr,10);
	    (F[Fnum-1].lags)[j] = strtol(endptr+1,&endptr,10);

	    if(*endptr != '\0'){
	      fprintf(stderr,"ERROR (%s): variable name %s must be of the form x#l#\n",
		    GB_PROGNAME,stmp3);
	      exit(-1);
	    }

	  }

	}

	/* possibly update the value of the maximum index */
	if( (F[Fnum-1].indeces)[j] > F[Fnum-1].maxindex)
	  F[Fnum-1].maxindex = (F[Fnum-1].indeces)[j];

	/* possibly update the value of the maximum lag */
	if( (F[Fnum-1].lags)[j] > F[Fnum-1].maxlag)
	  F[Fnum-1].maxlag = (F[Fnum-1].lags)[j];

	/* check index  */
	if((F[Fnum-1].indeces)[j]>columns){
	  fprintf(stderr,"ERROR (%s): column %zd not present in data\n",
		  GB_PROGNAME,(F[Fnum-1].indeces)[j]);
	  exit(-1);
	}

	/* check lag */
	if((F[Fnum-1].lags)[j]>rows){
	  fprintf(stderr,"ERROR (%s): lag %zd is longer then the data length\n",
		  GB_PROGNAME,(F[Fnum-1].lags)[j]);
	  exit(-1);
	}

      }

      /* allocate list of values */
      F[Fnum-1].values = (double *) my_alloc(F[Fnum-1].argnum*sizeof(double));

    }
    free(piece);
  }
  
  /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
  if(o_verbose==1){  /* check the builded functions */

    size_t i,j;
    
    for(i=0;i<Fnum;i++){
      fprintf (stderr,"f(x) = %s\t",evaluator_get_string (F[i].f));
      fprintf (stderr,"variables: ");
      for(j=0;j<F[i].argnum;j++){
	fprintf (stderr,"%s ",F[i].args[j]);
      }
      fprintf (stderr," ");
      fprintf (stderr,"indeces: ");
      for(j=0;j<F[i].argnum;j++){
	fprintf (stderr,"%zd ",F[i].indeces[j]);
      }
      fprintf (stderr,"lags: ");
      for(j=0;j<F[i].argnum;j++){
	fprintf (stderr,"%zd ",F[i].lags[j]);
      }
      fprintf (stderr,"\tmax index: %zd\n",F[i].maxindex);
      fprintf (stderr," max lag: %zd\n",F[i].maxlag);
    }
  }
  /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
  
  /* output */
  if(o_table==1){/*columns by columns value*/
    size_t i,j,h,c;
    size_t largest=1;
    size_t firstrow=0;

    double **out;

    /* compute the first row to print */
    for(j=0;j<Fnum;j++)
      if(F[j].maxlag>firstrow) firstrow=F[j].maxlag;


    /* compute the largest column shift */
    for(j=0;j<Fnum;j++)
      if(F[j].maxindex>largest) largest=F[j].maxindex;

    /* allocate output */
    out = (double **) my_alloc((columns-largest+1)*Fnum*sizeof(double *));
    for(i=0;i<(columns-largest+1)*Fnum;i++)
      out[i] = (double *) my_alloc((rows-firstrow)*sizeof(double));

    /* compute output */
    for(i=firstrow;i<rows;i++)
      for(c=0;c<columns-largest+1;c++)
	for(j=0;j<Fnum;j++){
	  for(h=0;h<F[j].argnum;h++)
	    (F[j].values)[h] = ( F[j].indeces[h]> 0 ? data[c+F[j].indeces[h]-1][i-F[j].lags[h]] : i+1);
	  out[c*Fnum+j][i-firstrow]=evaluator_evaluate (F[j].f,F[j].argnum,F[j].args,F[j].values);
	}

    /* print output */
    if(o_transpose == 0)
      PRINTMATRIXBYCOLS(stdout,out,rows-firstrow,(columns-largest+1)*Fnum);
    else if (o_transpose == 1)
      PRINTMATRIXBYROWS(stdout,out,(columns-largest+1)*Fnum,rows-firstrow);
    
    for(i=0;i<(columns-largest+1)*Fnum;i++)
      free(out[i]);
    free(out);

  }
  else if(o_recursive_printall==1){/*recursive evaluation with intermediary results*/
    size_t i,j,h,c;
    size_t firstrow=0;
    double **out;
    size_t cols,col;

    /* compute the first row to print */
    for(j=0;j<Fnum;j++)
      if(F[j].maxlag>firstrow) firstrow=F[j].maxlag;

    /* allocate output */
    cols=0;
    for(j=0;j<Fnum;j++)
      cols += columns-F[j].maxindex+1;
    out = (double **) my_alloc(cols*sizeof(double *));
    for(i=0;i<cols;i++)
      out[i] = (double *) my_alloc((rows-firstrow)*sizeof(double));

    /* compute output */
    for(i=firstrow;i<rows;i++){
      col=0;
      for(j=0;j<Fnum;j++){
	double recursive=recursive_start;	
	for(c=0;c<=columns-F[j].maxindex;c++){
	  for(h=0;h<F[j].argnum;h++) {
	    if(!strcmp((F[j].args)[h],"x"))
	      (F[j].values)[h]=recursive;
	    else if (F[j].indeces[h] == 0)
	      (F[j].values)[h]=i+1;
	    else 
	      (F[j].values)[h] = data[c+F[j].indeces[h]-1][i-F[j].lags[h]];
	  }
	  recursive = evaluator_evaluate (F[j].f,F[j].argnum,F[j].args,F[j].values);
	  out[col][i-firstrow] = recursive;
	  col++;
	}
      }
    }

    /* print output */
    if(o_transpose == 0)
      PRINTMATRIXBYCOLS(stdout,out,rows-firstrow,cols);
    else if (o_transpose == 1)
      PRINTMATRIXBYROWS(stdout,out,cols,rows-firstrow);

    for(i=0;i<cols;i++)
      free(out[i]);
    free(out);

  }
  else if(o_recursive==1){/*recursive evaluation*/
    size_t i,j,h,c;
    size_t firstrow=0;
    double **out;

    /* compute the first row to print */
    for(j=0;j<Fnum;j++)
      if(F[j].maxlag>firstrow) firstrow=F[j].maxlag;

    /* allocate output */
    out = (double **) my_alloc(Fnum*sizeof(double *));
    for(i=0;i<Fnum;i++)
      out[i] = (double *) my_alloc((rows-firstrow)*sizeof(double));

    /* compute output */
    for(i=firstrow;i<rows;i++)
      for(j=0;j<Fnum;j++){
	double recursive=recursive_start;	
	for(c=0;c<=columns-F[j].maxindex;c++){
	  for(h=0;h<F[j].argnum;h++) {
	    if(!strcmp((F[j].args)[h],"x"))
	      (F[j].values)[h]=recursive;
	    else if (F[j].indeces[h] == 0)
	      (F[j].values)[h]=i+1;
	    else 
	      (F[j].values)[h] = data[c+F[j].indeces[h]-1][i-F[j].lags[h]];
	  }
	  recursive = evaluator_evaluate (F[j].f,F[j].argnum,F[j].args,F[j].values);
	}
	out[j][i-firstrow]=recursive;
      }

    /* print output */
    if(o_transpose == 0)
      PRINTMATRIXBYCOLS(stdout,out,rows-firstrow,Fnum);   
    else if (o_transpose == 1)
      PRINTMATRIXBYROWS(stdout,out,Fnum,rows-firstrow);

    for(i=0;i<Fnum;i++)
      free(out[i]);
    free(out);

  }
  else{
    size_t i,j,h;
    /*       double **output = (double **) my_alloc(Fnum*sizeof(double *));       */
    /*       for(i=0;i<Fnum;i++) output[i] = (double *) my_alloc(rows*sizeof(double)); */
    
    size_t firstrow=0;
    double **out;

    /* compute the first row to print */
    for(j=0;j<Fnum;j++)
      if(F[j].maxlag>firstrow) firstrow=F[j].maxlag;

    /* allocate output */
    out = (double **) my_alloc(Fnum*sizeof(double *));
    for(i=0;i<Fnum;i++)
      out[i] = (double *) my_alloc((rows-firstrow)*sizeof(double));

    /* compute output */
    for(i=firstrow;i<rows;i++)
      for(j=0;j<Fnum;j++){
	for(h=0;h<F[j].argnum;h++)
	  (F[j].values)[h] = (F[j].indeces[h]>0 ?  data[F[j].indeces[h]-1][i-F[j].lags[h]] : i+1);
	out[j][i-firstrow] = evaluator_evaluate (F[j].f,F[j].argnum,F[j].args,F[j].values);
      }
    
    /* print output */
    if(o_transpose == 0)
      PRINTMATRIXBYCOLS(stdout,out,rows-firstrow,Fnum);
    else if (o_transpose == 1)
      PRINTMATRIXBYROWS(stdout,out,Fnum,rows-firstrow);

    for(i=0;i<Fnum;i++)
      free(out[i]);
    free(out);

  }
  
  /* free allocated space */
  {
    size_t i;

    for(i=0;i<columns;i++)
      free(data[i]);
    free(data);

    for(i=0;i<Fnum;i++){
      evaluator_destroy(F[i].f);      
      free(F[i].indeces);
      free(F[i].lags);
      free(F[i].values);
    }

  }

  exit(0);

}
