/*
  gbhisto2d (ver. 5.6) -- Produce 2D histogram from data
  Copyright (C) 1998-2018 Giulio Bottazzi

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  (version 2) as published by the Free Software Foundation;
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#include "tools.h"

int main(int argc,char* argv[]){

  size_t xsteps=10;
  size_t ysteps=10;
  int o_output=0;/*output type*/
  
  char *splitstring = strdup(" \t");

  int o_verbose=0;
  
  double **data=NULL;
  size_t size=0;

  /* COMMAND LINE PROCESSING */
    
  /* variables for reading command line options */
  /* ------------------------------------------ */
  int opt;
  /* ------------------------------------------ */
    

  /* initialize global variables */
  initialize_program(argv[0]);
    
  /* read the command line */    
  while((opt=getopt_long(argc,argv,"n:M:vhF:",gb_long_options, &gb_option_index))!=EOF){
    if(opt==0){
      gbutils_header(argv[0],stdout);
      exit(0);
    }
    else if(opt=='?'){
      fprintf(stderr,"option %c not recognized\n",optopt);
      exit(-1);
    }
    else if(opt=='n'){
      /*the number of bins*/
      if(!strchr (optarg,',')){
	xsteps=ysteps=atoi(optarg);
      }
      else{
	char *stmp1=strdup (optarg);
	xsteps = atoi(strtok (stmp1,","));
	ysteps = atoi(strtok (NULL,","));
	free(stmp1);
      }
    }
    else if(opt=='M'){
      /*set the type of output*/
      o_output = atoi(optarg);

      if(o_output<0 || o_output>8){
	fprintf(stderr,"ERROR (%s): output option '%d' not recognized. Try option -h.\n",
		GB_PROGNAME,o_output);
	exit(-1);
      }
    }
    else if(opt=='v'){
      /*set verbose output*/
      o_verbose=1;
    }
    else if(opt=='F'){
      /*set the fields separator string*/
      free(splitstring);
      splitstring = strdup(optarg);
    }
    else if(opt=='h'){
      /*print short help*/

      fprintf(stdout,"2D histogram on a regular grid. Data are read from standard input as couples \n");
      fprintf(stdout,"(X,Y). If data are treated as continuous, equispaced bins are built and their \n");
      fprintf(stdout,"center coordinates together with the required statistics are printed. For\n");
      fprintf(stdout,"discrete variables, bins are built directly from the given variables. Transition\n");
      fprintf(stdout,"matrices (option -M 5 and -M 8) have final states across the rows. In case of\n");
      fprintf(stdout,"continuous variables it is also possible to consider rectangular matrices.\n");
      fprintf(stdout,"\nUsage: %s [options]\n\n",argv[0]);
      fprintf(stdout,"Options :\n");
      fprintf(stdout," -M  choose the statistics to print for each bin (default 0)\n");
      fprintf(stdout,"      0  occurrences number for continuous binned variables (*)\n");
      fprintf(stdout,"      1  relative frequency for continuous binned variables (*)\n");
      fprintf(stdout,"      2  empirical density  for continuous binned variables (*)\n");
      fprintf(stdout,"      3  occurrences number for discrete symmetric variables (bins{X}=bins{Y})\n");
      fprintf(stdout,"      4  relative frequency for discrete symmetric variables (bins{X}=bins{Y})\n");
      fprintf(stdout,"      5  transition matrix  (X=>Y) for discrete symmetric variables (bins{X}=bins{Y})\n");
      fprintf(stdout,"      6  occurrences number for discrete asymmetric variables\n");
      fprintf(stdout,"      7  relative frequency for discrete asymmetric variables\n");
      fprintf(stdout,"      8  transition matrix  (X=>Y) for continuous binned variables (*)\n");
      fprintf(stdout," -n  number of equispaced bins where the histogram is computed. For output marked\n");
      fprintf(stdout,"     with (*),  use comma ',' to specify different values for x and y coordinates.\n");
      fprintf(stdout,"     (default 10) \n");
      fprintf(stdout," -v  verbose mode\n");
      fprintf(stdout," -F  specify the input fields separators (default \" \\t\")\n");
      fprintf(stdout," -h  this help\n");
      fprintf(stdout,"Examples:\n");
      fprintf(stdout," gbhisto2d -n 20 < file  compute a 2D histrogram using 20x20 bins.\n");
      fprintf(stdout," gbhisto2d -M 8 -n 4 < file  compute transition matrix on equispaced bins for\n");
      fprintf(stdout,"                             continuous variables.\n");
      exit(0);
    }
  }
  /* END OF COMMAND LINE PROCESSING */

  /* initialize global variables */
  initialize_program(argv[0]);

  /* load the data */
  load2(&data,&size,0,splitstring);

  denan_pairs(&(data[0]),&(data[1]),&size);
  
  /* ++++++++++++++++++++++++++++ */
  if(o_verbose){
    double xmean,xvar,xsd,xskew,xkurt,xadev,xmin,xmax;

    fprintf(stdout,"#output type:\t");
    switch(o_output){
    case 0:
      fprintf(stdout,"absolute frequency");
      break;
    case 1:
      fprintf(stdout,"relative frequency");
      break;
    case 2:
      fprintf(stdout,"empirical density");
      break;
    case 3:
      fprintf(stdout,"occurencies number");
      break;
    case 4:
      fprintf(stdout,"relative frequency");
      break;
    case 5:
      fprintf(stdout,"transition matrix");
      break;
    case 6:
      fprintf(stdout,"occurencies number (asym)");
      break;
    case 7:
      fprintf(stdout,"relative frequency (asym)");
      break;
    }
    fprintf(stdout,"\n");

    fprintf(stdout,"#data statistics:\n");
    fprintf(stdout,
	    "#  mean      stdev     skewness  kurtosis  ave.dev.  min       max       obs.\n");
    moment(data[0],size,&xmean,&xadev,&xsd,&xvar,&xskew,&xkurt,&xmin,&xmax);
    fprintf(stdout,"#X %+.2e %+.2e %+.2e %+.2e %+.2e %+.2e %+.2e %zd\n",
	    xmean,xsd,xskew,xkurt,xadev,xmin,xmax,size);
    moment(data[1],size,&xmean,&xadev,&xsd,&xvar,&xskew,&xkurt,&xmin,&xmax);
    fprintf(stdout,"#Y %+.2e %+.2e %+.2e %+.2e %+.2e %+.2e %+.2e %zd\n",
	    xmean,xsd,xskew,xkurt,xadev,xmin,xmax,size);

  }
  /* ++++++++++++++++++++++++++++ */

  /* continuous variables */
  if(o_output < 3 || o_output==8){

    size_t steps=xsteps*ysteps;
    
    size_t i,j;
    double xstepsize,ystepsize;
    int *histo;
    
    double xmin = DBL_MAX;  /*set maximum double */
    double xmax = -DBL_MAX;  /*set to minimum double */
    double ymin = DBL_MAX;  /*set maximum double */
    double ymax = -DBL_MAX;  /*set to minimum double */

    /* ++++++++++++++++++++++++++++ */
    if(o_verbose){
      fprintf(stdout,"#x bins number:\t%zd\n",xsteps);
      fprintf(stdout,"#y bins number:\t%zd\n",ysteps);
    }    
    /* ++++++++++++++++++++++++++++ */
    
    /* determination of xmax and xmin */
    for(i=0;i<size;i++){
      const double dtmp0 = data[0][i];
      const double dtmp1 = data[1][i];
      
      if( xmin > dtmp0) xmin = dtmp0;
      else if( xmax < dtmp0) xmax = dtmp0;
      
      if( ymin > dtmp1) ymin = dtmp1;
      else if( ymax < dtmp1) ymax = dtmp1;
    }
    /* ------------------------------ */
    
    histo = (int *) malloc(steps*sizeof(int));
    for(i=0;i<steps;i++){
      histo[i]=0;
    }
    
    xstepsize=(xmax-xmin)/xsteps;
    ystepsize=(ymax-ymin)/ysteps;
    
    for(i=0;i<size;i++){
      const double xval = data[0][i];
      const double yval = data[1][i];
      int xindex = (xval<xmax ? (int) floor((xval-xmin)/xstepsize) : (int) xsteps-1);
      int yindex = (yval<ymax ? (int) floor((yval-ymin)/ystepsize) : (int) ysteps-1);
      
      xindex = (xindex<0?0:xindex);
      yindex = (yindex<0?0:yindex);
      histo[xindex*xsteps+yindex]++;
    }

    if(o_output==8){
      for(i=0;i<xsteps;i++){
	double marginal=0.0;
	for(j=0;j<ysteps;j++)
	  marginal += histo[i*xsteps+j];
	if(marginal != 0.0 )
	  for(j=0;j<ysteps;j++)
	    printf(FLOAT_SEP,(double) histo[i*xsteps+j]/marginal);
	else
	  for(j=0;j<ysteps;j++)
	    printf(EMPTY_SEP,"NaN");
	printf("\n");
      }
    }
    else if(o_output==2){  
      for(i=0;i<xsteps;i++){
	for(j=0;j<ysteps;j++){
	  printf(FLOAT_SEP,xmin+xstepsize*(i+.5));
	  printf(FLOAT_SEP,ymin+ystepsize*(j+.5));
	  printf(FLOAT_NL,(double) histo[i*xsteps+j]/(size*xstepsize*ystepsize));
	}
	printf("\n");
      }
    }
    else if(o_output==1){
      for(i=0;i<xsteps;i++){
	for(j=0;j<ysteps;j++){
	  printf(FLOAT_SEP,xmin+xstepsize*(i+.5));
	  printf(FLOAT_SEP,ymin+ystepsize*(j+.5));
	  printf(FLOAT_NL,(double) histo[i*xsteps+j]/size);
	}
	printf("\n");
      }
    }
    else {
      for(i=0;i<xsteps;i++){
	for(j=0;j<ysteps;j++){
	  printf(FLOAT_SEP,xmin+xstepsize*(i+.5));
	  printf(FLOAT_SEP,ymin+ystepsize*(j+.5));
	  printf(INT_NL, histo[i*xsteps+j]);
	}
	printf("\n");
      }
    }
  }
  /* discrete symmetric variables */
  else if(o_output < 6){

    size_t i,j;
    
    double *labels=NULL;
    size_t labelsnum=0;
    
    double **histo;

    /* define the (ordered) list of labels */
    {

      /* merge initial and final state */
      double *merged = (double *) my_alloc(2*size*sizeof(double));
      memcpy(merged,data[0],size*sizeof(double));
      memcpy(merged+size,data[1],size*sizeof(double));
      
      qsort(merged,2*size,sizeof(double),sort_by_value);
      
      labelsnum=1;
      labels=my_realloc(labels,labelsnum*sizeof(double));
      labels[labelsnum-1]= merged[0];
      for(i=1;i<2*size;i++){
	if (merged[i] != merged[i-1]){
	  labelsnum++;
	  labels=my_realloc(labels,labelsnum*sizeof(double));
	  labels[labelsnum-1]= merged[i];
	}
      }
      free(merged);
    }

    /* ++++++++++++++++++++++++++++ */
    if(o_verbose){
      fprintf(stdout,"#labels:\n");
      fprintf(stdout,"#");
      for(i=0;i<labelsnum;i++)
	fprintf(stdout," %g",labels[i]);
      fprintf(stdout,"\n");
    }    
    /* ++++++++++++++++++++++++++++ */


    histo = (double **) my_alloc(labelsnum*sizeof(double *));
    for(i=0;i<labelsnum;i++)
      histo[i] = (double *) my_calloc(labelsnum,sizeof(double));
    
    
    for(i=0;i<size;i++){

      double *xvalue, *yvalue;
      size_t xindex,yindex;

      xvalue = bsearch (&(data[0][i]),labels,labelsnum,
			sizeof(double),sort_by_value);

      yvalue = bsearch (&(data[1][i]),labels,labelsnum,
			sizeof(double),sort_by_value);

      if(xvalue && yvalue){
	xindex= xvalue-labels;
	yindex= yvalue-labels;
      }
      else{
	fprintf(stderr,"ERROR (%s): problem in finding indeces. Possible bug.\n",GB_PROGNAME);
	exit(-1);
      }

      histo[xindex][yindex] ++;
    }

    switch(o_output){
    case 3:
      for(i=0;i<labelsnum;i++){
	for(j=0;j<labelsnum;j++)
	  printf(" %4d",(int) histo[i][j]);
	printf("\n");
      }
      break;
    case 4:
      for(i=0;i<labelsnum;i++){
	for(j=0;j<labelsnum;j++)
	  printf(" %8.4g",histo[i][j]/size);
	printf("\n");
      }
      break;
    case 5:
      for(i=0;i<labelsnum;i++){
	double marginal=0.0;
	for(j=0;j<labelsnum;j++)
	  marginal += histo[i][j];
	if(marginal != 0.0 )
	  for(j=0;j<labelsnum;j++)
	    printf(" %8.4g",histo[i][j]/marginal);
	else
	  for(j=0;j<labelsnum;j++)
	    printf(" %8s","NaN");
	printf("\n");
      }
      break;
    }

  }
  /* discrete asymmetric variables */
  else if(o_output < 8){

    size_t i,j;
    
    double *xlabels=NULL;
    size_t xlabelsnum=0;
    
    double *ylabels=NULL;
    size_t ylabelsnum=0;

    double **histo;

    /* define the (ordered) list of x and y labels */
    {

      double *xsorted = (double *) my_alloc(size*sizeof(double));
      double *ysorted = (double *) my_alloc(size*sizeof(double));

      memcpy(xsorted,data[0],size*sizeof(double));
      memcpy(ysorted,data[1],size*sizeof(double));
      
      qsort(xsorted,size,sizeof(double),sort_by_value);
      qsort(ysorted,size,sizeof(double),sort_by_value);

      xlabelsnum=1;
      xlabels=my_realloc(xlabels,xlabelsnum*sizeof(double));
      xlabels[xlabelsnum-1]= xsorted[0];
      for(i=1;i<size;i++){
	if (xsorted[i] != xsorted[i-1]){
	  xlabelsnum++;
	  xlabels=my_realloc(xlabels,xlabelsnum*sizeof(double));
	  xlabels[xlabelsnum-1]= xsorted[i];
	}
      }
      
      ylabelsnum=1;
      ylabels=my_realloc(ylabels,ylabelsnum*sizeof(double));
      ylabels[ylabelsnum-1]= ysorted[0];
      for(i=1;i<size;i++){
	if (ysorted[i] != ysorted[i-1]){
	  ylabelsnum++;
	  ylabels=my_realloc(ylabels,ylabelsnum*sizeof(double));
	  ylabels[ylabelsnum-1]= ysorted[i];
	}
      }

      free(xsorted);
      free(ysorted);
    }

    /* ++++++++++++++++++++++++++++ */
    if(o_verbose){
      fprintf(stdout,"#x-labels (rows):\n");
      fprintf(stdout,"#");
      for(i=0;i<xlabelsnum;i++)
	fprintf(stdout," %g",xlabels[i]);
      fprintf(stdout,"\n");
      fprintf(stdout,"#y-labels (columns):\n");
      fprintf(stdout,"#");
      for(i=0;i<ylabelsnum;i++)
	fprintf(stdout," %g",ylabels[i]);
      fprintf(stdout,"\n");
    }
    /* ++++++++++++++++++++++++++++ */


    histo = (double **) my_alloc(xlabelsnum*sizeof(double *));
    for(i=0;i<xlabelsnum;i++)
      histo[i] = (double *) my_calloc(ylabelsnum,sizeof(double));
    
    
    for(i=0;i<size;i++){

      double *xvalue, *yvalue;
      size_t xindex,yindex;

      xvalue = bsearch (&(data[0][i]),xlabels,xlabelsnum,
			sizeof(double),sort_by_value);

      yvalue = bsearch (&(data[1][i]),ylabels,ylabelsnum,
			sizeof(double),sort_by_value);

      if(xvalue && yvalue){
	xindex= xvalue-xlabels;
	yindex= yvalue-ylabels;
      }
      else{
	fprintf(stderr,"ERROR (%s): problem in finding indeces. Possible bug.\n",GB_PROGNAME);
	exit(-1);
      }

      histo[xindex][yindex] ++;
    }

    switch(o_output){
    case 6:
      for(i=0;i<xlabelsnum;i++){
	for(j=0;j<ylabelsnum;j++)
	  printf(" %4d",(int) histo[i][j]);
	printf("\n");
      }
      break;
    case 7:
      for(i=0;i<xlabelsnum;i++){
	for(j=0;j<ylabelsnum;j++)
	  printf(" %8.4g",histo[i][j]/size);
	printf("\n");
      }
      break;
    }

  }
  
  return 0;
}
