/*
  gbnlmult (ver. 5.6) -- Solve systems of non linear simultaneous equations 
  Copyright (C) 2007-2018 Giulio Bottazzi

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  (version 2) as published by the Free Software Foundation;
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#include "tools.h"
#include "matheval.h"
#include "assert.h"

#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_multifit_nlin.h>
#include <gsl/gsl_cdf.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_multimin.h>


/* handy structure used in computation */
typedef struct function {
  void *f;
  size_t argnum;
  char **argname;
  void **df;
  void ***ddf;
} Function;

typedef struct system {
  Function *fun;
  size_t fnum;
} System;


struct objdata {
  System * S;
  size_t rows;
  size_t columns;
  double **data;
};


/* Ordinary Least Squares estimation */
/* --------------------------------- */

int
ols_obj_f (const gsl_vector * x, void *params,
	   gsl_vector * f){

  const System *S = ((struct objdata *)params)->S;
  const size_t rows=((struct objdata *)params)->rows;
  const size_t columns=((struct objdata *)params)->columns;
  double **data=((struct objdata *)params)->data;
  const size_t pnum = x->size;
  double values[pnum+columns];
  size_t i,j,h;
  const size_t fnum =S->fnum;

  
  /* set the parameter values */
  for(i=columns;i<columns+pnum;i++)
    values[i] = gsl_vector_get(x,i-columns);

  for(h=0 ; h<fnum ;h++){
    Function F=(S->fun)[h];

    for(i=0;i<rows;i++){
      
      /* set the variables values */
      for(j=0;j<columns;j++)
	values[j] = data[j][i];
      
      gsl_vector_set (f, i+h*rows,
		      evaluator_evaluate (F.f,columns+pnum,F.argname,values));
    }
  }

  return GSL_SUCCESS;
}



int
ols_obj_df (const gsl_vector * x, void *params,
       gsl_matrix * J){

  const System *S = ((struct objdata *)params)->S;
  const size_t rows=((struct objdata *)params)->rows;
  const size_t columns=((struct objdata *)params)->columns;
  double **data=((struct objdata *)params)->data;
  const size_t pnum = x->size;
  double values[pnum+columns];
  size_t i,j,h;
  const size_t fnum =S->fnum;

  for(i=columns;i<columns+pnum;i++)
    values[i] = gsl_vector_get(x,i-columns);

  for(h=0 ; h<fnum ;h++){
    Function F=(S->fun)[h];

    for(i=0;i<rows;i++){
    
      for(j=0;j<columns;j++)
	values[j] = data[j][i];

      for(j=0;j<pnum;j++){
	gsl_matrix_set (J,i+h*rows, j,
			evaluator_evaluate ((F.df)[j],columns+pnum,F.argname,values));
      }
    }
  }

  return GSL_SUCCESS;
}



int
ols_obj_fdf (const gsl_vector * x, void *params,
	  gsl_vector * f, gsl_matrix * J){


  const System *S = ((struct objdata *)params)->S;
  const size_t rows=((struct objdata *)params)->rows;
  const size_t columns=((struct objdata *)params)->columns;
  double **data=((struct objdata *)params)->data;
  const size_t pnum = x->size;
  double values[pnum+columns];
  size_t i,j,h;
  const size_t fnum =S->fnum;

  for(i=columns;i<columns+pnum;i++)
    values[i] = gsl_vector_get(x,i-columns);

  for(h=0 ; h<S->fnum ;h++){
    Function F=(S->fun)[h];

    for(i=0;i<rows;i++){
    
      for(j=0;j<columns;j++)
	values[j] = data[j][i];
      
      gsl_vector_set (f, i+h*rows,
		    evaluator_evaluate (F.f,columns+pnum,F.argname,values));

/*     for(j=0;j<columns+pnum;j++) */
/*       printf("%s=%f ",F->argname[j],values[j]); */
/*     printf("-> %f\n",evaluator_evaluate (F->f,columns+pnum,F->argname,values)); */

      for(j=0;j<pnum;j++){
	gsl_matrix_set (J, i+h*fnum, j,evaluator_evaluate ((F.df)[j],columns+pnum,F.argname,values));
      }

    }
  }

  return GSL_SUCCESS;

}


/* compute variance-covariance matrix of fitted parameters */
gsl_matrix * ols_varcovar(const int o_varcovar,
			  gsl_multifit_fdfsolver *s,
			  const struct objdata obj,
			  const size_t Pnum){

  size_t i,j,h,k;

  const size_t rows = obj.rows;
  const size_t columns = obj.columns;
  double **data = obj.data;
  const System *S = obj.S;
  const double sumsq = pow(gsl_blas_dnrm2(s->f),2.);
  const double sigma2 = sumsq/(rows-Pnum);

  gsl_matrix *covar = gsl_matrix_alloc (Pnum,Pnum);

  switch(o_varcovar){
  case 0: /* inverse "reduced Hessian", as in Numerical Recipes */
    {

#ifdef GSL_VER_2
      gsl_matrix *J = gsl_matrix_alloc(rows, Pnum); 
      
      gsl_multifit_fdfsolver_jac(s, J); 
#else
      gsl_matrix *J = s->J;
#endif

      gsl_multifit_covar (J, 1e-6, covar);

      gsl_matrix_scale (covar,sigma2);

#ifdef GSL_VER_2
      gsl_matrix_free (J); 
#endif
      
    }
    break;
  case 1: /* J^{-1} */
    {
      
      gsl_permutation * P = gsl_permutation_alloc (Pnum);
      int signum;
      
      gsl_matrix *J = gsl_matrix_calloc (Pnum,Pnum);
      gsl_matrix *dJ = gsl_matrix_calloc (Pnum,Pnum);
      
      double f,df1,df2;
      
      double values[columns+Pnum];
      
      /* fill with optimal parameter */
      for(i=columns;i<columns+Pnum;i++)
	values[i] = gsl_vector_get(s->x,i-columns);
      
      /* compute information matrix */
      for(k=0 ; k<S->fnum ;k++){
	Function F=(S->fun)[k];

	for(i=0;i<rows;i++){
	
	  for(j=0;j<columns;j++)
	    values[j] = data[j][i];
	
	  f = evaluator_evaluate (F.f,columns+Pnum,F.argname,values);
	
	  for(j=0;j<Pnum;j++){
	    df1 = 
	      evaluator_evaluate ((F.df)[j],columns+Pnum,F.argname,values);
	    for(h=0;h<Pnum;h++){
	      df2 = 
		evaluator_evaluate ((F.df)[h],columns+Pnum,F.argname,values);
	      gsl_matrix_set (dJ,j,h,df1*df2*f*f);
	    }
	  }
	  gsl_matrix_add (J,dJ);
	}
      }
      
      /* invert information matrix; dJ store temporary LU decomp. */
      gsl_matrix_memcpy (dJ,J);
      gsl_linalg_LU_decomp (dJ,P,&signum);
      gsl_linalg_LU_invert (dJ,P,covar);
      
      
      gsl_matrix_free(J);
      gsl_matrix_free(dJ);
      gsl_permutation_free(P);

      gsl_matrix_scale (covar,sigma2*sigma2);
      
    }
    break;
    
  case 2: /* H^{-1} */
    {
      gsl_permutation * P = gsl_permutation_alloc (Pnum);
      int signum;
      
      gsl_matrix *H = gsl_matrix_calloc (Pnum,Pnum);
      gsl_matrix *dH = gsl_matrix_calloc (Pnum,Pnum);
      
      double f,df1,df2,ddf;

      double values[columns+Pnum];
      
      /* fill with optimal parameter */
      for(i=columns;i<columns+Pnum;i++)
	values[i] = gsl_vector_get(s->x,i-columns);
      
      /* compute Hessian */
      for(k=0 ; k<S->fnum ;k++){
	Function F=(S->fun)[k];

	for(i=0;i<rows;i++){
	
	  for(j=0;j<columns;j++)
	    values[j] = data[j][i];
	  
	  f = evaluator_evaluate (F.f,columns+Pnum,F.argname,values);
	  
	  for(j=0;j<Pnum;j++){
	    df1 = evaluator_evaluate ((F.df)[j],columns+Pnum,F.argname,values);
	    for(h=0;h<Pnum;h++){
	      df2=evaluator_evaluate ((F.df)[h],columns+Pnum,F.argname,values);
	      ddf=evaluator_evaluate ((F.ddf)[j][h],columns+Pnum,F.argname,values);
	      gsl_matrix_set (dH,j,h,df1*df2+f*ddf);
	    }
	  }
	  gsl_matrix_add (H,dH);
	}
      }
	
      /* invert Hessian; dH store temporary LU decomp. */
      gsl_matrix_memcpy (dH,H);
      gsl_linalg_LU_decomp (dH,P,&signum);
      gsl_linalg_LU_invert (dH,P,covar);
      
      
      gsl_matrix_free(H);
      gsl_matrix_free(dH);
      gsl_permutation_free(P);

      gsl_matrix_scale (covar,sigma2);

    }
    break;
    
  case 3: /* H^{-1} J H^{-1} */
    {
      
      gsl_permutation * P = gsl_permutation_alloc (Pnum);
      int signum;
      
      gsl_matrix *H = gsl_matrix_calloc (Pnum,Pnum);
      gsl_matrix *dH = gsl_matrix_calloc (Pnum,Pnum);
      
      gsl_matrix *J = gsl_matrix_calloc (Pnum,Pnum);
      gsl_matrix *dJ = gsl_matrix_calloc (Pnum,Pnum);
      
      double f,df1,df2,ddf;
      
      double values[columns+Pnum];
      
      /* fill with optimal parameter */
      for(i=columns;i<columns+Pnum;i++)
	values[i] = gsl_vector_get(s->x,i-columns);
      
      /* compute Hessian and Jacobian */
      for(k=0 ; k<S->fnum ;k++){
	Function F=(S->fun)[k];

	for(i=0;i<rows;i++){
	  
	  for(j=0;j<columns;j++)
	    values[j] = data[j][i];
	  
	  f = evaluator_evaluate (F.f,columns+Pnum,F.argname,values);
	
	  for(j=0;j<Pnum;j++){
	    df1 = evaluator_evaluate ((F.df)[j],columns+Pnum,F.argname,values);
	    for(h=0;h<Pnum;h++){
	      df2 = evaluator_evaluate ((F.df)[h],columns+Pnum,F.argname,values);
	      ddf = evaluator_evaluate ((F.ddf)[j][h],columns+Pnum,F.argname,values);
	      gsl_matrix_set (dH,j,h,df1*df2+f*ddf);
	      gsl_matrix_set (dJ,j,h,df1*df2*f*f);
	    }
	  }
	  gsl_matrix_add (H,dH);
	  gsl_matrix_add (J,dJ);
	}
      }
      
      /* invert Hessian; dH store temporary LU decomp. */
      gsl_matrix_memcpy (dH,H);
      gsl_linalg_LU_decomp (dH,P,&signum);
      gsl_linalg_LU_invert (dH,P,H);
      
      /* dJ = H^{-1} J ; covar = dJ H^{-1} */
      gsl_blas_dgemm (CblasNoTrans,
		      CblasNoTrans,1.0,H,J,0.0,dJ);
      gsl_blas_dgemm (CblasNoTrans,
		      CblasNoTrans,1.0,dJ,H,0.0,covar);
      
      
      gsl_matrix_free(H);
      gsl_matrix_free(dH);
      gsl_matrix_free(J);
      gsl_matrix_free(dJ);
      gsl_permutation_free(P);
      
    }
    break;
    
  }

  return covar;
}



/* compute variance-covariance matrix of residuals */
gsl_matrix * res_varcovar(const gsl_multifit_fdfsolver *s,
			  const struct objdata obj,
			  const size_t Pnum){


  size_t i,j,h,k;

  const size_t rows = obj.rows;
  const size_t columns = obj.columns;
  double **data = obj.data;
  const System *S = obj.S;
  const size_t fnum = S->fnum;

  double values[columns+Pnum];


  gsl_matrix *covar = gsl_matrix_calloc (fnum,fnum);
  gsl_matrix *dy = gsl_matrix_calloc (fnum,fnum);


  /* fill with optimal parameters */
  for(i=columns;i<columns+Pnum;i++)
    values[i] = gsl_vector_get(s->x,i-columns);
  
  /* compute covariance matrix */    
  for(i=0;i<rows;i++){
    
    for(j=0;j<columns;j++)
      values[j] = data[j][i];

    for(h=0 ; h<fnum ;h++){
      Function F1=(S->fun)[h];
      double f1 = evaluator_evaluate (F1.f,columns+Pnum,F1.argname,values);
      for(k=0 ; k<fnum ;k++){
	Function F2=(S->fun)[k];
	double f2 = evaluator_evaluate (F2.f,columns+Pnum,F2.argname,values);
	gsl_matrix_set (dy,h,k,f1*f2);
      }
    }

    gsl_matrix_add (covar,dy);
  }
  

  gsl_matrix_free(dy);

  gsl_matrix_scale (covar,1./sqrt(rows));

  return covar;

}


int main(int argc,char* argv[]){

  int i;

  char *splitstring = strdup(" \t");
  int o_verbose=0;
  int o_output=0;
  int o_varcovar=0;

  /* data from sdtin */
  size_t rows=0,columns=0;
  double **data=NULL;

  /* parameter of the function to minimize */
  char **Param=NULL;
  double *Pval=NULL;
  gsl_matrix *Pcovar=NULL;  /* parameters covariance matrix */
  gsl_matrix *Rcovar=NULL;  /* residuals covariance matrix */
  size_t Pnum=0;
    
  /* definition of the system */
  System S;

  /* minimization tolerance  */
  double eps=1e-5;

  /* variables for reading command line options */
  /* ------------------------------------------ */
  int opt;
  /* ------------------------------------------ */

  /* COMMAND LINE PROCESSING */
  while((opt=getopt_long(argc,argv,"v:hF:O:V:M:e:",gb_long_options, &gb_option_index))!=EOF){
    if(opt==0){
      gbutils_header(argv[0],stdout);
      exit(0);
    }
    else if(opt=='?'){
      fprintf(stderr,"option %c not recognized\n",optopt);
      exit(-1);
    }
    else if(opt=='h'){
      /*print help*/
      fprintf(stdout,"Least square estimation of a system of j non linear equations. Read data \n");
      fprintf(stdout,"in columns (X_1 .. X_N). The j-th equation is specified by a function f_j(x1,x2...)\n");
      fprintf(stdout,"using variables names x1,x2,..,xN for the first, second...N-th column of data.\n");
      fprintf(stdout,"The f_j(x1,x2,...)'s are assumed i.i.d. according to a multivariate gaussian\n");
      fprintf(stdout,"distribution.\n");
      fprintf(stdout,"\nUsage: %s [options] <function definition> \n\n",argv[0]);
      fprintf(stdout,"Options:\n");
      fprintf(stdout," -O  type of output (default 0)\n");
      fprintf(stdout,"      0  parameters                    \n");
      fprintf(stdout,"      1  parameters and errors         \n");
      fprintf(stdout,"      2  <variables> and residuals     \n");
      fprintf(stdout,"      3  parameters and variance matrix\n");
      fprintf(stdout," -V  variance matrix estimation (default 0)\n");
      fprintf(stdout,"      0 <gradF gradF^t>                \n");
      fprintf(stdout,"      1  < J^{-1} >                    \n");
      fprintf(stdout,"      2 < H^{-1} >                     \n");
      fprintf(stdout,"      3  < H^{-1} J H^{-1} >           \n");
      fprintf(stdout," -e  minimization tolerance (default 1e-5)\n");
      fprintf(stdout," -v  verbosity level (default 0)\n");
      fprintf(stdout,"      0  just results                   \n");
      fprintf(stdout,"      1  comment headers                \n");
      fprintf(stdout,"      2  summary statistics             \n");
      fprintf(stdout,"      3  covariance matrix           \n");
      fprintf(stdout,"      4  minimization steps             \n");
      fprintf(stdout,"      5  model definition               \n");
      fprintf(stdout," -F  input fields separators (default \" \\t\")\n");
      fprintf(stdout," -h  this help                      \n");
      exit(0);
    }
    else if(opt=='e'){
      /* set the minimization tolerance */
      eps = fabs(atof(optarg));
    }
    else if(opt=='F'){
      /*set the fields separator string*/
      free(splitstring);
      splitstring = strdup(optarg);
    }
    else if(opt=='O'){
      /* set the type of output */
      o_output = atoi(optarg);
      if(o_output<0 || o_output>3){
	fprintf(stderr,"ERROR (%s): output option '%d' not recognized. Try option -h.\n",
		GB_PROGNAME,o_output);
	exit(-1);
      }
    }
    else if(opt=='V'){
      /* set the type of covariance */
      o_varcovar = atoi(optarg);
      if(o_varcovar<0 || o_varcovar>3){
	fprintf(stderr,"ERROR (%s): variance option '%d' not recognized. Try option -h.\n",
		GB_PROGNAME,o_varcovar);
	exit(-1);
      }
    }
    else if(opt=='v'){
      o_verbose = atoi(optarg);
    }
  }
  /* END OF COMMAND LINE PROCESSING */

  /* initialize global variables */
  initialize_program(argv[0]);

  /* load data */
  loadtable(&data,&rows,&columns,0,splitstring);

  /* parse line for functions and variables specification */
  /* ---------------------------------------------------- */
  if(optind == argc){
    fprintf(stderr,"ERROR (%s): please provide a system to fit.\n",
	    GB_PROGNAME);
    exit(-1);
  }

  S.fnum=0;
  S.fun=NULL;
  for(i=optind;i<argc;i++){
    char *piece=strdup (argv[i]);
    char *stmp1=piece;
    char *stmp2;
    
    while( (stmp2=strsep (&stmp1,",")) != NULL ){/*take a piece*/
      char *stmp3 = strdup(stmp2);
      char *stmp4 = stmp3;
      char *stmp5;
/*       fprintf(stderr,"token:%s\n",stmp3); */

      /* initial condition */
      if( (stmp5=strsep(&stmp4,"=")) != NULL && stmp4 != NULL ){
	if( strlen(stmp5)>0 && strlen(stmp4)>0){
	  Pnum++;
	  Param=(char **) my_realloc((void *)  Param,Pnum*sizeof(char *));
	  Pval=(double *) my_realloc((void *) Pval,Pnum*sizeof(double));
	  
	  Param[Pnum-1] = strdup(stmp5);
	  Pval[Pnum-1] = atof(stmp4);
	}
      }
      else{ /* allocate new function */
	S.fnum++;
	S.fun = (Function *) my_realloc((void *) S.fun,S.fnum*sizeof(Function));
	(S.fun[S.fnum-1]).f = evaluator_create (stmp3);
	assert((S.fun[S.fnum-1]).f);
      }
      free(stmp3);
    }
    free(piece);

  }
  /* ---------------------------------------------------- */


  /* check functions definition and build new list */
  /* -------------------------------------------- */

  /* check that something to fit is provided */
  if(Pnum==0){
    fprintf(stderr,"ERROR (%s): please provide a parameter to estimate.\n",
	    GB_PROGNAME);
    exit(-1);
  }


  /* check the definition of function */
  {
    size_t i,j,h;
    char **NewParam=NULL;
    double *NewPval=NULL;
    size_t NewPnum=0;
    size_t totstorednum=0;

    char ***storedname= (char ***) my_alloc(S.fnum*sizeof(char **));
    size_t *storednum = (size_t *) my_alloc(S.fnum*sizeof(size_t));

    /* retrive list of arguments and their number */
    /* notice that storedname is not allocated */
    for(h=0;h<S.fnum;h++){

      Function F=S.fun[h];
      int argnum;

      evaluator_get_variables (F.f,&(storedname)[h],&argnum);
      totstorednum += storednum[h] = (size_t) argnum;

    }

    /* check the definition of the function */
    for(h=0;h<S.fnum;h++){

      for(i=0;i<storednum[h];i++){
	char *stmp1 = storedname[h][i];
	if(*stmp1 == 'x'){
	  size_t index = (atoi(stmp1+1)>0?atoi(stmp1+1):0);
	  if(index>columns){
	    fprintf(stderr,"ERROR (%s): column %zd not present in data\n",
		    GB_PROGNAME,index);
	    exit(-1);
	  }
	}
	else {
	  for(j=0;j<Pnum;j++)
	    if( strcmp(Param[j],stmp1)==0 ) break;
	  if(j==Pnum){
	    fprintf(stderr,"ERROR (%s): parameter %s without initial value\n",
		    GB_PROGNAME,stmp1);
	    exit(-1);
	  }
	}
      }

    }

    /* remove unnecessary parameters */
    for(i=0;i<Pnum;i++){
      char found=0;
      for(h=0;h<S.fnum;h++)
	for(j=0;j<storednum[h];j++)
	  if( strcmp(Param[i],storedname[h][j])==0 ) found=1;

      if(found==0){
	fprintf(stderr,"WARNING (%s): irrelevant parameter %s removed\n",
		GB_PROGNAME,Param[i]);
	continue;
      }
      NewPnum++;
      NewParam=(char **) my_realloc((void *)  NewParam,NewPnum*sizeof(char *));
      NewPval=(double *) my_realloc((void *) NewPval,NewPnum*sizeof(double));
      NewParam[NewPnum-1] = strdup(Param[i]);
      NewPval[NewPnum-1] = Pval[i];
    }

    for(i=0;i<Pnum;i++)
      free(Param[i]);
    free(Param);
    free(Pval);
    
    Param = NewParam;
    Pval = NewPval;
    Pnum= NewPnum;

    free(storednum);
    free(storedname);

      
  }
  /* -------------------------------------------- */


  /* implement consistent definition of functions */
  /* -------------------------------------------- */
  {
    size_t i,j,h;

    for(h=0;h<S.fnum;h++){
      Function *F= &(S.fun[h]);

      /* prepare the new list of argument names */
      F->argnum=columns+Pnum;
      F->argname = (char **) my_alloc((columns+Pnum)*sizeof(char *));

         
      for(i=0;i<columns;i++){
	int length;
	length = snprintf(NULL,0,"x%zd",i+1);
	F->argname[i] = (char *) my_alloc((length+1)*sizeof(char));
	snprintf(F->argname[i],length+1,"x%zd",i+1);
      }
      
      for(i=columns;i<columns+Pnum;i++){
	
	int length;
	length = snprintf(NULL,0,"%s",Param[i-columns]);
	F->argname[i] = (char *) my_alloc((length+1)*sizeof(char));
	snprintf(F->argname[i],length+1,"%s",Param[i-columns]);
      }
      
      /* define first order derivatives */
      F->df = (void **) my_alloc(Pnum*sizeof(void *));
      for(i=0;i<Pnum;i++){
	F->df[i] = evaluator_derivative (F->f,Param[i]);
	assert(F->df[i]);
      }
      
      /* define second order derivatives */
      if(o_varcovar>1){
	F->ddf = (void ***) my_alloc(Pnum*sizeof(void **));
	for(i=0;i<Pnum;i++)
	  F->ddf[i] = (void **) my_alloc(Pnum*sizeof(void *));
	
	for(i=0;i<Pnum;i++)
	  for(j=0;j<Pnum;j++){
	    F->ddf[i][j] = evaluator_derivative ((F->df)[i],Param[j]);
	    assert((F->ddf)[i][j]);
	  }
      }

    }
  }
  /* -------------------------------------------- */

  
  /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
  /* print builded functions and variables */
  if(o_verbose>4){
    size_t i,j,h;

    fprintf(stderr," ------------------------------------------------------------\n");

    fprintf(stderr,"  Model [xi: i-th data column]:\n");
    for(h=0;h<S.fnum;h++){
      Function F=(S.fun)[h];
      fprintf(stderr,"     %s ~ e_%zd \n", evaluator_get_string (F.f),h+1);
    }

    fprintf (stderr,"\n  Parameters and initial conditions:\n");
    for(i=0;i<Pnum;i++)
      fprintf (stderr,"      %s = %f\n", Param[i],Pval[i]);

    fprintf (stderr,"\n  Model first derivatives:\n");
    for(h=0;h<S.fnum;h++){
      Function F=(S.fun)[h];
      for(i=0;i<Pnum;i++)
	fprintf (stderr,"     d f_%zd(x) / d%s = %s\n",
		 h+1,Param[i],evaluator_get_string ((F.df)[i]));
      fprintf (stderr,"\n");
    }

    if(o_varcovar>1){
      fprintf (stderr,"\n  Model second derivatives:\n");
      for(h=0;h<S.fnum;h++){
	Function F=(S.fun)[h];
	for(i=0;i<Pnum;i++)
	  for(j=0;j<Pnum;j++)
	    fprintf (stderr,"     d^2 f_%zd(x) / d%s d%s = %s\n",h+1,Param[i],Param[j],
		     evaluator_get_string ((F.ddf)[i][j]));
	fprintf (stderr,"\n");
      }
    }
  }
  /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */


  /* PARAMETERS ESTIMATION */
  /* --------------------- */

  {
    
    const gsl_multifit_fdfsolver_type *T;
    gsl_multifit_fdfsolver *s;
    gsl_multifit_function_fdf obj;

    size_t i;

    /* iteration controls */
    size_t iter=0;
    int solver_status,object_status=GSL_CONTINUE,estimate_status=GSL_CONTINUE;
    double oldf;
    
    /* set initial condition */
    gsl_vector_view x = gsl_vector_view_array (Pval, Pnum);
    struct objdata param;

    /* set the parameter for the function */
    param.S = &S;
    param.rows = rows;
    param.columns = columns;
    param.data =data ;

    /* set the object structure */
    obj.f = ols_obj_f;
    obj.df = ols_obj_df;
    obj.fdf = ols_obj_fdf;
    obj.n = rows*S.fnum;
    obj.p = Pnum;
    obj.params = &param;
    
    /* initialize the solver */
    T = gsl_multifit_fdfsolver_lmsder;
    s = gsl_multifit_fdfsolver_alloc (T,obj.n,obj.p);
    gsl_multifit_fdfsolver_set (s, &obj,&x.vector);


    /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
    if(o_verbose>3){
      fprintf(stderr," ------------------------------------------------------------\n");
      fprintf(stderr," Iter  ");
	for(i=0;i<Pnum;i++)
	  fprintf(stderr,"%-9s ",Param[i]);
      fprintf(stderr,"|f(x)|      d|f(x)|      |dx|       status\n");
    }
    /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

    /* set the initial value of the object function */
    oldf=gsl_blas_dnrm2 (s->f);
    do
      {
	double f,g,df,dg;
	solver_status = gsl_multifit_fdfsolver_iterate (s);

	/* compute increment in object and estimates */
	f=gsl_blas_dnrm2 (s->f);
	df=fabs(oldf-f);
	oldf=f;
	g=gsl_blas_dnrm2 (s->x);
	dg=gsl_blas_dnrm2 (s->dx);
	
	iter++;

	/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
	/* print status */
	if(o_verbose>3){
	  fprintf (stderr,"%3zd  ",iter);
	  for(i=0;i<Pnum;i++)
	    fprintf (stderr,"%8.5f ",gsl_vector_get (s->x, i));
	  fprintf (stderr," %8.5e  %8.5e  %8.5e %s\n",
		   f,df,dg,gsl_strerror (solver_status));
	}
	/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

/* 	if (status) */
/* 	  break; */

        /* check condition on estimates precision */
	if( estimate_status == GSL_CONTINUE && 
	    dg < eps && dg < eps*g )
	  estimate_status = GSL_SUCCESS;

	/* check condition of object convergence */
	if( object_status == GSL_CONTINUE &&
	    df>0 && df < eps && df < eps*fabs(f) )
	  object_status = GSL_SUCCESS;

      }
    while ( (estimate_status == GSL_CONTINUE || object_status == GSL_CONTINUE)
	    && iter < 500);

    /* store final values */
    for(i=0;i<Pnum;i++)
      Pval[i] = gsl_vector_get(s->x,i);

    /* build the parameters covariance matrix */
    Pcovar = ols_varcovar(o_varcovar,s,param,Pnum);

    /* build the residuals covariance matrix */
    Rcovar = res_varcovar(s,param,Pnum);

    /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
    if(o_verbose>1){
      size_t i,j,h;

      
      fprintf(stderr,
	      " ------------------------------------------------------------\n");
      
      fprintf(stderr," residuals variance-covariance structure \n\n");
      
      for(i=0;i<Rcovar->size1;i++){
	fprintf(stderr," e_%zd = %+f | ",
		i+1, sqrt(gsl_matrix_get(Rcovar,i,i)));
	for(j=0;j<Rcovar->size2;j++)
	  if(j != i)
	    fprintf(stderr,"%+f ",
		    gsl_matrix_get(Rcovar,i,j)/sqrt(gsl_matrix_get(Rcovar,i,i)*gsl_matrix_get(Rcovar,j,j)));
	  else
	    fprintf(stderr,"%+f ",1.0);
	fprintf(stderr,"|\n");
      }

      for(i=0;i<Rcovar->size1;i++){
	const double sumsq=gsl_matrix_get(Rcovar,i,i)*sqrt(rows);
	size_t ndf = 0;

	char **storedname=NULL;
	int storednum=0;
	
	/* notice that storedname is not allocated */
	evaluator_get_variables (S.fun[i].f,&storedname,&storednum);

	for(j=0;j<Pnum;j++)
	  {
	    char found=0;
	    
	    for(h=0;h<storednum;h++)
	      if( strcmp(Param[j],storedname[h])==0 ) found=1;
	    
	    if(found==0) ndf ++;
	  }

	ndf = rows-ndf;

	fprintf(stderr,"\n  equation %zd:\n",i+1);
	fprintf(stderr,"  sum of squared residual           (SSR) = %f\n",
		sumsq);
	fprintf(stderr,"  number degrees of freedom         (ndf) = %zd\n",
		ndf);
	fprintf(stderr,"  residuals stdev           sqrt(SSR/ndf) = %f\n",
	      sqrt(sumsq/ndf));
	fprintf(stderr,"  chi-square test           P(>SSR | ndf) = %e\n",
		gsl_cdf_chisq_Q (sumsq,ndf));
      }
    }
    /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */


    /* free space */
    gsl_multifit_fdfsolver_free (s);

  }




  /* output */
  /* ------ */

  /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
  if(o_verbose>2){
    size_t i,j;

    fprintf(stderr," ------------------------------------------------------------\n");
    fprintf(stderr,"  variance matrix                         = ");
    switch(o_varcovar){
    case 0: fprintf(stderr,"<gradF gradF^t>\n"); break;
    case 1: fprintf(stderr,"J^{-1}\n"); break;
    case 2: fprintf(stderr,"H^{-1}\n"); break;
    case 3: fprintf(stderr,"H^{-1} J H^{-1}\n"); break;
    }
    
    fprintf(stderr,"\n");
    for(i=0;i<Pnum;i++){
      fprintf(stderr," %s = %+f +/- %f (%5.1f%%) | ",
	      Param[i],Pval[i],
	      sqrt(gsl_matrix_get(Pcovar,i,i)),
	      100.*sqrt(gsl_matrix_get(Pcovar,i,i))/fabs(Pval[i]));
      for(j=0;j<Pnum;j++)
	if(j != i)
	  fprintf(stderr,"%+f ",
		  gsl_matrix_get(Pcovar,i,j)/sqrt(gsl_matrix_get(Pcovar,i,i)*gsl_matrix_get(Pcovar,j,j)));
	else
	  fprintf(stderr,"%+f ",1.0);
	fprintf(stderr,"|\n");
    }    
  }

  if(o_verbose>1)
    fprintf(stderr," ------------------------------------------------------------\n");
  /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
  
  switch(o_output){
  case 0:
    {
      size_t i;
      /* +++++++++++++++++++++++++++++++++++ */
      if(o_verbose>0){
	fprintf(stdout,"#");
	for(i=0;i<Pnum-1;i++)
	  fprintf(stdout,EMPTY_SEP,Param[i]);
	i=Pnum-1;
	fprintf(stdout,EMPTY_NL,Param[i]);
      }
      /* +++++++++++++++++++++++++++++++++++ */

      printline(stdout,Pval,Pnum);
    }
    break;
  case 1:
    {
      size_t i;

      /* +++++++++++++++++++++++++++++++++++ */
      if(o_verbose>0){
	fprintf(stdout,"#");
	for(i=0;i<Pnum-1;i++){
	  fprintf(stdout,EMPTY_SEP,Param[i]);
	  fprintf(stdout,EMPTY_SEP,"+/-");
	}
	i=Pnum-1;
	fprintf(stdout,EMPTY_SEP,Param[i]);
	fprintf(stdout,EMPTY_NL,"+/-");
      }
      /* +++++++++++++++++++++++++++++++++++ */
      
      for(i=0;i<Pnum-1;i++){
	fprintf(stdout,FLOAT_SEP,Pval[i]);
	fprintf(stdout,FLOAT_SEP,sqrt(gsl_matrix_get(Pcovar,i,i)));
      }
      i=Pnum-1;
      fprintf(stdout,FLOAT_SEP,Pval[i]);
      fprintf(stdout,FLOAT_NL,sqrt(gsl_matrix_get(Pcovar,i,i)));
    }
    break;
  case 2:
    {
      
      size_t i,j;
      double values[Pnum+columns];
      
      /* set the parameter values */
      for(i=columns;i<columns+Pnum;i++)
	values[i] = Pval[i-columns];
      
      /* +++++++++++++++++++++++++++++++++++ */
      if(o_verbose>0){
	fprintf(stderr,"#");
	for(i=0;i<Pnum-1;i++){
	  fprintf(stderr,EMPTY_SEP,Param[i]);
	  fprintf(stderr,EMPTY_SEP,"+/-");
	}
	i=Pnum-1;
	fprintf(stderr,EMPTY_SEP,Param[i]);
	fprintf(stderr,EMPTY_NL,"+/-");
	
	for(i=0;i<Pnum-1;i++){
	  fprintf(stderr,FLOAT_SEP,Pval[i]);
	  fprintf(stderr,FLOAT_SEP,sqrt(gsl_matrix_get(Pcovar,i,i)));
	}
	i=Pnum-1;
	fprintf(stderr,FLOAT_SEP,Pval[i]);
	fprintf(stderr,FLOAT_NL,sqrt(gsl_matrix_get(Pcovar,i,i)));
      }
      /* +++++++++++++++++++++++++++++++++++ */


      /* +++++++++++++++++++++++++++++++++++ */
/*       if(o_verbose>0){ */
/* 	fprintf(stdout,"#"); */
/* 	for(i=0;i<columns;i++){ */
/* 	  char *string= (char *) my_alloc(sizeof(char)*4); */
/* 	  snprintf(string,4,"x%zd",i+1); */
/* 	  fprintf(stdout,EMPTY_SEP,string); */
/* 	  free(string); */
/* 	} */
/* 	fprintf(stdout,"%s\n",evaluator_get_string (F.f)); */
/*       } */
      /* +++++++++++++++++++++++++++++++++++ */

      for(i=0;i<rows;i++){
	
	/* print columns and set the variables value */
	for(j=0;j<columns;j++){
	  fprintf(stdout,FLOAT_SEP,data[j][i]);
	  values[j] = data[j][i];
	}
	/* print functions residual */
	{
	  Function F;

	  for(j=0 ; j<S.fnum-1 ;j++){
	    F=(S.fun)[j];
	    fprintf(stdout,FLOAT_SEP,
		    evaluator_evaluate (F.f,columns+Pnum,F.argname,values));
	  }

	  F=(S.fun)[j];
	  fprintf(stdout,FLOAT_NL,
		  evaluator_evaluate (F.f,columns+Pnum,F.argname,values));
	}
      }

    }
    break;
  case 3:
    {
      size_t i;

      /* +++++++++++++++++++++++++++++++++++ */
      if(o_verbose>0){
	fprintf(stdout,"#");
	for(i=0;i<Pnum-1;i++)
	  fprintf(stdout,EMPTY_SEP,Param[i]);
	i=Pnum-1;
	fprintf(stdout,EMPTY_NL,Param[i]);
      }
      /* +++++++++++++++++++++++++++++++++++ */
      
      printline(stdout,Pval,Pnum);
      
      fprintf(stdout,"\n\n");
      
      for(i=0;i<Pnum;i++){
	size_t j;
	for(j=0;j<Pnum-1;j++)
	  fprintf(stdout,FLOAT_SEP,gsl_matrix_get(Pcovar,i,j));
	j=Pnum-1;
	fprintf(stdout,FLOAT_NL,gsl_matrix_get(Pcovar,i,j));
      }
    }
    break;
  }
  

  /* deallocate function and data  */
  {
    size_t h,i;

    for(h=0 ; h<S.fnum ;h++){
      Function F=(S.fun)[h];
      
      evaluator_destroy(F.f);

      for(i=0;i<F.argnum;i++)
	  free(F.argname[i]);
      free(F.argname);
    }

    for(i=0;i<columns;i++) 
      free(data[i]);
    free(data);

  }

  return 0;

}
