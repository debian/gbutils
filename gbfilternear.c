/*
  gbfilternear (ver. 5.6) -- Filter too near data point in Euclidean metric
  Copyright (C) 2008-2018 Giulio Bottazzi

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  (version 2) as published by the Free Software Foundation;
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#include "tools.h"

int main(int argc,char* argv[]){

  /* data storage variables */
  size_t rows=0,columns=0;
  double **vals=NULL;

  /* minimum distance */
  double mindist = 1;
  char *deleted;

  /* options */
  int o_verbose=0;

  char *splitstring = strdup(" \t");
  
  /* variables for reading command line options */
  /* ------------------------------------------ */
  int opt;
  /* ------------------------------------------ */

  /* COMMAND LINE PROCESSING */    
  while((opt=getopt_long(argc,argv,"vhF:d:",gb_long_options, &gb_option_index))!=EOF){
    if(opt==0){
      gbutils_header(argv[0],stdout);
      exit(0);
    }
    else if(opt=='?'){
      fprintf(stderr,"option %c not recognized\n",optopt);
      exit(-1);
    }
    else if(opt=='h'){
      /*print help*/
      fprintf(stdout,"Filter out too near points. Each row represents Cartesian coordinates\n");
      fprintf(stdout,"of a point in an Euclidean space, whose dimension is set by the number\n");
      fprintf(stdout,"of columns. Rows are removed which are nearer than a minimal distance\n");
      fprintf(stdout,"set with the option '-d'. The order is relevant as first entries are\n");
      fprintf(stdout,"the last to be removed.\n");
      fprintf(stdout,"\nUsage: %s [options]\n\n",argv[0]);
      fprintf(stdout,"Options:\n");
      fprintf(stdout," -d  minimal allowed distance (default 1)\n");
      fprintf(stdout," -F  specify the input fields separators (default \" \\t\")\n");
      fprintf(stdout," -h  this help\n");
      fprintf(stdout," -v  verbose mode\n");
      exit(0);
    }
    else if(opt=='F'){
      /*set the fields separator string*/
      free(splitstring);
      splitstring = strdup(optarg);
    }
    else if(opt=='d'){
      /*set the number of quantiles*/
       mindist=atof(optarg);
    }
    else if(opt=='v'){
      /*set the verbose mode*/
      o_verbose=1;
    }
  }    
  /* END OF COMMAND LINE PROCESSING */

  /* initialize global variables */
  initialize_program(argv[0]);

  /* read data vals[col][row] */
  loadtable(&vals,&rows,&columns,0,splitstring);

  /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
  if(o_verbose){
    fprintf(stderr,"loaded points:      %zd\n",rows);
    fprintf(stderr,"dimension of space: %zd\n",columns);
  }
  /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */


  /* allocate space for array of deleted entries */
  deleted = (char *) my_calloc(rows,sizeof(char));

  /* compute deleted entries */
  {
    size_t i,j,h;
    double distance;

    for(i=0;i<rows;i++){
      if(deleted[i]==1) continue;
      for(j=i+1;j<rows;j++)
	{
	  if(deleted[j]==1) continue;
	  distance = 0;
	  for(h=0;h<columns;h++)
	    distance += pow(vals[h][i] - vals[h][j],2);
	  distance=sqrt(distance);
	  if(distance < mindist) deleted[j]=1;
  	}
    }

  }

  /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
  if(o_verbose){
    size_t i,delnum=0;

    for(i=0;i<rows;i++)
      if(deleted[i]==1) delnum++;

    fprintf(stderr,"minimum radius:     %g\n",mindist);
    fprintf(stderr,"deleted entries:    %zd\n",delnum);

  }
  /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

  /* print remaining entries */
  {
    size_t i,j;

    for(i=0;i<rows;i++)
      if(deleted[i]==0){
	for(j=0;j<columns-1;j++)
	  printf(FLOAT_SEP,vals[j][i]);
	printf(FLOAT_NL,vals[columns-1][i]);
      }
  }


  return 0;

}
