/*
  gplaepfit (ver. 6.0) -- Fit a (less) asymmetric power exponential
  distribution
  
  Copyright (C) 2018 Giulio Bottazzi

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  (version 2) as published by the Free Software Foundation;
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/


/*
  Verbosity levels:
  0 just the final ouput
  1 the results of intermediate steps
  2 internal information on intermediate steps
  3 gory details on intermediate steps
*/


#include "tools.h"
#include "multimin.h"
#include "assert.h"

#include <gsl/gsl_sf_gamma.h>
#include <gsl/gsl_sf_psi.h>


/* Global Variables */
/* ---------------- */

/* store data */
double *Data; /*the array of data*/
size_t Size;/*the number of data*/

/* output management */
FILE *Fmessages;/*stream for computation messages*/
FILE *Fparams;/*strem for final parameters*/
FILE *Ffitted;/*strem for distribution or density*/

/*------------------*/


/* Output Functions */
/*----------------- */

void printcumul(double x[]){

  int i;
  double dtmp1; 
  const double bl=x[0];
  const double br=x[1];
  const double a=x[2];
  const double m=x[3];

  const double gl=gsl_sf_gamma(1./bl+1.)*pow(bl,1./bl);
  const double gr=gsl_sf_gamma(1./br+1.)*pow(br,1./br);
  const double gsum=gl+gr;

  for(i=0;i<Size;i++){
    if(Data[i]>m){
      dtmp1=pow((Data[i]-m)/a,br)/br;
      dtmp1=(gl+gr*gsl_sf_gamma_inc_P(1./br,dtmp1))/gsum;
     }
    else{
      dtmp1=pow((m-Data[i])/a,bl)/bl;
      dtmp1=(gl-gl*gsl_sf_gamma_inc_P(1./bl,dtmp1))/gsum;
    }
    fprintf(Ffitted,FLOAT_SEP,Data[i]);
    fprintf(Ffitted,FLOAT_NL,dtmp1);
  }

}

void printdensity(double x[]){

  int i;
  const double bl=x[0];
  const double br=x[1];
  const double a=x[2];
  const double m=x[3];

  const double norm=a*(pow(bl,1/bl)*gsl_sf_gamma(1./bl+1.)
		       +pow(br,1/br)*gsl_sf_gamma(1./br+1));

  for(i=0;i<Size;i++){
    double dtmp1=Data[i];
    fprintf(Ffitted,FLOAT_SEP,dtmp1);
    dtmp1-=m;
    if(dtmp1>=0){
      fprintf(Ffitted,FLOAT_NL,exp(-pow(dtmp1/a,br)/br)/norm);
    }
    else{
      fprintf(Ffitted,FLOAT_NL,exp(-pow(-dtmp1/a,bl)/bl)/norm);
    }
  }
  
}
/*----------------- */


/* Object Function */
/*---------------- */

void objf(const size_t n,const double *x,void *params,double *f){

  double dtmp1;
  unsigned utmp1;

  double sumL=0.0;
  double sumR=0.0;

  const double bl=x[0];
  const double br=x[1];
  const double a=x[2];
  const double mu=x[3];

  gsl_sf_result result;
  int status;

  if( (status = gsl_sf_gamma_e(1./bl+1.,&result)) ){
    fprintf(Fmessages,"gamma status  = %s\n", gsl_strerror(status));
    fprintf(Fmessages,"bl=%e\n",bl);
    exit(-1);
  }

  if( (status = gsl_sf_gamma_e(1./br+1.,&result)) ){
    fprintf(Fmessages,"gamma status  = %s\n", gsl_strerror(status));
    fprintf(Fmessages,"br=%e\n",br);
    exit(-1);
  }


  for(utmp1=0;utmp1<Size;utmp1++){
    if(Data[utmp1]>mu) break;    
    sumL+=pow(mu-Data[utmp1],bl);
  }
  for(;utmp1<Size;utmp1++){
    sumR+=pow(Data[utmp1]-mu,br);
  }

  sumL /= Size;
  sumR /= Size;
  
  dtmp1 = pow(bl,1./bl)*gsl_sf_gamma(1./bl+1.)
    +pow(br,1./br)*gsl_sf_gamma(1./br+1.);
  
  *f = log(a*dtmp1)
    +sumL/(pow(a,bl)*bl)+sumR/(pow(a,br)*br);

}


void objdf(const size_t n,const double *x,void *params,double *df){

  double dtmp1;
  
  const double bl=x[0];
  const double br=x[1];
  const double a=x[2];
  const double mu=x[3];

  unsigned utmp1;
  double sumL=0.0;
  double sumR=0.0;
  double sumL1=0.0;
  double sumR1=0.0;
  double sumLl=0.0;
  double sumRl=0.0;

  gsl_sf_result result;
  int status;
  

  for(utmp1=0;utmp1<Size;utmp1++){
    if(Data[utmp1]<mu){
      sumL1+= (dtmp1 = pow(mu-Data[utmp1],bl-1.) );
      dtmp1*= mu-Data[utmp1];
      sumL+= dtmp1;
      sumLl+=log(mu-Data[utmp1])*dtmp1;
    }
    else if (Data[utmp1]>mu){
      sumR1+=(dtmp1 = pow(Data[utmp1]-mu,br-1.));
      dtmp1*= Data[utmp1]-mu;
      sumR+=dtmp1;
      sumRl+=log(Data[utmp1]-mu)*dtmp1;
    }
  }

  sumL /= Size;
  sumR /= Size;
  sumL1 /= Size;
  sumR1 /= Size;
  sumLl /= Size;
  sumRl /= Size;


  if( (status = gsl_sf_gamma_e(1./bl+1.,&result)) ){
    fprintf(Fmessages,"gamma status  = %s\n", gsl_strerror(status));
    fprintf(Fmessages,"bl=%e\n",bl);
    exit(-1);
  }

  if( (status = gsl_sf_gamma_e(1./br+1.,&result)) ){
    fprintf(Fmessages,"gamma status  = %s\n", gsl_strerror(status));
    fprintf(Fmessages,"br=%e\n",br);
    exit(-1);
  }

  if( (status = gsl_sf_psi_e(1./bl+1.,&result)) ){
    fprintf(Fmessages,"psi status  = %s\n", gsl_strerror(status));
    fprintf(Fmessages,"bl=%e\n",bl);
    exit(-1);
  }

  if( (status = gsl_sf_psi_e(1./br+1.,&result)) ){
    fprintf(Fmessages,"psi status  = %s\n", gsl_strerror(status));
    fprintf(Fmessages,"br=%e\n",br);
    exit(-1);
  }


  dtmp1 = pow(bl,1./bl)*gsl_sf_gamma(1./bl+1.)+pow(br,1./br)*gsl_sf_gamma(1./br+1.);

  df[0] = (1.-log(bl)-gsl_sf_psi(1./bl+1.))*gsl_sf_gamma(1./bl+1.)*pow(bl,1./bl-2.)/dtmp1
    -(1./(bl*bl) + log(a)/bl)*sumL/pow(a,bl) + sumLl/(pow(a,bl)*bl);
  
  df[1] = (1.-log(br)-gsl_sf_psi(1./br+1.))*gsl_sf_gamma(1./br+1.)*pow(br,1./br-2.)/dtmp1
    -(1./(br*br) + log(a)/br)*sumR/pow(a,br) + sumRl/(pow(a,br)*br);
  
  df[2] = 1./a - sumL/pow(a,bl+1.) - sumR/pow(a,br+1.);
  
  df[3] = sumL1/pow(a,bl) - sumR1/pow(a,br);

}

void objfdf(const size_t n,const double *x,void *params,double *f,double *df){


  double dtmp1;
  
  const double bl=x[0];
  const double br=x[1];
  const double a=x[2];
  const double mu=x[3];

  unsigned utmp1;
  double sumL=0.0;
  double sumR=0.0;
  double sumL1=0.0;
  double sumR1=0.0;
  double sumLl=0.0;
  double sumRl=0.0;


  gsl_sf_result result;
  int status;

  if( (status = gsl_sf_gamma_e(1./bl+1.,&result)) ){
    fprintf(Fmessages,"gamma status  = %s\n", gsl_strerror(status));
    fprintf(Fmessages,"bl=%e\n",bl);
    exit(-1);
  }

  if( (status = gsl_sf_gamma_e(1./br+1.,&result)) ){
    fprintf(Fmessages,"gamma status  = %s\n", gsl_strerror(status));
    fprintf(Fmessages,"br=%e\n",br);
    exit(-1);
  }

  if( (status = gsl_sf_psi_e(1./bl+1.,&result)) ){
    fprintf(Fmessages,"psi status  = %s\n", gsl_strerror(status));
    fprintf(Fmessages,"bl=%e\n",bl);
    exit(-1);
  }

  if( (status = gsl_sf_psi_e(1./br+1.,&result)) ){
    fprintf(Fmessages,"psi status  = %s\n", gsl_strerror(status));
    fprintf(Fmessages,"br=%e\n",br);
    exit(-1);
  }


  for(utmp1=0;utmp1<Size;utmp1++){
    if(Data[utmp1]<mu){
      sumL1+= (dtmp1 = pow(mu-Data[utmp1],bl-1.) );
      dtmp1*= mu-Data[utmp1];
      sumL+= dtmp1;
      sumLl+=log(mu-Data[utmp1])*dtmp1;
    }
    else if (Data[utmp1]>mu){
      sumR1+=(dtmp1 = pow(Data[utmp1]-mu,br-1.));
      dtmp1*= Data[utmp1]-mu;
      sumR+=dtmp1;
      sumRl+=log(Data[utmp1]-mu)*dtmp1;
    }
  }

  sumL /= Size;
  sumR /= Size;
  sumL1 /= Size;
  sumR1 /= Size;
  sumLl /= Size;
  sumRl /= Size;

  dtmp1 = pow(bl,1./bl)*gsl_sf_gamma(1./bl+1.)+pow(br,1./br)*gsl_sf_gamma(1./br+1.);


  *f = log(a*dtmp1)+sumL/(pow(a,bl)*bl)+sumR/(pow(a,br)*br);

  df[0] = (1.-log(bl)-gsl_sf_psi(1./bl+1.))*gsl_sf_gamma(1./bl+1.)*pow(bl,1./bl-2.)/dtmp1
    -(1./(bl*bl) + log(a)/bl)*sumL/pow(a,bl) + sumLl/(pow(a,bl)*bl);
  
  df[1] = (1.-log(br)-gsl_sf_psi(1./br+1.))*gsl_sf_gamma(1./br+1.)*pow(br,1./br-2.)/dtmp1
    -(1./(br*br) + log(a)/br)*sumR/pow(a,br) + sumRl/(pow(a,br)*br);
  
  df[2] = 1./a - sumL/pow(a,bl+1.) - sumR/pow(a,br+1.);
  
  df[3] = sumL1/pow(a,bl) - sumR1/pow(a,br);

}
/*---------------- */



int main(int argc,char* argv[]){
  
  /* store guess */
  double x[4]={2.,2.,1.,0}; /* x[0]=bl x[1]=br x[2]=a x[3]=mu */
  double fmin;
  unsigned type[4];
  double xmin[4];
  double xmax[4];

  /* store possibly provided values for parameters */
  double provided_m=0;
  unsigned is_m_provided=0;

  /* set various options */
  unsigned O_verbose=0;
  unsigned O_output=0;
  unsigned O_method=1;

  /* global optimization parameters */
  struct multimin_params global_oparams={.1,1e-2,100,1e-3,1e-5,2,0};
  /* interval optimization parameters */
  struct multimin_params interv_oparams={.01,1e-3,200,1e-3,1e-5,2,0};
  /* increment in the number of intervals to expore */
  unsigned interv_step=10;

  char *splitstring = strdup(" \t");


  /* COMMAND LINE PROCESSING */
  /* ----------------------- */ 
  int opt;
    
  /* read the command line */    
  while((opt=getopt_long(argc,argv,"x:O:s:d:hV:I:G:s:m:M:F:",gb_long_options, &gb_option_index))!=EOF){
    if(opt==0){
      gbutils_header(argv[0],stdout);
      exit(0);
    }
    else if(opt=='?'){
      fprintf(stderr,"option %c not recognized\n",optopt);
      exit(-1);
    }
    else if(opt=='h'){
      /*print short help*/
      fprintf(stdout,"Fit skewed power exponential density. Read from files or from standard input    \n\n");
      fprintf(stdout,"Usage: %s [options] [files]\n\n",argv[0]);
      fprintf(stdout," Options:                                                            \n");
      fprintf(stdout," -O  output type (default 0)                \n");
      fprintf(stdout,"      0  parameter bl br a m and log-likelihood   \n");
      fprintf(stdout,"      1  the estimated distribution function computed on the provided points     \n");
      fprintf(stdout,"      2  the estimated density function computed on the provided points \n");
      fprintf(stdout," -x  set initial conditions bl,br,a,m  (default 2,2,1,1,0)\n");
      fprintf(stdout," -m  the mode is not estimated but is set to the value provided\n");
      fprintf(stdout," -s  number of intervals to explore at each iteration (default 10)\n");
      fprintf(stdout," -V  verbosity level (default 0)           \n");
      fprintf(stdout,"      0  just the final result        \n");
      fprintf(stdout,"      1  intermediate steps results   \n");
      fprintf(stdout,"      2  intermediate steps internals \n");
      fprintf(stdout,"      3+  details of optim. routine   \n");
      fprintf(stdout," -M  active estimation steps. The value is the sum of (default 1)\n");
      fprintf(stdout,"      0  no optimization\n");
      fprintf(stdout,"      1  global and local optimization \n");
      fprintf(stdout," -G  set global optimization options. Fields are step,tol,iter,eps,msize,algo.\n");
      fprintf(stdout,"     Empty field implies default (default .1,1e-2,100,1e-3,1e-5,2)\n");
      fprintf(stdout," -I  set local optimization options. Fields are step,tol,iter,eps,msize,algo.\n");
      fprintf(stdout,"     Empty field implies default (default .01,1e-3,200,1e-3,1e-5,2)\n");
      fprintf(stdout,"The optimization parameters are");
      fprintf(stdout," step  initial step size of the searching algorithm                  \n");
      fprintf(stdout," tol  line search tolerance iter: maximum number of iterations      \n");
      fprintf(stdout," eps  gradient tolerance : stopping criteria ||gradient||<eps       \n");
      fprintf(stdout," msize  simplex max size : stopping criteria ||max edge||<msize     \n");
      fprintf(stdout," algo  optimization methods: 0 Fletcher-Reeves, 1 Polak-Ribiere,     \n");
      fprintf(stdout,"       2 Broyden-Fletcher-Goldfarb-Shanno, 3 Steepest descent,           \n");
      fprintf(stdout,"       4 Nelder-Mead simplex, 5 Broyden-Fletcher-Goldfarb-Shanno ver.2   \n");
      fprintf(stdout,"       6 Nelder-Mead simplex ver. 2, 7 Nelder-Mead simplex rnd init.  \n");
      fprintf(stdout,"Examples:\n");
      fprintf(stdout," gblaepfit -m 1 <file  estimate bl,br,a with m=1\n");
      exit(0);
    }
    else if(opt=='F'){
      /*set the fields separator string*/
      free(splitstring);
      splitstring = strdup(optarg);
    }
    else if(opt=='V'){
      O_verbose= (unsigned) atoi(optarg);
      global_oparams.verbosity = (O_verbose>2?O_verbose-2:0);
      interv_oparams.verbosity = (O_verbose>2?O_verbose-2:0);
    }
    else if(opt=='M'){
      O_method = (unsigned) atoi(optarg);
    }
    else if(opt=='s'){
      interv_step=(unsigned) atoi(optarg);
    }
    else if(opt=='G'){
      char *stmp1=strdup (optarg);
      char *stmp2;
      char *stmp3=stmp1;
      if( (stmp2=strsep (&stmp1,",")) != NULL){
	if(strlen(stmp2)>0) 
	  global_oparams.step_size=atof(stmp2);
      };
      if( (stmp2=strsep (&stmp1,",")) != NULL){
	if(strlen(stmp2)>0) 
	  global_oparams.tol=atof(stmp2);
      };
      if( (stmp2=strsep (&stmp1,",")) != NULL){
	if(strlen(stmp2)>0) 
	  global_oparams.maxiter=(unsigned) atoi(stmp2);
      };
      if( (stmp2=strsep (&stmp1,",")) != NULL){
	if(strlen(stmp2)>0) 
	  global_oparams.epsabs=atof(stmp2);
      };
      if( (stmp2=strsep (&stmp1,",")) != NULL){
	if(strlen(stmp2)>0)
	  global_oparams.maxsize=atof(stmp2);
      }
      if( (stmp2=strsep (&stmp1,",")) != NULL){
	if(strlen(stmp2)>0) 
	  global_oparams.method= (unsigned) atoi(stmp2);
      };
      free(stmp3);
    }
    else if(opt=='I'){
      char *stmp1=strdup (optarg);
      char *stmp2;
      char *stmp3=stmp1;
      if( (stmp2=strsep (&stmp1,",")) != NULL){
	if(strlen(stmp2)>0) 
	  interv_oparams.step_size=atof(stmp2);
      };
      if( (stmp2=strsep (&stmp1,",")) != NULL){
	if(strlen(stmp2)>0) 
	  interv_oparams.tol=atof(stmp2);
      };
      if( (stmp2=strsep (&stmp1,",")) != NULL){
	if(strlen(stmp2)>0) 
	  interv_oparams.maxiter=(unsigned) atoi(stmp2);
      };
      if( (stmp2=strsep (&stmp1,",")) != NULL){
	if(strlen(stmp2)>0) 
	  interv_oparams.epsabs=atof(stmp2);
      };
      if( (stmp2=strsep (&stmp1,",")) != NULL){
	if(strlen(stmp2)>0)
	  interv_oparams.maxsize=atof(stmp2);
      }
      if( (stmp2=strsep (&stmp1,",")) != NULL){
	if(strlen(stmp2)>0) 
	  interv_oparams.method= (unsigned) atoi(stmp2);
      };
      free(stmp3);
    }
    else if(opt=='O'){
      O_output=atoi(optarg);
    }
    else if(opt=='m'){
      provided_m=atof(optarg);
      is_m_provided=1;
    }
    else if(opt=='x'){
      char *stmp1=strdup (optarg);
      char *stmp2;
      char *stmp3=stmp1;
      if( (stmp2=strsep (&stmp1,",")) != NULL){
	if(strlen(stmp2)>0)
	  x[0]=atof(stmp2);
      }
      if( (stmp2=strsep (&stmp1,",")) != NULL){
	if(strlen(stmp2)>0) 
	  x[1]=atof(stmp2);
      }
      if( (stmp2=strsep (&stmp1,",")) != NULL){
	if(strlen(stmp2)>0) 
	  x[2]=atof(stmp2);
      }
      if( (stmp2=strsep (&stmp1,",")) != NULL){
	if(strlen(stmp2)>0) 
	  x[3]=atof(stmp2);
      }
      free(stmp3);
    }
  }

  /* initialize global variables */
  initialize_program(argv[0]);

  /* customized admin. of errors */
  /* --------------------------- */
  gsl_set_error_handler_off ();


  /* set the various streams */
  /* ----------------------- */
  if(O_output==0){
    Fmessages=stderr;
    Fparams=stdout;
    Ffitted=stderr;
  }
  else{
    Fmessages=stderr;
    Fparams=stderr;
    Ffitted=stdout;
  };

  /* load Data */
  /* --------- */

  if(O_verbose>=1)
    fprintf(Fmessages,"#--- START LOADING DATA\n");
  
  load(&Data,&Size,0,splitstring);

  if (Size <= 1) {
    fprintf(Fmessages,"the number of onservations must be at least 2\n");
    exit(-1);
  }

  
  /* sort data */
  /* --------- */
  qsort(Data,Size,sizeof(double),sort_by_value);


  /* initial values */
  /* -------------- */
  if (is_m_provided)
    x[3] = provided_m;

  objf(4,x,NULL,&fmin);

  /* output of initial values */
  /* ------------------------ */
  if(O_verbose>=1){
    fprintf(Fmessages,"#--- INITIAL VALUES\n");
    fprintf(Fmessages,"#>>> bl=%.3e br=%.3e a=%.3e m=%.3e ll=%.3e\n",x[0],x[1],x[2],x[3],fmin);
  }

  /* chose the metod */
  /* --------------- */

  switch(O_method){
  case 0: /* no minimization */
    break;
    
  case 1:
    /* maximum likelyhood estimation */
    /* ----------------------------- */
  
    if(is_m_provided){
      if(O_verbose>=1)
	fprintf(Fmessages,"#--- UNCONSTRAINED OPTIMIZATION\n");
      
      /* set initial minimization boundaries */
      /* ----------------------------------- */
      
      type[0] = 1; xmin[0]=0; xmax[0]=0; x[0]=2.;
      type[1] = 1; xmin[1]=0; xmax[1]=0; x[1]=2.;
      type[2] = 4; xmin[2]=0; xmax[2]=0; x[2]=1.;
      type[3] = 3; xmin[3]=provided_m; xmax[3]=provided_m; x[3]=provided_m;
      
      /* perform global minimization */
      /* --------------------------- */
      
      multimin(4,x,&fmin,type,xmin,xmax,objf,objdf,objfdf,NULL,global_oparams);
      
    } else {
      
      double dtmp1;    
      double xtmp[4]; /* store the temporary minimum  */
      unsigned oldindex,utmp1,index,max,min;
      
      if(O_verbose>=1)
	fprintf(Fmessages,"#--- UNCONSTRAINED OPTIMIZATION\n");
      
      /* set initial minimization boundaries */
      /* ----------------------------------- */
      
      type[0] = 4; xmin[0]=0; xmax[0]=0; x[0]=2.;
      type[1] = 4; xmin[1]=0; xmax[1]=0; x[1]=2.;
      type[2] = 4; xmin[2]=0; xmax[2]=0; x[2]=1.;
      type[3] = 0; xmin[3]=0; xmax[3]=0; x[3]=0.;
      
      /* perform global minimization */
      /* --------------------------- */
      
      multimin(4,x,&fmin,type,xmin,xmax,objf,objdf,objfdf,NULL,global_oparams);
      
      if(O_verbose>=1){      
	fprintf(Fmessages,"#>>> bl=%.3e br=%.3e a=%.3e m=%.3e ll=%.3e\n",x[0],x[1],x[2],x[3],fmin);
	fprintf(Fmessages,"#--- OPTIMIZATION ON INTERVALS\n");
      }
      
      /* perform interval minimization */
      /* -------------------------- */
      
      type[3] = 3; 
      
      /* find initial index s.t. m \in [Data[index],Data[index+1]] */
      for(utmp1=0;Data[utmp1]<=x[3] && utmp1<Size;utmp1++);
      if(utmp1 == 0)
	index = 0;
      else if (utmp1 == Size)
	index= Size-2;
      else index = utmp1-1;
      
      xmin[3]=Data[index]; xmax[3]=Data[index+1]; x[3]=.5*(xmin[3]+xmax[3]);
      
      multimin(4,x,&fmin,type,xmin,xmax,objf,objdf,objfdf,NULL,interv_oparams);
      
      xtmp[0]=x[0];
      xtmp[1]=x[1];
      xtmp[2]=x[2];
      xtmp[3]=x[3];
      max=min=index;
      
      if(O_verbose>=2)
	fprintf(Fmessages,"#>>> [%+.3e:%+.3e] ll=%e\n",Data[index],Data[index+1],fmin);
      
      /* compute new local minima and compare with global minimum */    
      do {
	oldindex=index;
	/* move to the right */
	for(utmp1=max+1;
	    utmp1<=max+interv_step && utmp1<Size-1;
	    utmp1++){
	  
	  /* set boundaries on m */
	  xmin[3]=Data[utmp1]; xmax[3]=Data[utmp1+1];
	  /* set initial condition */
	  xtmp[3]=.5*(xmin[3]+xmax[3]);
	  /* reset the value to best estimation */
	  xtmp[0]=x[0];xtmp[1]=x[1];xtmp[2]=x[2];
	  
	  multimin(4,xtmp,&dtmp1,type,xmin,xmax,objf,objdf,objfdf,NULL,interv_oparams);
	  
	  if(dtmp1<fmin){/* found new minimum */
	    index=utmp1;
	    x[0]=xtmp[0]; x[1]=xtmp[1]; x[2]=xtmp[2]; x[3]=xtmp[3];
	    fmin=dtmp1;
	    
	  if(O_verbose>=1)
	    fprintf(Fmessages,"#>>> [%+.3e:%+.3e] ll=%e\n",
		    Data[utmp1],Data[utmp1+1],dtmp1);
	  }
	  else {/* NOT found new minimum */
	    if(O_verbose>=2)
	      fprintf(Fmessages,"#    [%+.3e:%+.3e] ll=%e\n",
		      Data[utmp1],Data[utmp1+1],dtmp1);
	  }
	  
	}
	max=utmp1-1;
	/* reset the value to best estimation */
	xtmp[0]=x[0];xtmp[1]=x[1];xtmp[2]=x[2];
	/* move to the left */
	for(utmp1=min-1;
	    (int) utmp1 >= (int) min-interv_step && (int) utmp1 >= 0;
	    utmp1--){

	  /* set boundaries on m */
	  xmin[3]=Data[utmp1]; xmax[3]=Data[utmp1+1];
	  /* set initial condition */
	  xtmp[3]=.5*(xmin[3]+xmax[3]);
	  /* reset the value to best estimation */
	  xtmp[0]=x[0];xtmp[1]=x[1];xtmp[2]=x[2];
	  
	  multimin(4,xtmp,&dtmp1,type,xmin,xmax,objf,objdf,objfdf,NULL,interv_oparams);
	  
	  if(dtmp1<fmin){/* found new minimum */
	    index=utmp1;
	    x[0]=xtmp[0]; x[1]=xtmp[1]; x[2]=xtmp[2]; x[3]=xtmp[3];
	    fmin=dtmp1;
	    
	    if(O_verbose>=1)
	      fprintf(Fmessages,"#>>> [%+.3e:%+.3e] ll=%e\n",
		      Data[utmp1],Data[utmp1+1],dtmp1);
	  }
	  else {/* NOT found new minimum */
	    if(O_verbose>=2)
	      fprintf(Fmessages,"#    [%+.3e:%+.3e] ll=%e\n",
		      Data[utmp1],Data[utmp1+1],dtmp1);
	  }
	  
	}
	
	min=utmp1+1;
      }
      while(index!=oldindex);
      
      if(O_verbose>=1)
	fprintf(Fmessages,"#--- intervals explored: %d\n",max-min);
      
    }

    break;
  default:
    fprintf(Fmessages,"Minimization method not recognized! Try %s -h",argv[0]);
    exit(EXIT_FAILURE);
  }

  fprintf(Fparams,FLOAT_SEP,x[0]);
  fprintf(Fparams,FLOAT_SEP,x[1]);
  fprintf(Fparams,FLOAT_SEP,x[2]);
  fprintf(Fparams,FLOAT_SEP,x[3]);
  fprintf(Fparams,FLOAT_NL,fmin);

  if(O_output==1)
    printcumul(x);
  else if(O_output==2)
    printdensity(x);

  free(Data);
  free(splitstring);
  exit(0);

}
