/*
  gbnear  (ver. 5.6) -- Produce nearest neighborhood density estimate
  Copyright (C) 2001-2018 Giulio Bottazzi

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  (version 2) as published by the Free Software Foundation;
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#include "tools.h"


double dk2(const double *data, const int size, const double x, const int k)
{

  int left,right,i;
  double toleft,toright,res;
  

  left=0;
  right=size-1;

  do{
    const int index = (right+left)/2 ;
    const double dtmp1 = data[ index ];
    if(x>dtmp1) left=index;
    else right=index;
  } while (right-left>1);
  
  toright = data[right]-x;
  toleft = x-data[left];

  res =( toright<toleft ? toright : toleft );

  /*============*/
/*   fprintf(stderr,"#--------------------\n",x); */
/*   fprintf(stderr,"#x=%f in ([%zd]=%f,[%zd]=%f)\n", */
/* 	  x,left,data[left],right,data[right]); */
  /*============*/

  for(i=2;i<=k;i++){
    if(toright<toleft){/* must be expanded to right */
      if(right < size-1){	/* possible */
	right++;
	toright=data[right]-x;
	if(toright>toleft)
	  res = toleft;
	else if(toright<toleft)
	  res = toright;
	else{
	  res = toright;
	  i++;
	}
      }
      else{			/* not possible */
	res=toleft=x-data[left];
	if(left>0)left--; else break;
      }
    }
    else if(toleft<toright){/* must be expanded to left */
      if(left >0){	/* possible */
	left--;
	toleft = x-data[left];
	if(toright>toleft)
	  res = toleft;
	else if(toright<toleft)
	  res = toright;
	else{
	  res = toright;
	  i++;
	}
      }
      else{			/* not possible */
	res = toright = data[right]-x;
	if(right<size-1) right++; else break;
      }
    }
    else{/* can be expand in both direction */
      if(right < size-1 && left > 0){ /* move in both direction */
	const double tmp_toright = data[right+1]-x;
	const double tmp_toleft = x-data[left-1];
	if(tmp_toleft>tmp_toright){
	  right++;
	  toright=tmp_toright;
	  res=(toright>toleft?toleft:toright);
	}
	else if(tmp_toright>tmp_toleft){
	  left--;
	  toleft=tmp_toleft;
	  res=(toright>toleft?toleft:toright);
	}
	else{
	  right++;
	  left--;
	  res=toright=tmp_toright;
	  toleft=tmp_toleft;
	  i++;
	}
      }
      else if (right>=size-1 && left>0){/* move to left */
	left--;
	res = toleft = x-data[left];
      }
      else if (left==0 && right<size-1){ /* move to right */
	right++;
	res = toright = data[right]-x;
      }
      else break;
    }
/*     fprintf(stderr,"#%zd ([%zd]=%f,[%zd]=%f) res=%f\n", */
/* 	    i,left,data[left],right,data[right],res); */
  }
/*   fprintf(stderr,"#-> res=%f\n",res); */

  /*============*/
/*   fprintf(stderr,"#----- [%zd]=%f x[%zd]=%f res= %f \n", */
/* 	  left,data[left],right,data[right],res); */
  /*============*/
  

  return(res);
}


/* NO LONGER USED... TO BE REMOVED!!! */
double dk(const double *data, const int size,
	  const int k, const int ind){

  const double x = data[ind];

  int i;
  int left=ind;
  int right=ind;
  
  double toright=0;
  double toleft=0;

  double res=0;

  for(i=0;i<k;i++){
    if(right < size-1 && left>0){ /* move in both direction */
      if(toright==toleft){
	const double tmp_toright = data[right+1]-x;
	const double tmp_toleft = x-data[left-1];
	if(tmp_toleft>tmp_toright){
	  res=toright=tmp_toright;
	  right++;
	}
	else if(tmp_toright>tmp_toleft){
	  res=toleft=tmp_toleft;
	  left++;
	}
	else{
	  res=toright=tmp_toright;
	  right++;
	  toleft=tmp_toleft;
	  left++;
	  i++;
	}
      }
      else if(toright<toleft){
	right++;
	res = toright = data[right]-x;
      }
      else{
	left--;
	res = toleft = x-data[left];
      }
    }
    else if (right==size-1 && left>0){	/* move to left */
      left--;
      res = toleft = x-data[left];
    }
    else if (left==0 && right<size-1){ /* move to right */
      right++;
      res = toright = data[right]-x;
    }
    else break;
  }

/*   printf("[ x[%zd]=%f  <- x[%zd]=%f -> x[%zd]=%f ] => %f \n", */
/* 	 left,data[left],ind,data[ind],right,data[right], */
/* 	 res); */

  return(res );
  
}


int main(int argc,char* argv[]){

    double *data=NULL;
    size_t size,i;
    /*int left,right;*/

    char *splitstring = strdup(" \t");

    /* OPTIONS */
    int o_kerneltype=1;
    int o_verbose=0;

    /* PARAMETERS */
    size_t n=10; /* number of points */
    int k=2; /* number of neighbors */


    /* COMMAND LINE PROCESSING */
    
    /* variables for reading command line options */
    /* ------------------------------------------ */
    int opt;
    /* ------------------------------------------ */
    
    
    /* read the command line */    
    while((opt=getopt_long(argc,argv,"vn:K:k:hF:",gb_long_options, &gb_option_index))!=EOF){
      if(opt==0){
      gbutils_header(argv[0],stdout);
      exit(0);
    }
    else if(opt=='?'){
	fprintf(stderr,"option %c not recognized\n",optopt);
	exit(-1);
      }
      else if(opt=='k'){
	/*the number of neighbors*/
	k= atoi(optarg);
      }
      else if(opt=='n'){
	/*the number of points*/
	n= (size_t) atoi(optarg);
      }
      else if(opt=='K'){
	/*the Kernel to use*/
	o_kerneltype = atoi(optarg);
      }
      else if(opt=='v'){
	/*increase verbosity*/
	o_verbose=1;
      }
      else if(opt=='F'){
	/*set the fields separator string*/
	free(splitstring);
	splitstring = strdup(optarg);
      }
      else if(opt=='h'){
	fprintf(stdout,
		"Nearest neighbors density estimation. Data are read from standard input. The\n");
	fprintf(stdout,
		"density is computed on a regular grid.                                      \n");
	fprintf(stdout,"\nUsage: %s [options]\n\n",argv[0]);
	fprintf(stdout,"Options:\n");
	fprintf(stdout," -n  number of points where the density is computed (default 10)\n");
	fprintf(stdout," -k  number of neighbors points considered (default 2)\n");
	fprintf(stdout," -K  choose the kernel to use: 0 Epanenchnikov, 1 Rectangular\n");
	fprintf(stdout,"     (default 0)\n");
	fprintf(stdout," -v  verbose mode\n");
	fprintf(stdout," -F  specify the input fields separators (default \" \\t\") \n");
	fprintf(stdout," -h  this help\n");
	exit(0);
      }
    }
    /* END OF COMMAND LINE PROCESSING */

    /* initialize global variables */
    initialize_program(argv[0]);

    /* load data */
    load(&data,&size,0,splitstring);

    /*order the values*/
    qsort(data,size,sizeof(double),sort_by_value);

    /* ++++++++++++++++++++++++++++ */
    if(o_verbose == 1){
      /* bandwidth */
      fprintf(stdout,"# neighbors     %d\n",k);
      /* kernel type */
      fprintf(stdout,"kernel     ");
      switch(o_kerneltype){
      case 0:
	fprintf(stdout,"Epanechnikov");
	break;
      case 1:
	fprintf(stdout,"Rectangular");
	break;
      }
      fprintf(stdout,"\n");
      fprintf(stdout,"# bins           %zd\n",n);
      fprintf(stdout,"# observations   %zd\n",size);
      fprintf(stdout,"range      [%.3e,%.3e]\n",data[0],data[size-1]);
    }
    /* ++++++++++++++++++++++++++++ */


    if(o_kerneltype == 1){	/* rectangular */
      const double xlow=data[0];
      const double xhigh=data[size-1];
      const double xstep=(xhigh-xlow)/(n-1);
      for(i=0;i<n;i++){
	const double x = xlow+i*xstep;
	const double h = dk2(data,size,x,k);
	printf(FLOAT_SEP,x);
	printf(FLOAT_NL,k/(2.*h*size));
      }
    }
    else if(o_kerneltype == 0){	/* Epanenchnikov */
      const double xlow=data[0];
      const double xhigh=data[size-1];
      const double xstep=(xhigh-xlow)/(n-1);
      double zmin,zmax,sum;
      size_t j;
      for(i=0;i<n;i++){
	const double x = xlow+i*xstep;
	const double h = dk2(data,size,x,k);
	zmin=x-sqrt(5)*h;
	zmax=x+sqrt(5)*h;
	sum=0;
	for(j=0;j<size;j++){
	  if(data[j]<zmin){
	    continue;
	  }
	  else if (data[j]<=zmax){
	    sum+= Kepanechnikov((x-data[j])/h);
	  }
	  else{
	    break;
	  }
	}
	printf(FLOAT_SEP,x);
	printf(FLOAT_NL,sum/(h*size));
      }
    }
    else{
      fprintf(stdout,"Kernel type not recognized. use -h\n");
      exit(-1);
    }


    exit(0);

}
