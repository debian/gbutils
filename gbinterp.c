/*
  gbinterp (ver. 5.6) -- Compute equispaced curve from interpolated
  data or interpolated values from equispaced data

  Copyright (C) 2007-2018 Giulio Bottazzi

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  (version 2) as published by the Free Software Foundation;
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#include "tools.h"

#include <gsl/gsl_errno.h>
#include <gsl/gsl_spline.h>


int main(int argc,char* argv[]){


  double **data=NULL; /* array of values */
  size_t size=0; /* length of array vals */

  char *splitstring = strdup(" \t");

  gsl_interp_accel *accelerator;
  gsl_interp * interpolator;
  
  size_t n=10; /* number of points */
  double rmin=0,rmax=0; /* boundaries of the range */
  double step; /* distance between the evaluations*/
  double *x,*y; /* array of original x and y vals*/
  double *z=NULL;	/* list of point bwhere the interpolation is printed */
  size_t i;

  int o_verbose=0;
  int o_method=0;
  int o_setrange=0;
  int o_setnum=0;
  int o_output=0;
  int o_outrange=0;

  int o_fromfile=0;
  char *infilename = strdup("");
  int infile;



  /* COMMAND LINE PROCESSING */
  
  /* variables for reading command line options */
  /* ------------------------------------------ */
  int opt;
  /* ------------------------------------------ */
  
  /* read the command line */    
  while((opt=getopt_long(argc,argv,"vn:hr:M:O:F:I:",gb_long_options, &gb_option_index))!=EOF){
    if(opt==0){
      gbutils_header(argv[0],stdout);
      exit(0);
    }
    else if(opt=='?'){
      fprintf(stderr,"option %c not recognized\n",optopt);
      exit(-1);
    }
    else if(opt=='F'){
      /*set the fields separator string*/
      free(splitstring);
      splitstring = strdup(optarg);
    }
    else if(opt=='I'){
      /*set the filename from which x-values are red*/
      o_fromfile=1; 
      free(infilename);
      infilename = strdup(optarg);
    }
    else if(opt=='n'){
      /*set the number of points*/
      n = atoi(optarg);
      o_setnum=1;
    }
    else if(opt=='M'){
      /*set the method to use*/
      o_method = atoi(optarg);
    }
    else if(opt=='O'){
      /*set the type of output */
      o_output = atoi(optarg);
    }
    else if(opt=='r'){
      /*set the range */
      if(!strchr (optarg,',')){
	rmin=rmax=atof(optarg);
      }
      else{
	char *stmp1=strdup (optarg);
	rmin = atof(strtok (stmp1,","));
	rmax = atof(strtok (NULL,","));
	free(stmp1);
      }
      o_setrange=1;
    }
    else if(opt=='v'){
      /*increase verbosity*/
      o_verbose = 1;
    }
    else if(opt=='h'){
      /*print short help*/
      fprintf(stdout,"Compute the interpolating function of user provided data on a regular mesh.\n");
      fprintf(stdout,"Data are read from standard input as couples (X,Y). The interpolation (x,\n");
      fprintf(stdout,"f(x)) is printed for x in [rmin,rmax]. When rmin=rmax print a single value.\n");
      fprintf(stdout,"With option -I the points where the interpolation is computed are read from\n");
      fprintf(stdout,"a file.\n");
      fprintf(stdout,"\nUsage: %s [options]\n\n",argv[0]);
      fprintf(stdout,"Options:\n");
      fprintf(stdout," -n  number of equispaced points (default 10)\n");
      fprintf(stdout," -r  set the range 'rmin,rmax'   (default full x range)\n");
      fprintf(stdout," -I  read interpolation points from a file                 \n");
      fprintf(stdout," -M  the interpolation method: (default 0)\n");
      fprintf(stdout,"      0  linear       (requires at least 2 points)\n");
      fprintf(stdout,"      1  cspline      (requires at least 3 points)\n");
      fprintf(stdout,"      2  Akima spline (requires at least 5 points)\n");
      fprintf(stdout," -O  the type of output: (default 0)\n");
      fprintf(stdout,"      0  interpolation                                      \n");
      fprintf(stdout,"      1  first derivative                                   \n");
      fprintf(stdout,"      2  second derivative                                  \n");
      fprintf(stdout," -F  specify the input fields separators (default \" \\t\") \n");
      fprintf(stdout," -h  print this help\n");
      fprintf(stdout," -v  verbose mode\n");
      exit(0);
    }
  }
  /* END OF COMMAND LINE PROCESSING */

  /* initialize global variables */
  initialize_program(argv[0]);

  /* load data */
  load2(&data,&size,0,splitstring);

  /* sort with respect to first values */
  sort2(data[0],data[1],size,2);

  /* set handy pointers */
  x=data[0];
  y=data[1];

  /* set the grid where the function is computed */
  if(o_fromfile == 0){
    
    if(o_setrange){
      if(rmin<x[0]) rmin=x[0];
      if(rmax>x[size-1]) rmax=x[size-1];
    }
    else{
      rmin=x[0];
      rmax=x[size-1];  
    }

    if(!o_setnum && rmin==rmax) n=1;
    
    if(n>1){
      step=(rmax-rmin)/(n-1.);
    }
    else{
      step=0;
      rmax=rmin = (rmax+rmin)/2;
    }

    /* define the list of output points */
    z= (double *) my_alloc(sizeof(double)*n);
    for(i=0;i<n;i++) z[i]= rmin+step*i;

  }
  else {

    if ( (infile = open(infilename,O_RDONLY)) == -1 ){
      fprintf(stderr,"ERROR (%s): Cannot open file %s\n",argv[0],infilename);
      exit(-1);
    }
    else{
      load(&z,&n,infile,splitstring);
      close(infile);
    }

  }

    

  /* set the method to use */
  switch(o_method){
  case 0:
    interpolator = gsl_interp_alloc (gsl_interp_linear,size);
    break;
  case 1:
    interpolator = gsl_interp_alloc (gsl_interp_cspline,size);
    break;
  case 2:
    interpolator = gsl_interp_alloc (gsl_interp_akima,size);
    break;
  default:
    fprintf(stderr,"ERROR (%s): unknown interpolation method: %d; use %s -h\n",
	    GB_PROGNAME,o_method,argv[0]);
    exit(+1);
  }


  /* ++++++++++++++++++++++++++++ */
  if(o_verbose > 0){
    fprintf(stdout,"#read couples         %zd\n",size);
    fprintf(stdout,"#x support            [%f,%f]\n",x[0],x[size-1]);
    fprintf(stdout,"#grid size            %zd\n",n);
    fprintf(stdout,"#grid range           [%f,%f]\n",rmin,rmax);
    fprintf(stdout,"#interpolation type   %s\n",gsl_interp_name (interpolator));
    fprintf(stdout,"#interpolation output ");
    switch(o_output){
    case 0:
      fprintf(stdout,"value\n");
      break;
    case 1:
      fprintf(stdout,"1st derivative\n");
      break;
    case 2:
      fprintf(stdout,"2nd derivative\n");
      break;
    }
  }
  /* ++++++++++++++++++++++++++++ */


  /* initialize the interpolator */
  gsl_interp_init(interpolator,x,y,size);

  /* allocate the lookup accelerator */
  accelerator=gsl_interp_accel_alloc();

  /* perform evaluation */
  switch(o_output){
  case 0:
    for(i=0;i<n;i++)
      if(z[i] < x[0])
	{
	  o_outrange=1;
	  fprintf(stdout,FLOAT_SEP,z[i]);
	  fprintf(stdout,FLOAT_NL,y[0]);
	}
      else if(z[i] > x[size-1])
	{
	  o_outrange=1;
	  fprintf(stdout,FLOAT_SEP,z[i]);
	  fprintf(stdout,FLOAT_NL,y[size-1]);
	}
      else
	{
	  fprintf(stdout,FLOAT_SEP,z[i]);
	  fprintf(stdout,FLOAT_NL,gsl_interp_eval (interpolator,x,y,z[i],accelerator));
	}
  break;
  case 1:
    for(i=0;i<n;i++)
      if(z[i] < x[0])
	{
	  o_outrange=1;
	  fprintf(stdout,FLOAT_SEP,z[i]);
	  fprintf(stdout,FLOAT_NL,0);
	}
      else if(z[i] > x[size-1])
	{
	  o_outrange=1;
	  fprintf(stdout,FLOAT_SEP,z[i]);
	  fprintf(stdout,FLOAT_NL,0);
	}
      else
	{
	  fprintf(stdout,FLOAT_SEP,z[i]);
	  fprintf(stdout,FLOAT_NL,gsl_interp_eval_deriv (interpolator,x,y,z[i],accelerator));
	}
    break;
  case 2:
    for(i=0;i<n;i++)
      if(z[i] < x[0])
	{
	  o_outrange=1;
	  fprintf(stdout,FLOAT_SEP,z[i]);
	  fprintf(stdout,FLOAT_NL,0);
	}
      else if(z[i] > x[size-1])
	{
	  o_outrange=1;
	  fprintf(stdout,FLOAT_SEP,z[i]);
	  fprintf(stdout,FLOAT_NL,0);
	}
      else
	{
	  fprintf(stdout,FLOAT_SEP,z[i]);
	  fprintf(stdout,FLOAT_NL,gsl_interp_eval_deriv2 (interpolator,x,y,z[i],accelerator));
	}
    break;
  default:
    fprintf(stderr,"ERROR (%s): unknown output type: %d; use %s -h\n",
	    GB_PROGNAME,o_output,argv[0]);
    exit(-1);
  }
  
  if(o_outrange==1)
    fprintf(stderr,"WARNING (%s): some points are outside the input range\n",
	    GB_PROGNAME);


  gsl_interp_accel_free (accelerator);
  gsl_interp_free (interpolator);
  free(x);
  free(y);
  free(z);

  exit(0);
}
