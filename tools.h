#ifndef GBUTILS_H
#define GBUTILS_H 1

/*insert GNU extensions*/
#define _GNU_SOURCE
/*in particular, use of NAN extension*/


/* gnulib headers */
#include "lib/getdelim.h"
#include "lib/getline.h"
#include "lib/getsubopt.h"
#include "lib/strchrnul.h"
/* -------------- */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <float.h>
#include <assert.h>
#include <fcntl.h>
#include <locale.h>
#include <langinfo.h>

#include <getopt.h>

#include "config.h"

#if defined HAVE_LIBGSL
#include <gsl/gsl_errno.h>
#endif

#if defined HAVE_LIBZ

#include <stddef.h>
#include <sys/types.h>
#include <zlib.h>

#endif


#if defined HAVE_LIBZ

#define GBFILEP   gzFile
#define GBGETLINE gzgetline
#define GBFDOPEN  gzdopen
#define GBCLOSE   gzclose
#define GBTELL    gztell

#else

#define GBFILEP   FILE *
#define GBGETLINE getline
#define GBFDOPEN  fdopen
#define GBCLOSE   fclose
#define GBTELL    ftell

#endif


/* used by <errno.h> */
/* extern int errno; */

extern char *GB_PROGNAME;


void initialize_program(const char *);
void finalize_program();

/* variables for reading command line options */
/* ------------------------------------------ */
/* extern char *optarg; */
/* extern int optind, opterr, optopt; */
/* ------------------------------------------ */

/* long options */

extern struct option gb_long_options[];

extern int gb_option_index;


/* Output Management */
extern char *FLOAT,*FLOAT_SEP,*FLOAT_NL;
extern char *EMPTY,*EMPTY_SEP,*EMPTY_NL;
extern char *INT,*INT_SEP,*INT_NL;
extern char *SEP,*NL;

void printline(FILE *,double *,const size_t);
void printmatrixbyrows(FILE *,double **,const size_t,const size_t);
void printmatrixbycols(FILE *,double **,const size_t,const size_t);
void printnomatrixbycols(FILE *,double **,size_t *,const size_t);
void printcol(FILE *,double *,const size_t);

void printmatrixbycols_bin(FILE *,double **,const size_t,const size_t);
void printnomatrixbycols_bin(FILE *,double **,size_t *,const size_t);
void printcol_bin(FILE *,double *,const size_t);
/* ----------------- */

/* Input Management */
extern char GBTHSEP;
extern char GBRADIX;

double mystrtod(const char *);
/* ---------------- */

void *my_alloc(size_t);

void *my_calloc(size_t,size_t);

void *my_realloc(void *,size_t);

char *my_strndup(const char *, size_t);

int blocks_count(long **,size_t *,int);


/* Data loading functions */

int load(double **,size_t *,int,const char*);

int loadtable(double ***,size_t *,size_t *,int,const char*);

int loadblocks(double ****,size_t *,size_t **,size_t **,int,const char *);

int loadtableblocks(double ****,size_t *,size_t *,size_t *,int, const char *);

double ** readblock(size_t *,size_t *,unsigned *,GBFILEP, const char *);

int load2(double ***,size_t *,int,const char*);

int load3(double ***,size_t *,int,const char*);

int load_bin(double **,size_t *,int);

int loadtable_bin(double ***,size_t *,size_t *,int);

int loadblocks_bin(double ****,size_t *,size_t **,size_t **,int);

/* ---------------------- */


/* pointer to data printing function */

extern void (* PRINTMATRIXBYCOLS) (FILE *,double **,const size_t,const size_t);
extern void (* PRINTMATRIXBYROWS) (FILE *,double **,const size_t,const size_t);
extern void (* PRINTNOMATRIXBYCOLS) (FILE *,double **,size_t *,const size_t);
extern void (* PRINTCOL) (FILE *,double *,const size_t);

/* -------------------------------- */

int sort_by_value(const void *, const void *);

void moment(const double [], const size_t, double *, double *,
	    double *, double *, double *, double *,
	    double *,double *);

void moment_nonan(const double [], const size_t, double *, double *,
	    double *, double *, double *, double *,
		  double *, double *, double *);

void moment_short(const double [], const size_t, double *,
		  double *,double *,double *);

void moment_short2(double **, const size_t, 
		   double *, double *,double *,
		   double *,double *);

void moment_short3(double **, const size_t, 
		   double *, double *,double *,
		   double *,double *);

void gbutils_header(char const *,FILE *);

void moment_short_nonan(const double [], const size_t, double *,
			double *,double *,double *, double *);

void denan(double **,size_t *);

void denan_pairs(double **,double **,size_t *);

void denan_data(double ***,size_t *,size_t *);

int sort2(double *, double *, long int, long int);

int sortn(double **, const long int, const long int, const long int, const long int);


/* -------------------------------------- */
/* for cygwin portability */
#ifndef NAN 
#define NAN nan("") 
#endif

/* -------------------------------------- */
/* getline if zlib present                */

#if defined HAVE_LIBZ

#define GETNLINE_NO_LIMIT ((size_t) -1)

/* Read into a buffer *LINEPTR returned from malloc (or NULL),
   pointing to *LINESIZE bytes of space.  Store the input bytes
   starting at *LINEPTR + OFFSET, and null-terminate them.  Reallocate
   the buffer as necessary, but if NMAX is not GETNLINE_NO_LIMIT
   then do not allocate more than NMAX bytes; if the line is longer
   than that, read and discard the extra bytes.  Stop reading after
   after the first occurrence of DELIM1 or DELIM2, whichever comes
   first; a delimiter equal to EOF stands for no delimiter.  Read the
   input bytes from STREAM.
   Return the number of bytes read and stored at *LINEPTR + OFFSET (not
   including the NUL terminator), or -1 on error or EOF.  */
extern ssize_t gzgetndelim2 (char **lineptr, size_t *linesize, size_t offset,
                           size_t nmax, int delim1, int delim2,
                           gzFile stream);

extern ssize_t gzgetline (char **_lineptr, size_t *_linesize, gzFile _stream);

extern ssize_t gzgetdelim (char **_lineptr, size_t *_linesize, int _delimiter,
			  gzFile _stream);

#endif


/* utility functions for kernel analysis */

extern const double Krectangular_K2;
extern const double Klaplace_K2;
extern const double Kgauss_K2;
extern const double Kepanechnikov_K2;

double nearbyint(double); /* for some strange reason this seems required */

double Krectangular(double);

double Krectangular_tilde(double);

double Klaplace(double);

double Klaplace_tilde(double);

double Kgauss(double);

double Kgauss_tilde(double);

double Kepanechnikov(double);

double Kepanechnikov_tilde(double);

double Krectangular2d(double);

double Kepanechnikov2d(double);

double Ksilverman2d_1(double);

double Ksilverman2d_2(double);


/* end GBUTILS_H */
#endif
