/*
  gbker  (ver. 5.6) -- Produce kernel density estimation
  Copyright (C) 2001-2018 Giulio Bottazzi

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  (version 2) as published by the Free Software Foundation;
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/


#include "tools.h"

#if defined HAVE_LIBGSL

#include <gsl/gsl_fft_real.h>
#include <gsl/gsl_fft_halfcomplex.h>
#include <gsl/gsl_fft_real.h>
#include <gsl/gsl_cdf.h>

#endif

int main(int argc,char* argv[]){

  double *data=NULL;
  size_t size;
  
  char *splitstring = strdup(" \t");
  
  /* OPTIONS */
  int o_method=0;
  int o_kerneltype=0;
  int o_setbandwidth=0;
  int o_verbose=0;
  int o_window=0;

  double wmax,wmin;
  
  double ave,sdev,min,max; /* data statistics */
  
  size_t M=64; /* number of points */
    
  double scale=1; /* optional scaling of the smoothing parameter */
  double h=1;/* the smoothing parameter */
  
  double (*K) (double) = Kgauss_tilde; /* the kernel to use */
  
  /* confidence interval */
  int o_pconf = 0;
  double pconf,zconf,sconf;
  
  
  /* COMMAND LINE PROCESSING */
  
  /* variables for reading command line options */
  /* ------------------------------------------ */
  int opt;
  /* ------------------------------------------ */
  
  
  /* read the command line */    
  while((opt=getopt_long(argc,argv,"H:K:n:S:hvM:F:p:w:",gb_long_options, &gb_option_index))!=EOF){
    if(opt==0){
      gbutils_header(argv[0],stdout);
      exit(0);
    }
    else if(opt=='?'){
      fprintf(stderr,"option %c not recognized\n",optopt);
      exit(-1);
    }
    else if(opt=='S'){
      /*set the scale parameter for smoothing*/
      scale=atof(optarg);
    }
    else if(opt=='n'){
      /*the number of bins*/
      M=(int) atoi(optarg);
    }
    else if(opt=='w'){
      /*set the support window */

      char *stmp1=strdup (optarg);
      char *stmp2;
      char *stmp3=stmp1;

      stmp2=strsep (&stmp1,",");
      if(strlen(stmp2)>0) 
	wmin=atof(stmp2);
      if(stmp1 != NULL && strlen(stmp1)>0)
	wmax=atof(stmp1);
      free(stmp3);

      if(wmax<wmin){
	const double dtmp1 = wmax;
	wmax=wmin;
	wmin=dtmp1;
      }

      o_window=1;
    }
    else if(opt=='p'){
      /*set the confidence interval*/
      o_pconf=1;
      pconf = atof(optarg);
      if( pconf <0 || pconf >=1){
	fprintf(stderr,"ERROR (%s): confidence interval %f out of boud\n",
		GB_PROGNAME,pconf);
	exit(1);
      }
      else
	zconf=gsl_cdf_ugaussian_Pinv(1-pconf/2);
    }
    else if(opt=='K'){
      /*the Kernel to use*/
      o_kerneltype = atoi(optarg);
    }
    else if(opt=='H'){
      /*set the kernel bandwidth*/
      o_setbandwidth=1;
      h = atof(optarg);
    }
    else if(opt=='M'){
      /*set the method to use*/
      o_method= atoi(optarg);
    }
    else if(opt=='v'){
      /*increase verbosity*/
      o_verbose=1;
    }
    else if(opt=='F'){
      /*set the fields separator string*/
      free(splitstring);
      splitstring = strdup(optarg);
    }
    else if(opt=='h'){
      /*print short help*/
      fprintf(stdout,"Compute the kernel density estimation on an equispaced grid. Data are read\n");
      fprintf(stdout,"from standard input. The kernel bandwidth, if not provided with the option\n");
      fprintf(stdout,"-H, is set automatically using simple heuristics. Normally the whole support\n");
      fprintf(stdout,"is displayed. The option -w can be used to restrict the computation to specific\n");
      fprintf(stdout,"range of values. The method used to compute the density is set by the option -M.\n");
      fprintf(stdout,"The boundaries of the grid are slightly different in the different cases.\n\n");
      fprintf(stdout,"Usage:  %s [options]                                                      \n\n",argv[0]);
      fprintf(stdout,"Options:\n");
      fprintf(stdout," -n  number of equispaced points/bins where the density is computed (default 64)\n");
      fprintf(stdout," -w  set the grid boundaries with min,max (default whole support)\n");
      fprintf(stdout," -H  set the kernel bandwidth\n");
      fprintf(stdout," -S  scale the automatic kernel bandwidth\n");
      fprintf(stdout," -K  choose the kernel to use  (default 0)                                \n");
      fprintf(stdout,"      0  Epanenchnikov\n");
      fprintf(stdout,"      1  Rectangular\n");
      fprintf(stdout,"      2  Gaussian\n");
      fprintf(stdout,"      3  Laplacian\n");
      fprintf(stdout," -M  choose the method for density computation (default 0)\n");
      fprintf(stdout,"      0  use FFT (# grid points rounded to nearest power of 2)     \n");
      fprintf(stdout,"      1  use discrete convolution (compact kernels only; bins>2)   \n");
      fprintf(stdout,"      2  explicit summation (boundaries excluded)\n");
      fprintf(stdout," -F  specify the input fields separators (default \" \\t\")\n");
      fprintf(stdout,"Examples:\n");
      fprintf(stdout," gbker -n 128 -K 2 < file  use a Gaussian kernel to compute the density on 128\n");
      fprintf(stdout,"                           equispaced points with the FFT approach. All the data\n");
      fprintf(stdout,"                           in 'file' are used, multiple columns are pooled.\n");
      fprintf(stdout," gbker -K 3 -M 2 < file  use a Laplacian kernel to compute the density on 64 points.\n");
      fprintf(stdout,"                         The method used is the explicit summation.\n");
      exit(0);
    }
  }
  /* END OF COMMAND LINE PROCESSING */

  /* initialize global variables */
  initialize_program(argv[0]);
  
  /* load the data */
  load(&data,&size,0,splitstring);
  
  /* sort the data */
  qsort(data,size,sizeof(double),sort_by_value);
  
  /* compute the statistics */
  moment_short(data,size,&ave,&sdev,&min,&max);

  /* correct if windows provided */
  if(o_window==1){
    min=wmin;
    max=wmax;
    }

  /* the parameter h is set for a Gaussian kernel [Silverman p.48] */
  /* if not provided on the command line */
  if(o_setbandwidth == 0){
    const double inter = fabs(data[3*size/4]-data[size/4])/1.34;
    const double A = (sdev>inter && inter>0 ? inter : sdev);
    h=scale*0.9*A/pow(size,.2);
  }
  
  /* nearest power of two if FFT */
  if(o_method==0)
    M = (size_t) pow(2,nearbyint(log((double) M)/M_LN2));
  
  /* ++++++++++++++++++++++++++++ */
  if(o_verbose == 1){
    /* bandwidth */
    fprintf(stdout,"bandwidth  %e ",h);
    if(o_setbandwidth == 0){
      fprintf(stdout,"(automatic");
      if(scale != 1 )
	fprintf(stdout," scaled by %f",scale);
      fprintf(stdout,")");
    }
    else{
      fprintf(stdout,"(provided)");
    }
    fprintf(stdout,"\n");

    /* kernel type */
    fprintf(stdout,"kernel     ");
    switch(o_kerneltype){
    case 0:
      fprintf(stdout,"Epanechnikov");
      break;
    case 1:
      fprintf(stdout,"Rectangular");
      break;
    case 2:
      fprintf(stdout,"Gaussian");
      break;
    case 3:
      fprintf(stdout,"Laplacian");
      break;
    }
    fprintf(stdout,"\n");

    /* confidence interval */
    if(o_pconf == 1){
      fprintf(stdout,"confidence %g\n",pconf);
    }

  }
  /* ++++++++++++++++++++++++++++ */

  /* set the confidence rescaling factor */
  if(o_pconf==1)
    switch(o_kerneltype){
    case 0:
      sconf = zconf*sqrt(Kepanechnikov_K2/(h*size));
      break;
    case 1:
      sconf = zconf*sqrt(Krectangular_K2/(h*size));
      break;
    case 2:
      sconf = zconf*sqrt(Kgauss_K2/(h*size));
      break;
    case 3:
      sconf = zconf*sqrt(Klaplace_K2/(h*size));
      break;
    default:
      fprintf(stderr,"ERROR (%s): unknown kernel; use option -h\n",
	      GB_PROGNAME);
      exit(1);
    }


  if(o_method==0){/*=== FFT methods ===*/

    size_t i;
    double *csi;/* the density */
    
    /* the boundaries of the interval are set according... */
    /*       const double a = min-4*h; */
    /*       const double b = max+4*h; */
    /*       const double delta = (b-a)/(M-1); */
    
    const double margin=.05;
    const double a = min - 4*h - (max-min+4*h)*margin;
    const double b = max + 4*h + (max-min+4*h)*margin;
    const double delta = (b-a)/(M-1);
    
    /* choose the kernel to use */
    switch(o_kerneltype){
    case 0:
      K=Kepanechnikov_tilde;
      break;
    case 1:
      K=Krectangular_tilde;
      break;
    case 2:
      K=Kgauss_tilde;
      break;
    case 3:
      K=Klaplace_tilde;
      break;
    default:
      fprintf(stderr,"ERROR (%s): unknown kernel; use option -h\n",
	      GB_PROGNAME);
      exit(+1);
    }

    /* ++++++++++++++++++++++++++++ */
    if(o_verbose == 1){
      /* method */
      fprintf(stdout,"method     FFT\n");
      /* binning */
      fprintf(stdout,"# bins     %zd\n",M);
      fprintf(stdout,"range      [%.3e,%.3e]\n",a,b);
    }
    /* ++++++++++++++++++++++++++++ */
    
    /* allocate the bins */
    csi = (double *) my_alloc(M*sizeof(double));
    
    /* prepare the binnings */
    for(i=0;i<M;i++){
      csi[i]=0.0;
    }
    
    /* fill the prebinning */
    /* the points are in a+j*delta*/
    for(i=0;i<size;i++){
      const double dtmp1 = (data[i]-a)/delta;
      const int index = floor(dtmp1);
      const double epsilon = dtmp1-index;
      if(index>= (int) M || index<0) continue;
      csi[index]+=1-epsilon;
      if(index+1 <(int) M) csi[index+1]+=epsilon;
      else csi[0]+=epsilon;
    }
    
    /* normalization */
    for(i=0;i<M;i++){
      csi[i]/=delta*size;
    }
    
    
    /* fast Fourier transform */
#if defined HAVE_LIBGSL
    gsl_fft_real_radix2_transform (csi,1,M);
#endif
    
    /* multiply for the kernel */
    for(i=0;i<=M/2;i++){
      csi[i]*=K(h*2*M_PI*i/(b-a));
    }
    for(i=1;i<M/2;i++){
      csi[M-i]*=K(h*2*M_PI*i/(b-a));
    }
    
    /* transform back */
#if defined HAVE_LIBGSL
    gsl_fft_halfcomplex_radix2_inverse (csi,1,M);
#endif
    
    /* output */
    if(o_pconf==0)
      for(i=0;i<M;i++){
	printf(FLOAT_SEP,a+i*delta);
	printf(FLOAT_NL,csi[i]);
      }
    else{
      for(i=0;i<M;i++){
	printf(FLOAT_SEP,a+i*delta);
	printf(FLOAT_SEP,csi[i]);
	printf(FLOAT_NL,sconf*sqrt(fabs(csi[i])));
      }
    }
    
  }
  else if(o_method==1){ /*=== convolution ===*/
    
    double *csi;/* the density */
    
    double sum;
    size_t i;
    
    size_t J;/* maximum index for the kernel */
    double *Kvals;/* values of the kernel */      
    double *Kj;/* pointer to values of the kernel */
    
    
    /* the boundaries of the interval are set to span the interval
       [xmin-.5*delta,xmax+.5*delta] */
    const double delta = (max-min)/((double) M-2.0);
    const double a = min-.5*delta;

    
    /* choose the kernel to use */
    switch(o_kerneltype){
    case 0:
      K=Kepanechnikov;
      J = (int) floor(sqrt(5)*h/delta);
      break;
    case 1:
      K=Krectangular;
      J = (int) floor(h/delta);
      break;
    default:
      fprintf(stderr,"ERROR (%s): kernel unknown or not compact; use option -h\n",
	      GB_PROGNAME);
      exit(+1);
    }
    
    /* ++++++++++++++++++++++++++++ */
    if(o_verbose == 1){
      /* method */
      fprintf(stdout,"method     discrete convolution\n");
      /* binning */
      fprintf(stdout,"# bins     %zd\n",M);
      fprintf(stdout,"range      [%.3e,%.3e]\n",a,a+(M-1)*delta);
      fprintf(stdout,"# K vals   %zd\n",2*J+1);
    }
    /* ++++++++++++++++++++++++++++ */
    
    
    
    /* set the values of the kernel */
    Kvals = (double *) my_alloc((2*J+1)*sizeof(double));
    for(i=0;i<=2*J;i++){
      Kvals[i]=K((i-(double) J)*delta/h)/h;
    }
    /* set pointer to central value */
    Kj = Kvals+J;

    /* ++++++++++++++++++++++++++++ */
    if(o_verbose == 1){
      /* check */
      double sum=0;
      for(i=0;i<=2*J;i++)
	sum+=Kvals[i];
      fprintf(stdout,"sum K vals %.3e\n",sum*delta);
    }
    /* ++++++++++++++++++++++++++++ */
    
    /* allocate the bins */
    csi = (double *) my_alloc(M*sizeof(double));
    
    /* prepare the binning */
    for(i=0;i<M;i++){
      csi[i]=0.0;
    }
    
    /* fill the pre-binning. the points are in a+j*delta. notice that
       ALL the observations must lie between a and a+(M-1)*delta */
    for(i=0;i<size;i++){
      const double dtmp1 = (data[i]-a)/delta;
      const size_t index = (size_t) floor(dtmp1);
      const double epsilon = dtmp1-index;

      if(dtmp1<0 || index >= M-1 )
	fprintf(stderr,"ERROR (%s): problem in grid values; possible bug\n",
		GB_PROGNAME);

      csi[index]+=1-epsilon;
      csi[index+1]+=epsilon;
    }

    /* normalization */
    for(i=0;i<M;i++){
      csi[i]/=size;
    }    

    /* print the result on a grid made of M+2*J points: J equispaced
       points are added at the beginning and at the end of the
       range. */
    if(o_pconf==0)
      for(i=0;i<M+2*J;i++){
	int j;
	const double z = a+((int) i-(int) J)*delta;
	const int jmin = (i>2*J?-J:J-i);
	const int jmax = (i<M-1?J:M-1+J-i);
	sum=0;
	for(j=jmin;j<=jmax;j++){
	  sum+=Kj[j]*csi[i-J+j];
	}
	printf(FLOAT_SEP,z);
	printf(FLOAT_NL,sum);
      }
    else
      for(i=0;i<M+2*J;i++){
	int j;
	const double z = a+((int) i-(int) J)*delta;
	const int jmin = (i>2*J?-J:J-i);
	const int jmax = (i<M-1?J:M-1+J-i);
	sum=0;
	for(j=jmin;j<=jmax;j++){
	  sum+=Kj[j]*csi[i-J+j];
	}
	printf(FLOAT_SEP,z);
	printf(FLOAT_SEP,sum);
	printf(FLOAT_NL,sconf*sqrt(fabs(sum)));
      }
    
  }
  else if(o_method==2){ /*=== Exact Summation ===*/
    
    /* the boundaries of the interval are set according... */
    const double delta = (max-min)/(M+1);
    const double a = min+delta;
        
    /* ++++++++++++++++++++++++++++ */
    if(o_verbose == 1){
      /* method */
      fprintf(stdout,"method      exact summation\n");
      /* binning */
      fprintf(stdout,"# points      %zd\n",M);
      fprintf(stdout,"range       [%.3e,%.3e]\n",a,a+(M-1)*delta);
      fprintf(stdout,"step        %e\n",delta);
    }
    /* ++++++++++++++++++++++++++++ */
    
    switch(o_kerneltype){
    case 0:{/* Epanechnikov */
      
      double sum;
      int i;
      size_t j;

      K=Kepanechnikov;
      for(i=0;i<M;i++){
	const double z = a+i*delta;
	const double zmin = z-sqrt(5)*h;
	const double zmax = z+sqrt(5)*h;

	sum=0;
	for(j=0;j<size;j++){
	  if(data[j]<zmin){
	    continue;
	  }
	  else if (data[j]<=zmax){
	    sum+= K((z-data[j])/h);
	  }
	  else{
	    break;
	  }
	}
	sum /= h*size;

	printf(FLOAT_SEP,z);
	if(o_pconf==0)
	  printf(FLOAT_NL,sum);
	else{
	  printf(FLOAT_SEP,sum);
	  printf(FLOAT_NL,sconf*sqrt(fabs(sum)));
	}
      }

      break;
    }
    case 1:{/* Rectangular */

      double sum;
      int i;
      size_t j;
      
      K=Krectangular;
      for(i=0;i<M;i++){
	const double z = a+i*delta;
	const double zmin = z-h;
	const double zmax = z+h;
	sum=0.0;
	for(j=0;j<size;j++){
	  if(data[j]<=zmin){
	    continue;
	  }
	  else if (data[j]<=zmax){
	    sum+= K((z-data[j])/h);
	  }
	  else{
	    break;
	  }
	}
	sum /= h*size;

	printf(FLOAT_SEP,z);
	if(o_pconf==0)
	  printf(FLOAT_NL,sum);
	else{
	  printf(FLOAT_SEP,sum);
	  printf(FLOAT_NL,sconf*sqrt(fabs(sum)));
	}
      }
      break;
    }
    case 2:{/* Gaussian */
      
      double sum;
      int i;
      size_t j;
      
      K=Kgauss;	
      for(i=0;i<M;i++){
	const double z = a+i*delta;
	sum=0;
	for(j=0;j<size;j++){
	  sum+= K((z-data[j])/h);
	}
	sum /= h*size;

	printf(FLOAT_SEP,z);
	if(o_pconf==0)
	  printf(FLOAT_NL,sum);
	else{
	  printf(FLOAT_SEP,sum);
	  printf(FLOAT_NL,sconf*sqrt(fabs(sum)));
	}
      }
      
      break;
    }
    case 3:{/* Laplacian */
      
      double sum;
      int i;
      size_t j;
      
      K=Klaplace;	
      for(i=0;i<M;i++){
	const double z = a+i*delta;
	sum=0;
	for(j=0;j<size;j++){
	  sum+= K((z-data[j])/h);
	}
	sum /= h*size;

	printf(FLOAT_SEP,z);
	if(o_pconf==0)
	  printf(FLOAT_NL,sum);
	else{
	  printf(FLOAT_SEP,sum);
	  printf(FLOAT_NL,sconf*sqrt(fabs(sum)));
	}
      }
      
      break;
    }
    default:
      fprintf(stderr,"ERROR (%s): unknown kernel; use option -h\n",
	      GB_PROGNAME);
      exit(+1);
    }
    
  }
  else{/*=== unknown method ===*/
    fprintf(stderr,"ERROR (%s): unknown method; use option -h\n",
	    GB_PROGNAME);
    exit(+1);
  }
  
  
  exit(0);
}
