/*
  gbalafit (ver. 1.2.1) -- Fit an asymmetric Laplace (double exponential) density

  Copyright (C) 2018 Giulio Bottazzi

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  (version 2) as published by the Free Software Foundation;
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/


/*
  Verbosity levels:
  0 just the final ouput
  1 the results of intermediate steps
  2 internal information on intermediate steps
  3 gory details on intermediate steps
*/


#include "tools.h"
#include "multimin.h"
#include "assert.h"


/* Global Variables */
/* ---------------- */

/* store data */
double *Data; /*the array of data*/
size_t Size;/*the number of data*/

/* output management */
FILE *Fmessages;/*stream for computation messages*/
FILE *Fparams;/*strem for final parameters*/
FILE *Ffitted;/*strem for distribution or density*/

/*------------------*/


/* Output Functions */
/*----------------- */

void printcumul(const double m, const double al, const double ar){

  int i;
  double dtmp1; 

  const double Asum=al+ar;

  for(i=0;i<Size;i++){
    if(Data[i]>m){
      dtmp1=(Data[i]-m)/ar;
      dtmp1=1-exp(-dtmp1)*ar/Asum;
     }
    else{
      dtmp1=(m-Data[i])/al;
      dtmp1=exp(-dtmp1)*al/Asum;
    }
    fprintf(Ffitted,FLOAT_SEP,Data[i]);
    fprintf(Ffitted,FLOAT_NL,dtmp1);
  }

}

void printdensity(const double m, const double al, const double ar){

  int i;

  const double norm=al+ar;

  for(i=0;i<Size;i++){
    double dtmp1=Data[i];
    fprintf(Ffitted,FLOAT_SEP,dtmp1);
    dtmp1=dtmp1-m;
    if(dtmp1>=0){
      fprintf(Ffitted,FLOAT_NL,exp(-dtmp1/ar)/norm);
    }
    else{
      fprintf(Ffitted,FLOAT_NL,exp( dtmp1/al)/norm);
    }
  }
  
}
/*----------------- */


/* Object Function */
/*---------------- */

double nll(const double m){

  unsigned utmp1;

  double sumL=0.0;
  double sumR=0.0;


/*   fprintf(Fmessages,"#objf bl=%.3e br=%.3e al=%.3e ar=%.3e m=%.3e\n", */
/* 	  x[0],x[1],x[2],x[3],x[4]); */

  for(utmp1=0;utmp1<Size;utmp1++){
    if(Data[utmp1]>m) break;
    sumL+=m-Data[utmp1];
  }
  for(;utmp1<Size;utmp1++){
    sumR+=Data[utmp1]-m;
  }

  sumL = sqrt(sumL/Size);
  sumR = sqrt(sumR/Size);

  return 2*log(sumL+sumR)+1.;

}


int main(int argc,char* argv[]){
  
  /* store guess */
  double m,al,ar;
  
  /* store loglike */
  double fmin;

  /* store possibly provided values for parameters */
  double provided_m=0;
  unsigned is_m_provided=0;

  /* set various options */
  unsigned O_verbose=0;
  unsigned O_output=0;

  /* range for option -O3 */
  double xmin=0.0,xmax=0.0;
  unsigned steps=10;

  char *splitstring = strdup(" \t");


  /* COMMAND LINE PROCESSING */
  /* ----------------------- */ 
  int opt;
  
  /* read the command line */    
  while((opt=getopt_long(argc,argv,"n:O:m:V:x:h",gb_long_options, &gb_option_index))!=EOF){
    if(opt==0){
      gbutils_header(argv[0],stdout);
      exit(0);
    }
    else if(opt=='?'){
      fprintf(stderr,"option %c not recognized\n",optopt);
      return(-1);
    }
    else if(opt=='h'){
      /*print short help*/
      fprintf(stdout,"Fit asymmetric Laplace density. Read data from files or from standard input  \n");
      fprintf(stdout,"With output option '4' prints the log-likelihood as a function of m   \n");
      fprintf(stdout,"Usage: %s [options] [files]\n\n",argv[0]);
      fprintf(stdout,"input.                                                             \n\n");
      fprintf(stdout," Options:                                                            \n");
      fprintf(stdout," -O  output type (default 0)                \n");
      fprintf(stdout,"      0  parameters m al ar and negative log-likelihood\n");
      fprintf(stdout,"      1  the estimated distribution function computed on the provided points     \n");
      fprintf(stdout,"      2  the estimated density function computed on the provided points \n");
      fprintf(stdout,"      3  parameters m,al,ar their standard errors and correlations \n");
      fprintf(stdout,"      4  log-likelihood profile \n");
      fprintf(stdout," -m  the mode is not estimated but is set to the value provided\n");
      fprintf(stdout," -x  initial value of m or plot range if output type is '4' (default 0)\n");
      fprintf(stdout," -n  number of plotted points if output type is '4' (default 10)\n");
      fprintf(stdout," -V  verbosity level (default 0)           \n");
      fprintf(stdout,"      0  just the final result        \n");
      fprintf(stdout,"      1  headings and summary table   \n");
      fprintf(stdout,"      2  intermediate steps results   \n");
      fprintf(stdout,"Examples:\n");
      fprintf(stdout," gbalafit -m 1 <file  estimate al and ar with m=1\n");
      fprintf(stdout," gbalafit -O 4 -x -1,1 <file  print the log-likelihood on a regular grid\n");
      fprintf(stdout,"                              from -1 to 1. The grid has 10 points. \n");
      exit(0);
    }
    else if(opt=='V'){
      O_verbose=atoi(optarg);
    }
    else if(opt=='O'){
      O_output=atoi(optarg);
    }
    else if(opt=='m'){
      provided_m=atof(optarg);
      is_m_provided=1;
    }
    else if(opt=='x'){
      char *stmp1=strdup (optarg);
      char *stmp2;
      char *stmp3=stmp1;
      if( (stmp2=strsep (&stmp1,",")) && stmp1 != NULL ){
	if(strlen(stmp2)>0) 
	  xmin=atof(stmp2);
	xmax=atof(stmp1);
      }
      else{
	xmin=xmax=m=atof(optarg);
      }
      free(stmp3);
    }
    else if(opt=='n'){
      steps = atoi(optarg);
    }
    else if(opt=='F'){
      /*set the fields separator string*/
      free(splitstring);
      splitstring = strdup(optarg);
    }
  }

  /* initialize global variables */
  initialize_program(argv[0]);

  /* customized admin. of errors */
  /* --------------------------- */
  gsl_set_error_handler_off ();

  
  /* set the various streams */
  /* ----------------------- */
  if(O_output==0){
    Fmessages=stderr;
    Fparams=stdout;
    Ffitted=stderr;
  }
  else{
    Fmessages=stderr;
    Fparams=stderr;
    Ffitted=stdout;
  }

  /* load Data */
  /* --------- */
  if(O_verbose>=2)
    fprintf(Fmessages,"#--- LOADING DATA\n");
  
  load(&Data,&Size,0,splitstring);

  if (Size <= 1) {
    fprintf(Fmessages,"the number of onservations must be at least 2\n");
    exit(-1);
  }

  /* sort data */
  /* --------- */
  qsort(Data,Size,sizeof(double),sort_by_value);


  /* ---------------------- */
  if(O_output<4){

    /* store Fisher sqrt(variance-covariance)*/
    /* sigma[0] = std.err on m */
    /* sigma[1] = std.err on al */
    /* sigma[2] = std.err on ar */
    /* sigma[3] = corr.coeff m vs. al */
    /* sigma[4] = corr.coeff m vs. ar */
    double sigma[5];

    size_t i;
    double sl=0,sr=0;

    /* decide the value of m  */
    if (is_m_provided){
      m = provided_m;
    }
    else{
      size_t minindex;
    
      fmin=DBL_MAX;
      for(i=1;i<Size-1;i++){
	const double dtmp1 = nll(Data[i]);
	if(dtmp1<fmin) {
	  fmin=dtmp1;
	  minindex = i;
	}
/* 	printf("%d mll[%e]=%e [%e] ",i,Data[i],dtmp1,fmin); */
/* 	if(minindex == i) */
/* 	  printf("*"); */
/* 	printf("\n"); */
      }
      m =  Data[minindex];
    }

    /* decide the value of al and ar  */
    for(i=0;i<Size;i++){
      if(Data[i]>m) break;      
      sl+=m-Data[i];
    }
    sl/=Size;
      
    for(;i<Size;i++){
      sr+=Data[i]-m;
    }
    sr/=Size;
      
    al=sl+sqrt(sl*sr);
    ar=sr+sqrt(sl*sr);

    /* compute variance-covariance  */
    sigma[0] = 2*al*ar;
    sigma[1] = al*(al+ar);
    sigma[2] = ar*(al+ar);
    
    sigma[3] = 1./sqrt(2.*ar*(al+ar));
    sigma[4] = -1./sqrt(2.*al*(al+ar));

    if(O_verbose>=1){
      fprintf(Fmessages,"#\n");
      fprintf(Fmessages,"#--- FINAL RESULT -----------------------------------\n");
      fprintf(Fmessages,"#                           | correlation matrix\n");
      fprintf(Fmessages,"#      value     std.err    |  m       al     ar\n");
      fprintf(Fmessages,"#m  = %- 10.4g %-10.4g | % .4f % .4f % .4f\n",
	      m,sigma[0]/sqrt(Size),1.,sigma[3],sigma[4]);
      fprintf(Fmessages,"#al = %- 10.4g %-10.4g | % .4f % .4f % .4f\n",
	      al,sigma[1]/sqrt(Size),sigma[3],1.,0.);
      fprintf(Fmessages,"#ar = %- 10.4g %-10.4g | % .4f % .4f % .4f\n",
	      ar,sigma[2]/sqrt(Size),sigma[4],0.,1.);
      fprintf(Fmessages,"#----------------------------------------------------\n");
    }


    
    /* print result */
    switch(O_output){
    case 0: /* print parameters b,a,m, and log-likelihood */
      if(O_verbose>=1){
	fprintf(Fmessages,"#");
	fprintf(Fmessages,EMPTY_SEP,"m");
	fprintf(Fmessages,EMPTY_SEP,"al");
	fprintf(Fmessages,EMPTY_SEP,"ar");
	fprintf(Fmessages,EMPTY_NL,"log-like");
      }
      fprintf(Fparams,FLOAT_SEP,m);
      fprintf(Fparams,FLOAT_SEP,al);
      fprintf(Fparams,FLOAT_SEP,ar);
      fprintf(Fparams,FLOAT_NL,fmin);
      break;
    case 1: /* print distribution function */
      if(O_verbose>=1){
	fprintf(Fmessages,"#");
	fprintf(Fmessages,EMPTY_SEP,"m");
	fprintf(Fmessages,EMPTY_SEP,"al");
	fprintf(Fmessages,EMPTY_SEP,"ar");
	fprintf(Fmessages,EMPTY_NL,"log-like");
      }
      printcumul(m,al,ar);
    break;
    case 2: /* print density */
      if(O_verbose==0){
	fprintf(Fmessages,"#");
	fprintf(Fmessages,EMPTY_SEP,"m");
	fprintf(Fmessages,EMPTY_SEP,"al");
	fprintf(Fmessages,EMPTY_SEP,"ar");
	fprintf(Fmessages,EMPTY_NL,"log-like");
      }
      printdensity(m,al,ar);
      break;
    case 3: /* print parameters final estimates and standard errors*/
      if(O_verbose>=1){
	fprintf(Fmessages,"#");
	fprintf(Fmessages,EMPTY_SEP,"m");
	fprintf(Fmessages,EMPTY_SEP,"al");
	fprintf(Fmessages,EMPTY_SEP,"ar");
	fprintf(Fmessages,EMPTY_SEP,"sigma_m");
	fprintf(Fmessages,EMPTY_SEP,"sigma_al");
	fprintf(Fmessages,EMPTY_SEP,"sima_ar");
	fprintf(Fmessages,EMPTY_SEP,"corr_m_al");
	fprintf(Fmessages,EMPTY_SEP,"corr_m_ar");
	fprintf(Fmessages,EMPTY_NL,"log-like");
      }
      fprintf(Fparams,FLOAT_SEP,m);
      fprintf(Fparams,FLOAT_SEP,al);
      fprintf(Fparams,FLOAT_SEP,ar);
      fprintf(Fparams,FLOAT_SEP,sigma[0]/sqrt(Size));
      fprintf(Fparams,FLOAT_SEP,sigma[1]/sqrt(Size));
      fprintf(Fparams,FLOAT_SEP,sigma[2]/sqrt(Size));
      fprintf(Fparams,FLOAT_SEP,sigma[3]);
      fprintf(Fparams,FLOAT_NL,sigma[4]);
      break;
    }

  }
  else if(O_output==4){/* print the negative log-likelihood */

    size_t i;

    if(xmin==xmax) steps = 1;

    for(i=0;i<steps;i++){
      const double val=xmin*(1.-i/((double) steps -1))+ xmax*i/((double) steps -1);
      printf("%f %f\n",val,nll(val));
    }

    printf("\n\n");

    for(i=0;i<Size;i++){
      const double val=Data[i];
      printf("%f %f\n",val,nll(val));
    }

  }

  free(Data);
  free(splitstring);
  exit(0);

}
