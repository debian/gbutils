/*
  tools.c (ver. 5.4.5) -- Various utilities functions for gbutils
  Copyright (C) 2001-2018 Giulio Bottazzi

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  (version 2) as published by the Free Software Foundation;
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#include "tools.h"

/* ======================= */
/* PROGRAMS INITIALIZATION */
/* ======================= */

char *GB_PROGNAME=NULL;
char *FLOAT=NULL,*FLOAT_SEP=NULL,*FLOAT_NL=NULL;
char *EMPTY=NULL,*EMPTY_SEP=NULL,*EMPTY_NL=NULL;
char *INT=NULL,*INT_SEP=NULL,*INT_NL=NULL;
char *SEP=NULL,*NL=NULL;

/* locale parsing of thousand */
char LCTHSEP='\0';


void initialize_program(const char *progname){

  GB_PROGNAME=strdup(progname);

  /* set locale according to environment variables and thousands
     separator according to locale*/
  if (setlocale(LC_NUMERIC,"") == NULL)
    fprintf(stderr,"WARNING:Cannot set LC_NUMERIC to default locale\n");
  else{
/*     char *thsep=nl_langinfo (THOUSANDS_SEP); */
    char *thsep=nl_langinfo (THOUSEP);
    if(strlen(thsep)>0)
      LCTHSEP=thsep[0];
  }

  /* read the environment */
  if(FLOAT==NULL && getenv("GB_OUT_FLOAT_FORMAT"))
    FLOAT = strdup(getenv("GB_OUT_FLOAT_FORMAT"));
  if(EMPTY==NULL && getenv("GB_OUT_EMPTY_FORMAT"))
    EMPTY = strdup(getenv("GB_OUT_EMPTY_FORMAT"));
  if(SEP==NULL && getenv("GB_OUT_SEP"))
    SEP = strdup(getenv("GB_OUT_SEP"));
  if(NL==NULL && getenv("GB_OUT_NL"))
    NL = strdup(getenv("GB_OUT_NL"));
  
  /* set the variables */
  if(FLOAT==NULL && EMPTY==NULL){

    FLOAT=strdup("% 12.6e");
    EMPTY = strdup("%-13s");
    INT = strdup("% 13d");

  }
  else if(FLOAT!=NULL && EMPTY==NULL){
    
    int length,itmp1;
    
    length=snprintf(NULL,0,FLOAT,M_PI);
    if(length < 0){
      fprintf(stderr,"ERROR (%s): wrong output specification: %s\n",
	      GB_PROGNAME,FLOAT);
      exit(-1);
    }

    itmp1 = snprintf(NULL,0,"%% %dd",length);
    if(itmp1<0){
      fprintf(stderr,"ERROR (%s): wrong output specification: %s\n",
	      GB_PROGNAME,FLOAT);
      exit(-1);
    }
    INT = (char *) my_alloc((itmp1+1)*sizeof(char));
    snprintf(INT,itmp1+1,"%% %dd",length);


    itmp1 = snprintf(NULL,0,"%%-%ds",length);
    if(itmp1<0){
      fprintf(stderr,"ERROR (%s): wrong output specification: %s\n",
	      GB_PROGNAME,FLOAT);
      exit(-1);
    }
    EMPTY = (char *) my_alloc((itmp1+1)*sizeof(char));
    snprintf(EMPTY,itmp1+1,"%%-%ds",length);

  }
  else if(FLOAT==NULL && EMPTY!=NULL){

    FLOAT=strdup("% 12.6e");
    INT = strdup("% 13d");

  }
  else{

    int length,itmp1;

    length=snprintf(NULL,0,FLOAT,M_PI);
    if(length < 0){
      fprintf(stderr,"ERROR (%s): wrong output specification: %s\n",
	      GB_PROGNAME,FLOAT);
      exit(-1);
    }

    itmp1 = snprintf(NULL,0,"%% %dd",length);
    if(itmp1<0){
      fprintf(stderr,"ERROR (%s): wrong output specification: %s\n",
	      GB_PROGNAME,FLOAT);
      exit(-1);
    }
    INT = (char *) my_alloc((itmp1+1)*sizeof(char));
    snprintf(INT,itmp1+1,"%% %dd",length);

  }

  if(SEP==NULL)
      SEP = strdup(" ");

  if(NL==NULL)
      NL = strdup("\n");

/*   fprintf(stderr,"FLOAT='%s' (%d) SEP='%s' (%d)\n", */
/* 	  FLOAT,strlen(FLOAT),SEP,strlen(SEP)); */


  FLOAT_SEP = (char *) my_alloc((strlen(FLOAT)+strlen(SEP)+1)*sizeof(char));
  strcpy(FLOAT_SEP,FLOAT);
  strcat(FLOAT_SEP,SEP);

  FLOAT_NL=(char *) my_alloc((strlen(FLOAT)+3)*sizeof(char));
  strcpy(FLOAT_NL,FLOAT);
  strcat(FLOAT_NL,NL);

  EMPTY_SEP=(char *) my_alloc((strlen(EMPTY)+strlen(SEP)+1)*sizeof(char));
  strcpy(EMPTY_SEP,EMPTY);
  strcat(EMPTY_SEP,SEP);

  EMPTY_NL=(char *) my_alloc((strlen(EMPTY)+3)*sizeof(char));
  strcpy(EMPTY_NL,EMPTY);
  strcat(EMPTY_NL,NL);

  INT_SEP = (char *) my_alloc((strlen(INT)+strlen(SEP)+1)*sizeof(char));
  strcpy(INT_SEP,INT);
  strcat(INT_SEP,SEP);

  INT_NL=(char *) my_alloc((strlen(INT)+3)*sizeof(char));
  strcpy(INT_NL,INT);
  strcat(INT_NL,NL);

#if defined HAVE_LIBGSL
  /* suspend error handling */
  if(getenv("GB_ERROR_HANDLER_OFF"))
    gsl_set_error_handler_off();
#endif


}


void finalize_program(){

  free(GB_PROGNAME);
  free(FLOAT); free(EMPTY); free(SEP); free(NL); free(INT);
  free(FLOAT_SEP); free(EMPTY_SEP); free(INT_SEP);
  free(FLOAT_NL); free(EMPTY_NL); free(INT_NL);

}


/* long options */

struct option gb_long_options[] = {
  {"version", no_argument,       NULL,  0 },
  {"help", no_argument,       NULL,  'h' },
  {0,         0,                 0,  0 }
};

int gb_option_index = 0;




/* ================= */
/* UTILITY FUNCTIONS */
/* ================= */


double mystrtod(const char *nptr){

  /* pointer to end of interpreted string */
  char *endptr;

  double res;

  if(LCTHSEP != '\0' && strchr(nptr,LCTHSEP) != NULL ){
    char *new = strdup(nptr);
    char *src = new;
    char *dst = new;
    
    do
      {
	while (*src == LCTHSEP)
	  ++src;
      }
    while ((*dst++ = *src++) != '\0');

    res=strtod(new,&endptr);

    free(new);

  }
  else
    res = strtod(nptr,&endptr);


  if(*endptr != '\0'){
    fprintf(stderr,"WARNING (%s) : error on conversion: %s -> %e; generate a NAN\n",GB_PROGNAME,nptr,res);
    res=NAN;
  }

  return res;
}


void *my_alloc(size_t size){
    void *temp;
    if(!(temp = malloc(size))){
	perror("malloc, memory allocation failed");
	exit(1);
    }
    return temp;
}

void *my_calloc(size_t length,size_t size){
    void *temp;
    if(!(temp = calloc(length,size))){
	perror("calloc, memory allocation failed");
	exit(1);
    }
    return temp;
}

void *my_realloc(void *ptr,size_t size){
  void *temp=NULL;
  if(size == 0){
    free(ptr);
  }
  else if(!(temp = realloc(ptr,size))){
    perror("realloc, memory allocation failed");
    exit(1);
  }
  return temp;
}

/* replacement for systems without a strndup function  */
/* copyright by Stefan Soucek ssoucek at coactive.com */

char *my_strndup(const char *str, size_t len)
{
	register size_t n;
	register char *dst;

	n = strlen(str);
	if (len < n)
    	        n = len;
	dst = (char *) my_alloc(n+1);
	if (dst) {
	        memcpy(dst, str, n);
		dst[n] = '\0';
	}
	return dst;
}



/* ================ */
/* OUTPUT FUNCTIONS */
/* ================ */

/* default ASCII (or ASCII.gz) functions */

void (* PRINTMATRIXBYCOLS) (FILE *,double **,const size_t,const size_t) = printmatrixbycols;
void (* PRINTMATRIXBYROWS) (FILE *,double **,const size_t,const size_t) = printmatrixbyrows;
void (* PRINTNOMATRIXBYCOLS) (FILE *,double **,size_t *,const size_t) = printnomatrixbycols;
void (* PRINTCOL) (FILE *,double *,const size_t) = printcol;


void printline(FILE *out,double * data,const size_t size){

  size_t i;

  for(i=0;i<size-1;i++)
    fprintf(out,FLOAT_SEP,data[i]);

  fprintf(out,FLOAT_NL,data[size-1]);
}

void printcol(FILE *out,double * data,const size_t size){

  size_t i;

  for(i=0;i<size;i++)
    fprintf(out,FLOAT_NL,data[i]);
}


/* print matrix rows x columns */
void printmatrixbyrows(FILE *out,double ** data,const size_t rows,const size_t columns){

  size_t i,j;

  if( strcspn (FLOAT,"fge") < strlen(FLOAT) ){
    for(j=0;j<rows;j++){
      for(i=0;i<columns-1;i++)
	fprintf(out,FLOAT_SEP,data[j][i]);
      fprintf(out,FLOAT_NL,data[j][columns-1]);
    }
  }
  else if( strcspn (FLOAT,"d") < strlen(FLOAT) ){
    for(j=0;j<rows;j++){
      for(i=0;i<columns-1;i++)
	fprintf(out,FLOAT_SEP,(int) floor(data[j][i]));
      fprintf(out,FLOAT_NL,(int) floor(data[j][columns-1]));
    }
  }
  else{
    fprintf(stderr,"ERROR (%s): wrong output specification: %s\n",
	    GB_PROGNAME,FLOAT);
    exit(-1);
  }
}

/* print matrix columns x rows */
void printmatrixbycols(FILE *out,double ** data,const size_t rows,const size_t columns){

  size_t i,j;

  if( strcspn (FLOAT,"fge") < strlen(FLOAT) ){
    for(j=0;j<rows;j++){
      for(i=0;i<columns-1;i++)
	fprintf(out,FLOAT_SEP,data[i][j]);
      fprintf(out,FLOAT_NL,data[columns-1][j]);
    }
  }
  else if( strcspn (FLOAT,"d") < strlen(FLOAT) ){
    for(j=0;j<rows;j++){
      for(i=0;i<columns-1;i++)
	fprintf(out,FLOAT_SEP,(int) floor(data[i][j]));
      fprintf(out,FLOAT_NL, (int) floor(data[columns-1][j]));
    }
  }
  else{
    fprintf(stderr,"ERROR (%s): wrong output specification: %s\n",
	    GB_PROGNAME,FLOAT);
    exit(-1);
  }

}

/* print generic data not in matrix form */
void
printnomatrixbycols(FILE *out,double **data,size_t *rows,const size_t columns){

  size_t maxrows;
  size_t i,j;

  /*determine the maximum index range*/
  maxrows=0;
  for(j=0;j<columns;j++)
    if(rows[j] > maxrows) maxrows=rows[j];
  
  /* print the result */
  if( strcspn (FLOAT,"fge") < strlen(FLOAT) ){
    for(i=0;i<maxrows;i++){
      for(j=0;j<columns-1;j++){
	if(i < rows[j])
	  fprintf(out,FLOAT_SEP,data[j][i]);
	else
	  fprintf(out,EMPTY_SEP," ");
      }
      if(i < rows[columns-1])
	fprintf(out,FLOAT_NL,data[columns-1][i]);
      else
	fprintf(out,EMPTY_NL," ");
    }
  }
  else if( strcspn (FLOAT,"d") < strlen(FLOAT) ){
    for(i=0;i<maxrows;i++){
      for(j=0;j<columns-1;j++){
	if(i < rows[j])
	  fprintf(out,FLOAT_SEP,(int) floor(data[j][i]));
	else
	  fprintf(out,EMPTY_SEP," ");
      }
      if(i < rows[columns-1])
	fprintf(out,FLOAT_NL,(int) floor(data[columns-1][i]));
      else
	fprintf(out,EMPTY_NL," ");
    }
  }
  else{
    fprintf(stderr,"ERROR (%s): wrong output specification: %s\n",GB_PROGNAME,FLOAT);
    exit(-1);
  }
  
}


/* print matrix columns x rows, binary version */
void printmatrixbycols_bin(FILE *out,double ** data,const size_t rows,const size_t columns){

  size_t i;


  /* write number of columns */
  if( fwrite(&columns, sizeof(size_t),1,out) != 1){
    fprintf(stderr,"ERROR (%s): binary output failed\n",
	    GB_PROGNAME);
    exit(-1);
  }

  /* write number of rows */
  for(i=0;i<columns;i++)
    if( fwrite(&rows, sizeof(size_t),1,out) != 1){
      fprintf(stderr,"ERROR (%s): binary output failed\n",
	      GB_PROGNAME);
      exit(-1);
    }

  /* write data column after column*/
  for(i=0;i<columns;i++)
    if( fwrite(data[i], sizeof(double),rows,out) != rows){
      fprintf(stderr,"ERROR (%s): binary output failed\n",
	      GB_PROGNAME);
      exit(-1);
    }

}


/* print generic data not in matrix form, binary version */
void
printnomatrixbycols_bin(FILE *out,double **data,size_t *rows,const size_t columns){

  size_t i;


  /* write number of columns */
  if( fwrite(&columns, sizeof(size_t),1,out) != 1){
    fprintf(stderr,"ERROR (%s): binary output failed\n",
	    GB_PROGNAME);
    exit(-1);
  }

  /* write number of rows */
  for(i=0;i<columns;i++)
    if( fwrite(rows+i, sizeof(size_t),1,out) != 1){
      fprintf(stderr,"ERROR (%s): binary output failed\n",
	      GB_PROGNAME);
      exit(-1);
    }

  /* write data column after column*/
  for(i=0;i<columns;i++)
    if( fwrite(data[i], sizeof(double),rows[i],out) != rows[i]){
      fprintf(stderr,"ERROR (%s): binary output failed\n",
	      GB_PROGNAME);
      exit(-1);
    }

}


/* print matrix columns x rows, binary version */
void printcol_bin(FILE *out,double * data,const size_t rows){

  const size_t columns=1;

  /* write number of columns, which is 1 */
  if( fwrite(&columns, sizeof(size_t),1,out) != 1){
    fprintf(stderr,"ERROR (%s): binary output failed\n",
	    GB_PROGNAME);
    exit(-1);
  }

  /* write number of rows, which is 'rows' */
  if( fwrite(&rows, sizeof(size_t),1,out) != 1){
    fprintf(stderr,"ERROR (%s): binary output failed\n",
	    GB_PROGNAME);
    exit(-1);
  }

  /* write the data column */
  if( fwrite(data, sizeof(double),rows,out) != rows){
    fprintf(stderr,"ERROR (%s): binary output failed\n",
	    GB_PROGNAME);
    exit(-1);
  }

}



/* =============== */
/* INPUT FUNCTIONS */
/* =============== */

/*

  Summary of loading functions:

  readblock: read a single block; the shape is decided by the data if
  both rows and columns are set to zero. Otherwise only the provided
  number of columns or rows is read. Data are separated by
  'delimiters'. The number of lines read is also set.
  
  loadblocks: read a series of blocks with possibly different columns
  and rows

  loadtableblocks: read a series of blocks with the SAME number of
  columns and rows

  readblock  <- load: read the first block as an array
             <- loadtable: read fist block as a matrix
	     <- load2: read first two columns of the first block 
	     <- load3: read first three columns of the first block 

  for the binary format:


  myfread: utility function to read binary data

  load_bin: read the first block as an array

  loadblocks_bin: read the entire structure of blocks

  loadtable_bin: read data in a single table ignoring block structure

 */



/* ----------------------------------------------------- */

double ** readblock(size_t *rows,size_t *columns,unsigned *linenum,GBFILEP input, const char *delimiters){

  char *token,*stmp1;
  char *line=NULL;
  size_t linedim=0;
  size_t i=0; /*counter*/
  unsigned seplines=0;
  size_t row=0;            /* progressive number of row */
  size_t nrow=0;	   /* allocated number of rows (multiple of Mem_block)  */
  char notenough=0;	   /* missing values in some column */
  char failedconversion=0; /* strtod did not convert */
  char partialconversion=0; /* strtod did not use the entire string */
  char underflow=0; /* strtod generated an underflow */
  char overflow=0; /* strtod generated an overflow */

  double **data=NULL;
  const size_t Mem_block=64;

  /* skip initial empty lines and comments --------------- */
  do{

    if( GBGETLINE (&line,&linedim,input) == -1)
      return data;

    (*linenum)++;
  }
  while(strspn(line," \t\n") == strlen(line) || line[0]=='#');
  /* ----------------------------------------------------- */

  /* decide number of columns ---------------------------- */
  stmp1 = strdup (line);

  if(stmp1 [strlen(stmp1)-1]=='\n')
    stmp1 [strlen(stmp1)-1]='\0';
  
  token = strtok (stmp1, delimiters);
  i=0;
  if(token)
    {
      do {i++;
      }
      while((token = strtok (NULL, delimiters)) != NULL );
    }
  else{
    fprintf(stderr,"ERROR (%s) : no data in file... exiting!\n",GB_PROGNAME);
    exit(-1);
  }
  free(stmp1);

  
  if(*columns == 0){
    *columns=i;
  }
  else if( i > *columns) {
    fprintf(stderr,"Warning (%s) : Reading only the first %zd columns of %zd provided. \n",GB_PROGNAME,*columns,i);
  }
  else if( i < *columns) {
    fprintf(stderr,"ERROR (%s) : wrong block shape... exiting!\n",GB_PROGNAME);
    exit(-1);
  }
  /* ----------------------------------------------------- */

  /* table initialization -------------------------------- */
  data = (double **) my_alloc((*columns)*sizeof(double*));
  nrow = Mem_block;
  for(i=0;i<*columns;i++){
    (data)[i] = (double *) my_alloc(nrow*sizeof(double));
  }
  /* ----------------------------------------------------- */
 
  /* read the data --------------------------------------- */
  seplines=0;
  row=0;
  (*linenum)--;
  do{
    (*linenum)++;

    stmp1 = strdup (line);
    if(stmp1 [strlen(stmp1)-1]=='\n')
      stmp1 [strlen(stmp1)-1]='\0';

    
    
    /* checking if empty or comment --------- */
    if( strspn(stmp1," \t") == strlen(stmp1) ){ /*empty line*/
      ++seplines;
      if(seplines==2){
	free(stmp1);
	break;
      }
      else continue;
    }
    else{
      seplines=0;
    }
    
    token = strtok (stmp1, delimiters);
    if(token[0] == '#') /*comment line*/
      {
	free(stmp1);
	continue;
      }
    /* -------------------------------------- */


    /* check if nrow must be increased --- */ 
    if(row >= nrow){
      nrow+=Mem_block;
      for(i=0;i<*columns;i++){
	(data)[i] = (double *) my_realloc((data)[i],nrow*sizeof(double));
      }
    }
    /* ----------------------------------- */
   
    
    /* reading the line --------------------- */
    i=0;
    
    do{
      char *endptr;
      errno=0;
      (data)[i][row] = strtod(token,&endptr);
      if(errno!=0 || *endptr != '\0') 		/* something went wrong */
	{
	  if(errno == ERANGE) { /* overflow or underflow */
	    if( (data)[i][row] == 0 )
	      underflow=1;
	    else
	      overflow=1;
	  }
	  else if(endptr == token) { /* no conversion */
	    (data)[i][row] = NAN;
	    failedconversion=1;
	  }
	  else if(*endptr != '\0') { /* partial conversion */
	    partialconversion=1;
	  }
	}
      i++;
    }
    while( i< *columns &&
	   ((token = strtok (NULL, delimiters)) != NULL) );
    
    if(i< *columns){		/* missing values */
      do {(data)[i][row]=NAN; i++;} while(i< *columns);
      notenough=1;
    }
        
    /* -------------------------------------- */
    
    row++;
    free(stmp1);
  }
  while(GBGETLINE (&line,&linedim,input) !=-1);
  /* ----------------------------------------------------- */

  /* print message if missing values or failed conversion -- */
  if(notenough==1)
    fprintf(stderr,"WARNING (%s): missing values in some columns; filling with NAN\n",GB_PROGNAME);
  if(failedconversion==1)
    fprintf(stderr,"WARNING (%s): conversion(s) failed; filling with NAN\n",GB_PROGNAME);
  if(partialconversion==1)
    fprintf(stderr,"WARNING (%s): conversion(s) failed; used part of the string\n",GB_PROGNAME);
  if(underflow==1)
    fprintf(stderr,"WARNING (%s): conversion(s) failed; underflow occurred\n",GB_PROGNAME);
  if(overflow==1)
    fprintf(stderr,"WARNING (%s): conversion(s) failed; overflow occurred\n",GB_PROGNAME);
  /* ------------------------------------------------------- */


  /* set the final shape of the table -------------------- */
  if(*rows == 0){
    *rows=row;
  }
  if( row > *rows) {
    fprintf(stderr,"Warning (%s) : Reading only the first %zd rows  of %zd provided. \n",GB_PROGNAME,*rows,row);
  }   
  else if( row < *rows) {
    fprintf(stderr,"ERROR (%s) : wrong block shape... exiting!\n",GB_PROGNAME);
    exit(-1);
  }   
  
  for(i=0;i<*columns;i++){
    (data)[i] = (double *) my_realloc((data)[i],(*rows)*sizeof(double));
  }
  /* ----------------------------------------------------- */
 

  free(line);

  return data;

}

/* 

   read a series of blocks with possibly different columns
   and rows
   
   Parameters:

   double ****data   : address of a three dim. arry of data (output)
   size_t *blocks    : pointer to the array of blocks
   size_t **rows     : pointer to the array of number of rows
   size_t **columns  : pointer to the array of number of columns
   int fildes        : file descriptor
   const char *delimiters : delimiters to use

   Return value:
   
   number of read lines

 */


int loadblocks(double ****data,size_t *blocks,size_t **rows,size_t **columns,int fildes,const char *delimiters){
  
  
  unsigned linenum=0;  /* number of lines read */  
  size_t nblock=*blocks;     /* number of blocks */
  double ** block=NULL;     /* pointer to block  */
  size_t newrows=0,newcolumns=0; 	/* rows and columns of the file read */
  
  /* input stream opening -------------------------------- */
  GBFILEP input = GBFDOPEN (fildes,"r");
  /* ----------------------------------------------------- */

  /* read blocks ----------------------------------------- */
  while( (block = readblock(&newrows, &newcolumns, &linenum, input, delimiters)) != NULL)
    {
      
      nblock++;
      *data = (double ***) my_realloc(*data,nblock*sizeof(double**));
      (*data)[nblock-1] = block;
      
      *rows = (size_t *) my_realloc(*rows,nblock*sizeof(size_t));
      *columns = (size_t *) my_realloc(*columns,nblock*sizeof(size_t));

      (*rows)[nblock-1]=newrows;
      (*columns)[nblock-1]=newcolumns;

      newrows=0;
      newcolumns=0;      
    }
  /* ----------------------------------------------------- */
  
  /* input stream closing -------------------------------- */
  GBCLOSE (input);
  /* ----------------------------------------------------- */
 
  *blocks=nblock;
  
  return linenum;
}



/* 
   Load the first block of data in an single array.
   
   Parameters:

   double **data: address of arry of data (output)
   int *size    : the size of the array   (output)
   FILE * input : file stream             (input)

   Return value:
   
   number of read lines

 */

int load(double **data,size_t *size,int fildes,const char *delimiters){

  double **vals=NULL;
  size_t rows=0,columns=0,index,i,j;

  unsigned linenum=0; /*number of lines read*/

  /* input stream opening -------------------------------- */
  GBFILEP input = GBFDOPEN (fildes,"r");
  /* ----------------------------------------------------- */

  vals = readblock(&rows,&columns, &linenum,input, delimiters);

  /* input stream closing -------------------------------- */
  GBCLOSE (input);
  /* ----------------------------------------------------- */

  *size = columns*rows;

  *data = (double *) my_alloc((*size)*sizeof(double));
  
  index=0;
  for(i=0;i<columns;i++)
    for(j=0;j<rows;j++){
      (*data)[index] =vals[i][j];
      index++;
    }

  for(i=0;i<columns;i++)
    free(vals[i]);
  free(vals);
  
  return linenum;
}


/* 
   Load the first block of data in a single table.

   Parameters:

   double ***data: table of data     (output)
   int *rows     : number of rows    (output)
   int *columns  : number of columns (input/output)
   GBFILEP input  : file stream       (input)

   Output:

   number of read lines

   If *data == NULL the routine read the specified number of columns,
   or all columns if column = 0.

 */

int loadtable(double ***data,size_t *rows,size_t *columns,int fildes,const char *delimiters){

  unsigned linenum=0; /*number of lines read*/

  /* input stream opening -------------------------------- */
  GBFILEP input = GBFDOPEN (fildes,"r");
  /* ----------------------------------------------------- */

  *data = readblock(rows,columns, &linenum,input, delimiters);

  /* input stream closing -------------------------------- */
  GBCLOSE (input);
  /* ----------------------------------------------------- */

  return linenum;

}


/* 
   Parameters:

   double ***data: two columns of data (output)
   int *size     : number of rows      (output)
   GBFILEP input  : file stream         (input)

   Output:

   number of read lines

 */

int load2(double ***data,size_t *size,int fildes,const char *delimiters){

  unsigned linenum=0; /*number of lines read*/

  size_t columns=2;

  /* input stream opening -------------------------------- */
  GBFILEP input = GBFDOPEN (fildes,"r");
  /* ----------------------------------------------------- */

  *data = readblock(size,&columns,&linenum,input, delimiters);

  /* input stream closing -------------------------------- */
  GBCLOSE (input);
  /* ----------------------------------------------------- */

  return linenum;

}



/* 
   Parameters:

   double ***data: three columns of data (output)
   int *size     : number of rows        (output)
   GBFILEP input  : file stream           (input)

   Output:

   number of read lines

 */

int load3(double ***data,size_t *size,int fildes,const char *delimiters){

  unsigned linenum=0; /*number of lines read*/

  size_t columns=3;

  /* input stream opening -------------------------------- */
  GBFILEP input = GBFDOPEN (fildes,"r");
  /* ----------------------------------------------------- */

  *data = readblock(size,&columns,&linenum,input, delimiters);

  /* input stream closing -------------------------------- */
  GBCLOSE (input);
  /* ----------------------------------------------------- */

  return linenum;

}

/* ----------------------------------------------------- */
/* loadtableblocks */

int loadtableblocks(double ****data,size_t *blocks,size_t *rows,size_t *columns,int fildes,const char *delimiters){
  
  
  unsigned linenum=0;  /* number of lines read */  
  size_t nblock=0;     /* number of blocks */
  double ** block;     /* pointer to block  */
  
  /* input stream opening -------------------------------- */
  GBFILEP input = GBFDOPEN (fildes,"r");
  /* ----------------------------------------------------- */
  
  /* read blocks ----------------------------------------- */
  while( (block = readblock(rows, columns, &linenum, input, delimiters)) != NULL)
    {
      
      nblock++;
      *data = (double ***) my_realloc(*data,nblock*sizeof(double**));
      (*data)[nblock-1] = block;
      
      if(*blocks!=0 && nblock>*blocks){
	fprintf(stderr,"Warning (%s): Reading block %zd-th after %zd.\n",GB_PROGNAME,nblock-*blocks,*blocks);
      }
      
    }
  /* ----------------------------------------------------- */
  
  /* input stream closing -------------------------------- */
  GBCLOSE (input);
  /* ----------------------------------------------------- */
 
  *blocks=nblock;
  
  return linenum;
}

/* ----------------------------------------------------- */




/* 
   utility function to read binary data and print error message if
   problems arise

 */

void
myfread(void *ptr, size_t size, size_t nmemb, FILE *stream){

  if(fread(ptr,size,nmemb,stream) != nmemb){
    fprintf(stderr,"ERROR (%s): problems in binary file... exiting!\n",
	    GB_PROGNAME);
    exit(-1);
  }

}

/* 

   Load the whole data set in a single array, ignoring columns and
   blocks.

   Parameters:

   double **data: arry of data          (output)
   int *size    : the size of the array (output)
   FILE * input : file stream           (input)

   Output:
   
   number of read lines

 */

int load_bin(double **data,size_t *size,int fildes){

  size_t i,tempcols,temprows; /* temporary number of columns */

  FILE *binput =fdopen (fildes,"r");

  /* read the first block */
  myfread(&tempcols,sizeof(size_t),1,binput);
  *size=0;
  for(i=0;i<tempcols;i++){
    myfread(&temprows,sizeof(size_t),1,binput);
    *size+=temprows;
  }
  *data = (double *) my_alloc((*size)*sizeof(double));
  myfread((*data),sizeof(double),(*size),binput);

  while( fread(&tempcols,sizeof(size_t),1,binput) > 0){
    size_t tempsize=0;
    
    for(i=0;i<tempcols;i++){
      myfread(&temprows,sizeof(size_t),1,binput);
      tempsize+=temprows;
    }
    *data = (double *) my_realloc(*data,(tempsize+*size)*sizeof(double));
    myfread((*data)+*size,sizeof(double),tempsize,binput);

    *size +=tempsize;
  }

  /* should be changed: this is a duplicate data. GB Feb 2010 */
  return *size;

}


/* 
   Parameters:

   double ****data          : address of table of data        (output)
   size_t **rows            : address of number of blocks    (output)
   size_t **rows            : address of array of rows       (output)
   size_t **columns         : address of array of columns    (output)

   Output:

   number of rows read

 */

int loadblocks_bin(double ****data,size_t *blocks,size_t **rows,size_t **columns,int fildes){

  int rowsnum=0;  /* number of lines read */  
  size_t nblock=*blocks;     /* number of blocks */

  size_t newcolumns,newrows;

  /* input stream opening -------------------------------- */
  FILE *binput =fdopen (fildes,"r");
  /* ----------------------------------------------------- */


  /* read blocks ----------------------------------------- */
  while(fread(&newcolumns,sizeof(size_t),1,binput) && feof(binput)==0)
    {

      size_t i;

      size_t *temprows;

      /* increase the number of blocks */
      nblock++;
      *data = (double ***) my_realloc(*data,nblock*sizeof(double**));

      *rows = (size_t *) my_realloc(*rows,nblock*sizeof(size_t));
      *columns = (size_t *) my_realloc(*columns,nblock*sizeof(size_t));

      /* read number of rows */
      temprows = (size_t *) my_alloc(newcolumns*sizeof(size_t));
      myfread(temprows,sizeof(size_t),newcolumns,binput);

      /* check rows structure */
      newrows=0;
      for(i=0;i<newcolumns;i++){
	if(temprows[i] > newrows) 
	  newrows= temprows[i];
      }

      (*rows)[nblock-1]=newrows;
      (*columns)[nblock-1]=newcolumns;

      rowsnum += newrows;


      /* initialize the block */
      (*data)[nblock-1] = (double **) my_alloc(newcolumns*sizeof(double*));
      for(i=0;i<newcolumns;i++)
	(*data)[nblock-1][i] = (double *) my_alloc(newrows*sizeof(double));

      /* load data */
      for(i=0;i<newcolumns;i++){
	myfread((*data)[nblock-1][i],sizeof(double),temprows[i],binput);
	if(temprows[i]<newrows){
	  size_t j;
	  fprintf(stderr,"WARNING (%s): not enought data at column %zd; filling with NAN\n",
		  GB_PROGNAME,i);
	  for(j=temprows[i];j<newrows;j++)
	    (*data)[nblock-1][i][j]=NAN;
	}
      }

      
      free(temprows);
    }
  /* ----------------------------------------------------- */
  
  /* input stream closing -------------------------------- */
  fclose(binput);
  /* ----------------------------------------------------- */
 
  *blocks=nblock;
  
  return rowsnum;

}


/* 

   Load the data in a single table, ignoring block structures. Notice
   that blocks are stacked one below the other.

   Parameters:

   double ***data: table of data     (output)
   int *rows     : number of rows    (output)
   int *columns  : number of columns (input/output)
   GBFILEP input  : file stream       (input)

   Output:

   number of read lines

   The routine read the specified number of columns, or all columns if
   column = 0.

 */

int loadtable_bin(double ***data,size_t *rows,size_t *columns,int fildes){

  size_t i,tempcols; /* temporary number of columns */

  size_t *temprows;  /* array of temporary number of rows */

  FILE *binput =fdopen (fildes,"r");

  /* read number of columns */
  myfread(&tempcols,sizeof(size_t),1,binput);

  /* read number of rows */
  temprows = (size_t *) my_alloc(tempcols*sizeof(size_t));
  myfread(temprows,sizeof(size_t),tempcols,binput);

  /* check columns requested */
  if(*columns == 0){
    *columns=tempcols;
  }
  else if( tempcols > *columns) {
    fprintf(stderr,"WARNING (%s): read only the %zd first columns of %zd provided\n",GB_PROGNAME,
	    *columns,tempcols);
  }

  /* check rows structure */
  *rows=0;
  for(i=0;i<*columns;i++){
    if(temprows[i] >*rows) 
      *rows= temprows[i];
  }

  /* initialize the array */
  *data = (double **) my_alloc((*columns)*sizeof(double*));
  for(i=0;i<*columns;i++)
    (*data)[i] = (double *) my_alloc((*rows)*sizeof(double));

  /* load data */
  for(i=0;i<*columns;i++){
    myfread((*data)[i],sizeof(double),temprows[i],binput);
    if(temprows[i]<*rows){
      size_t j;
      fprintf(stderr,"WARNING (%s): not enought data at column %zd; filling with NAN\n",
	      GB_PROGNAME,i);
      for(j=temprows[i];j<*rows;j++)
	(*data)[i][j]=NAN;
    }
  }

  free(temprows);
  fclose(binput);

  return *rows;

}





int sort_by_value(const void *a, const void *b){
  const double *da = (const double *) a; 
  const double *db = (const double *) b; 
  
  return (*da > *db) - (*da < *db); 
}


void moment(const double data[], const size_t size, double *ave, double *adev,
	    double *sdev, double *var, double *skew, double *curt,
	    double *min,double *max){

  size_t i;         /*index*/
  double dtmp1,dtmp2,error;
  double lmin,lmax,lave,ladev,lsdev,lvar,lskew,lcurt; /* local vars */

  if (size == 0) {
    fprintf (stderr,"WARNING (%s): Sample size is zero. All stats are set to NaN\n", 
	       GB_PROGNAME);

    *min=NAN;
    *max=NAN;
    *ave=NAN;
    *adev=NAN;
    *var=NAN;
    *sdev=NAN;
    *skew=NAN;
    *curt=NAN;

    return ;
  }
  else if (size <= 1) {

    *min=data[0];
    *max=data[0];
    *ave=data[0];
    *adev=0;
    *var=0;
    *sdev=0;
    *skew=0;
    *curt=0;
 

    return ;
  }

  /* find total sum; min max values */
  lmin = lmax =data[0];
  dtmp1=0.0;/*total sum*/
  for (i=0;i<size;i++) {
    if(data[i]< lmin){
      lmin = data[i];
    }
    else if(data[i]> lmax){
      lmax = data[i];
    }
    dtmp1 += data[i];
  }
  lave=dtmp1/size;

  /* build statistics */
  error=ladev=lvar=lskew=lcurt=0.0;
  for (i=0;i<size;i++) {
    dtmp1 = data[i]-lave;
    dtmp2=dtmp1*dtmp1;

    error += dtmp1;/* as suggested in Numerical Recipes */

    ladev += fabs(dtmp1);
    lvar += dtmp2;
    lskew += dtmp2*dtmp1;
    lcurt += dtmp2*dtmp2;
  }
  ladev /= size;
  lvar=(lvar-error*error/size)/(size-1.);
  lsdev=sqrt(lvar);
  if (lvar>0) {
    lskew /= (size-1.)*lvar*lsdev;
    lcurt=lcurt/((size-1.)*lvar*lvar)-3.0;
  } else {
    fprintf(stderr,"WARNING (%s): No skew/kurtosis when variance=0\n",GB_PROGNAME);
    lskew=lcurt=NAN;
  }
  
  
 
  /* set the return values */
  *min=lmin;
  *max=lmax;
  *ave=lave;
  *adev=ladev;
  *var=lvar;
  *sdev=lsdev;
  *skew=lskew;
  *curt=lcurt;

}


/* in variable 'nonan' the number of finite values is returned */
void moment_nonan(const double data[], const size_t size, double *ave, 
		  double *adev, double *sdev, double *var, double *skew,
		  double *curt,double *min,double *max, double *nonan){

  size_t i;         /*index*/
  double dtmp1,dtmp2,error;
  double lmin,lmax,lave,ladev,lsdev,lvar,lskew,lcurt; /* local vars */
  size_t lsize;

  if (size == 0) {
    *min=NAN;
    *max=NAN;
    *ave=NAN;
    *adev=NAN;
    *var=NAN;
    *sdev=NAN;
    *skew=NAN;
    *curt=NAN;
    *nonan=0;

    return ;
  }
  else if (size <= 1) {

    *min=data[0];
    *max=data[0];
    *ave=data[0];
    *adev=0;
    *var=0;
    *sdev=0;
    *skew=0;
    *curt=0;

    if( finite(data[0]) ){
      *nonan=1;
    }
    else{
      *nonan=0;
    }
    
    return ;
  }

  /* find total sum; min max values */
  lmin = lmax =data[0];
  dtmp1=0.0;/*total sum*/
  lsize=0;
  for (i=0;i<size;i++) {
    if (!finite(data[i])) continue;
    lsize++;
    if(data[i]< lmin){
      lmin = data[i];
    }
    else if(data[i]> lmax){
      lmax = data[i];
    }
    dtmp1 += data[i];
  }
  lave=dtmp1/lsize;

  /* build statistics */
  error=ladev=lvar=lskew=lcurt=0.0;
  for (i=0;i<size;i++) {
    if (!finite(data[i])) continue;
    dtmp1 = data[i]-lave;
    dtmp2=dtmp1*dtmp1;

    error += dtmp1;/* as suggested in Numerical Recipes */

    ladev += fabs(dtmp1);
    lvar += dtmp2;
    lskew += dtmp2*dtmp1;
    lcurt += dtmp2*dtmp2;
  }
  ladev /= lsize;
  lvar=(lvar-error*error/lsize)/(lsize-1.);
  lsdev=sqrt(lvar);
  if (lvar>0) {
    lskew /= (lsize-1.)*lvar*lsdev;
    lcurt=lcurt/((lsize-1.)*lvar*lvar)-3.0;
  } else {
    lskew=lcurt=NAN;
  }

  /* set the return values */
  *min=lmin;
  *max=lmax;
  *ave=lave;
  *adev=ladev;
  *var=lvar;
  *sdev=lsdev;
  *skew=lskew;
  *curt=lcurt;
  *nonan= (double) lsize;
  
}


void moment_short(const double data[], const size_t size, double *ave,
                  double *sdev,double *min,double *max){
		    
  size_t i;         /*index*/
  double dtmp1,error;
  double lmin,lmax,lave,lsdev; /* local vars */
  
  if (size <= 1) {
    fprintf(stderr,"ERROR (%s): at least 2 observations are required\n",
	    GB_PROGNAME);
    exit(+1);
  }

  /* find total sum; min max values */
  lmin = lmax =data[0];
  dtmp1=0.0;/*total sum*/
  for (i=0;i<size;i++) {
    if(data[i]< lmin){
      lmin = data[i];
    }
    else if(data[i]> lmax){
      lmax = data[i];
    }
    dtmp1 += data[i];
  }
  lave=dtmp1/size;

  /* build statistics */
  error=lsdev=0.0;
  for (i=0;i<size;i++) {
    dtmp1 = data[i]-lave;
    error += dtmp1;/* as suggested in Numerical Recipes */
    lsdev += dtmp1*dtmp1;
  }
  lsdev=sqrt((lsdev-error*error/size)/(size-1.));

  /* set the return values */
  *min=lmin;
  *max=lmax;
  *ave=lave;
  *sdev=lsdev;
  
}

/* data[0] contains the first column, data[1] the second */
void moment_short2(double **data, const size_t n, 
		  double *ave, double *sdev,double *cov,
		  double *min,double *max){
  size_t j;
  double error0,error1,s0,s1;

  if (n <= 1) {
    fprintf(stderr,"at least 2 observations are required\n");
    exit(+1);
  }
  min[0] = max[0] =data[0][0];
  min[1] = max[1] =data[1][0];

  s0=s1=0.0;/*total sum*/
  for (j=0;j<n;j++) {/*find sum, min and max*/
    const double dtmp0= data[0][j];
    const double dtmp1= data[1][j];
    if(dtmp0< min[0]){
      min[0] = dtmp0;
    }
    else if(dtmp0> max[0]){
      max[0] = dtmp0;
    }
    if(dtmp1< min[1]){
      min[1] = dtmp1;
    }
    else if(dtmp1> max[1]){
      max[1] = dtmp1;
    }
    s0 += dtmp0;
    s1 += dtmp1;
  }

  ave[0]=s0/n;
  ave[1]=s1/n;
  *cov=sdev[0]=sdev[1]=0.0;
  error0=error1=0.0;
  for (j=0;j<n;j++) {
    error0 += (s0=data[0][j]-ave[0]);
    error1 += (s1=data[1][j]-ave[1]);
    sdev[0] += s0*s0;
    sdev[1] += s1*s1;
    *cov += s0*s1;    
  }
  sdev[0]=sqrt((sdev[0]-error0*error0/n)/(n-1));
  sdev[1]=sqrt((sdev[1]-error1*error1/n)/(n-1));
  *cov=(*cov-error0*error1/n)/(n-1);
}


/* data[0] contains the first column, data[1] the second */
void moment_short3(double **data, const size_t n, 
		  double *ave, double *sdev,double *covxy,
		  double *min,double *max){
  size_t j;
  double error0,error1,ep2,s0,s1,s2;

  if (n <= 1) {
    fprintf(stderr,"at least 2 observations are required\n");
    exit(+1);
  }
  min[0] = max[0] =data[0][0];
  min[1] = max[1] =data[1][0];
  min[2] = max[2] =data[2][0];

  s0=s1=s2=0.0;/*total sum*/
  for (j=0;j<n;j++) {/*find sum, min and max*/
    const double dtmp0= data[0][j];
    const double dtmp1= data[1][j];
    const double dtmp2= data[2][j];
    if(dtmp0< min[0]){
      min[0] = dtmp0;
    }
    else if(dtmp0> max[0]){
      max[0] = dtmp0;
    }
    if(dtmp1< min[1]){
      min[1] = dtmp1;
    }
    else if(dtmp1> max[1]){
      max[1] = dtmp1;
    }
    if(dtmp2< min[2]){
      min[2] = dtmp2;
    }
    else if(dtmp2> max[2]){
      max[2] = dtmp2;
    }
    s0 += dtmp0;
    s1 += dtmp1;
    s2 += dtmp2;
  }

  ave[0]=s0/n;
  ave[1]=s1/n;
  ave[2]=s2/n;
  *covxy=sdev[0]=sdev[1]=0.0;
  error0=error1=ep2=0.0;
  for (j=0;j<n;j++) {
    error0 += (s0=data[0][j]-ave[0]);
    error1 += (s1=data[1][j]-ave[1]);
    ep2 += (s2=data[2][j]-ave[2]);
    sdev[0] += s0*s0;
    sdev[1] += s1*s1;
    sdev[2] += s2*s2;
    *covxy += s0*s1;    
  }
  sdev[0]=sqrt((sdev[0]-error0*error0/n)/(n-1));
  sdev[1]=sqrt((sdev[1]-error1*error1/n)/(n-1));
  sdev[2]=sqrt((sdev[2]-ep2*ep2/n)/(n-1));
  *covxy=(*covxy-error0*error1/n)/(n-1);
}


void gbutils_header(char const *command,FILE *out){
  fprintf(out,"%s %s\n\n",command,PACKAGE_VERSION);
  fprintf(out,"Copyright (C) 2001-2018 Giulio Bottazzi\n");
  fprintf(out,"This program is free software; you can redistribute it and/or\n");
  fprintf(out,"modify it under the terms of the GNU General Public License\n");
  fprintf(out,"(version 2) as published by the Free Software Foundation;\n\n");
  fprintf(out,"This program is distributed in the hope that it will be useful,\n");
  fprintf(out,"but WITHOUT ANY WARRANTY; without even the implied warranty of\n");
  fprintf(out,"MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n");
  fprintf(out,"GNU General Public License for more details.\n\n");
  fprintf(out,"Written by Giulio Bottazzi\n\n");
  fprintf(out,"Report bugs to <gbutils@googlegroups.com>\n\n");
  fprintf(out,"Package home page <http://cafim.sssup.it/~giulio/software/gbutils/index.html>\n");
}


void moment_short_nonan(const double data[], const size_t size, double *ave,
			double *sdev,double *min,double *max,double *nonan){
		    
  size_t i,realsize;         /*index*/
  double dtmp1,error;
  double lmin,lmax,lave,lsdev; /* local vars */
  
  if (size <= 1) {
    fprintf(stderr,"ERROR (%s): at least 2 observations are required\n",
	    GB_PROGNAME);
    exit(+1);
  }

  /* find total sum; min max values */
  lmin = lmax =data[0];
  dtmp1=0.0;/*total sum*/
  realsize=0;
  for (i=0;i<size;i++) {
    if (!finite(data[i])) continue;
    realsize++;
    if(data[i]< lmin){
      lmin = data[i];
    }
    else if(data[i]> lmax){
      lmax = data[i];
    }
    dtmp1 += data[i];
  }
  lave=dtmp1/realsize;

  /* build statistics */
  error=lsdev=0.0;
  for (i=0;i<size;i++) {
    if (!finite(data[i])) continue;
    dtmp1 = data[i]-lave;
    error += dtmp1;/* as suggested in Numerical Recipes */
    lsdev += dtmp1*dtmp1;
  }
  lsdev=sqrt((lsdev-error*error/realsize)/(realsize-1.));

  /* set the return values */
  *min=lmin;
  *max=lmax;
  *ave=lave;
  *sdev=lsdev;
  *nonan= (double) realsize;
}

void
denan(double **vals,size_t *size)
{
  size_t i;
  int len=0;
  
  double * data = (double *) my_alloc((*size)*sizeof(double));

  for(i=0;i<*size;i++){
    if(finite((*vals)[i]))
      {
	data[len]=(*vals)[i];
	len++;
      }
  }

  data = (double *) my_realloc((void *) data,len*sizeof(double));
  
  *size=len;
  free(*vals);
  *vals=data;
}

/* remove nan in pairs */
void
denan_pairs(double **vals1,double **vals2,size_t *size)
{
  size_t i;
  int len=0;
  
  double * data1 = (double *) my_alloc((*size)*sizeof(double));
  double * data2 = (double *) my_alloc((*size)*sizeof(double));

  for(i=0;i<*size;i++){
    if(finite((*vals1)[i]) && finite((*vals2)[i]))
      {
	data1[len]=(*vals1)[i];
	data2[len]=(*vals2)[i];
	len++;
      }
  }

  data1 = (double *) my_realloc((void *) data1,len*sizeof(double));
  data2 = (double *) my_realloc((void *) data2,len*sizeof(double));
  
  *size=len;
  free(*vals1);
  free(*vals2);
  *vals1=data1;
  *vals2=data2;
}



void denan_data(double *** vals,size_t *rows,size_t *columns){

  size_t i=0;
  size_t j=0;
  size_t newrows=0;

  /* check if denan is actually required */
  for(i=0;i<*rows;i++)
    for(j=0;j<*columns;j++)
      if(!finite((*vals)[j][i]))
	goto FOUNDNAN;

  /* all values are OK; exit */
 FOUNDNAN:
  if(i==*rows && j==*columns) return;

  /* allocate space */
  double **newvals = (double **) my_alloc((*columns)*sizeof(double *));
  for(i=0;i<*columns;i++)
    newvals[i] = (double *) my_alloc((*rows)*sizeof(double));

  /* fill space */
  for(i=0;i<*rows;i++){
    for(j=0;j<*columns;j++)
      if(!finite((*vals)[j][i]))
	  break;
    if(j==*columns)
      {
	for(j=0;j<*columns;j++)
	  newvals[j][newrows]=(*vals)[j][i];
	newrows++;
      }
  }

  /* deallocate old space */
  if(*columns>0)
    for(i=0;i<*columns;i++)
      free((*vals)[i]);
  free(*vals);

  /* new dimensions */
  *rows=newrows;

  /* reallocate space */
  for(i=0;i<*columns;i++)
    newvals[i] = (double *) my_realloc((void *) (newvals[i]), (*rows)*sizeof(double));

  *vals = newvals;

}


    
/* 
   translated by f2c (version 20030320) from dsort.c.
*/


#define abs(x) ((x) >= 0 ? (x) : -(x))

/* ***BEGIN PROLOGUE  DSORT */
/* ***PURPOSE  Sort an array and optionally make the same interchanges in */
/*            an auxiliary array.  The array may be sorted in increasing */
/*            or decreasing order.  A slightly modified QUICKSORT */
/*            algorithm is used. */
/* ***LIBRARY   SLATEC */
/* ***CATEGORY  N6A2B */
/* ***TYPE      DOUBLE PRECISION (SSORT-S, DSORT-D, ISORT-I) */
/* ***KEYWORDS  SINGLETON QUICKSORT, SORT, SORTING */
/* ***AUTHOR  Jones, R. E., (SNLA) */
/*           Wisniewski, J. A., (SNLA) */
/* ***DESCRIPTION */

/*   DSORT sorts array DX and optionally makes the same interchanges in */
/*   array DY.  The array DX may be sorted in increasing order or */
/*   decreasing order.  A slightly modified quicksort algorithm is used. */

/*   Description of Parameters */
/*      DX - array of values to be sorted   (usually abscissas) */
/*      DY - array to be (optionally) carried along */
/*      N  - number of values in array DX to be sorted */
/*      KFLAG - control parameter */
/*            =  2  means sort DX in increasing order and carry DY along. */
/*            =  1  means sort DX in increasing order (ignoring DY) */
/*            = -1  means sort DX in decreasing order (ignoring DY) */
/*            = -2  means sort DX in decreasing order and carry DY along. */

/* ***REFERENCES  R. C. Singleton, Algorithm 347, An efficient algorithm */
/*                 for sorting with minimal storage, Communications of */
/*                 the ACM, 12, 3 (1969), pp. 185-187. */

int sort2(double *dx, double *dy, long int n, long int kflag)
{
    /* System generated locals */
    long int i__1;

    /* Local variables */
    static long int i__, j, k, l, m;
    static double r__, t;
    static long int ij, il[21], kk, nn, iu[21];
    static double tt, ty, tty;

    /* Parameter adjustments */
    --dy;
    --dx;

    /* Function Body */
    nn = n;
    if (nn < 1) {
      fprintf(stderr,"SLATEC DSORT: The number of values to be sorted is not positive.");
      return 0;
    }

    kk = abs(kflag);
    if (kk != 1 && kk != 2) {
      fprintf(stderr,"SLATEC DSORT: The sort control parameter, K, is not 2, 1, -1, or -2.");
	return 0;
    }

/*     Alter array DX to get decreasing order if needed */

    if (kflag <= -1) {
	i__1 = nn;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    dx[i__] = -dx[i__];
/* L10: */
	}
    }

    if (kk == 2) {
	goto L100;
    }

/*     Sort DX only */

    m = 1;
    i__ = 1;
    j = nn;
    r__ = .375;

L20:
    if (i__ == j) {
	goto L60;
    }
    if (r__ <= .5898437) {
	r__ += .0390625;
    } else {
	r__ += -.21875;
    }

L30:
    k = i__;

/*     Select a central element of the array and save it in location T */

    ij = i__ + (long int) ((j - i__) * r__);
    t = dx[ij];

/*     If first element of array is greater than T, interchange with T */

    if (dx[i__] > t) {
	dx[ij] = dx[i__];
	dx[i__] = t;
	t = dx[ij];
    }
    l = j;

/*     If last element of array is less than than T, interchange with T */

    if (dx[j] < t) {
	dx[ij] = dx[j];
	dx[j] = t;
	t = dx[ij];

/*        If first element of array is greater than T, interchange with T */

	if (dx[i__] > t) {
	    dx[ij] = dx[i__];
	    dx[i__] = t;
	    t = dx[ij];
	}
    }

/*     Find an element in the second half of the array which is smaller */
/*     than T */

L40:
    --l;
    if (dx[l] > t) {
	goto L40;
    }

/*     Find an element in the first half of the array which is greater */
/*     than T */

L50:
    ++k;
    if (dx[k] < t) {
	goto L50;
    }

/*     Interchange these elements */

    if (k <= l) {
	tt = dx[l];
	dx[l] = dx[k];
	dx[k] = tt;
	goto L40;
    }

/*     Save upper and lower subscripts of the array yet to be sorted */

    if (l - i__ > j - k) {
	il[m - 1] = i__;
	iu[m - 1] = l;
	i__ = k;
	++m;
    } else {
	il[m - 1] = k;
	iu[m - 1] = j;
	j = l;
	++m;
    }
    goto L70;

/*     Begin again on another portion of the unsorted array */

L60:
    --m;
    if (m == 0) {
	goto L190;
    }
    i__ = il[m - 1];
    j = iu[m - 1];

L70:
    if (j - i__ >= 1) {
	goto L30;
    }
    if (i__ == 1) {
	goto L20;
    }
    --i__;

L80:
    ++i__;
    if (i__ == j) {
	goto L60;
    }
    t = dx[i__ + 1];
    if (dx[i__] <= t) {
	goto L80;
    }
    k = i__;

L90:
    dx[k + 1] = dx[k];
    --k;
    if (t < dx[k]) {
	goto L90;
    }
    dx[k + 1] = t;
    goto L80;

/*     Sort DX and carry DY along */

L100:
    m = 1;
    i__ = 1;
    j = nn;
    r__ = .375;

L110:
    if (i__ == j) {
	goto L150;
    }
    if (r__ <= .5898437) {
	r__ += .0390625;
    } else {
	r__ += -.21875;
    }

L120:
    k = i__;

/*     Select a central element of the array and save it in location T */

    ij = i__ + (long int) ((j - i__) * r__);
/* GB Fri Nov 25 10:50:36 CET 2005 */
/*     fprintf(stderr,"%d\n",ij); */
    t = dx[ij];
    ty = dy[ij];

/*     If first element of array is greater than T, interchange with T */

    if (dx[i__] > t) {
	dx[ij] = dx[i__];
	dx[i__] = t;
	t = dx[ij];
	dy[ij] = dy[i__];
	dy[i__] = ty;
	ty = dy[ij];
    }
    l = j;

/*     If last element of array is less than T, interchange with T */

    if (dx[j] < t) {
	dx[ij] = dx[j];
	dx[j] = t;
	t = dx[ij];
	dy[ij] = dy[j];
	dy[j] = ty;
	ty = dy[ij];

/*        If first element of array is greater than T, interchange with T */

	if (dx[i__] > t) {
	    dx[ij] = dx[i__];
	    dx[i__] = t;
	    t = dx[ij];
	    dy[ij] = dy[i__];
	    dy[i__] = ty;
	    ty = dy[ij];
	}
    }

/*     Find an element in the second half of the array which is smaller */
/*     than T */

L130:
    --l;
    if (dx[l] > t) {
	goto L130;
    }

/*     Find an element in the first half of the array which is greater */
/*     than T */

L140:
    ++k;
    if (dx[k] < t) {
	goto L140;
    }

/*     Interchange these elements */

    if (k <= l) {
	tt = dx[l];
	dx[l] = dx[k];
	dx[k] = tt;
	tty = dy[l];
	dy[l] = dy[k];
	dy[k] = tty;
	goto L130;
    }

/*     Save upper and lower subscripts of the array yet to be sorted */

    if (l - i__ > j - k) {
	il[m - 1] = i__;
	iu[m - 1] = l;
	i__ = k;
	++m;
    } else {
	il[m - 1] = k;
	iu[m - 1] = j;
	j = l;
	++m;
    }
    goto L160;

/*     Begin again on another portion of the unsorted array */

L150:
    --m;
    if (m == 0) {
	goto L190;
    }
    i__ = il[m - 1];
    j = iu[m - 1];

L160:
    if (j - i__ >= 1) {
	goto L120;
    }
    if (i__ == 1) {
	goto L110;
    }
    --i__;

L170:
    ++i__;
    if (i__ == j) {
	goto L150;
    }
    t = dx[i__ + 1];
    ty = dy[i__ + 1];
    if (dx[i__] <= t) {
	goto L170;
    }
    k = i__;

L180:
    dx[k + 1] = dx[k];
    dy[k + 1] = dy[k];
    --k;
    if (t < dx[k]) {
	goto L180;
    }
    dx[k + 1] = t;
    dy[k + 1] = ty;
    goto L170;

/*     Clean up */

L190:
    if (kflag <= -1) {
	i__1 = nn;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    dx[i__] = -dx[i__];
/* L200: */
	}
    }
    return 0;
} /* sort2 */



int sortn(double **data, const long int len, const long int n, const long int pos, const long int kflag)
{
    /* System generated locals */
    long int i__1;

    /* Local variables */
    static long int i__, j, k, l, m,h;
    static double r__, t;
    static long int ij, il[21], kk, nn, iu[21];
    static double tt;

    double *ty,*tty, *dx;
    
    if(pos<len)
      dx = data[pos];
    else{
      fprintf(stderr,"SLATEC DSORT: The position of the column to use to sort is wrong.");
      return 0;
    }

    /* Allocate space for the rest of the array */
    ty =  (double *) my_alloc(len*sizeof(double));
    tty = (double *) my_alloc(len*sizeof(double));
 

    /* Parameter adjustments */
    --dx;

    /* Function Body */
    nn = n;
    if (nn < 1) {
      fprintf(stderr,"SLATEC DSORT: The number of values to be sorted is not positive.");
      free(ty);
      free(tty);
      return 0;
    }

    kk = abs(kflag);
    if (kk != 1 && kk != 2) {
      fprintf(stderr,"SLATEC DSORT: The sort control parameter, K, is not 2, 1, -1, or -2.");
      free(ty);
      free(tty);
      return 0;
    }

/*     Alter array DX to get decreasing order if needed */

    if (kflag <= -1) {
	i__1 = nn;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    dx[i__] = -dx[i__];
/* L10: */
	}
    }

    if (kk == 2) {
	goto L100;
    }

/*     Sort DX only */

    m = 1;
    i__ = 1;
    j = nn;
    r__ = .375;

L20:
    if (i__ == j) {
	goto L60;
    }
    if (r__ <= .5898437) {
	r__ += .0390625;
    } else {
	r__ += -.21875;
    }

L30:
    k = i__;

/*     Select a central element of the array and save it in location T */

    ij = i__ + (long int) ((j - i__) * r__);
    t = dx[ij];

/*     If first element of array is greater than T, interchange with T */

    if (dx[i__] > t) {
	dx[ij] = dx[i__];
	dx[i__] = t;
	t = dx[ij];
    }
    l = j;

/*     If last element of array is less than than T, interchange with T */

    if (dx[j] < t) {
	dx[ij] = dx[j];
	dx[j] = t;
	t = dx[ij];

/*        If first element of array is greater than T, interchange with T */

	if (dx[i__] > t) {
	    dx[ij] = dx[i__];
	    dx[i__] = t;
	    t = dx[ij];
	}
    }

/*     Find an element in the second half of the array which is smaller */
/*     than T */

L40:
    --l;
    if (dx[l] > t) {
	goto L40;
    }

/*     Find an element in the first half of the array which is greater */
/*     than T */

L50:
    ++k;
    if (dx[k] < t) {
	goto L50;
    }

/*     Interchange these elements */

    if (k <= l) {
	tt = dx[l];
	dx[l] = dx[k];
	dx[k] = tt;
	goto L40;
    }

/*     Save upper and lower subscripts of the array yet to be sorted */

    if (l - i__ > j - k) {
	il[m - 1] = i__;
	iu[m - 1] = l;
	i__ = k;
	++m;
    } else {
	il[m - 1] = k;
	iu[m - 1] = j;
	j = l;
	++m;
    }
    goto L70;

/*     Begin again on another portion of the unsorted array */

L60:
    --m;
    if (m == 0) {
	goto L190;
    }
    i__ = il[m - 1];
    j = iu[m - 1];

L70:
    if (j - i__ >= 1) {
	goto L30;
    }
    if (i__ == 1) {
	goto L20;
    }
    --i__;

L80:
    ++i__;
    if (i__ == j) {
	goto L60;
    }
    t = dx[i__ + 1];
    if (dx[i__] <= t) {
	goto L80;
    }
    k = i__;

L90:
    dx[k + 1] = dx[k];
    --k;
    if (t < dx[k]) {
	goto L90;
    }
    dx[k + 1] = t;
    goto L80;

/*     Sort DX and carry DY along */

L100:
    m = 1;
    i__ = 1;
    j = nn;
    r__ = .375;

L110:
    if (i__ == j) {
	goto L150;
    }
    if (r__ <= .5898437) {
	r__ += .0390625;
    } else {
	r__ += -.21875;
    }

L120:
    k = i__;

/*     Select a central element of the array and save it in location T */

    ij = i__ + (long int) ((j - i__) * r__);
/* GB Fri Nov 25 10:50:36 CET 2005 */
/*     fprintf(stderr,"%d\n",ij); */
    t = dx[ij];
    for(h=0;h<len;h++)  ty[h] = data[h][ij-1];

/*     If first element of array is greater than T, interchange with T */

    if (dx[i__] > t) {
	for(h=0;h<len;h++) data[h][ij-1] = data[h][i__-1];
	for(h=0;h<len;h++) data[h][i__-1] = ty[h];
	for(h=0;h<len;h++) ty[h] = data[h][ij-1];
	t = dx[ij];

    }
    l = j;

/*     If last element of array is less than T, interchange with T */

    if (dx[j] < t) {

	for(h=0;h<len;h++) data[h][ij-1] = data[h][j-1];
	for(h=0;h<len;h++) data[h][j-1] = ty[h];
	for(h=0;h<len;h++) ty[h] = data[h][ij-1];
	t = dx[ij];

/*        If first element of array is greater than T, interchange with T */

	if (dx[i__] > t) {
	    for(h=0;h<len;h++) data[h][ij-1] = data[h][i__-1];
	    for(h=0;h<len;h++) data[h][i__-1] = ty[h];
	    for(h=0;h<len;h++) ty[h] = data[h][ij-1];
	    t = dx[ij];
	}
    }

/*     Find an element in the second half of the array which is smaller */
/*     than T */

L130:
    --l;
    if (dx[l] > t) {
	goto L130;
    }

/*     Find an element in the first half of the array which is greater */
/*     than T */

L140:
    ++k;
    if (dx[k] < t) {
	goto L140;
    }

/*     Interchange these elements */

    if (k <= l) {
	for(h=0;h<len;h++) tty[h] = data[h][l-1];
	for(h=0;h<len;h++) data[h][l-1] = data[h][k-1];
	for(h=0;h<len;h++) data[h][k-1] = tty[h];
	goto L130;
    }

/*     Save upper and lower subscripts of the array yet to be sorted */

    if (l - i__ > j - k) {
	il[m - 1] = i__;
	iu[m - 1] = l;
	i__ = k;
	++m;
    } else {
	il[m - 1] = k;
	iu[m - 1] = j;
	j = l;
	++m;
    }
    goto L160;

/*     Begin again on another portion of the unsorted array */

L150:
    --m;
    if (m == 0) {
	goto L190;
    }
    i__ = il[m - 1];
    j = iu[m - 1];

L160:
    if (j - i__ >= 1) {
	goto L120;
    }
    if (i__ == 1) {
	goto L110;
    }
    --i__;

L170:
    ++i__;
    if (i__ == j) {
	goto L150;
    }
    t = dx[i__ + 1];
    for(h=0;h<len;h++) ty[h] = data[h][i__];
    if (dx[i__] <= t) {
	goto L170;
    }
    k = i__;

L180:
/*     dx[k + 1] = dx[k]; */
    for(h=0;h<len;h++) data[h][k] = data[h][k-1];
    --k;
    if (t < dx[k]) {
	goto L180;
    }
/*     dx[k + 1] = t; */
    for(h=0;h<len;h++) data[h][k] = ty[h];
    goto L170;

/*     Clean up */

L190:
    if (kflag <= -1) {
	i__1 = nn;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    dx[i__] = -dx[i__];
/* L200: */
	}
    }
    free(ty);
    free(tty);
    return 0;
} /* sortn */



#if defined HAVE_LIBZ

#if USE_UNLOCKED_IO
# include "unlocked-io.h"
#endif

#include <limits.h>
#if HAVE_INTTYPES_H
# include <inttypes.h>
#endif
#if HAVE_STDINT_H
# include <stdint.h>
#endif
#ifndef PTRDIFF_MAX
# define PTRDIFF_MAX ((ptrdiff_t) (SIZE_MAX / 2))
#endif
#ifndef SIZE_MAX
# define SIZE_MAX ((size_t) -1)
#endif
#ifndef SSIZE_MAX
# define SSIZE_MAX ((ssize_t) (SIZE_MAX / 2))
#endif

/* The maximum value that getndelim2 can return without suffering from
   overflow problems, either internally (because of pointer
   subtraction overflow) or due to the API (because of ssize_t).  */
#define GETNDELIM2_MAXIMUM (PTRDIFF_MAX < SSIZE_MAX ? PTRDIFF_MAX : SSIZE_MAX)

/* Try to add at least this many bytes when extending the buffer.
   MIN_CHUNK must be no greater than GETNDELIM2_MAXIMUM.  */
#define MIN_CHUNK 64

ssize_t
gzgetndelim2 (char **lineptr, size_t *linesize, size_t offset, size_t nmax,
            int delim1, int delim2, gzFile stream)
{
  size_t nbytes_avail;		/* Allocated but unused bytes in *LINEPTR.  */
  char *read_pos;		/* Where we're reading into *LINEPTR. */
  ssize_t bytes_stored = -1;
  char *ptr = *lineptr;
  size_t size = *linesize;

  if (!ptr)
    {
      size = nmax < MIN_CHUNK ? nmax : MIN_CHUNK;
      ptr = my_alloc (size);
      if (!ptr)
	return -1;
    }

  if (size < offset)
    goto done;

  nbytes_avail = size - offset;
  read_pos = ptr + offset;

  if (nbytes_avail == 0 && nmax <= size)
    goto done;

  for (;;)
    {
      /* Here always ptr + size == read_pos + nbytes_avail.  */

      int c;

      /* We always want at least one byte left in the buffer, since we
	 always (unless we get an error while reading the first byte)
	 NUL-terminate the line buffer.  */

      if (nbytes_avail < 2 && size < nmax)
	{
	  size_t newsize = size < MIN_CHUNK ? size + MIN_CHUNK : 2 * size;
	  char *newptr;

	  if (! (size < newsize && newsize <= nmax))
	    newsize = nmax;

	  if (GETNDELIM2_MAXIMUM < newsize - offset)
	    {
	      size_t newsizemax = offset + GETNDELIM2_MAXIMUM + 1;
	      if (size == newsizemax)
		goto done;
	      newsize = newsizemax;
	    }

	  nbytes_avail = newsize - (read_pos - ptr);
	  newptr = realloc (ptr, newsize);
	  if (!newptr)
	    goto done;
	  ptr = newptr;
	  size = newsize;
	  read_pos = size - nbytes_avail + ptr;
	}

      c = gzgetc (stream);
      if (c == EOF)
	{
	  /* Return partial line, if any.  */
	  if (read_pos == ptr)
	    goto done;
	  else
	    break;
	}

      if (nbytes_avail >= 2)
	{
	  *read_pos++ = c;
	  nbytes_avail--;
	}

      if (c == delim1 || c == delim2)
	/* Return the line.  */
	break;
    }

  /* Done - NUL terminate and return the number of bytes read.
     At this point we know that nbytes_avail >= 1.  */
  *read_pos = '\0';

  bytes_stored = read_pos - (ptr + offset);

 done:
  *lineptr = ptr;
  *linesize = size;

  return bytes_stored;
}


ssize_t
gzgetdelim (char **lineptr, size_t *linesize, int delimiter, gzFile stream)
{
  return gzgetndelim2 (lineptr, linesize, 0, GETNLINE_NO_LIMIT, delimiter, EOF,
                     stream);
}

ssize_t
gzgetline (char **lineptr, size_t *linesize, gzFile stream)
{
  return gzgetdelim (lineptr, linesize, '\n', stream);

}

#endif

/* ===================================== */
/* UTILITY FUNCTIONS FOR KERNEL ANALYSIS */
/* ===================================== */

/* 1-dimensional */

const double Krectangular_K2= .5;
const double Klaplace_K2= 1/(2*M_SQRT2);
const double Kgauss_K2= 1/(2*M_PI);
const double Kepanechnikov_K2= 3./(5*2.23606797749978969641);

double Krectangular(double x) {
  return(.5);
}

double Krectangular_tilde(double x) {
  if(x==0){
    return(1);
  }
  else{
    return(sin(x)/x);
  }
}

double Klaplace(double x) {
  return(exp(-M_SQRT2*fabs(x))/M_SQRT2);
}


double Klaplace_tilde(double x) {
  return(1./(1.+x*x*0.5));
}


double Kgauss(double x) {
  return(exp(-x*x/2.)/(sqrt(2.*M_PI)));
}


double Kgauss_tilde(double x) {
  return(exp(-x*x/2));
}


double Kepanechnikov(double x) {
  return 3.*(1-x*x/5.)/(4.*sqrt(5));
}

double Kepanechnikov_tilde(double x) {
  if(x==0){
    return(1);
  }
  else{
    double dtmp1=x*sqrt(5);
    return(3*(sin(dtmp1)-dtmp1*cos(dtmp1))/(dtmp1*dtmp1*dtmp1));
  }
}

/* 2-dimensional */

double Krectangular2d(double x) {
  return( (x<1. ? 1./M_PI : 0.0) );
}


double Kepanechnikov2d(double x) {
  return( (x<1. ? 2.*(1-x)/M_PI : 0.0) );
}

double Ksilverman2d_1(double x) {
  const double dtmp1= 1-x;
  return( (x<1. ? 3.*dtmp1*dtmp1/M_PI : 0.0) );
}

double Ksilverman2d_2(double x) {
  const double dtmp1= 1-x;
  return( (x<1. ? 4.*dtmp1*dtmp1*dtmp1/M_PI : 0.0) );
}
