/*
  gbgcorr  (ver. 5.6) -- Gaussian kernel correlation dimension.
  Copyright (C) 2006-2018 Cees Diks and Giulio Bottazzi

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  (version 2) as published by the Free Software Foundation;
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
  02111-1307, USA.
*/

#include "tools.h"


int main(int argc,char* argv[]){


  /* OPTIONS */
  int o_verbose=0;
  int o_nref=0;
  int o_sigma=0;

  /* variables for range and num. steps */
  unsigned nres=24;

  unsigned ntau=1;  /* delay time */
  unsigned ndim=2;  /* maximum embedding dimension */
  unsigned ntc=1;   /* Theiler correction */
  unsigned nref;  /* number of reference points */
  unsigned npt;	  /* number of delay vectors */

  /* random generator variables */
  unsigned seed=0;

  double *r2;    /* bandwidths */
  double **mm;   /* correlation integrals */
  double **var;  /* variance of correlation integrals */
  double **epsilon;  /* variance of correlation integrals */
  double *index; /* array of indices */

  size_t i,ii,m,k,j,d;


  /* data storage */
  size_t ntot;
  double *x=NULL;

  char *splitstring = strdup(" \t");

  /* variables for reading command line options */
  /* ------------------------------------------ */
  int opt;
  /* ------------------------------------------ */
  
  /* COMMAND LINE PROCESSING */

  while((opt=getopt_long(argc,argv,"hvF:r:l:m:t:R:p:s",gb_long_options, &gb_option_index))!=EOF){
    if(opt==0){
      gbutils_header(argv[0],stdout);
      exit(0);
    }
    else if(opt=='?'){
      fprintf(stderr,"option %c not recognized\n",optopt);
      exit(-1);
    }
    else if(opt=='h'){
      /*print help*/
      fprintf(stdout,
	      "Compute the correlation dimension with a Gaussian kernel.\n");
      fprintf(stdout,
	      "One block of output is printed for each embedding dimension,\n");
      fprintf(stdout,
	      "the first column report the bandwidth, the second the associated\n");
      fprintf(stdout,
	      "correlation coefficient. Bandwidths are equispaced in a log scale\n");
      fprintf(stdout,
	      "from 1 (maximum) to 2^(-n+1) (minimum). n can be set on the command \n");
      fprintf(stdout,
	      "line.\n");

      fprintf(stdout,"\nUsage: %s [options]\n\n",argv[0]);
      fprintf(stdout,"Options:                                                               \n");
      fprintf(stdout," -h  this help\n");
      fprintf(stdout," -l  time lag (default 1)\n");
      fprintf(stdout," -m  maximum embedding dimension (default 2)\n");
      fprintf(stdout," -t  Theiler correction (default 1)\n");
      fprintf(stdout," -R  seed (default 0)\n");
      fprintf(stdout," -p  number of reference points (default 1)\n");
      fprintf(stdout," -r  number of bandwidths  (default 24)\n");
      fprintf(stdout," -s  print standard errors \n");
      fprintf(stdout," -F  specify the input fields separators (default \" \\t\")\n");
      exit(0);
    }
    else if(opt=='F'){
      /*set the fields separator string*/
      free(splitstring);
      splitstring = strdup(optarg);
    }
    else if(opt=='s'){
      o_sigma = 1;
    }
    else if(opt=='l'){
      ntau = (unsigned) atoi(optarg);
    }
    else if(opt=='m'){
      ndim = (unsigned) atoi(optarg);
    }
    else if(opt=='t'){
      ntc = (unsigned) atoi(optarg);
    }
    else if(opt=='R'){
      /*RNG seed*/
      seed= (unsigned) atoi(optarg);
    }
    else if(opt=='p'){
      o_nref=1;
      nref= (unsigned) atoi(optarg);
    }
    else if(opt=='v'){
      /*increase verbosity*/
      o_verbose=1;
    }
    else if(opt=='r'){
      nres= (unsigned) atoi(optarg);
    }
  }    
  /* END OF COMMAND LINE PROCESSING */

  /* initialize global variables */
  initialize_program(argv[0]);


  /* initialize RNG */
  srand(seed);
  
  /* load data */
  load(&x,&ntot,0,splitstring);
  
  /* remove NaN */
  denan(&x,&ntot);

  /* normalize to 1/2 variance */
  {
    double mean=0.0, meansq=0.0, fac;
    
    for (i=0;i<ntot;i++) { 
      mean += x[i];
      meansq += x[i]*x[i];
    }

    mean   /= ntot;
    meansq /= ntot;
    
    fac = .5/(sqrt(meansq - mean*mean));
    
    for (i=0;i<ntot;i++)
      x[i] *= fac;
  }

  /* set the default value for npt */
  npt = ntot-(ndim-1)*ntau;

  /* if not set from the command line, or if set too large, nref is
     set equal to npt */
  if(!o_nref || nref>npt)
    nref=npt;

  /* ++++++++++++++++++++++++++++ */
  if(o_verbose == 1){
    fprintf(stderr,"delay                   : %d\n",ntau);
    fprintf(stderr,"max embedding dimension : %d\n",ndim);
    fprintf(stderr,"Theiler correction      : %d\n",ntc);
    fprintf(stderr,"RNG seed                : %d\n",seed);
    fprintf(stderr,"Max. num. ref. points   : %d\n",npt);
    fprintf(stderr,"Num. of ref. points     : %d\n",nref);
  }
  /* ++++++++++++++++++++++++++++ */


  /* allocation and initialization of output variables */
  r2 = my_alloc(nres*sizeof(double));

  for (k=0;k<nres;k++)
    r2[k] = 1./pow(2.0,k);

  mm = my_alloc(ndim*sizeof(double*));
  for (k=0;k<ndim;k++)
    mm[k] = my_calloc(nres,sizeof(double));

  var = my_alloc(ndim*sizeof(double*));
  for (k=0;k<ndim;k++)
    var[k] = my_calloc(nres,sizeof(double));

  epsilon = my_alloc(ndim*sizeof(double*));
  for (k=0;k<ndim;k++)
    epsilon[k] = my_calloc(nres,sizeof(double));

  /* allocation and selection of random reference points */

  index = my_calloc(npt,sizeof(double));
  
  for (i=0;i!=npt;i++) index[i] = i;

  if (nref!=npt) {
    for (i=0;i!=npt;i++) index[i] = i;
    for (i=0;i!=nref;i++) {
      k = i + (int)((npt-i)*(double)(rand()/(double)(RAND_MAX)));
      j = index[k];
      index[k] = index[i];
      index[i] = j;
    }
  }

  /* calculation of gaussian kernel correlation integral */

  {
    size_t ncount = 0;

    for (ii=0;ii!=nref;ii++) {
      /* +++++++++++++++++++++++++++ */
      if (o_verbose == 1 && ii%10==0)
	fprintf(stderr,"\rrp %zd", ii);
      /* +++++++++++++++++++++++++++ */
      i = index[ii];
      for (j=0;j!=npt;j++)
	if (abs(i-j) >= ntc) {
	  double dis2 = 0.0;
	  ncount++;
	  for (m=0;m!=ndim;m++) {
	    double tmp;
	    dis2 += (x[i+m*ntau]-x[j+m*ntau])*(x[i+m*ntau]-x[j+m*ntau]);
	    tmp = exp(-dis2);
/* 	    for (k=0;(k!=nres && tmp >= 1.E-24) ;k++) { */
	    for (k=0;k!=nres;k++) {
	      mm[m][k] += tmp;
	      tmp *= tmp;
/* 	      var[m][k] += tmp; */
	    }
	  }
	}
    }

    for (m=0;m!=ndim;m++)
      for (k=0;k!=nres;k++)
	mm[m][k] /= (double) ncount;
    
    if(o_sigma) {
      for (ii=0;ii!=nref;ii++) {
	/* +++++++++++++++++++++++++++ */
	if (o_verbose == 1 && ii%10==0)
	  fprintf(stderr,"\rrp %zd", ii);
	/* +++++++++++++++++++++++++++ */
	i = index[ii];
	for (j=0;j!=npt;j++)
	  if (abs(i-j) >= ntc) {
	    double dis2 = 0.0;
	    ncount++;
	    for (m=0;m!=ndim;m++) {
	      double tmp;
	      dis2 += (x[i+m*ntau]-x[j+m*ntau])*(x[i+m*ntau]-x[j+m*ntau]);
	      tmp = exp(-dis2);
	      /* 	    for (k=0;(k!=nres && tmp >= 1.E-24) ;k++) { */
	      for (k=0;k!=nres;k++) {
		epsilon[m][k] += tmp-mm[m][k];
		var[m][k] += epsilon[m][k]*epsilon[m][k];
		tmp *= tmp;
	      }
	    }
	  }
      }
      
      
      for (m=0;m!=ndim;m++)
	for (k=0;k!=nres;k++)
	  var[m][k] = 
	    (var[m][k]-epsilon[m][k]*epsilon[m][k]/ncount)/(ncount-1.);
    }

    /* ++++++++++++++++++++++++++++++++ */
    if(o_verbose)
      printf("\n#ncount = %zd\n", ncount);
    /* ++++++++++++++++++++++++++++++++ */

  }


  /* output */


  for (d=0;d!=ndim;d++){
    for (i=nres-1;i!=-1;i--){
      if (mm[d][i]!=0){
	printf(INT_SEP,d+1);
	printf(FLOAT_SEP,sqrt(r2[i]));
	printf(FLOAT_SEP,mm[d][i]);
	if(o_sigma)
	  printf(FLOAT_SEP,sqrt(var[d][i]));
	printf("\n");
      }
    }
    printf("\n\n");
  }

  return 0;

}
