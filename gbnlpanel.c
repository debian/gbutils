/*
  gbnlpanel  (ver. 5.6) -- Non-linear panel regression
  Copyright (C) 2009-2018 Giulio Bottazzi and Davide Pirino

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  (version 2) as published by the Free Software Foundation;
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#include "tools.h"   
#include "matheval.h"
#include "assert.h"
#include "multimin.h"

#include <math.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_multifit_nlin.h>
#include <gsl/gsl_cdf.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_multimin.h>


/* handy structure used in computation */
typedef struct function {
  void *f;
  size_t argnum;
  char **argname;
  void **df;
  void ***ddf;
} Function;


struct objdata {
  Function * F;
  size_t rows;
  size_t columns;
  size_t blocks;
  double ***data;
};


/* Fixed effect object function and variance-covariance */
/* ---------------------------------------------------- */

void
obj_fixed_f (const size_t Pnum, const double *x,void *params,double *fval){
  
  const Function *F = ((struct objdata *)params)->F;
  const size_t rows=((struct objdata *)params)->rows;
  const size_t columns=((struct objdata *)params)->columns;
  const size_t blocks=((struct objdata *)params)->blocks;
  double ***data=((struct objdata *)params)->data;

  double values[Pnum+blocks];

  size_t b,c,r; 		/* counter for blocks, columns and rows */
  size_t nan=0;			/* total number of nan */
  double trend_f; // sum f over columns (time) and rows (realizations).
  

  /* set the parameters */
  for(b=blocks;b<blocks+Pnum;b++)
    values[b] = x[b-blocks];
 
  /* initializing trend  */
  trend_f=0;
  
  /* trend computations */  
  for(r=0;r<rows;r++){
    size_t rnan=0;
    for(c=0;c<columns;c++){
      for(b=0;b<blocks;b++)
	values[b] = data[b][c][r];
      
      double dtmp1=evaluator_evaluate (F->f,blocks+Pnum,F->argname,values);
      if(isfinite(dtmp1)){
	trend_f +=dtmp1; 
      }
      else
	rnan++;      
    }
    nan+=rnan;
  }
  trend_f/=(rows*columns-nan);
  
  *fval=0;
  for(r=0;r<rows;r++){
    double rsf=0;
    size_t rnan=0;
    for(c=0;c<columns;c++){
      for(b=0;b<blocks;b++)
	values[b] = data[b][c][r];
      double dtmp1=evaluator_evaluate (F->f,blocks+Pnum,F->argname,values);
      if(isfinite(dtmp1)){
	rsf+=dtmp1;
      }
      else
	rnan++;
    }
    for(c=0;c<columns;c++){
      for(b=0;b<blocks;b++)
	values[b] = data[b][c][r];
     
      double dtmp1=evaluator_evaluate (F->f,blocks+Pnum,F->argname,values)+trend_f-(rsf/(columns-rnan));
      if(isfinite(dtmp1))
        *fval+=pow(dtmp1,2);
    }
  }      
  *fval/=(rows*columns-nan);
  
}


void
obj_fixed_df (const size_t Pnum, const double *x,void *params,double *grad){
  
  const Function *F = ((struct objdata *)params)->F;
  const size_t rows=((struct objdata *)params)->rows;
  const size_t columns=((struct objdata *)params)->columns;
  const size_t blocks=((struct objdata *)params)->blocks;
  double ***data=((struct objdata *)params)->data;
  double values[Pnum+blocks];
 
  size_t h,b,c,r;  /* counter for parameters, blocks, columns and rows */
  size_t nan=0;	   /* total number of nan */
  double trend_f; // sum f over columns (time) and rows (realizations).
  double trend_df[Pnum]; // sum df over columns (time) and rows (realizations).
 


  /* set the parameters */
  for(b=blocks;b<blocks+Pnum;b++)
    values[b] = x[b-blocks];
  
 

  /* initializing grad and trends  */

  
  trend_f=0;
  for(h=0;h<Pnum;h++){
    trend_df[h]=0;
    grad[h]=0;
  }
   
  /* trends computations */
  
  for(r=0;r<rows;r++){
    size_t rnan=0;
    for(c=0;c<columns;c++){
      
      for(b=0;b<blocks;b++)
	values[b] = data[b][c][r];
      
      double dtmp1=evaluator_evaluate (F->f,blocks+Pnum,F->argname,values);
      if(isfinite(dtmp1)){
	trend_f +=dtmp1; 
	for(h=0;h<Pnum;h++){
	  double dtmp2=evaluator_evaluate ((F->df)[h],blocks+Pnum,F->argname,values);
	  trend_df[h]+=dtmp2;
	}  
      }else
	rnan++;
    }
    nan+=rnan;
  }
  trend_f/=(rows*columns-nan);
    
  for(h=0;h<Pnum;h++)
    trend_df[h]/=(rows*columns-nan);
    
  
  
  for(r=0;r<rows;r++){
    double rsdf[Pnum];
    double rsf=0;
    size_t rnan=0;
    for(h=0;h<Pnum;h++)
      rsdf[h]=0;
    
    for(c=0;c<columns;c++){
      for(b=0;b<blocks;b++)
	values[b] = data[b][c][r];
      
      double dtmp1=evaluator_evaluate (F->f,blocks+Pnum,F->argname,values);
      if(isfinite(dtmp1)){
	rsf+=dtmp1;
	for(h=0;h<Pnum;h++){
	  double dtmp2=evaluator_evaluate ((F->df)[h],blocks+Pnum,F->argname,values);
	  rsdf[h]+=dtmp2;
	}
      }
      else
	rnan++;
    }
    
    for(c=0;c<columns;c++){
      for(b=0;b<blocks;b++)
	values[b] = data[b][c][r];
      
      double dtmp1=evaluator_evaluate (F->f,blocks+Pnum,F->argname,values)+trend_f-(rsf/(columns-rnan));
      if(isfinite(dtmp1)){
	for(h=0;h<Pnum;h++){
	  double dtmp2=evaluator_evaluate ((F->df)[h],blocks+Pnum,F->argname,values)+trend_df[h]-(rsdf[h]/(columns-rnan)); 
	  grad[h]+=dtmp1*dtmp2; 
	}       
      }
    }
  }      
  for(h=0;h<Pnum;h++)
    grad[h]/=(0.5*(rows*columns-nan));
  
}



void
obj_fixed_fdf (const size_t Pnum, const double *x,void *params,double *f,double *grad){
      
  const Function *F = ((struct objdata *)params)->F;
  const size_t rows=((struct objdata *)params)->rows;
  const size_t columns=((struct objdata *)params)->columns;
  const size_t blocks=((struct objdata *)params)->blocks;
 
  double ***data=((struct objdata *)params)->data;
  double values[Pnum+blocks];
  size_t h,b,c,r;  /* counter for parameters, blocks, columns and rows */
  double trend_f; // sum f over columns (time) and rows (realizations).
  double trend_df[Pnum]; // sum df over columns (time) and rows (realizations).
  size_t nan=0;

  /* initializing grad and trends  */
  trend_f=0;
  for(h=0;h<Pnum;h++){
    trend_df[h]=0;
    grad[h]=0;
  }
   
  /* trends computations */
  
  for(r=0;r<rows;r++){
    size_t rnan=0;
    for(c=0;c<columns;c++){
      for(b=0;b<blocks;b++){
	values[b] = data[b][c][r];
      }
      double dtmp1;
      dtmp1=evaluator_evaluate (F->f,blocks+Pnum,F->argname,values);
      if(isfinite(dtmp1)){
	trend_f +=dtmp1; 
	for(h=0;h<Pnum;h++){
	  double dtmp2=evaluator_evaluate ((F->df)[h],blocks+Pnum,F->argname,values);
	  trend_df[h]+=dtmp2;
	}
      }
      else
	rnan++;      
    }
    nan+=rnan;
  }
  
  trend_f/=(rows*columns-nan);
  
  for(h=0;h<Pnum;h++)
    trend_df[h]/=(rows*columns-nan);
  
  
  *f=0;
  for(r=0;r<rows;r++){
    double rsdf[Pnum];
    double rsf=0;
    size_t rnan=0;
    for(h=0;h<Pnum;h++)
      rsdf[h]=0;
    
    for(c=0;c<columns;c++){
      for(b=0;b<blocks;b++)
	values[b] = data[b][c][r];
      
      double dtmp1=evaluator_evaluate (F->f,blocks+Pnum,F->argname,values);
      if(isfinite(dtmp1)){
	rsf+=dtmp1;
	for(h=0;h<Pnum;h++){
	  double dtmp2=evaluator_evaluate ((F->df)[h],blocks+Pnum,F->argname,values);
	  rsdf[h]+=dtmp2;
	}
      }
      else
	rnan++;
    }
    
    for(c=0;c<columns;c++){
      for(b=0;b<blocks;b++)
	values[b] = data[b][c][r];
      
      double dtmp1=evaluator_evaluate (F->f,blocks+Pnum,F->argname,values)+trend_f-(rsf/(columns-rnan));
      if(isfinite(dtmp1)){
        *f+=pow(dtmp1,2);
	for(h=0;h<Pnum;h++){
	  double dtmp2=evaluator_evaluate ((F->df)[h],blocks+Pnum,F->argname,values)+trend_df[h]-(rsdf[h]/(columns-rnan)); 
	  grad[h]+=dtmp1*dtmp2; 
	}       
      }
    }
  }  
    
  *f/=(rows*columns-nan);
  for(h=0;h<Pnum;h++)
    grad[h]/=(0.5*(rows*columns-nan));
  
}


/* compute variance-covariance matrix */
gsl_matrix *fixed_varcovar(const int o_varcovar,  const double *x, const struct objdata params, const size_t Pnum){

  size_t b,c,r;  // counter for blocks, columns, rows.
  size_t h,k;    // counter for matrix elements.
  const size_t rows=((struct objdata )params).rows;
  const size_t columns=((struct objdata )params).columns;
  const size_t blocks=((struct objdata )params).blocks;
  double ***data=((struct objdata )params).data;
  const Function *F = ((struct objdata )params).F;
  gsl_matrix *covar = gsl_matrix_alloc (Pnum,Pnum);
  
  double values[blocks+Pnum];
  double fixed[rows];   // fixed effect coefficient
  double sigma2,sigma4; // second and fourth power of the standard deviation of the error term.
  

  /* fill with optimal parameter */
  for(b=blocks;b<blocks+Pnum;b++)
    values[b] = x[b-blocks];
  
  /* ----------------------------------------------------- */

  double trend_f; // sum f over columns (time) and rows (realizations).
  double trend_df[Pnum]; // sum df over columns (time) and rows (realizations).
  double trend_ddf[Pnum][Pnum]; // sum ddf over columns (time) and rows (realizations).
  size_t nan=0; // total number of nan. 

  /* initializing obj and trends  */

  
  trend_f=0;
  for(h=0;h<Pnum;h++){
    trend_df[h]=0;
    for(k=0;k<Pnum;k++){
      trend_ddf[h][k]=0;
    }
  }
   
  /* trends computations */
  
  for(r=0;r<rows;r++){
    size_t rnan=0;
    for(c=0;c<columns;c++){
      for(b=0;b<blocks;b++){
	values[b] = data[b][c][r];
      }
      double dtmp1;
      dtmp1=evaluator_evaluate (F->f,blocks+Pnum,F->argname,values);
      if(isfinite(dtmp1)){
	trend_f +=dtmp1; 
	for(h=0;h<Pnum;h++){
	  double dtmp2=evaluator_evaluate ((F->df)[h],blocks+Pnum,F->argname,values);
	  trend_df[h]+=dtmp2;
	  
	  if(o_varcovar>0){
	    for(k=0;k<Pnum;k++){
	      double dtmp3=evaluator_evaluate ((F->ddf)[h][k],blocks+Pnum,F->argname,values);
	      trend_ddf[h][k]+=dtmp3;
	    }
	  }
	}
      }
      else
	rnan++;
      
    }
    nan+=rnan;
  }

  if(rows*columns-nan!=0){
    trend_f/=(rows*columns-nan);
  
    for(h=0;h<Pnum;h++){
      trend_df[h]/=(rows*columns-nan);
      for(k=0;k<Pnum;k++){
	trend_ddf[h][k]/=(rows*columns-nan);
      }
    }
  } 
 
  
  sigma2=0;
  for(r=0;r<rows;r++){
    size_t rnan=0; // numebr of nan per columns.
    double rsf=0;
    double rsf2=0;
    for(c=0;c<columns;c++){
      for(b=0;b<blocks;b++){
	values[b] = data[b][c][r];
      }
      double dtmp1=evaluator_evaluate (F->f,blocks+Pnum,F->argname,values);
      if(isfinite(dtmp1)){
	rsf += dtmp1;
        rsf2 += pow(dtmp1,2);
      }
      else{
	rnan++;
      }
    }
    if(columns-rnan!=0)
      rsf /= (columns-rnan);
    
    fixed[r]=rsf-trend_f;
    for(c=0;c<columns;c++){
      for(b=0;b<blocks;b++){
	values[b] = data[b][c][r];
      }
      double dtmp1=evaluator_evaluate (F->f,blocks+Pnum,F->argname,values);
      if(isfinite(dtmp1)){
	sigma2 += pow(dtmp1-fixed[r],2);
      }
    }
  }

  if(rows*columns-nan!=0){
    sigma2 /= (rows*columns-nan);
    sigma4 = pow(sigma2,2);
  }
  /* ----------------------------------------------------- */
  
  
  switch(o_varcovar){
    
  case 0:
    {
      gsl_permutation * P = gsl_permutation_alloc (Pnum);
      int signum;
      gsl_matrix *J = gsl_matrix_calloc (Pnum,Pnum);
      gsl_matrix *dJ = gsl_matrix_calloc (Pnum,Pnum);

      
      for(r=0;r<rows;r++){
	double rsdf[Pnum];
        double rsf=0;
	size_t rnan=0;
	for(k=0;k<Pnum;k++)
	  rsdf[k]=0;

	for(c=0;c<columns;c++){
	  for(b=0;b<blocks;b++)
	    values[b] = data[b][c][r];
	  
	  double dtmp1=evaluator_evaluate (F->f,blocks+Pnum,F->argname,values);
	  if(isfinite(dtmp1)){
            rsf+=dtmp1;
	    for(k=0;k<Pnum;k++){
	      double dtmp2=evaluator_evaluate ((F->df)[k],blocks+Pnum,F->argname,values);
	      rsdf[k]+=dtmp2;
	    }
	  }
	  else
	    rnan++;
	}

	for(c=0;c<columns;c++){
	  for(b=0;b<blocks;b++)
	    values[b] = data[b][c][r];
	  
       
	  
	  double dtmp1=pow(evaluator_evaluate (F->f,blocks+Pnum,F->argname,values)+trend_f-(rsf/(columns-rnan)),2);
	  if(isfinite(dtmp1)){
	    for(h=0;h<Pnum;h++){
	      double dtmp2=evaluator_evaluate ((F->df)[h],blocks+Pnum,F->argname,values)+trend_df[h]-(rsdf[h]/(columns-rnan)); 
	      for(k=0;k<Pnum;k++){
		double dtmp3=evaluator_evaluate ((F->df)[k],blocks+Pnum,F->argname,values)+trend_df[k]-(rsdf[k]/(columns-rnan)); 
		gsl_matrix_set(dJ,h,k,dtmp2*dtmp3);
	      }
	    }
	    gsl_matrix_scale (dJ,dtmp1);
	    gsl_matrix_add (J,dJ);
	  }
      	}
      }  
      gsl_matrix_memcpy (dJ,J);
      gsl_linalg_LU_decomp (dJ,P,&signum);
      gsl_linalg_LU_invert (dJ,P,covar);
      gsl_matrix_free(J);
      gsl_matrix_free(dJ);
      gsl_permutation_free(P);
      gsl_matrix_scale (covar,sigma4);
    }
    break;
    
  case 1: /* H^{-1} */
    {
      gsl_permutation * P = gsl_permutation_alloc (Pnum);
      gsl_matrix *H = gsl_matrix_calloc (Pnum,Pnum);
      gsl_matrix *dH = gsl_matrix_calloc (Pnum,Pnum);
      
      int signum;
      
      
      /* compute hessian  */
      for(r=0;r<rows;r++){
	double rsdf[Pnum];
	double rsddf[Pnum][Pnum];
	size_t rnan=0;
	for(h=0;h<Pnum;h++){
	  rsdf[h]=0;
	  for(k=0;k<Pnum;k++){
	    rsddf[h][k]=0;
	  }
	}
	
	for(c=0;c<columns;c++){
	  
	  for(b=0;b<blocks;b++)
	    values[b] = data[b][c][r];
      
	  for(h=0;h<Pnum;h++){
	    double dtmp1=evaluator_evaluate ((F->df)[h],blocks+Pnum,F->argname,values);
	    if(isfinite(dtmp1)){
	      rsdf[h]+=dtmp1;
	      for(k=0;k<Pnum;k++){
		double dtmp2=evaluator_evaluate ((F->ddf)[h][k],blocks+Pnum,F->argname,values);
		rsddf[h][k]+=dtmp2;
	      }
	    }
	  }    
	}
	
	for(c=0;c<columns;c++){
	  for(b=0;b<blocks;b++)
	    values[b] = data[b][c][r];
	  
	  double dtmp1=evaluator_evaluate (F->f,blocks+Pnum,F->argname,values);
	  if(isfinite(dtmp1)){
	    for(h=0;h<Pnum;h++){
	      double dtmp2=evaluator_evaluate ((F->df)[h],blocks+Pnum,F->argname,values);
	      for(k=0;k<Pnum;k++){
		double dtmp3=evaluator_evaluate ((F->df)[k],blocks+Pnum,F->argname,values);
		double dtmp4=evaluator_evaluate ((F->ddf)[h][k],blocks+Pnum,F->argname,values);
		double res=dtmp2*dtmp3;
		res+=dtmp1*dtmp4;
		res+=-dtmp2*rsdf[k]/(columns-rnan);
		res+=-2.0*dtmp1*rsddf[h][k]/(columns-rnan);
		res+=dtmp2*trend_df[k];
		res+=2.0*dtmp1*trend_ddf[h][k];
		gsl_matrix_set(dH,k,h,res);
	      }
	    }
	    gsl_matrix_add (H,dH);
	  }
      	}
      }
      /* invert Hessian; dH store temporary LU decomp. */
      gsl_matrix_memcpy (dH,H);
      gsl_linalg_LU_decomp (dH,P,&signum);
      gsl_linalg_LU_invert (dH,P,covar);
      gsl_matrix_free(H);
      gsl_matrix_free(dH);
      gsl_permutation_free(P);
      gsl_matrix_scale (covar,sigma2);
    }
    break;
    
    
  case 2: /* H^{-1} J H^{-1} */
    {
      gsl_permutation * P = gsl_permutation_alloc (Pnum);
      int signum;
      gsl_matrix *H = gsl_matrix_calloc (Pnum,Pnum);
      gsl_matrix *dH = gsl_matrix_calloc (Pnum,Pnum);
      gsl_matrix *J = gsl_matrix_calloc (Pnum,Pnum);
      gsl_matrix *dJ = gsl_matrix_calloc (Pnum,Pnum);
      size_t h,k;    
      
      for(r=0;r<rows;r++){
	double rsf=0;
	double rsdf[Pnum];
	double rsddf[Pnum][Pnum];
	size_t rnan=0;
	for(h=0;h<Pnum;h++){
	  rsdf[h]=0;
	  for(k=0;k<Pnum;k++){
	    rsddf[h][k]=0;
	  }
	}
	
	for(c=0;c<columns;c++){
	  
	  for(b=0;b<blocks;b++)
	    values[b] = data[b][c][r];
	  
	  double dtmp1=evaluator_evaluate (F->f,blocks+Pnum,F->argname,values);
	  if(isfinite(dtmp1)){
	    rsf+=dtmp1;
	    for(h=0;h<Pnum;h++){
	      double dtmp2=evaluator_evaluate ((F->df)[h],blocks+Pnum,F->argname,values);
	      rsdf[h]+=dtmp2;
	      
	      for(k=0;k<Pnum;k++){
		double dtmp3=evaluator_evaluate ((F->ddf)[h][k],blocks+Pnum,F->argname,values);
		rsddf[h][k]+=dtmp3;
	      }
	    }
	    
	  }
	  else
	    rnan++;
	  
	}
	
	for(c=0;c<columns;c++){
	  for(b=0;b<blocks;b++)
	    values[b] = data[b][c][r];
	  
	  double dtmp1=evaluator_evaluate (F->f,blocks+Pnum,F->argname,values);
	  if(isfinite(dtmp1)){
	    for(h=0;h<Pnum;h++){
	      double dtmp2=evaluator_evaluate ((F->df)[h],blocks+Pnum,F->argname,values);
	      double dtmph=evaluator_evaluate ((F->df)[h],blocks+Pnum,F->argname,values)+trend_df[h]-(rsdf[h]/(columns-rnan));
	      for(k=0;k<Pnum;k++){
		double dtmp3=evaluator_evaluate ((F->df)[k],blocks+Pnum,F->argname,values);
		double dtmp4=evaluator_evaluate ((F->ddf)[h][k],blocks+Pnum,F->argname,values);
		double dtmpk=dtmp3+trend_df[k]-(rsdf[k]/(columns-rnan)); 
		double res=dtmp2*dtmp3;
		res+=dtmp1*dtmp4;
		res+=-dtmp2*rsdf[k]/(columns-rnan);
		res+=-2.0*dtmp1*rsddf[h][k]/(columns-rnan);
		res+=dtmp2*trend_df[k];
		res+=2.0*dtmp1*trend_ddf[h][k];
		gsl_matrix_set(dH,k,h,res);
		gsl_matrix_set(dJ,k,h,dtmph*dtmpk);
	      }
	    }
            gsl_matrix_scale(dJ,pow(dtmp1+trend_f-(rsf/(columns-rnan)),2.0));
	    gsl_matrix_add (H,dH);
            gsl_matrix_add (J,dJ);
	  }
	}
      }
      gsl_matrix_memcpy (dH,H);
      gsl_linalg_LU_decomp (dH,P,&signum);
      gsl_linalg_LU_invert (dH,P,H);
      gsl_blas_dgemm (CblasNoTrans,
		      CblasNoTrans,1.0,H,J,0.0,dJ);
      gsl_blas_dgemm (CblasNoTrans,
		      CblasNoTrans,1.0,dJ,H,0.0,covar);
      gsl_matrix_free(H);
      gsl_matrix_free(dH);
      gsl_matrix_free(J);
      gsl_matrix_free(dJ);
      gsl_permutation_free(P); 
    }
    break;


  case 3: /* J^{-1} */
    {
      gsl_permutation * P = gsl_permutation_alloc (Pnum);
      int signum;
      gsl_matrix *J = gsl_matrix_calloc (Pnum,Pnum);
      gsl_matrix *dJ = gsl_matrix_calloc (Pnum,Pnum);

      /* compute information matrix */
      
      for(r=0;r<rows;r++){
	for(c=0;c<columns;c++){
	  for(b=0;b<blocks;b++){
	    values[b] = data[b][c][r];
	  }
	  double dtmp1=pow(evaluator_evaluate (F->f,blocks+Pnum,F->argname,values)-fixed[r],2);
	  if(isfinite(dtmp1)){
	    for(h=0;h<Pnum;h++){
	      double dtmp2=evaluator_evaluate((F->df)[h],blocks+Pnum,F->argname,values);
	      for(k=0;k<Pnum;k++){
		double dtmp3=evaluator_evaluate((F->df)[k],blocks+Pnum,F->argname,values);
		gsl_matrix_set(dJ,k,h,dtmp1*dtmp2*dtmp3);
	      }	  
	    }
	    gsl_matrix_add (J,dJ);
	  }
      	}
      }      
      gsl_matrix_memcpy (dJ,J);
      gsl_linalg_LU_decomp (dJ,P,&signum);
      gsl_linalg_LU_invert (dJ,P,covar);
      gsl_matrix_free(J);
      gsl_matrix_free(dJ);
      gsl_permutation_free(P);
      gsl_matrix_scale (covar,sigma4);
    }
    break;
    
  case 4: /* H^{-1} */
    {
      gsl_permutation * P = gsl_permutation_alloc (Pnum);
      gsl_matrix *H = gsl_matrix_calloc (Pnum,Pnum);
      gsl_matrix *dH = gsl_matrix_calloc (Pnum,Pnum);
      
      int signum;
      
      /* compute hessian  */
       for(r=0;r<rows;r++){
	for(c=0;c<columns;c++){
	  for(b=0;b<blocks;b++){
	    values[b] = data[b][c][r];
	  }
	  double dtmp1=evaluator_evaluate (F->f,blocks+Pnum,F->argname,values)-fixed[r];
	  if(isfinite(dtmp1)){
	    for(h=0;h<Pnum;h++){
	      double dtmp2=evaluator_evaluate((F->df)[h],blocks+Pnum,F->argname,values);
	      for(k=0;k<Pnum;k++){
		double dtmp3=evaluator_evaluate((F->df)[k],blocks+Pnum,F->argname,values);
	        double dtmp4=evaluator_evaluate((F->ddf)[h][k],blocks+Pnum,F->argname,values);
		gsl_matrix_set(dH,k,h,dtmp2*dtmp3+dtmp1*dtmp4);
	      }	  
	    }
	    gsl_matrix_add (H,dH);
	  }
      	}
      }      
      /* invert Hessian; dH store temporary LU decomp. */
      gsl_matrix_memcpy (dH,H);
      gsl_linalg_LU_decomp (dH,P,&signum);
      gsl_linalg_LU_invert (dH,P,covar);
      gsl_matrix_free(H);
      gsl_matrix_free(dH);
      gsl_permutation_free(P);
      gsl_matrix_scale (covar,sigma2);
    }
    break;
    
  case 5: /* H^{-1} J H^{-1} */
    {
      
      gsl_permutation * P = gsl_permutation_alloc (Pnum);
      int signum;
      
      gsl_matrix *H = gsl_matrix_calloc (Pnum,Pnum);
      gsl_matrix *dH = gsl_matrix_calloc (Pnum,Pnum);
      
      gsl_matrix *J = gsl_matrix_calloc (Pnum,Pnum);
      gsl_matrix *dJ = gsl_matrix_calloc (Pnum,Pnum);
      
      /* compute Hessian and information matrix */
       for(r=0;r<rows;r++){
	for(c=0;c<columns;c++){
	  for(b=0;b<blocks;b++){
	    values[b] = data[b][c][r];
	  }
	  double dtmp1=evaluator_evaluate (F->f,blocks+Pnum,F->argname,values)-fixed[r];
	  if(isfinite(dtmp1)){
	    for(h=0;h<Pnum;h++){
	      double dtmp2=evaluator_evaluate((F->df)[h],blocks+Pnum,F->argname,values);
	      for(k=0;k<Pnum;k++){
		double dtmp3=evaluator_evaluate((F->df)[k],blocks+Pnum,F->argname,values);
	        double dtmp4=evaluator_evaluate((F->ddf)[h][k],blocks+Pnum,F->argname,values);
		gsl_matrix_set(dH,k,h,dtmp2*dtmp3+dtmp1*dtmp4);
                gsl_matrix_set(dJ,k,h,pow(dtmp1,2)*dtmp2*dtmp3);
	      }	  
	    }
	    gsl_matrix_add (H,dH);
            gsl_matrix_add (J,dJ);
	  }
      	}
      }      
  
      /* invert Hessian; dH store temporary LU decomp. */
      gsl_matrix_memcpy (dH,H);
      gsl_linalg_LU_decomp (dH,P,&signum);
      gsl_linalg_LU_invert (dH,P,H);
      
      /* dJ = H^{-1} J ; covar = dJ H^{-1} */
      gsl_blas_dgemm (CblasNoTrans,
		      CblasNoTrans,1.0,H,J,0.0,dJ);
      gsl_blas_dgemm (CblasNoTrans,
		      CblasNoTrans,1.0,dJ,H,0.0,covar);
      
      
      gsl_matrix_free(H);
      gsl_matrix_free(dH);
      gsl_matrix_free(J);
      gsl_matrix_free(dJ);
      gsl_permutation_free(P);
      
    }
    break;
    
  }
  
  
 return covar;

}

/* ----------------------------------------------------- */


/* Random effect object function and variance-covariance */
/* ----------------------------------------------------- */


void
obj_rand_f (const size_t Pnum, const double *x,void *params,double *fval){
  
  const Function *F = ((struct objdata *)params)->F;
  const size_t rows=((struct objdata *)params)->rows;
  const size_t columns=((struct objdata *)params)->columns;
  const size_t blocks=((struct objdata *)params)->blocks;
  double ***data=((struct objdata *)params)->data;
  double res=0;
  double values[Pnum+blocks];
  size_t r,b,c;
   
  for(b=blocks;b<blocks+Pnum;b++)
    values[b] = x[b-blocks];
  


  double dtmp1=0;
  double dtmp2=0;
  size_t nan=0;
  for(r=0;r<rows;r++){
    double dtmp3=0;
    double dtmp4=0;
    size_t rnan=0;
    for(c=0;c<columns;c++){
      for(b=0;b<blocks;b++)
	values[b] = data[b][c][r];
    
      double dtmp5=evaluator_evaluate (F->f,blocks+Pnum,F->argname,values);
     
      if(isfinite(dtmp5)){
	dtmp3 +=dtmp5;
	dtmp4 +=pow(dtmp5,2);
      }
      else{
	rnan++;
      }
    }
    nan +=rnan;
    if (columns-rnan!=0)
      dtmp1+=dtmp4-pow(dtmp3,2)/(columns-rnan);
    
    dtmp2+=pow(dtmp3,2);
  }
  dtmp1/=(rows*(columns-1)-nan);
  dtmp2/=(rows*columns-nan);
  
  res=(columns-(double)(nan/rows)-1)*log(dtmp1)+log(dtmp2);
  res /=(2*(columns-(double)(nan/rows))); 
  *fval=res;
 
}


void
obj_rand_df (const size_t Pnum, const double *x,void *params,double *grad){
  
  const Function *F = ((struct objdata *)params)->F;
  const size_t rows=((struct objdata *)params)->rows;
  const size_t columns=((struct objdata *)params)->columns;
  const size_t blocks=((struct objdata *)params)->blocks;
  double ***data=((struct objdata *)params)->data;
  double values[Pnum+blocks];
  double df1[Pnum],df2[Pnum];
  size_t k,b,r,c;    
 
  /* set the parameters */

  for(b=blocks;b<blocks+Pnum;b++)
    values[b] = x[b-blocks];
   
  for(k=0;k<Pnum;k++){
    df1[k]=0;
    df2[k]=0; 
  }
  
  double n_1=0, n_2=0;   // Normalization factors. 
  size_t nan=0;			/* total number of nan */
  for(r=0;r<rows;r++){
    
    double rsf=0,rsf2=0,rsdf[Pnum],rsfdf[Pnum],rsfdf_tau[Pnum];/* sum of f, df and fdf and fdf_tau */   
    size_t rnan=0;  /* nan's per row */
		                              
    /* initialize vectors */
    for(k=0;k<Pnum;k++)
      rsfdf_tau[k]=rsdf[k]=rsfdf[k]=0;
    
    for(c=0;c<columns;c++){
  
      
      for(b=0;b<blocks;b++)
	values[b] = data[b][c][r];
      
      double dtmp1 = evaluator_evaluate (F->f,blocks+Pnum,F->argname,values);
      
      if (isfinite(dtmp1)){
	rsf += dtmp1;
        rsf2 += dtmp1*dtmp1;
	for(k=0;k<Pnum;k++){	  
	  double dtmp2 = evaluator_evaluate ((F->df)[k],blocks+Pnum,F->argname,values);
	  rsdf[k] += dtmp2;
	  rsfdf[k] += dtmp1*dtmp2;
	}
      }
    }
    for(c=0;c<columns;c++){
     
      
      for(b=0;b<blocks;b++)
	values[b] = data[b][c][r];
      
      double dtmp1 = evaluator_evaluate (F->f,blocks+Pnum,F->argname,values);
      
      if (isfinite(dtmp1)){
	for(k=0;k<Pnum;k++){
	  rsfdf_tau[k] += dtmp1*rsdf[k];
	}
      } 
      else
	rnan++;
    }
    nan += rnan;
    if(columns-rnan!=0)
      n_1 += rsf2-pow(rsf,2)/(columns-rnan);
    
    n_2 += pow(rsf,2);
    
    /* update total sums */
    for(k=0;k<Pnum;k++){
      if(columns-rnan!=0)
	df1[k] += rsfdf[k] - rsfdf_tau[k]/(columns-rnan);
      
      df2[k] += rsfdf_tau[k];
    }
  }
  for(k=0;k<Pnum;k++){
    grad[k]= (columns-(double)(nan/rows))*(df1[k]/n_1)+(df2[k]/n_2);
    grad[k]/=(columns-(double)(nan/rows));
  } 
} 

void
obj_rand_fdf (const size_t Pnum, const double *x,void *params,double *f,double *grad){
  
  const Function *F = ((struct objdata *)params)->F;
  const size_t rows=((struct objdata *)params)->rows;
  const size_t columns=((struct objdata *)params)->columns;
  const size_t blocks=((struct objdata *)params)->blocks;
  double ***data=((struct objdata *)params)->data;
  double values[Pnum+blocks];
  double df1[Pnum],df2[Pnum];
  size_t k,r,b,c;
  double res;
  
  for(b=blocks;b<blocks+Pnum;b++)
    values[b] = x[b-blocks];
   
   for(k=0;k<Pnum;k++){
    df1[k]=0;
    df2[k]=0; 
  }
  
  double n_1=0, n_2=0;   	/* Normalization factors. */
  size_t nan=0;			/* Total number of nan.   */
  for(r=0;r<rows;r++){
    
    double rsf=0,rsf2=0,rsdf[Pnum],rsfdf[Pnum],rsfdf_tau[Pnum];/* sum of f, df and fdf and fdf_tau */   
    size_t rnan=0; 		                               /* nan's per row */
    /* initialize vectors */
    for(k=0;k<Pnum;k++)
      rsfdf_tau[k]=rsdf[k]=rsfdf[k]=0;
    
    for(c=0;c<columns;c++){
      double dtmp1;
      
      for(b=0;b<blocks;b++)
	values[b] = data[b][c][r];
      
      dtmp1 = evaluator_evaluate (F->f,blocks+Pnum,F->argname,values);
      
      if (isfinite(dtmp1)){
	rsf += dtmp1;
        rsf2 += dtmp1*dtmp1;
	for(k=0;k<Pnum;k++){	  
	  double dtmp2 = evaluator_evaluate ((F->df)[k],blocks+Pnum,F->argname,values);
	  rsdf[k] += dtmp2;
	  rsfdf[k] += dtmp1*dtmp2;
	}
      }
    }
    for(c=0;c<columns;c++){
      double dtmp1;
      
      for(b=0;b<blocks;b++)
	values[b] = data[b][c][r];
      
      dtmp1 = evaluator_evaluate (F->f,blocks+Pnum,F->argname,values);
      
      if (isfinite(dtmp1)){
	for(k=0;k<Pnum;k++){
	  rsfdf_tau[k] += dtmp1*rsdf[k];
	}
      } 
      else
	rnan++;
    }
    nan += rnan;
    if(columns-rnan!=0)
      n_1 += rsf2-pow(rsf,2)/(columns-rnan);
    
    n_2 += pow(rsf,2);
    
    /* update total sums */
    for(k=0;k<Pnum;k++){
      if(columns-rnan!=0)
	df1[k] += rsfdf[k] - rsfdf_tau[k]/(columns-rnan);
      
      df2[k] += rsfdf_tau[k];
    }
  }
  

  res=(columns-(double)(nan/rows))*log(n_1/(rows*(columns-1)-nan))+log(n_2/(rows*columns-nan));
  res/=(2*(columns-(double)(nan/rows)));
  *f=res;
  

  for(k=0;k<Pnum;k++){
    grad[k]= (columns-(double)(nan/rows))*(df1[k]/n_1)+(df2[k]/n_2);
    grad[k]/=(columns-(double)(nan/rows));
  } 


 
    
}

gsl_matrix *rand_varcovar(const int o_varcovar,  const double *x, const struct objdata params, const size_t Pnum){

  size_t i;
  const size_t rows=((struct objdata )params).rows;
  const size_t columns=((struct objdata )params).columns;
  const size_t blocks=((struct objdata )params).blocks;
  double ***data=((struct objdata )params).data;
  const Function *F = ((struct objdata )params).F;
  gsl_matrix *covar = gsl_matrix_alloc (Pnum,Pnum);
  double values[blocks+Pnum];
  size_t r,b,c;
  
  /* fill with optimal parameter */
  for(i=blocks;i<blocks+Pnum;i++){
    values[i] = x[i-blocks];
  }
        
  double sigma2=0,sigma2c=0;
  size_t nan=0;
  for(r=0;r<rows;r++){
    double rsf=0,rsf2=0;
    size_t rnan=0; 	 
    for(c=0;c<columns;c++){
      for(b=0;b<blocks;b++)
	values[b] = data[b][c][r];
      
      double dtmp1= evaluator_evaluate (F->f,blocks+Pnum,F->argname,values);
      
      if (isfinite(dtmp1)){
	rsf+=dtmp1;
	rsf2+=pow(dtmp1,2);
      }
      else
	rnan++;
    }
    nan+=rnan;
    if(columns-rnan>1){
      sigma2c+=(pow(rsf,2)-rsf2)/(columns-1-rnan);
      sigma2+=rsf2-(pow(rsf,2)/(columns-rnan));
    }
  }
  sigma2c/=(rows*columns-nan);
  sigma2/=(rows*(columns-1)-nan);
  size_t Tcorr=columns-(double)(nan/rows);
  double sigma4=pow(sigma2,2);
  double ratio=sigma2c/(Tcorr*sigma2c+sigma2);
 
  switch(o_varcovar){
  
  case 0: /* J^{-1} */
    {
      gsl_permutation * P = gsl_permutation_alloc (Pnum);
      int signum;
      gsl_matrix *J = gsl_matrix_calloc (Pnum,Pnum);
      gsl_matrix *dJ = gsl_matrix_calloc (Pnum,Pnum);
      size_t h,k;
      
      for(r=0;r<rows;r++){
        double rsdf[Pnum];
	for(k=0;k<Pnum;k++)
	  rsdf[k]=0;
   	
	for(c=0;c<columns;c++){
	  for(b=0;b<blocks;b++)
	    values[b] = data[b][c][r];
	  
	  double dtmp1= evaluator_evaluate (F->f,blocks+Pnum,F->argname,values);
	  
	  if (isfinite(dtmp1)){
	    for(k=0;k<Pnum;k++)
	      rsdf[k]+=evaluator_evaluate ((F->df)[k],blocks+Pnum,F->argname,values);
	  }
	  
	}
    	for(c=0;c<columns;c++){
	  for(b=0;b<blocks;b++)
	    values[b] = data[b][c][r];
	 
          double dtmp1= evaluator_evaluate (F->f,blocks+Pnum,F->argname,values);
	  
	  if (isfinite(dtmp1)){
	    for(k=0;k<Pnum;k++){
	      double dtmp2=evaluator_evaluate ((F->df)[k],blocks+Pnum,F->argname,values)+ratio*rsdf[k];
	      for(h=0;h<Pnum;h++){
		double dtmp3=evaluator_evaluate ((F->df)[h],blocks+Pnum,F->argname,values)+ratio*rsdf[h];
		double res=dtmp2*dtmp3;
		gsl_matrix_set(dJ,k,h,res);
	      }
	    }
	    gsl_matrix_scale(dJ,pow(dtmp1,2));
	    gsl_matrix_add (J,dJ);
	  } 
	}
      }
      gsl_matrix_memcpy (dJ,J);
      gsl_linalg_LU_decomp (dJ,P,&signum);
      gsl_linalg_LU_invert (dJ,P,covar);
      gsl_matrix_free(J);
      gsl_matrix_free(dJ);
      gsl_permutation_free(P);
      gsl_matrix_scale (covar,sigma4);
    }
    break;

  case 1: /* H^{-1} */
    {
      gsl_permutation * P = gsl_permutation_alloc (Pnum);
      gsl_matrix *H = gsl_matrix_calloc (Pnum,Pnum);
      gsl_matrix *dH = gsl_matrix_calloc (Pnum,Pnum);
      
      int signum;
      size_t h,k;    
      for(r=0;r<rows;r++){
        double rsdf[Pnum];
        double rsddf[Pnum][Pnum];
	for(k=0;k<Pnum;k++){
	  rsdf[k]=0;
	  for(h=0;h<Pnum;h++){
	    rsddf[h][k]=0;
	  }
   	}
	for(c=0;c<columns;c++){
	  for(b=0;b<blocks;b++)
	    values[b] = data[b][c][r];
	  
	  double dtmp1= evaluator_evaluate (F->f,blocks+Pnum,F->argname,values);
      
	  if (isfinite(dtmp1)){
	    for(k=0;k<Pnum;k++){
	      rsdf[k]+=evaluator_evaluate ((F->df)[k],blocks+Pnum,F->argname,values);
	      for(h=0;h<Pnum;h++){
		rsddf[h][k]+=evaluator_evaluate ((F->ddf)[h][k],blocks+Pnum,F->argname,values);
	      }
	    }  
	  }
	}
    	for(c=0;c<columns;c++){
	  for(b=0;b<blocks;b++)
	    values[b] = data[b][c][r];
	  
          double dtmp1= evaluator_evaluate (F->f,blocks+Pnum,F->argname,values);
	  
	  if (isfinite(dtmp1)){
	    for(k=0;k<Pnum;k++){
	      double dtmp2=evaluator_evaluate ((F->df)[k],blocks+Pnum,F->argname,values);
	      for(h=0;h<Pnum;h++){
		double dtmp3=evaluator_evaluate ((F->df)[h],blocks+Pnum,F->argname,values);
		double dtmp4=evaluator_evaluate ((F->ddf)[h][k],blocks+Pnum,F->argname,values);
		double res=dtmp2*dtmp3;
                res+=dtmp1*dtmp4;
                res+=ratio*dtmp3*rsdf[k];
                res+=ratio*dtmp1*rsddf[h][k];
		gsl_matrix_set (dH,k,h,res);
	      }
	    }
	    gsl_matrix_add (H,dH);
	  }
	} 
      }
      gsl_matrix_memcpy (dH,H);
      gsl_linalg_LU_decomp (dH,P,&signum);
      gsl_linalg_LU_invert (dH,P,covar);
      gsl_matrix_free(H);
      gsl_matrix_free(dH);
      gsl_permutation_free(P);
      gsl_matrix_scale (covar,sigma2);
    }
    break;

     case 2: /* H^{-1} J H^{-1} */
    {
      
      gsl_permutation * P = gsl_permutation_alloc (Pnum);
      int signum;
      gsl_matrix *H = gsl_matrix_calloc (Pnum,Pnum);
      gsl_matrix *dH = gsl_matrix_calloc (Pnum,Pnum);
      gsl_matrix *J = gsl_matrix_calloc (Pnum,Pnum);
      gsl_matrix *dJ = gsl_matrix_calloc (Pnum,Pnum);
      size_t h,k;    
      
      for(r=0;r<rows;r++){
        double rsdf[Pnum];
        double rsddf[Pnum][Pnum];
	for(k=0;k<Pnum;k++){
	  rsdf[k]=0;
	  for(h=0;h<Pnum;h++){
	    rsddf[h][k]=0;
	  }
   	}
	for(c=0;c<columns;c++){
	  for(b=0;b<blocks;b++)
	    values[b] = data[b][c][r];
	  
	  double dtmp1= evaluator_evaluate (F->f,blocks+Pnum,F->argname,values);
      
	  if (isfinite(dtmp1)){
	    for(k=0;k<Pnum;k++){
	      rsdf[k]+=evaluator_evaluate ((F->df)[k],blocks+Pnum,F->argname,values);
	      for(h=0;h<Pnum;h++){
		rsddf[h][k]+=evaluator_evaluate ((F->ddf)[h][k],blocks+Pnum,F->argname,values);
	      }
	    }  
	  }
	}
    	for(c=0;c<columns;c++){
	  for(b=0;b<blocks;b++)
	    values[b] = data[b][c][r];
	  
          double dtmp1= evaluator_evaluate (F->f,blocks+Pnum,F->argname,values);
	  
	  if (isfinite(dtmp1)){
	    for(k=0;k<Pnum;k++){
	      double dtmp2=evaluator_evaluate ((F->df)[k],blocks+Pnum,F->argname,values);
              double dtmp3=dtmp2+ratio*rsdf[k];
	      for(h=0;h<Pnum;h++){
		double dtmp4=evaluator_evaluate ((F->df)[h],blocks+Pnum,F->argname,values);
		double dtmp5=evaluator_evaluate ((F->ddf)[h][k],blocks+Pnum,F->argname,values);
		double dtmp6=dtmp4+ratio*rsdf[h];
		double res=dtmp2*dtmp4;
                res+=dtmp1*dtmp5;
                res+=ratio*dtmp4*rsdf[k];
                res+=ratio*dtmp1*rsddf[h][k];
                gsl_matrix_set (dJ,k,h,dtmp3*dtmp6);
                gsl_matrix_set (dH,k,h,res);
	      }
	    }
            gsl_matrix_scale(dJ,pow(dtmp1,2));
	    gsl_matrix_add (J,dJ);
	    gsl_matrix_add (H,dH);
	  }
	}
      }
      gsl_matrix_memcpy (dH,H);
      gsl_linalg_LU_decomp (dH,P,&signum);
      gsl_linalg_LU_invert (dH,P,H);
      gsl_blas_dgemm (CblasNoTrans,
		      CblasNoTrans,1.0,H,J,0.0,dJ);
      gsl_blas_dgemm (CblasNoTrans,
		      CblasNoTrans,1.0,dJ,H,0.0,covar);
      gsl_matrix_free(H);
      gsl_matrix_free(dH);
      gsl_matrix_free(J);
      gsl_matrix_free(dJ);
      gsl_permutation_free(P);
    } 
    break;
    
 case 3: /* J^{-1} */
    {
      gsl_permutation * P = gsl_permutation_alloc (Pnum);
      int signum;
      gsl_matrix *J = gsl_matrix_calloc (Pnum,Pnum);
      gsl_matrix *dJ = gsl_matrix_calloc (Pnum,Pnum);
      size_t h,k;
      
      for(r=0;r<rows;r++){
        double rsdf[Pnum];
	for(k=0;k<Pnum;k++)
	  rsdf[k]=0;
   	
	for(c=0;c<columns;c++){
	  for(b=0;b<blocks;b++)
	    values[b] = data[b][c][r];
	  
	  double dtmp1= evaluator_evaluate (F->f,blocks+Pnum,F->argname,values);
	  
	  if (isfinite(dtmp1)){
	    for(k=0;k<Pnum;k++)
	      rsdf[k]+=evaluator_evaluate ((F->df)[k],blocks+Pnum,F->argname,values);
	  }
	  
	}
    	for(c=0;c<columns;c++){
	  for(b=0;b<blocks;b++)
	    values[b] = data[b][c][r];
	 
          double dtmp1= evaluator_evaluate (F->f,blocks+Pnum,F->argname,values);
	  
	  if (isfinite(dtmp1)){
	    for(k=0;k<Pnum;k++){
	      double dtmp2=evaluator_evaluate ((F->df)[k],blocks+Pnum,F->argname,values)-ratio*rsdf[k];
	      for(h=0;h<Pnum;h++){
		double dtmp3=evaluator_evaluate ((F->df)[h],blocks+Pnum,F->argname,values)-ratio*rsdf[h];
		double res=dtmp2*dtmp3;
		gsl_matrix_set(dJ,k,h,res);
	      }
	    }
	    gsl_matrix_scale(dJ,pow(dtmp1,2));
	    gsl_matrix_add (J,dJ);
	  } 
	}
      }
      gsl_matrix_memcpy (dJ,J);
      gsl_linalg_LU_decomp (dJ,P,&signum);
      gsl_linalg_LU_invert (dJ,P,covar);
      gsl_matrix_free(J);
      gsl_matrix_free(dJ);
      gsl_permutation_free(P);
      gsl_matrix_scale (covar,sigma4);
    }
    break;

    
  case 4: /* H^{-1} */
    {
      gsl_permutation * P = gsl_permutation_alloc (Pnum);
      gsl_matrix *H = gsl_matrix_calloc (Pnum,Pnum);
      gsl_matrix *dH = gsl_matrix_calloc (Pnum,Pnum);
      
      int signum;
      size_t h,k;    
      for(r=0;r<rows;r++){
        double rsdf[Pnum];
        double rsddf[Pnum][Pnum];
	for(k=0;k<Pnum;k++){
	  rsdf[k]=0;
	  for(h=0;h<Pnum;h++){
	    rsddf[h][k]=0;
	  }
   	}
	for(c=0;c<columns;c++){
	  for(b=0;b<blocks;b++)
	    values[b] = data[b][c][r];
	  
	  double dtmp1= evaluator_evaluate (F->f,blocks+Pnum,F->argname,values);
      
	  if (isfinite(dtmp1)){
	    for(k=0;k<Pnum;k++){
	      rsdf[k]+=evaluator_evaluate ((F->df)[k],blocks+Pnum,F->argname,values);
	      for(h=0;h<Pnum;h++){
		rsddf[h][k]+=evaluator_evaluate ((F->ddf)[h][k],blocks+Pnum,F->argname,values);
	      }
	    }  
	  }
	}
    	for(c=0;c<columns;c++){
	  for(b=0;b<blocks;b++)
	    values[b] = data[b][c][r];
	  
          double dtmp1= evaluator_evaluate (F->f,blocks+Pnum,F->argname,values);
	  
	  if (isfinite(dtmp1)){
	    for(k=0;k<Pnum;k++){
	      double dtmp2=evaluator_evaluate ((F->df)[k],blocks+Pnum,F->argname,values);
	      for(h=0;h<Pnum;h++){
		double dtmp3=evaluator_evaluate ((F->df)[h],blocks+Pnum,F->argname,values);
		double dtmp4=evaluator_evaluate ((F->ddf)[h][k],blocks+Pnum,F->argname,values);
		double res=dtmp2*dtmp3;
                res+=dtmp1*dtmp4;
                res+=-ratio*dtmp3*rsdf[k];
                res+=-ratio*dtmp1*rsddf[h][k];
		gsl_matrix_set (dH,k,h,res);
	      }
	    }
	    gsl_matrix_add (H,dH);
	  }
	} 
      }
      gsl_matrix_memcpy (dH,H);
      gsl_linalg_LU_decomp (dH,P,&signum);
      gsl_linalg_LU_invert (dH,P,covar);
      gsl_matrix_free(H);
      gsl_matrix_free(dH);
      gsl_permutation_free(P);
      gsl_matrix_scale (covar,sigma2);
    }
    break;
 

  case 5: /* H^{-1} J H^{-1} */
    {
      
      gsl_permutation * P = gsl_permutation_alloc (Pnum);
      int signum;
      gsl_matrix *H = gsl_matrix_calloc (Pnum,Pnum);
      gsl_matrix *dH = gsl_matrix_calloc (Pnum,Pnum);
      gsl_matrix *J = gsl_matrix_calloc (Pnum,Pnum);
      gsl_matrix *dJ = gsl_matrix_calloc (Pnum,Pnum);
      size_t h,k;    
      
      for(r=0;r<rows;r++){
        double rsdf[Pnum];
        double rsddf[Pnum][Pnum];
	for(k=0;k<Pnum;k++){
	  rsdf[k]=0;
	  for(h=0;h<Pnum;h++){
	    rsddf[h][k]=0;
	  }
   	}
	for(c=0;c<columns;c++){
	  for(b=0;b<blocks;b++)
	    values[b] = data[b][c][r];
	  
	  double dtmp1= evaluator_evaluate (F->f,blocks+Pnum,F->argname,values);
      
	  if (isfinite(dtmp1)){
	    for(k=0;k<Pnum;k++){
	      rsdf[k]+=evaluator_evaluate ((F->df)[k],blocks+Pnum,F->argname,values);
	      for(h=0;h<Pnum;h++){
		rsddf[h][k]+=evaluator_evaluate ((F->ddf)[h][k],blocks+Pnum,F->argname,values);
	      }
	    }  
	  }
	}
    	for(c=0;c<columns;c++){
	  for(b=0;b<blocks;b++)
	    values[b] = data[b][c][r];
	  
          double dtmp1= evaluator_evaluate (F->f,blocks+Pnum,F->argname,values);
	  
	  if (isfinite(dtmp1)){
	    for(k=0;k<Pnum;k++){
	      double dtmp2=evaluator_evaluate ((F->df)[k],blocks+Pnum,F->argname,values);
              double dtmp3=dtmp2-ratio*rsdf[k];
	      for(h=0;h<Pnum;h++){
		double dtmp4=evaluator_evaluate ((F->df)[h],blocks+Pnum,F->argname,values);
		double dtmp5=evaluator_evaluate ((F->ddf)[h][k],blocks+Pnum,F->argname,values);
		double dtmp6=dtmp4-ratio*rsdf[h];
		double res=dtmp2*dtmp4;
                res+=dtmp1*dtmp5;
                res+=-ratio*dtmp4*rsdf[k];
                res+=-ratio*dtmp1*rsddf[h][k];
                gsl_matrix_set (dJ,k,h,dtmp3*dtmp6);
                gsl_matrix_set (dH,k,h,res);
	      }
	    }
            gsl_matrix_scale(dJ,pow(dtmp1,2));
	    gsl_matrix_add (J,dJ);
	    gsl_matrix_add (H,dH);
	  }
	}
      }
      gsl_matrix_memcpy (dH,H);
      gsl_linalg_LU_decomp (dH,P,&signum);
      gsl_linalg_LU_invert (dH,P,H);
      gsl_blas_dgemm (CblasNoTrans,
		      CblasNoTrans,1.0,H,J,0.0,dJ);
      gsl_blas_dgemm (CblasNoTrans,
		      CblasNoTrans,1.0,dJ,H,0.0,covar);
      gsl_matrix_free(H);
      gsl_matrix_free(dH);
      gsl_matrix_free(J);
      gsl_matrix_free(dJ);
      gsl_permutation_free(P);
    } 
    break;
  }
  
 
  return covar;
  
}



int main(int argc,char* argv[]){
 
  int i;

  char *splitstring = strdup(" \t");
  int o_verbose=0;
  int o_output=0;
  int o_varcovar=0;
  int o_model=0;

  /* data from sdtin */
  size_t rows=0,columns=0,blocks=0;
  double ***vals=NULL;

  /* definition of the function */
  Function F;
  char **Param=NULL;
  double *Pval=NULL;
  gsl_matrix *Pcovar=NULL;  /* covariance matrix */
  size_t Pnum=0;
  double llmin;

  /* object function parameters  */
  struct objdata param;

  /* minimization parameters */  
  /* step_size,tol,maxiter,epsabs,maxsize,method,verbosity */
  struct multimin_params mmparams={0.1,0.1,500,1e-6,1e-6,0};


  /* variables for reading command line options */
  /* ------------------------------------------ */
  int opt;
  /* ------------------------------------------ */
  
  /* COMMAND LINE PROCESSING */
  while((opt=getopt_long(argc,argv,"M:v:hF:O:V:e:A:",gb_long_options, &gb_option_index))!=EOF){
    if(opt==0){
      gbutils_header(argv[0],stdout);
      exit(0);
    }
    else if(opt=='?'){
      fprintf(stderr,"option %c not recognized\n",optopt);
      exit(-1);
    }
    else if(opt=='h'){
      /*print help*/
      fprintf(stdout,"\
Each row of a block represents a single process realization. Blocks\n\
are separated by two white spaces and represent model variables,\n\
columns represent the time variable. Input is read as\n\
(X1_[r,t],...,Xj_[r,t],...  XN_[r,t]), where r is the row, t the\n\
column and j the block. The model assumes that\n\
\n\
FUN(X1_[r,t],...,XN_[t,t]) - c_r = e_{r,t}\n\
\n\
with e i.i.d and 'c' arow specific element, which can be fixed (fixed\n\
effect) or a normal random variable (random effect).\n\
\n");
      fprintf(stdout,"\nUsage: %s [options] <FUN> \n\n",argv[0]);
      fprintf(stdout,"\
Options:\n\
 -M  type of model (default 0)\n\
      0  fixed effects\n\
      1  random effects\n\
 -O  type of output  (default 0)\n\
      0  parameters\n\
      1  parameters and errors\n\
      2  <variables> and panel statistics\n\
      3  parameters and variance matrix\n\
 -V  variance matrix estimation (default 0)\n\
      0  < J^{-1} >, computed via fully-reduced log-likelihood\n\
      1  < H^{-1} >, computed via fully-reduced log-likelihood\n\
      2  < H^{-1} J H^{-1} >, computed via fully-reduced log-likelihood\n\
      3  < J^{-1} >, computed via non-reduced log-likelihood\n\
      4  < H^{-1} >,  computed via non-reduced log-likelihood\n\
      5  < H^{-1} J H^{-1} >, computed via non-reduced log-likelihood\n\
 -v  verbosity level (default 0)\n\
      0  just results\n\
      1  comment headers\n\
      2  summary statistics\n\
      3  covariance matrix\n\
      4  minimization steps\n\
      5  model definition\n\
 -e  minimization tolerance (default 1e-6)\n\
 -F  input fields separators (default \" \\t\")\n\
 -h  this help\n\
 -A  comma separated MLL optimization options:\n\
     step,tol,iter,eps,msize,algo. Use empty fields for default.\n\
     (default 0.1,0.01,500,1e-6,1e-6,0)\n\
      step  initial step size of the searching algorithm\n\
      tol   line search tolerance iter: maximum number of iterations\n\
      eps  gradient tolerance : stopping criteria ||gradient||<eps\n\
      algo  optimization methods: 0 Fletcher-Reeves, 1 Polak-Ribiere, 2\n\
            Broyden-Fletcher-Goldfarb-Shanno, 3 Steepest descent (not\n\
            recommended), 4 simplex, 5 Broyden-Fletcher-Goldfarb-Shanno-2\n\
Examples:\n\
 gbnlpanel 'x1-a*x2-c,a=0,c=0' < data.dat  linear panel regression with one independent\n\
                                           variable\n");
      exit(0);
    }
    else if(opt=='A'){
   
      char *stmp1=strdup (optarg);
      char *stmp2;
      char *stmp3=stmp1;

      if( (stmp2=strsep (&stmp1,",")) != NULL){
	if(strlen(stmp2)>0) 
	  mmparams.step_size=atof(stmp2);
      }
      if( (stmp2=strsep (&stmp1,",")) != NULL){
	if(strlen(stmp2)>0) 
	  mmparams.tol=atof(stmp2);
      }
      if( (stmp2=strsep (&stmp1,",")) != NULL){
	if(strlen(stmp2)>0) 
	  mmparams.maxiter=(unsigned) atoi(stmp2);
      }
      if( (stmp2=strsep (&stmp1,",")) != NULL){
	if(strlen(stmp2)>0) 
	  mmparams.epsabs=atof(stmp2);
      }
      if( (stmp2=strsep (&stmp1,",")) != NULL){
	if(strlen(stmp2)>0) 
	  mmparams.maxsize=atof(stmp2);
      }
      if( (stmp2=strsep (&stmp1,",")) != NULL){
	if(strlen(stmp2)>0) 
	  mmparams.method= (unsigned) atoi(stmp2);
      }
     
      free(stmp3);
    }
    else if(opt=='F'){
      /*set the fields separator string*/
      free(splitstring);
      splitstring = strdup(optarg);
    }
    else if(opt=='M'){
      /* set the type of model */
      o_model = atoi(optarg);
      if(o_model<0 || o_model>1){
	fprintf(stderr,"ERROR (%s): model option '%d' not recognized. Try option -h.\n",GB_PROGNAME,o_model);
	exit(-1);
      }

    }
    else if(opt=='O'){
      /* set the type of output */
      o_output = atoi(optarg);
      if(o_output<0 || o_output>3){
	fprintf(stderr,"ERROR (%s): output option '%d' not recognized. Try option -h.\n",
		GB_PROGNAME,o_output);
	exit(-1);
      }
    }
    else if(opt=='V'){
      /* set the type of covariance */
      o_varcovar = atoi(optarg);
      if(o_varcovar<0 || o_varcovar>5){
	fprintf(stderr,"ERROR (%s): variance option '%d' not recognized. Try option -h.\n",
		GB_PROGNAME,o_varcovar);
	exit(-1);
      }
    }
    
    else if(opt=='v'){
      o_verbose = atoi(optarg);
      mmparams.verbosity=(o_verbose>2?o_verbose-2:0);
    }
  } 
  /* END OF COMMAND LINE PROCESSING */

  /* initialize global variables */
  initialize_program(argv[0]);
  
  /* load data */  
  loadtableblocks(&vals,&blocks,&rows,&columns,0,splitstring);


  /* parse line for functions and variables specification */
  if(optind == argc){
    fprintf(stderr,"ERROR (%s): please provide a function to fit.\n",
	    GB_PROGNAME);
    exit(-1);
  }
  
  for(i=optind;i<argc;i++){
    char *piece=strdup (argv[i]);
    char *stmp1=piece;
    char *stmp2;
    
    while( (stmp2=strsep (&stmp1,",")) != NULL ){/*take a piece*/
      char *stmp3 = strdup(stmp2);
      char *stmp4 = stmp3;
      char *stmp5;
      /* 	fprintf(stderr,"token:%s\n",stmp3); */

      /* initial condition */
      if( (stmp5=strsep(&stmp4,"=")) != NULL && stmp4 != NULL ){
	if( strlen(stmp5)>0 && strlen(stmp4)>0){
	  Pnum++;
	  Param=(char **) my_realloc((void *)  Param,Pnum*sizeof(char *));
	  Pval=(double *) my_realloc((void *) Pval,Pnum*sizeof(double));
	  
	  Param[Pnum-1] = strdup(stmp5);
	  Pval[Pnum-1] = atof(stmp4);
	}
      }
      else{ /* allocate new function */
	F.f = evaluator_create (stmp3);
	assert(F.f);
      }
      free(stmp3);
    }
    free(piece);
  }


  /* check that everything is correct */
  if(Pnum==0){
    fprintf(stderr,"ERROR (%s): please provide a parameter to estimate.\n",
	    GB_PROGNAME);
    exit(-1);
  }
 

  {
    size_t i,j;
    char **NewParam=NULL;
    double *NewPval=NULL;
    size_t NewPnum=0;

    char **storedname=NULL;
    size_t storednum=0;

    /* retrive list of arguments and their number */
    /* notice that storedname is not allocated */
    {
      int argnum;
      evaluator_get_variables (F.f,&storedname,&argnum);
      storednum = (size_t) argnum;
    }

    /* check the definition of the function */
    for(i=0;i<storednum;i++){
      char *stmp1 = storedname[i];
      if(*stmp1 == 'x'){
	size_t index = (atoi(stmp1+1)>0?atoi(stmp1+1):0);
	if(index>blocks){
	  fprintf(stderr,"ERROR (%s): block %zd not present in data\n",
		  GB_PROGNAME,index);
	  exit(-1);
	}
      }
      else {
	for(j=0;j<Pnum;j++)
	  if( strcmp(Param[j],stmp1)==0 ) break;
	if(j==Pnum){
	  fprintf(stderr,"ERROR (%s): parameter %s without initial value\n",
		  GB_PROGNAME,stmp1);
	  exit(-1);
	}
      }
    } 

    /* remove unnecessary parameters */
    for(i=0;i<Pnum;i++){
      for(j=0;j<storednum;j++)
	if( strcmp(Param[i],storedname[j])==0 ) break;
      if(j==storednum){
	fprintf(stderr,"WARNING (%s): irrelevant parameter %s removed\n",
		GB_PROGNAME,Param[i]);
	continue;
      }
      NewPnum++;
      NewParam=(char **) my_realloc((void *)  NewParam,NewPnum*sizeof(char *));
      NewPval=(double *) my_realloc((void *) NewPval,NewPnum*sizeof(double));
      NewParam[NewPnum-1] = strdup(Param[i]);
      NewPval[NewPnum-1] = Pval[i];
    }
  

    for(i=0;i<Pnum;i++)
      free(Param[i]);
    
    free(Param);
    free(Pval);
    
    Param = NewParam;
    Pval = NewPval;
    Pnum= NewPnum;
    
    if(storednum-Pnum<blocks){
      fprintf(stderr,"ERROR (%s): Number of function variables is not equal to number of rows. \n", GB_PROGNAME);
      exit(-1);
    }


    /* prepare the new list of argument names */
    F.argnum=blocks+Pnum;
    F.argname = (char **) my_alloc((blocks+Pnum)*sizeof(char *));
    
    for(i=0;i<blocks;i++){
      int length;
      length = snprintf(NULL,0,"x%zd",i+1);
      F.argname[i] = (char *) my_alloc((length+1)*sizeof(char));
      snprintf(F.argname[i],length+1,"x%zd",i+1);
    }

    for(i=blocks;i<blocks+Pnum;i++){
      int length;
      length = snprintf(NULL,0,"%s",Param[i-blocks]);
      F.argname[i] = (char *) my_alloc((length+1)*sizeof(char));
      snprintf(F.argname[i],length+1,"%s",Param[i-blocks]);
      
    }
 

    /* define first order derivatives */
    F.df = (void **) my_alloc(Pnum*sizeof(void *));
    for(i=0;i<Pnum;i++){
      F.df[i] = evaluator_derivative (F.f,Param[i]);
      assert(F.df[i]);
    } 

    /* define second order derivatives */
    if(o_varcovar>0){
      F.ddf = (void ***) my_alloc(Pnum*sizeof(void **));
      for(i=0;i<Pnum;i++)
	F.ddf[i] = (void **) my_alloc(Pnum*sizeof(void *));
      
      for(i=0;i<Pnum;i++)
	for(j=0;j<Pnum;j++){
	  F.ddf[i][j] = evaluator_derivative ((F.df)[i],Param[j]);
	  assert((F.ddf)[i][j]);
	}
    }
    
  }

  /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
  /* print builded functions and variables */
  if(o_verbose>4){
    size_t i,j;

    fprintf(stderr," ------------------------------------------------------------\n");

    fprintf(stderr,"  Model [xi: i-th data row]:\n");
    fprintf(stderr,"     %s-c ~ i.i.d. N(0,stdev) \n", evaluator_get_string (F.f) );
    fprintf (stderr,"\n  Parameters and initial conditions:\n");
    for(i=0;i<Pnum;i++)
      fprintf (stderr,"      %s = %f\n", Param[i],Pval[i]);

    fprintf (stderr,"\n  Model first derivatives:\n");
    for(i=0;i<Pnum;i++)
      fprintf (stderr,"     d f(x) / d%s = %s\n",Param[i],evaluator_get_string ((F.df)[i]));

    if(o_varcovar>0){
      fprintf (stderr,"\n  Model second derivatives:\n");
      for(i=0;i<Pnum;i++)
	for(j=0;j<Pnum;j++)
	  fprintf (stderr,"     d^2 f(x) / d%s d%s = %s\n",Param[i],Param[j],
		   evaluator_get_string ((F.ddf)[i][j]));
    }
  }

  /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */


  /* PARAMETERS ESTIMATION */
  /* --------------------- */

  /* set the parameter for the function */
    
  param.F = &F;
  param.rows = rows;
  param.columns = columns;
  param.blocks = blocks;
  param.data = vals;

  /* set the object structure */

  /* minimize negative log likelihood */
  /* -------------------------------- */
  if(o_model == 0){		/* fixed effects */

    multimin(Pnum,Pval,&llmin,NULL,NULL,NULL,obj_fixed_f,obj_fixed_df,obj_fixed_fdf,
	     (void *) &param, mmparams);

    /* build the covariance matrix */
    Pcovar = fixed_varcovar(o_varcovar,Pval,param,Pnum);

  }
  else{				/* random effects */
    multimin(Pnum,Pval,&llmin,NULL,NULL,NULL,obj_rand_f,obj_rand_df,obj_rand_fdf,
	     (void *) &param, mmparams);

    /* build the covariance matrix */
    Pcovar = rand_varcovar(o_varcovar,Pval,param,Pnum);
  }
 
  /* output */
  /* ------ */

  /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
  if(o_verbose>2){
    size_t i,j;

   
    fprintf(stderr,"  variance matrix                         = ");
    switch(o_varcovar){
    case 0: fprintf(stderr,"J^{-1} reduced\n"); break;
    case 1: fprintf(stderr,"H^{-1} reduced\n"); break;
    case 2: fprintf(stderr,"H^{-1} J H^{-1} reduced\n"); break;
    case 3: fprintf(stderr,"J^{-1} non-reduced\n"); break;
    case 4: fprintf(stderr,"H^{-1} non-reduced\n"); break;
    case 5: fprintf(stderr,"H^{-1} J H^{-1} non-reduced\n"); break;
    }
    
    fprintf(stderr,"\n");
    for(i=0;i<Pnum;i++){
      fprintf(stderr," %s = %+f +/- %f (%5.1f%%) | ",
	      Param[i],Pval[i],
	      sqrt(gsl_matrix_get(Pcovar,i,i)),
	      100.*sqrt(gsl_matrix_get(Pcovar,i,i))/fabs(Pval[i]));
      for(j=0;j<Pnum;j++)
	if(j != i)
	  fprintf(stderr,"%+f ",
		  gsl_matrix_get(Pcovar,i,j)/sqrt(gsl_matrix_get(Pcovar,i,i)*gsl_matrix_get(Pcovar,j,j)));
	else
	  fprintf(stderr,"%+f ",1.0);
	fprintf(stderr,"|\n");
    }    
  
  }
  /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */


  switch(o_output){
  case 0:
    {
      size_t i;
      /* +++++++++++++++++++++++++++++++++++ */
      if(o_verbose>0){
	fprintf(stdout,"#");
	for(i=0;i<Pnum-1;i++)
	  fprintf(stdout,EMPTY_SEP,Param[i]);
	i=Pnum-1;
	fprintf(stdout,EMPTY_NL,Param[i]);
      }
      /* +++++++++++++++++++++++++++++++++++ */

      printline(stdout,Pval,Pnum);
    }
    break;
  case 1:
    {
      size_t i;

      /* +++++++++++++++++++++++++++++++++++ */
      if(o_verbose>0){
	fprintf(stdout,"#");
	for(i=0;i<Pnum-1;i++){
	  fprintf(stdout,EMPTY_SEP,Param[i]);
	  fprintf(stdout,EMPTY_SEP,"+/-");
	}
	i=Pnum-1;
	fprintf(stdout,EMPTY_SEP,Param[i]);
	fprintf(stdout,EMPTY_NL,"+/-");
      }
      /* +++++++++++++++++++++++++++++++++++ */
      
      for(i=0;i<Pnum-1;i++){
	fprintf(stdout,FLOAT_SEP,Pval[i]);
	fprintf(stdout,FLOAT_SEP,sqrt(gsl_matrix_get(Pcovar,i,i)));
      }
      i=Pnum-1;
      fprintf(stdout,FLOAT_SEP,Pval[i]);
      fprintf(stdout,FLOAT_NL,sqrt(gsl_matrix_get(Pcovar,i,i)));
    }
    break;
  case 2:
    {
      
      size_t i,r,b,c;
      double values[Pnum+blocks];
      double fixed[rows];
      double trend_f=0;
      size_t nan=0;
      
      /* set the parameter values */
      for(b=blocks;b<blocks+Pnum;b++)
	values[b] = Pval[b-blocks];
      
      /* compute fixed effect and trends */
      /* initializing trend  */
      trend_f=0;
  
      /* trend computations */  
      for(r=0;r<rows;r++){
	size_t rnan=0;
	for(c=0;c<columns;c++){
	  for(b=0;b<blocks;b++)
	    values[b] = vals[b][c][r];
	  
	  double dtmp1=evaluator_evaluate (F.f,blocks+Pnum,F.argname,values);
	  if(isfinite(dtmp1)){
	    trend_f +=dtmp1; 
	  }
	  else
	    rnan++;      
	}
	nan+=rnan;
      }
      trend_f/=(rows*columns-nan);
      
      for(r=0;r<rows;r++){
	size_t rnan=0; // numebr of nan per columns.
	double rsf=0;
	double rsf2=0;
	for(c=0;c<columns;c++){
	  for(b=0;b<blocks;b++)
	    values[b] = vals[b][c][r];
	  
	  double dtmp1=evaluator_evaluate (F.f,blocks+Pnum,F.argname,values);
	  if(isfinite(dtmp1)){
	    rsf += dtmp1;
	    rsf2 += pow(dtmp1,2);
	  }
	  else
	    rnan++;
	  
	}
	if(columns-rnan!=0)
	  rsf /= (columns-rnan);
	
	fixed[r]=rsf-trend_f;
      }
      
     
      /* +++++++++++++++++++++++++++++++++++ */
      if(o_verbose>0){
	fprintf(stderr,"#");
	for(i=0;i<Pnum-1;i++){
	  fprintf(stderr,EMPTY_SEP,Param[i]);
	  fprintf(stderr,EMPTY_SEP,"+/-");
	}
	i=Pnum-1;
	fprintf(stderr,EMPTY_SEP,Param[i]);
	fprintf(stderr,EMPTY_NL,"+/-");
	
	for(i=0;i<Pnum-1;i++){
	  fprintf(stderr,FLOAT_SEP,Pval[i]);
	  fprintf(stderr,FLOAT_SEP,sqrt(gsl_matrix_get(Pcovar,i,i)));
	}
	i=Pnum-1;
	fprintf(stderr,FLOAT_SEP,Pval[i]);
	fprintf(stderr,FLOAT_NL,sqrt(gsl_matrix_get(Pcovar,i,i)));
      }
      /* +++++++++++++++++++++++++++++++++++ */
      if(o_model == 0){		/* fixed effects */
	for(r=0;r<rows;r++)
	  fprintf(stdout,"%+f \n",fixed[r]);
	
      }
      else if(o_model == 1){
	
	double sigma2=0,sigma2c=0;
	

	  size_t nan=0;
	  size_t r,b,c;
	  for(r=0;r<rows;r++){
	    double rsf=0,rsf2=0;
	    size_t rnan=0; 	 
	    for(c=0;c<columns;c++){
	      for(b=0;b<blocks;b++)
		values[b] = vals[b][c][r];
	      
	      double dtmp1= evaluator_evaluate (F.f,blocks+Pnum,F.argname,values);
	      
	      if (isfinite(dtmp1)){
		rsf+=dtmp1;
		rsf2+=pow(dtmp1,2);
	      }
	      else
		rnan++;
	    }
	    nan+=rnan;
            if(columns-rnan>1){
	      sigma2c+=(pow(rsf,2)-rsf2)/(columns-1-rnan);
	      sigma2+=rsf2-(pow(rsf,2)/(columns-rnan));
	    }
	  }
	  sigma2c/=(rows*columns-nan);
	  sigma2/=(rows*(columns-1)-nan);
      
	if (o_verbose>0){
	  fprintf(stderr,EMPTY_SEP,"#sigma2c");
	  fprintf(stderr,EMPTY_NL,"#sigma2");
          fprintf(stderr,FLOAT_SEP,sigma2c);
	  fprintf(stderr,FLOAT_NL,sigma2);
	}
	fprintf(stdout,FLOAT_SEP,sigma2c);
	fprintf(stdout,FLOAT_NL,sigma2);
	
	
      }
      
    
      
    }
    break;
  case 3:
    {
      size_t i;

      /* +++++++++++++++++++++++++++++++++++ */
      if(o_verbose>0){
	fprintf(stdout,"#");
	for(i=0;i<Pnum-1;i++)
	  fprintf(stdout,EMPTY_SEP,Param[i]);
	i=Pnum-1;
	fprintf(stdout,EMPTY_NL,Param[i]);
      }
      /* +++++++++++++++++++++++++++++++++++ */
      
      printline(stdout,Pval,Pnum);
      
      fprintf(stdout,"\n\n");
      
      for(i=0;i<Pnum;i++){
	size_t j;
	for(j=0;j<Pnum-1;j++)
	  fprintf(stdout,FLOAT_SEP,gsl_matrix_get(Pcovar,i,j));
	j=Pnum-1;
	fprintf(stdout,FLOAT_NL,gsl_matrix_get(Pcovar,i,j));
      }
    }
    break;
  }


    
  /* de-allocate function and data  */
  {
    size_t i;

    evaluator_destroy(F.f);

    for(i=0;i<Pnum;i++)
      evaluator_destroy(F.df[i]);

    free(F.df);

    for(i=0;i<F.argnum;i++)
      free(F.argname[i]);
    free(F.argname);
    
   
  }

  return 0;

}





