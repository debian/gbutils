/*
  gbgrid (ver. 5.6) -- Produce grid of data
  Copyright (C) 2008-2018 Giulio Bottazzi

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  (version 2) as published by the Free Software Foundation;
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#include "tools.h"
#include "matheval.h"
#include "assert.h"


/* handy structure used in computation */
typedef struct function {
  void *f;
  int argnum;
/*   char *args[] = { "x" }; */
  char **args;
} Function;


int main(int argc,char* argv[]){

  int i,j,h;

  int columns=10;
  int rows=10;

  int o_verbose=0;

  char *names[]={"c","r"};  
  double values[2]={1.,1.};  

  Function *F=NULL;
  int Fnum=0;



  /* COMMAND LINE PROCESSING */
  
  /* variables for reading command line options */
  /* ------------------------------------------ */
  int opt;
  /* ------------------------------------------ */
  
  while((opt=getopt_long(argc,argv,"hvc:r:o:",gb_long_options, &gb_option_index))!=EOF){
    if(opt==0){
      gbutils_header(argv[0],stdout);
      exit(0);
    }
    else if(opt=='?'){
      fprintf(stderr,"option %c not recognized\n",optopt);
      exit(-1);
    }
    else if(opt=='h'){
      /*print help*/
       fprintf(stdout,"Generate a grid using function f(r,c) where r is the row index and c is\n");
       fprintf(stdout,"the column index. Indeces start from 1. If more than one function are\n");
       fprintf(stdout,"provided they are printed one after the other.\n");
       fprintf(stdout,"\nUsage: %s [options] <function definition> \n\n",argv[0]);
       fprintf(stdout,"Options:\n");
       fprintf(stdout," -c  specify number of rows (default 10)\n");
       fprintf(stdout," -r  specify number of columns (default 10)\n");
       fprintf(stdout," -o  set the output format (default '%%12.6e')  \n");
       fprintf(stdout," -v  verbose mode\n");
       fprintf(stdout," -h  this help\n");
       fprintf(stdout,"Examples:\n");
       fprintf(stdout," gbgrid -c 1 -r 10 r+c  generates a vector with elements equal to the\n");
       fprintf(stdout,"                        sum of the row and column indexes\n");
       exit(0);
    }
    else if(opt=='v'){
      o_verbose=1;
    }
    else if(opt=='c'){
      columns=atoi(optarg);
    }
    else if(opt=='r'){
      rows=atoi(optarg);
    }
    else if(opt=='o'){
      /*set the ouptustring */
      FLOAT = strdup(optarg);
    }
  }
  /* END OF COMMAND LINE PROCESSING */

  /* initialize global variables */
  initialize_program(argv[0]);


  /* parse line for functions specification */
  for(i=optind;i<argc;i++){
    char *piece=strdup (argv[i]);
    char *stmp1=piece;
    char *stmp2;
    
    while( (stmp2=strsep (&stmp1,",")) != NULL ){/*take a piece*/
      char *stmp3 = strdup(stmp2);
      /* 	fprintf(stderr,"token:%s\n",stmp3); */
      
      /* allocate new function */
      Fnum++;
      F=(Function *) my_realloc((void *) F,Fnum*sizeof(Function));
      
      /* generate the formal expression */
      F[Fnum-1].f = evaluator_create (stmp3);
      assert(F[Fnum-1].f);
      free(stmp3);
      
      /* retrive list of argument */
      evaluator_get_variables (F[Fnum-1].f, &(F[Fnum-1].args), &(F[Fnum-1].argnum));

    }
    free(piece);
  }
  
  /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
  if(o_verbose==1){  /* check the builded functions */
    int i,j;
    
    for(i=0;i<Fnum;i++){
/*       char * string = (char *) malloc ((evaluator_calculate_length (F[i].f) + 1)*sizeof(char)); */
/*       evaluator_write (F[i].f, string); */
      fprintf (stderr,"f(x) = %s\t", evaluator_get_string (F[i].f));
      fprintf (stderr,"variables: ");
      for(j=0;j<F[i].argnum;j++){
	fprintf (stderr,EMPTY_SEP,F[i].args[j]);
      }
      fprintf (stderr,"\n");
    }
  }
  /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */


  /* output */
  if( strcspn (FLOAT,"fge") < strlen(FLOAT) ){

    for(i=0;i<rows;i++){
      values[1]=i+1.;
      for(j=0;j<columns;j++){
	values[0]=j+1.;
	for(h=0;h<Fnum;h++)
	  printf(FLOAT_SEP,evaluator_evaluate (F[h].f,2,names,values));
      }
      printf("\n");
    }

  }
  else if( strcspn (FLOAT,"d") < strlen(FLOAT) ){

    for(i=0;i<rows;i++){
      values[1]=i+1.;
      for(j=0;j<columns;j++){
	values[0]=j+1.;
	for(h=0;h<Fnum;h++)
	  printf(FLOAT_SEP, (int) floor(evaluator_evaluate (F[h].f,2,names,values)));
      }
      printf("\n");
    }

  }
  else{
    fprintf(stderr,"ERROR (%s): wrong output specification: %s\n",GB_PROGNAME,FLOAT);
    exit(-1);
  }
  

  return 0;

}
