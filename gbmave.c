/*
  gbmave (ver. 5.6) -- Produce moving average from data
  Copyright (C) 1998-2018 Giulio Bottazzi

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  (version 2) as published by the Free Software Foundation;
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#include "tools.h"

int main(int argc,char* argv[]){

    double **data=NULL; /* array of values */
    size_t rows=0,columns=0;
    double *average;

    size_t i,col;
    size_t lag=10;
    int o_printnum=0;

    char *splitstring = strdup(" \t");

    /* COMMAND LINE PROCESSING */
    
    /* variables for reading command line options */
    /* ------------------------------------------ */
    int opt;
    /* ------------------------------------------ */
    
    
    /* read the command line */    
    while((opt=getopt_long(argc,argv,"s:hnF:",gb_long_options, &gb_option_index))!=EOF){
	if(opt==0){
      gbutils_header(argv[0],stdout);
      exit(0);
    }
    else if(opt=='?'){
	    fprintf(stderr,"option %c not recognized\n",optopt);
	    exit(-1);
	}
	else if(opt=='F'){
	  /*set the fields separator string*/
	  free(splitstring);
	  splitstring = strdup(optarg);
	}
	else if(opt=='n'){
	    /*print the time steps numbers*/
	    o_printnum=1;
	}
	else if(opt=='s'){
	    /*set the lag*/
	    lag=(size_t) atoi(optarg);
	}
	else if(opt=='h'){
	    /*print short help*/
	  fprintf(stdout,"Compute moving average along each column. Data are read \n");
	  fprintf(stdout,"from standard input.\n");
	  fprintf(stdout,"\nUsage: %s [options]\n\n",argv[0]);
	  fprintf(stdout,"Options:\n");
	  fprintf(stdout," -s  number of steps to average (default 10)\n");
	  fprintf(stdout," -n  print a progressive tailing integer\n");
	  fprintf(stdout," -F  specify the input fields separators (default \" \\t\")\n");
	  fprintf(stdout," -h  this help\n");
	  exit(0);
	}
    }
    /* END OF COMMAND LINE PROCESSING */
    
    /* initialize global variables */
    initialize_program(argv[0]);

    /* load data: data[column][row] */
    loadtable(&data,&rows,&columns,0,splitstring);

    if(rows<lag){
      exit(-1);
    }

    average = (double *) my_calloc(columns,sizeof(double));

    for(col=0;col<columns;col++){
      for(i=0;i<lag;i++){
	average[col]+=data[col][i];
      }
      average[col]/=lag;
    }

    while(i<rows){
      if(o_printnum)
	printf(INT_SEP,i);

      for(col=0;col<columns-1;col++){
	printf(FLOAT_SEP,average[col]);
	average[col]-=data[col][i-lag]/lag;
	average[col]+=data[col][i]/lag; 
      }
      printf(FLOAT_NL,average[columns-1]);
      average[columns-1]-=data[columns-1][i-lag]/lag;
      average[columns-1]+=data[columns-1][i]/lag; 
      i++;
    }

    if(o_printnum)
      printf(INT_SEP,i);
    for(col=0;col<columns-1;col++){
      printf(FLOAT_SEP,average[col]);
    }
    printf(FLOAT_NL,average[columns-1]);

    return 0;
}
