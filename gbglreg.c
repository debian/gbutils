/*
  gbglreg  (ver. 5.6) -- Estimate general linear regression model
  Copyright (C) 2005-2018 Giulio Bottazzi

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  (version 2) as published by the Free Software Foundation;
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#include "tools.h"
#include <gsl/gsl_multifit.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_cdf.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_linalg.h>



#include <gsl/gsl_statistics_double.h>

void print_glinear_verbose(FILE *file,const int o_weight,const int o_model,
			   const size_t N, const gsl_vector * coeffs,
			   const gsl_matrix * cov,const double sumsq)
{

  size_t i,j;
  const size_t K = coeffs->size;

  fprintf(file,"\n");
  fprintf(file," ------------------------------------------------------------\n");
  fprintf(stderr,"  number of observations                  = %zd\n",N);
  fprintf(stderr,"  number of indep. variables              = %zd\n",K);
  fprintf(file,"\n");
  fprintf(stderr,"  model (C[i]: i-th column; ci: i-th coefficient; eps: residual;):\n\n");
  fprintf(stderr,"    C[%zd] ~ ",K+1);
  for(i=0;i<K-1;i++)
    fprintf(stderr," c%zd*C[%zd] +",i+1,i+1);
  if(o_model==0)
    fprintf(stderr," c%zd*1 +",K);
  else
    fprintf(stderr," c%zd*C[%zd] +",K,K);
  if(o_weight)
    fprintf(stderr," eps*C[%zd]\n",K+2);
  else
    fprintf(stderr," eps\n");
  
  fprintf(file,"\n");
  fprintf(file," ------------------------------------------------------------\n");
  if(!o_weight){
    fprintf(file,"  sum of squared residual           (SSR) = %f\n",sumsq);
    fprintf(file,"  number degrees of freedom         (ndf) = %zd\n",N-K);
    fprintf(file,"                            sqrt(SSR/ndf) = %f\n",sqrt(sumsq/(N-K)));
      fprintf(file,"  chi-square test           P(>SSR | ndf) = %e\n",
	      gsl_cdf_chisq_Q (sumsq,(N-K)));
  }
  else{
    fprintf(file,"  sum of weighted squared residual (WSSR) = %f\n",sumsq);
    fprintf(file,"  number degrees of freedom         (ndf) = %zd\n",N-K);
    fprintf(file,"                           sqrt(WSSR/ndf) = %f\n",sqrt(sumsq/(N-K)));
    fprintf(file,"  chi-square test          P(>WSSR | ndf) = %e\n",
	    gsl_cdf_chisq_Q (sumsq,(N-K)));
    
  }
  fprintf(file," ------------------------------------------------------------\n");
  
  /* Normal approximation to chi^2 */
  /*     fprintf(file,"            chisq. test Q(ndf/2,WSSR/2) = %e\n", */
  /* 	    gsl_cdf_gaussian_Q(sumsq-(size-cov->size1),sqrt(2*(size-cov->size1)))); */
  
  fprintf(file,"\n");
  fprintf(file," ------------------------------------------------------------\n");
  fprintf(file,"\n");
  for(i=0;i<K;i++){
    fprintf(file," c%zd= %+f +/- %f (%5.1f%%) | ",
	    i+1,gsl_vector_get(coeffs,i),
	    sqrt(gsl_matrix_get(cov,i,i)),
	    100.*sqrt(gsl_matrix_get(cov,i,i))/fabs(gsl_vector_get(coeffs,i)));
    for(j=0;j<coeffs->size;j++)
      fprintf(file,"%+f ",
	      gsl_matrix_get(cov,i,j)/sqrt(gsl_matrix_get(cov,i,i)*gsl_matrix_get(cov,j,j)));
    fprintf(file,"|\n");
  }
  fprintf(file," ------------------------------------------------------------\n");
  fprintf(file,"\n");

}



void hccs(gsl_vector *Y,gsl_matrix *X,gsl_vector *C,gsl_matrix *COV,int o_covtype)
{

  size_t i,j;
  
  const size_t rows = Y->size;
  const size_t columns = COV->size1+1;

  /* iS = (X^t X)^{-1} */
  gsl_matrix * iS = gsl_matrix_alloc (columns-1,columns-1);
  
  /* RESidual */
  gsl_vector * RES = gsl_vector_alloc (rows);
  
  /* HCCV - diagonal */
  gsl_vector * PHI = gsl_vector_alloc (rows);
  
  
  {/* iS = (X^t X)^{-1} */
    
    gsl_matrix * S = gsl_matrix_alloc (columns-1,columns-1);
    gsl_permutation * P = gsl_permutation_alloc (columns-1);
    int signum;
	  
    /*  S = X^t X */
    gsl_blas_dgemm (CblasTrans,CblasNoTrans,1.0,X,X,0.0,S);
	  
    /* iS = S^{-1} */
    gsl_linalg_LU_decomp (S,P,&signum);
    gsl_linalg_LU_invert (S,P,iS);
	  
    gsl_permutation_free(P);
    gsl_matrix_free(S);
  }
	
  /* RES=Y-XC */
  gsl_vector_memcpy (RES,Y);
  gsl_blas_dgemv (CblasNoTrans,-1.0,X,C,1.0,RES);
	
	
  switch(o_covtype ){
  case 1:	/* PHI_i = RES_i^2  */
    {
      gsl_vector_memcpy (PHI,RES);
      gsl_vector_mul (PHI,RES);
    }
    break;
  case 2: /* PHI_i = N/(N-K) RES_i^2  */
    {
      gsl_vector_memcpy (PHI,RES);
      gsl_vector_mul (PHI,RES);
      gsl_vector_scale (PHI,rows/(rows-columns+1));
    }
    break;
  case 3: /* PHI_i = RES_i^2/(1-H_i)  */
    {
	    
      for(i=0;i<rows;i++){
	gsl_vector_const_view VTMP1 = gsl_matrix_const_row (X,i);
	gsl_vector * VTMP2 = gsl_vector_alloc (columns-1);
	double dtmp1;
	double dtmp2=gsl_vector_get(RES,i);
	      
	gsl_blas_dgemv (CblasNoTrans,1.0,iS,&VTMP1.vector,0.0,VTMP2);
	      
	gsl_blas_ddot (&VTMP1.vector,VTMP2,&dtmp1);
	      
	gsl_vector_set(PHI,i,dtmp2*dtmp2/(1.-dtmp1));
	      
	gsl_vector_free(VTMP2);
      }
    }
    break;
  case 4: /* PHI_i = RES_i^2/(1-H_i)^2  */
    {
	    
      for(i=0;i<rows;i++){
	gsl_vector_const_view VTMP1 = gsl_matrix_const_row (X,i);
	gsl_vector * VTMP2 = gsl_vector_alloc (columns-1);
	double dtmp1;
	      
	gsl_blas_dgemv (CblasNoTrans,1.0,iS,&VTMP1.vector,0.0,VTMP2);
	      
	gsl_blas_ddot (&VTMP1.vector,VTMP2,&dtmp1);
	      
	dtmp1= gsl_vector_get(RES,i)/(1.-dtmp1);
	      
	gsl_vector_set(PHI,i,dtmp1*dtmp1);
	      
	gsl_vector_free(VTMP2);
      }
    }
    break;
  default:
    fprintf(stderr,"ERROR (%s): unrecognized covariance option:%d\n",
	    GB_PROGNAME,o_covtype);
    exit(-1);
    break;
  }

  /* redefine covariance matrix */
  {
	  
    gsl_matrix * MTMP1 = gsl_matrix_alloc (rows,columns-1);
    gsl_matrix * MTMP2 = gsl_matrix_alloc (columns-1,columns-1);

	  
    /* MTMP1 = X */
    gsl_matrix_memcpy (MTMP1,X);
	  
    /* MTMP1_{n,h} = MTMP1_{n,h} PHI_{n} */
    for(i=0;i<rows;i++)
      for(j=0;j<columns-1;j++)
	gsl_matrix_set(MTMP1,i,j,gsl_matrix_get(MTMP1,i,j)*gsl_vector_get(PHI,i));

/*     gsl_matrix_fprintf (stdout,COV,"1 : %f"); */
	  
    /* COV = MTMP1^t X */
    gsl_blas_dgemm (CblasTrans,CblasNoTrans,1.0,MTMP1,X,0.0,COV);

/*     gsl_matrix_fprintf (stdout,COV,"2 : %f"); */
/*     gsl_matrix_fprintf (stdout,iS,"iS: %f"); */
	  
    /* COV = iS CORR iS */
    gsl_blas_dgemm (CblasNoTrans,CblasNoTrans,1.0,COV,iS,0.0,MTMP2);

/*     gsl_matrix_fprintf (stdout,MTMP2,"3 : %f"); */

    gsl_blas_dsymm (CblasLeft,CblasUpper,1.0,iS,MTMP2,0.0,COV);

/*     gsl_matrix_fprintf (stdout,COV,"4 : %f"); */


    gsl_matrix_free(MTMP1);
    gsl_matrix_free(MTMP2);
  }

  gsl_matrix_free(iS);
  gsl_vector_free(RES);
  gsl_vector_free(PHI);


}



int main(int argc,char* argv[]){

  size_t rows=0,columns=0;
  double **data=NULL;

  size_t N; /* number of observations */
  size_t K; /* number of parameters */

  /* workspace */
  gsl_multifit_linear_workspace * WORK;
  
  /* imput variables */
  gsl_matrix * X;
  gsl_vector * Y;

  /* output variable */
  gsl_vector * C ;
  gsl_matrix * COV;
  double CHISQ;

  /* weights */
  gsl_vector * W=NULL;

  char *splitstring = strdup(" \t");

  int o_verbose=0;
  int o_output=0;
  int o_weight=0;
  int o_covtype=0;
  int o_model=1;

  
  /* variables for reading command line options */
  /* ------------------------------------------ */
  int opt;
  /* ------------------------------------------ */
  
  /* COMMAND LINE PROCESSING */
  while((opt=getopt_long(argc,argv,"v:hF:O:wV:M:",gb_long_options, &gb_option_index))!=EOF){
    if(opt==0){
      gbutils_header(argv[0],stdout);
      exit(0);
    }
    else if(opt=='?'){
      fprintf(stderr,"option %c not recognized\n",optopt);
      exit(-1);
    }
    else if(opt=='F'){
      /* set the fields separator string*/
      free(splitstring);
      splitstring = strdup(optarg);
    }
    else if(opt=='M'){
      /* set the model to use*/
      o_model = atoi(optarg);
    }
    else if(opt=='O'){
      /* set the type of output */
      o_output = atoi(optarg);
      if(o_output<0 || o_output>4){
	fprintf(stderr,"ERROR (%s): output option '%d' not recognized. Try option -h.\n",
		GB_PROGNAME,o_output);
	exit(-1);
      }
    }
    else if(opt=='w'){
      /* set third columns as weight */
      o_weight = 1;
    }
    else if(opt=='V'){
      /* set the type of covariance */
      o_covtype = atoi(optarg);
      if(o_covtype<0 || o_covtype>4){
	fprintf(stderr,"ERROR (%s): variance option '%d' not recognized. Try option -h.\n",
		GB_PROGNAME,o_covtype);
	exit(-1);
      }

    }
    else if(opt=='v'){
      /* set verbosity level*/
      o_verbose = atoi(optarg);
    }
    else if(opt=='h'){
      /* print short help */
      fprintf(stdout,"General Linear regression. Data are read in columns (X_1 .. X_N Y). The\n");
      fprintf(stdout,"last column contains the dependent observations. With option -w standard\n");
      fprintf(stdout,"errors associated with the observations can be provided. In this case data\n");
      fprintf(stdout,"are read as (X_1...X_N,Y,std(Y)).\n");
      fprintf(stdout,"\nUsage: %s [options]\n\n",argv[0]);
      fprintf(stdout,"Options:\n");
      fprintf(stdout," -M  the regression model (default 1)\n");
      fprintf(stdout,"      0  with estimated intercept\n");
      fprintf(stdout,"      1  with zero intercept       \n");
      fprintf(stdout," -w  consider standard errors   \n");
      fprintf(stdout," -O  the type of output (default 0) \n");
      fprintf(stdout,"      0  regression coefficients \n");
      fprintf(stdout,"      1  regression coefficients and errors\n");
      fprintf(stdout,"      2  x, fitted y, error on y, residual\n");
      fprintf(stdout,"      3  coefficients and variance matrix\n");
      fprintf(stdout,"      4  coefficients and explained variance\n");
      fprintf(stdout," -V  method to estimate variance matrix (default 0)\n");
      fprintf(stdout,"      0  ordinary least square estimator            \n");
      fprintf(stdout,"      1  heteroscedastic consistent White estimator \n");
      fprintf(stdout,"      2  Hinkley adjusted White estimator           \n");
      fprintf(stdout,"      3  Horn-Horn-Duncan adjusted White estimator  \n");
      fprintf(stdout,"      4  jacknife estimator                         \n");
      fprintf(stdout," -v  verbosity level (default 0)\n");
      fprintf(stdout,"      0  just output   \n");
      fprintf(stdout,"      1  commented headings   \n");
      fprintf(stdout,"      2  model details   \n");
      fprintf(stdout," -h  print this help    \n");
      fprintf(stdout," -F  specify the input fields separators (default \" \\t\")\n");
      exit(0);
    }
  }
  /* END OF COMMAND LINE PROCESSING */

  /* initialize global variables */
  initialize_program(argv[0]);

  /* load data */
  loadtable(&data,&rows,&columns,0,splitstring);

  /* set the method to use and initialize variables */
  N = rows;
  switch(o_model){
  case 0:
    if(o_weight)
      K = columns-1;
    else
      K = columns;
    break;
  case 1:
    if(o_weight)
      K = columns-2;
    else
      K = columns-1;
    break;
  default:
    fprintf(stderr,"ERROR (%s): unrecognized model:%d\n",
	    GB_PROGNAME,o_model);
    exit(-1);
    break;
  }

  /* allocate workspace */
  WORK = gsl_multifit_linear_alloc (N,K);
  
  /* allocate imput variables */
  X = gsl_matrix_alloc (N,K);
  Y = gsl_vector_alloc (N);
  
  /* allocate output variable */
  C =  gsl_vector_alloc (K);
  COV =  gsl_matrix_alloc (K,K);

  /* allocate weights */
  if(o_weight)
    W = gsl_vector_alloc (N);

  /* initialize input variables */
  if(o_weight){
    size_t i,j;
    
    for(i=0;i<rows;i++){
      gsl_vector_set (Y,i,data[columns-2][i]);
      gsl_vector_set (W,i,1./(data[columns-1][i]*data[columns-1][i]));
      for(j=0;j<columns-2;j++)
	gsl_matrix_set(X,i,j,data[j][i]);
    }
    
    if(o_model==0)
    for(i=0;i<rows;i++)
      gsl_matrix_set(X,i,K-1,1.0);
  }
  else{
    size_t i,j;
    
    for(i=0;i<rows;i++)
      for(j=0;j<columns-1;j++)
	gsl_matrix_set(X,i,j,data[j][i]);
    for(i=0;i<rows;i++)
      gsl_vector_set (Y,i,data[columns-1][i]);
  
    if(o_model==0)
    for(i=0;i<rows;i++)
      gsl_matrix_set(X,i,K-1,1.0);
    
  }

  {
    size_t i;
    for(i=0;i<columns;i++) free(data[i]);
    free(data);
  }

  /* fit the model */
  if(o_weight)
    gsl_multifit_wlinear (X,W,Y,C,COV, &CHISQ,WORK);
  else
    gsl_multifit_linear (X,Y,C,COV, &CHISQ,WORK);
  gsl_multifit_linear_free(WORK);

  /* possibly apply heteroscedastic consistent estimators */
  if(o_covtype > 0)
    hccs(Y,X,C,COV,o_covtype);

  /* ++++++++++++++++++++++++++++++++++++++++ */
  /* in case, print verbose output */
  if(o_verbose>1)
    print_glinear_verbose(stdout,o_weight,o_model,N,C,COV,CHISQ);
  /* ++++++++++++++++++++++++++++++++++++++++ */

  /* output */
  switch(o_output){
  case 0:
    {
      size_t i;

      /* ++++++++++++++++++++++++++++++++++++++++ */
      if(o_verbose>0){
	fprintf(stdout,"#");
	for(i=0;i<C->size;i++){
	  char *label;

	  int length;
	  length = snprintf(NULL,0,"c%zd",i+1);
	  label = (char *) my_alloc((length+1)*sizeof(char));
	  snprintf(label,length+1,"c%zd",i+1);

	  fprintf(stdout,EMPTY_SEP,label);
	  free(label);
	}
	fprintf(stdout,"\n");
      }
      /* ++++++++++++++++++++++++++++++++++++++++ */

      for(i=0;i<C->size;i++)
	fprintf(stdout,FLOAT_SEP,gsl_vector_get(C,i));
      fprintf (stdout,"\n");
      
    }      
    break;
  case 1:
    {

      size_t i;

      /* ++++++++++++++++++++++++++++++++++++++++ */
      if(o_verbose>0){
	fprintf(stdout,"#");
	for(i=0;i<C->size;i++){
	  char *label;

	  int length;
	  length = snprintf(NULL,0,"c%zd",i+1);
	  label = (char *) my_alloc((length+1)*sizeof(char));
	  snprintf(label,length+1,"c%zd",i+1);

	  fprintf(stdout,EMPTY_SEP,label);
	  free(label);
	  fprintf(stdout,EMPTY_SEP,"+/-");
	}
	fprintf(stdout,"\n");
      }
      /* ++++++++++++++++++++++++++++++++++++++++ */

      for(i=0;i<C->size;i++){
	fprintf(stdout,FLOAT_SEP,gsl_vector_get(C,i));
	fprintf(stdout,FLOAT_SEP,sqrt(gsl_matrix_get(COV,i,i)));
      }
      fprintf(stdout,"\n");

    }
    break;
  case 2:
    {
      size_t i,j;

      gsl_vector * RES = gsl_vector_alloc (N);
      gsl_vector * YERR = gsl_vector_alloc (N);


      /* compute residuals: RES=Y-XC */
      gsl_vector_memcpy (RES,Y);
      gsl_blas_dgemv (CblasNoTrans,-1.0,X,C,1.0,RES);


      /* compute errors YERR_n = \sum_{h,k} X_{n,h} X_{n,k} COV_{h,k}  */
      {
	  
	gsl_matrix * MTMP = gsl_matrix_alloc (N,K);
	  
	/* MTMP=X COV */
	gsl_blas_dgemm (CblasNoTrans,CblasNoTrans,1.0,X,COV,0.0,MTMP);

	/* YERR_n = \sum_h MTMP_{n,h} X_{n,h} */
	for(i=0;i<N;i++){
	  double dtmp1;
	  gsl_vector_const_view VTMP1 = gsl_matrix_const_row (X,i);
	  gsl_vector_const_view VTMP2 = gsl_matrix_const_row (MTMP,i);

	  gsl_blas_ddot (&VTMP1.vector,&VTMP2.vector,&dtmp1);
	  gsl_vector_set(YERR,i,sqrt(dtmp1));
	}
	  
	gsl_matrix_free (MTMP);

      }


      /* ++++++++++++++++++++++++++++++++++++++++ */
      if(o_verbose>0){
	fprintf(stdout,"#");
	for(i=0;i<C->size;i++){
	  char *label;

	  int length;
	  length = snprintf(NULL,0,"c%zd",i+1);
	  label = (char *) my_alloc((length+1)*sizeof(char));
	  snprintf(label,length+1,"c%zd",i+1);

	  fprintf(stdout,EMPTY_SEP,label);
	  free(label);
	  fprintf(stdout,EMPTY_SEP,"+/-");
	}
	fprintf(stdout,"\n");
	for(i=0;i<C->size;i++){
	  fprintf(stdout,FLOAT_SEP,gsl_vector_get(C,i));
	  fprintf(stdout,FLOAT_SEP,sqrt(gsl_matrix_get(COV,i,i)));
	}
	fprintf(stdout,"\n");
      }
      /* ++++++++++++++++++++++++++++++++++++++++ */


      for(i=0;i<N;i++){
	for(j=0;j<K;j++)
	  fprintf(stdout,FLOAT_SEP,gsl_matrix_get(X,i,j));
	fprintf(stdout,FLOAT_SEP, gsl_vector_get(Y,i)-gsl_vector_get(RES,i));
	fprintf(stdout,FLOAT_SEP, gsl_vector_get(YERR,i));
	fprintf(stdout,FLOAT_NL, gsl_vector_get(RES,i));
      }

    }
    break;
  case 3:
    {

      size_t i,j;

      /* ++++++++++++++++++++++++++++++++++++++++ */
      if(o_verbose>0){
	fprintf(stdout,"#");
	for(i=0;i<C->size;i++){
	  char *label;

	  int length;
	  length = snprintf(NULL,0,"c%zd",i+1);
	  label = (char *) my_alloc((length+1)*sizeof(char));
	  snprintf(label,length+1,"c%zd",i+1);

	  fprintf(stdout,EMPTY_SEP,label);
	  free(label);
	}
	fprintf(stdout,"\n");
      }
      /* ++++++++++++++++++++++++++++++++++++++++ */

      for(i=0;i<C->size;i++)
	fprintf(stdout,FLOAT_SEP,gsl_vector_get(C,i));
      fprintf(stdout,"\n\n\n");

      for(i=0;i<C->size;i++){
	for(j=0;j<C->size;j++)
	  fprintf(stdout,FLOAT_SEP,gsl_matrix_get(COV,i,j));
	fprintf(stdout,"\n");
      }
    }
    break;
  case 4:
    {

      size_t i,j;

      const double YVAR= gsl_stats_tss(Y->data,Y->stride,Y->size)/N; /* variance of dependent variable */
      const double RESVAR=CHISQ/N; /* variance of residuals */

      /* compute the varcovar matrix of regressors */
      /* XVAR_{i,j} = 1/(N-1) \sum_n (X_{n,i}-X_i) (X_{n,j}-X_j) with
	 X_i = 1/N \sum_n X_{n,i} */

      gsl_matrix *XVAR = gsl_matrix_alloc (K,K);
      for (i = 0; i < XVAR->size1; i++) {
	for (j = 0; j <= i; j++) {
	  const gsl_vector_view tmp1 = gsl_matrix_column (X, i);
	  const gsl_vector_view tmp2 = gsl_matrix_column (X, j);
	  const double dtmp1 = gsl_stats_covariance (tmp1.vector.data, tmp1.vector.stride,
				    tmp2.vector.data, tmp2.vector.stride, tmp1.vector.size);
	  gsl_matrix_set (XVAR, i, j, dtmp1);
	  gsl_matrix_set (XVAR, j, i, dtmp1);
	}
      }

      /* ++++++++++++++++++++++++++++++++++++++++ */
      if(o_verbose>0){
	fprintf(stdout,"#");
	for(i=0;i<C->size;i++){
	  char *label;

	  int length;
	  length = snprintf(NULL,0,"c%zd",i+1);
	  label = (char *) my_alloc((length+1)*sizeof(char));
	  snprintf(label,length+1,"c%zd",i+1);

	  fprintf(stdout,EMPTY_SEP,label);
	  free(label);
	  fprintf(stdout,EMPTY_SEP,"expl.var");
	}
	fprintf(stdout,"\n");
      }
      /* ++++++++++++++++++++++++++++++++++++++++ */

      for(i=0;i<C->size;i++){
	fprintf(stdout,FLOAT_SEP,gsl_vector_get(C,i));
	fprintf(stdout,FLOAT_SEP,gsl_matrix_get(XVAR,i,i)*gsl_vector_get(C,i)*gsl_vector_get(C,i)/(YVAR-RESVAR));
      }
      fprintf(stdout,"\n\n\n");

      /* print the matrix of s-scores 

	 S_{i,j}= C_i C_j XVAR_{i,j}/(YVAR-RESVAR)

       */
      for(i=0;i<XVAR->size1;i++){
	for(j=0;j<XVAR->size2;j++)
	  fprintf(stdout,FLOAT_SEP,gsl_matrix_get(XVAR,i,j)*gsl_vector_get(C,i)*gsl_vector_get(C,j)/(YVAR-RESVAR));
	fprintf(stdout,"\n");
      }

    }
    break;
  }



  return 0;

}
