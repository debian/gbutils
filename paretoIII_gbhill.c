#include "gbhill.h"


/* Pareto 3 distribution */
/* ------------------------ */

/* x[0]=a x[1]=b x[2]=s */

/* F[j] = 1-pow(x[j]/s +1 ,-a)*exp(-b*x[j]/s) */
/* -log(f[j]) = -log(1/s) + a*log(x[j]/s +1) - b*x[j]/s +log( b+a*pow((x[j]/s +1),-1) ) */

/* N: sample size k: number of observations used */

void
pareto3_nll (const size_t n, const double *x,void *params,double *fval){

  size_t i;

  /* log-likelihood parameters */
  const size_t min    = ((struct loglike_params *) params)->min;
  const size_t max    = ((struct loglike_params *) params)->max;
  const double N      = (double) ((struct loglike_params *) params)->size;
  const double d      = (double) ((struct loglike_params *) params)->d;
  const int    method = ((struct loglike_params *) params)->method;
  const double *data  = ((struct loglike_params *) params)->data;

  /* distribution parameters */
  const double a=x[0];
  const double b=x[1];
  const double s=x[2];
  const double k = ((double) max-min+1);

  *fval=0.0;
  for(i=min;i<=max;i++) *fval += a*log(data[i]/s+1) + b*data[i]/s -log(b+a/(data[i]/s+1));
  *fval = log(s) + (*fval)/k;

  switch(method){
  case 0:
    if(N>k){
      const double z=data[min];
      const double F = 1-pow(z/s +1 ,-a)*exp(-b*z/s);
      *fval += -(N/k-1.0)*log(F);
    }
    break;
  case 1:
    if(N>k){
      const double F = 1-pow(d/s +1 ,-a)*exp(-b*d/s);
      *fval += -(N/k-1.0)*log(F);
    }
    break;
  case 2:
    if(N>k){
      const double z=data[max];
      const double F = 1-pow(z/s +1 ,-a)*exp(-b*z/s);
      *fval +=  -(N/k-1.0)*log(1-F);
    } 
    break;
  case 3:
    if(N>k){
      const double F = 1-pow(d/s +1 ,-a)*exp(-b*d/s);
      *fval +=  -(N/k-1.0)*log(1-F);
    } 
    break;
  }
  
/*   fprintf(stderr,"[ f ] x1=%g x2=%g x2=%g f=%g\n",x[0],x[1],x[2],*fval); */
  
}


void
pareto3_dnll (const size_t n, const double *x,void *params,double *grad){

  size_t i;

  /* log-likelihood parameters */
  const size_t min    = ((struct loglike_params *) params)->min;
  const size_t max    = ((struct loglike_params *) params)->max;
  const size_t N      = ((struct loglike_params *) params)->size;
  const double d      = (double) ((struct loglike_params *) params)->d;
  const int    method = ((struct loglike_params *) params)->method;
  const double *data  = ((struct loglike_params *) params)->data;

  /* distribution parameters */
  const double a=x[0];
  const double b=x[1];
  const double s=x[2];
  const double k = ((double) max-min+1);


  grad[0]=0.0;
  for(i=min;i<=max;i++) grad[0] += log(data[i]/s +1) - 1/((data[i]/s +1)*(b+a/(data[i]/s +1)));
  grad[0]/=k;
  
  grad[1]=0.0;
  for(i=min;i<=max;i++) grad[1] += data[i]/s - 1/(b+a/(data[i]/s +1));
  grad[1]/=k;
  
  grad[2]=0.0; 
  for(i=min;i<=max;i++) grad[2] += a*data[i]/(s*(data[i]+s)) + b*data[i]/(s*s) +
			  a*data[i]/(pow(data[i]+s,2)*(b+a/(data[i]/s +1)));
  grad[2]= 1/s-grad[2]/k;
  
  
  switch(method){
    
  case 0:
    if(N>k) {
      const double z=data[min];
      const double dtmp1=z/s+1;
      
      const double F = 1-pow(dtmp1,-a)*exp(-b*z/s);
      const double dFda = exp(-b*z/s)*pow(dtmp1, -a)*log(dtmp1);
      const double dFdb = pow(dtmp1,-a)*exp(-b*z/s)*z/s;
      const double dFds = - exp(-b*z/s)*pow(dtmp1,-a)*(b+a/dtmp1)*z/(s*s);      
      
      grad[0] += -(N/k-1.0)*dFda/F;
      grad[1] += -(N/k-1.0)*dFdb/F;
      grad[2] += -(N/k-1.0)*dFds/F;
      
    }
    break;
  case 1:
    if(N>k) {
      const double dtmp1=d/s+1;
      
      const double F = 1-pow(dtmp1,-a)*exp(-b*d/s);
      const double dFda = exp(-b*d/s)*pow(dtmp1, -a)*log(dtmp1);
      const double dFdb = pow(dtmp1,-a )*exp(-b*d/s)*d/s;
      const double dFds = - exp(-b*d/s)*pow(dtmp1,-a)*(b+a/dtmp1)*d/(s*s);      
      
      grad[0] += -(N/k-1.0)*dFda/F;
      grad[1] += -(N/k-1.0)*dFdb/F;
      grad[2] += -(N/k-1.0)*dFds/F;
      
    }
    break;
  case 2:
    if(N>k){
      const double z=data[max];
      const double dtmp1=z/s+1;
      
      const double F = 1-pow(dtmp1,-a)*exp(-b*z/s);
      const double dFda = exp(-b*z/s)*pow(dtmp1, -a)*log(dtmp1);
      const double dFdb = pow(dtmp1,-a )*exp(-b*z/s)*z/s;
      const double dFds = - exp(-b*z/s)*pow(dtmp1,-a)*(b+a/dtmp1)*z/(s*s);      
      
      grad[0] += (N/k-1.0)*dFda/(1-F);
      grad[1] += (N/k-1.0)*dFdb/(1-F);
      grad[2] += (N/k-1.0)*dFds/(1-F);
      
    }
    break;
  case 3:
    if(N>k){
      const double dtmp1=d/s+1;
      
      const double F = 1-pow(dtmp1,-a)*exp(-b*d/s);
      const double dFda = exp(-b*d/s)*pow(dtmp1, -a)*log(dtmp1);
      const double dFdb = pow(dtmp1,-a )*exp(-b*d/s)*d/s;
      const double dFds = - exp(-b*d/s)*pow(dtmp1,-a)*(b+a/dtmp1)*d/(s*s);      
      
      grad[0] += (N/k-1.0)*dFda/(1-F);
      grad[1] += (N/k-1.0)*dFdb/(1-F);
      grad[2] += (N/k-1.0)*dFds/(1-F);
      
    }
    break;
  }
  

/*   fprintf(stderr,"[ df] x1=%g x2=%g x3=%g df/dx1=%g df/dx2=%g df/dx3=%g\n", */
/* 	  x[0],x[1],x[2],grad[0],grad[1],grad[2]); */

}


void
pareto3_nlldnll (const size_t n, const double *x,void *params,double *fval,double *grad){
  

  size_t i;

  /* log-likelihood parameters */
  const size_t min    = ((struct loglike_params *) params)->min;
  const size_t max    = ((struct loglike_params *) params)->max;
  const size_t N      = ((struct loglike_params *) params)->size;
  const double d      = (double) ((struct loglike_params *) params)->d;
  const int    method = ((struct loglike_params *) params)->method;
  const double *data  = ((struct loglike_params *) params)->data;

  /* distribution parameters */
  const double a=x[0];
  const double b=x[1];
  const double s=x[2];
  const double k = ((double) max-min+1);


  *fval=0.0;
  for(i=min;i<=max;i++) *fval += a*log(data[i]/s+1) + b*data[i]/s -log(b+a/(data[i]/s+1));
  *fval = log(s) + (*fval)/k;

  grad[0]=0.0;
  for(i=min;i<=max;i++) grad[0] += log(data[i]/s +1) - 1/((data[i]/s +1)*(b+a/(data[i]/s +1)));
  grad[0]/=k;
  
  grad[1]=0.0;
  for(i=min;i<=max;i++) grad[1] += data[i]/s - 1/(b+a/(data[i]/s +1));
  grad[1]/=k;
  
  grad[2]=0.0; 
  for(i=min;i<=max;i++) grad[2] += a*data[i]/(s*(data[i]+s)) + b*data[i]/(s*s) +
    a*data[i]/(pow(data[i]+s,2)*(b+a/(data[i]/s +1)));
  grad[2]= 1/s-grad[2]/k;


switch(method){
  
 case 0:
    if(N>k) {
      const double z=data[min];
      const double dtmp1=z/s+1;
      
      const double F = 1-pow(dtmp1,-a)*exp(-b*z/s);
      const double dFda = exp(-b*z/s)*pow(dtmp1, -a)*log(dtmp1);
      const double dFdb = pow(dtmp1,-a )*exp(-b*z/s)*z/s;
      const double dFds = - exp(-b*z/s)*pow(dtmp1,-a)*(b+a/dtmp1)*z/(s*s);      
      
      *fval += -(N/k-1.0)*log(F);    
      
      grad[0] += -(N/k-1.0)*dFda/F;
      grad[1] += -(N/k-1.0)*dFdb/F;
      grad[2] += -(N/k-1.0)*dFds/F;
    }
    break;
 case 1:
   if(N>k) {
     const double dtmp1=d/s+1;
     
     const double F = 1-pow(dtmp1,-a)*exp(-b*d/s);
     const double dFda = exp(-b*d/s)*pow(dtmp1, -a)*log(dtmp1);
     const double dFdb = pow(dtmp1,-a )*exp(-b*d/s)*d/s;
     const double dFds = - exp(-b*d/s)*pow(dtmp1,-a)*(b+a/dtmp1)*d/(s*s);      
      
      *fval += -(N/k-1.0)*log(F);    
      
      grad[0] += -(N/k-1.0)*dFda/F;
      grad[1] += -(N/k-1.0)*dFdb/F;
      grad[2] += -(N/k-1.0)*dFds/F;
   }
   break;
 case 2:
   if(N>k){
     //const double z=data[min];
     const double z=data[max];
     const double dtmp1=z/s+1;
     
     const double F = 1-pow(dtmp1,-a)*exp(-b*z/s);
     const double dFda = exp(-b*z/s)*pow(dtmp1, -a)*log(dtmp1);
     const double dFdb = pow(dtmp1,-a )*exp(-b*z/s)*z/s;
     const double dFds = - exp(-b*z/s)*pow(dtmp1,-a)*(b+a/dtmp1)*z/(s*s);      
     
     *fval += -(N/k-1.0)*log(1-F);
     grad[0] += (N/k-1.0)*dFda/(1-F);
     grad[1] += (N/k-1.0)*dFdb/(1-F);
     grad[2] += (N/k-1.0)*dFds/(1-F);
     
   }
   break;
 case 3:
   if(N>k){
     const double dtmp1=d/s+1;
     
     const double F = 1-pow(dtmp1,-a)*exp(-b*d/s);
     const double dFda = exp(-b*d/s)*pow(dtmp1, -a)*log(dtmp1);
     const double dFdb = pow(dtmp1,-a )*exp(-b*d/s)*d/s;
     const double dFds = - exp(-b*d/s)*pow(dtmp1,-a)*(b+a/dtmp1)*d/(s*s);      
     
     *fval += -(N/k-1.0)*log(1-F);
     grad[0] += (N/k-1.0)*dFda/(1-F);
     grad[1] += (N/k-1.0)*dFdb/(1-F);
     grad[2] += (N/k-1.0)*dFds/(1-F);
   }
   break;
 }
 
 
/*   fprintf(stderr,"[fdf] x1=%g x2=%g f=%g df/dx1=%g df/dx2=%g\n", */
/* 	  x[0],x[1],*fval,grad[0],grad[1]); */
 
}


double
pareto3_f (const double x,const size_t n, const double *par){

  const double a=par[0];
  const double b=par[1];
  const double s=par[2];

  return exp(-b*x/s)*pow(x/s +1.,-a)*(b+a/(x/s +1.))/s;

}

double
pareto3_F (const double x,const size_t n, const double *par){

  const double a=par[0];
  const double b=par[1];
  const double s=par[2];

  return 1-pow((x/s +1) ,-a)*exp(-b*x/s);

}



/* !!!!! NOT YET IMPLEMENTED. RETURN NANs !!!!! */
gsl_matrix *pareto3_varcovar(const int o_varcovar, double *x, void *params){

  size_t i,j;

  gsl_matrix *covar;
  covar = gsl_matrix_alloc (3,3);

  for(i=0;i<3;i++)
    for(j=0;j<3;j++)
      gsl_matrix_set (covar,i,j,NAN);
  
  return covar;
}
