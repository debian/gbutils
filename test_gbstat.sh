#!/bin/bash

# created:  2021-08-05 giulio
# Time-stamp: <2022-04-11 14:00:23 giulio>

res=`echo "
10 20
11 21
12 22
13 23
14 24
" | ./gbstat -s -O mean,var | ./gbfun 'abs(x1+x2-47)*10000' -o %d`

status=$?

[[ $status == 0 && $res -lt 1 ]]
