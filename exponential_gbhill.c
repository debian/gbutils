#include "gbhill.h"

/* Exponential distribution */
/* ------------------------ */

/* x[0]=mu x[1]=b */

/* F[k] = 1-exp(-(1/mu)*(x[k]-b)) */
/* -log(f[k]) = -log(1/mu)+(1/mu)*(x[k]-b)       */

/* N: sample size k: number of observations used */

void
exponential_nll (const size_t n, const double *x,void *params,double *fval){

  size_t i;

  /* log-likelihood parameters */
  const size_t min    = ((struct loglike_params *) params)->min;
  const size_t max    = ((struct loglike_params *) params)->max;
  const double N      = (double) ((struct loglike_params *) params)->size;
  const double d      = (double) ((struct loglike_params *) params)->d;
  const int    method = ((struct loglike_params *) params)->method;
  const double *data  = ((struct loglike_params *) params)->data;

  /* distribution parameters */
  const double m=x[0];
  const double b=x[1];

  const double k = ((double) max-min+1);

  *fval=0.0;
  for(i=min;i<=max;i++) *fval += data[i];
  *fval = (1./m)*(*fval)/k -log(1./m) - (1./m)*b;

  switch(method){
  case 0:
    if(N>k) *fval += -(N/k-1.0)*log(1.-exp(-(1./m)*(data[min]-b)));
    break;
  case 1:
    if(N>k) *fval += -(N/k-1.0)*log(1.-exp(-(1./m)*(d-b)));
    break;
  case 2:
    if(N>k) *fval += (N/k-1.0)*(1./m)*(data[max]-b);
    break;
  case 3:
    //if(N>k) *fval += -(N/k-1.0)*(1./m)*(d-b) ;
    if(N>k) *fval += (N/k-1.0)*(1./m)*(d-b) ;
    break;
  }

/*   fprintf(stderr,"[ f ] x1=%g x2=%g f=%g \n",x[0],x[1],*fval); */

}

void
exponential_dnll (const size_t n, const double *x,void *params,double *grad){

  size_t i;

  /* log-likelihood parameters */
  const size_t min    = ((struct loglike_params *) params)->min;
  const size_t max    = ((struct loglike_params *) params)->max;
  const double N      = (double) ((struct loglike_params *) params)->size;
  const double d      = ((struct loglike_params *) params)->d;
  const int    method = ((struct loglike_params *) params)->method;
  const double *data  = ((struct loglike_params *) params)->data;

  /* distribution parameters */
  const double m=x[0];
  const double b=x[1];

  const double k = ((double) max-min+1);


  grad[0]=0.0;
  for(i=min;i<=max;i++) grad[0] += data[i];
  grad[0]=(grad[0]/k -m -b)*(-1./pow(m,2));

  grad[1]=-(1./m);

  switch(method){

  case 0:
    if(N>k) {
      const double F = 1.-exp(-(1./m)*(data[min]-b));
      const double dFdm = exp(-(1./m)*(data[min]-b))*(data[min]-b);
      const double dFdb = -exp(-(1./m)*(data[min]-b))*(1./m);
      
      grad[0] += (-(N/k-1.0)*dFdm/F)*(-1./pow(m,2));

      //grad[1] += (-(N/k-1.0)*dFdb/F)*(-1./pow(m,2));  // <=== Double-Check!!
      grad[1] += (-(N/k-1.0)*dFdb/F);
    }
    break;
  case 1:
    if(N>k) {
      const double F = 1.-exp(-(1./m)*(d-b));
      const double dFdm = exp(-(1./m)*(d-b))*(d-b);
      const double dFdb = -exp(-(1./m)*(d-b))*(1./m);
      
      grad[0] += (-(N/k-1.0)*dFdm/F)*(-1./pow(m,2));
      grad[1] += (-(N/k-1.0)*dFdb/F);
      
    }
    break;
  case 2:
    if(N>k){
      grad[0] += (N/k-1.0)*(data[max]-b)*(-1./pow(m,2));
      grad[1] += -(1./m)*(N/k-1.0);
    }
    break;
  case 3:
    if(N>k){
      grad[0] += (N/k-1.0)*(d-b)*(-1./pow(m,2));
      grad[1] += -(1./m)*(N/k-1.0);
    }
    break;
  }


/*   fprintf(stderr,"[ df] x1=%g x2=%g df/dx1=%g df/dx2=%g\n", */
/* 	  x[0],x[1],grad[0],grad[1]); */

}

void
exponential_nlldnll (const size_t n, const double *x,void *params,double *fval,double *grad){


  size_t i;

  /* log-likelihood parameters */
  const size_t min    = ((struct loglike_params *) params)->min;
  const size_t max    = ((struct loglike_params *) params)->max;
  const double N      = (double) ((struct loglike_params *) params)->size;
  const double d      = (double) ((struct loglike_params *) params)->d;
  const int    method = ((struct loglike_params *) params)->method;
  const double *data  = ((struct loglike_params *) params)->data;

  /* distribution parameters */
  const double m=x[0];
  const double b=x[1];

  const double k = ((double) max-min+1);

  *fval=0.0;
  for(i=min;i<=max;i++) *fval += data[i];
  grad[0]=((*fval)/k -m -b)*(-1./pow(m,2));
  *fval = (1./m)*(*fval)/k -log(1./m) - (1./m)*b;
  grad[1]=-1./m;


  switch(method){

  case 0:
    if(N>k) {

      const double F = 1.-exp(-(1./m)*(data[min]-b));
      const double dFdm = exp(-(1./m)*(data[min]-b))*(data[min]-b);
      const double dFdb = -exp(-(1./m)*(data[min]-b))*(1./m);

      *fval += -(N/k-1.0)*log(1.-exp(-(1./m)*(data[min]-b)));
      grad[0] += (-(N/k-1.0)*dFdm/F)*(-1./pow(m,2));
      grad[1] += -(N/k-1.0)*dFdb/F;

      
    }
    break;
  case 1:
    {
      *fval += -(1./m)*(data[min-1]-b);
      grad[0] += (-data[min-1]+b)*(-1./pow(m,2));
    
      grad[1] += 1./m;
    }
    break;
  case 2:
    if(N>k) {
      const double F = 1.-exp(-(1./m)*(d-b));
      const double dFdm = exp(-(1./m)*(d-b))*(d-b);
      const double dFdb = -exp(-(1./m)*(d-b))*(1./m);

      *fval += -(N/k-1.0)*log(1.-exp(-(1./m)*(d-b)));
      
      grad[0] += (-(N/k-1.0)*dFdm/F)*(-1./pow(m,2));
      grad[1] += -(N/k-1.0)*dFdb/F;
     
    }
    break;
  case 3:
    if(N>k){
      *fval += (N/k-1.0)*(1./m)*(data[max]-b);
      grad[0] += (N/k-1.0)*(data[max]-b)*(-1./pow(m,2));
      grad[1] += -(1./m)*(N/k-1.0);
    }
    break;
  case 4:
    {
      const double F = 1.-exp(-(1./m)*(data[max+1]-b));
      const double dFdm = exp(-(1./m)*(data[max+1]-b))*(data[max+1]-b);
      const double dFdb = -exp(-(1./m)*(data[max+1]-b))*(1./m);

      *fval += log(F);
      grad[0] += (dFdm/F)*(-1./pow(m,2));
      grad[1] += dFdb/F;

    }
    break;
  case 5:
    if(N>k){
      *fval += (N/k-1.0)*(1./m)*(d-b);
      grad[0] += (N/k-1.0)*(d-b)*(-1./pow(m,2));
      grad[1] += -(1./m)*(N/k-1.0);
    }
    break;
  }


/*   fprintf(stderr,"[fdf] x1=%g x2=%g f=%g df/dx1=%g df/dx2=%g\n", */
/* 	  x[0],x[1],*fval,grad[0],grad[1]); */

}

double
exponential_f (const double x,const size_t n, const double *par){

  return gsl_ran_exponential_pdf(x-par[1],par[0]);

}

double
exponential_F (const double x,const size_t n, const double *par){

  return gsl_cdf_exponential_P (x-par[1],par[0]);

}

gsl_matrix *exponential_varcovar(const int o_varcovar, double *x, void *params){

  /* log-likelihood parameters */
  const size_t min    = ((struct loglike_params *) params)->min;
  const size_t max    = ((struct loglike_params *) params)->max;
  const double N      = (double) ((struct loglike_params *) params)->size;
  const double d      = (double) ((struct loglike_params *) params)->d;
  const int    method = ((struct loglike_params *) params)->method;
  const double *data  = ((struct loglike_params *) params)->data;

  /* distribution parameters */
  const double m=x[0];
  const double b=x[1];

  const double k = ((double) max-min+1);

  double dldm    = 0.0;
  double d2ldm2  = 0.0;
  double d2ldmdb = 0.0;
  double dldb    = 0.0;
  double d2ldb2  = 0.0;
  gsl_matrix *covar;
  
  double sum=0.0;
  
  double ZN = data[0];
  double Zk = data[min];

  double ZNk  = data[max];

  /* compute the sum of observations */
  {
    size_t i;
    
    for(i=min;i<=max;i++) sum += data[i];
  }

  switch(method){
  case 0:
  
     /* GSL allocation of memory for the covar matrix */
    covar = gsl_matrix_alloc (2,2);
    
    /* Compute ll partial derivatives. */
    dldm     = -((exp(b/m)*Zk-b*exp(b/m))*N+(-sum+k*m+b*k)*exp(Zk/m)-k*exp(b/m)*Zk+exp(b/m)*sum-k*m*exp(b/m))/(pow(m,2.)*exp(Zk/m)-pow(m,2.)*exp(b/m));
    d2ldm2   = -(((exp(b/m)*pow(Zk,2.)+(-2.*m-2.*b)*exp(b/m)*Zk+(2.*b*m+pow(b,2.))*exp(b/m))*exp(Zk/m)+2.*m*exp((2.*b)/m)*Zk-2.*b*m*exp((2.*b)/m))*N+(2.*m*sum-k*pow(m,2.)-2.*b*k*m)*exp((2.*Zk)/m)+(-k*exp(b/m)*pow(Zk,2.)+(2.*k*m+2.*b*k)*exp(b/m)*Zk-4*m*exp(b/m)*sum+(2.*k*pow(m,2.)+2.*b*k*m-pow(b,2.)*k)*exp(b/m))*exp(Zk/m)-2.*k*m*exp((2.*b)/m)*Zk+2.*m*exp((2.*b)/m)*sum-k*pow(m,2.)*exp((2.*b)/m))/(-2.*pow(m,4.)*exp(Zk/m+b/m)+pow(m,4.)*exp((2.*Zk)/m)+pow(m,4.)*exp((2.*b)/m));
    
    d2ldmdb  = -(((exp(b/m)*Zk+(-m-b)*exp(b/m))*exp(Zk/m)+m*exp((2.*b)/m))*N+k*m*exp((2.*Zk)/m)+((b*k-k*m)*exp(b/m)-k*exp(b/m)*Zk)*exp(Zk/m))/(-2.*pow(m,3.)*exp(Zk/m+b/m)+pow(m,3.)*exp((2.*Zk)/m)+pow(m,3.)*exp((2.*b)/m));

    dldb = -(exp(b/m)*N-k*exp(Zk/m))/(m*exp(Zk/m)-m*exp(b/m));

    d2ldb2 = -(exp(Zk/m+b/m)*N-k*exp(Zk/m+b/m))/(-2.*pow(m,2.)*exp(Zk/m+b/m)+pow(m,2.)*exp((2.*Zk)/m)+pow(m,2.)*exp(2.*b/m));
    
    break;
  case 1:
    /* GSL allocation of memory for the covar matrix */
    covar = gsl_matrix_alloc (2,2);
    
    dldm = (-k)*(((d-b)*exp(b/m)*N+(exp(b/m)-exp(d/m))*sum+(k*m+b*k)*exp(d/m)+(-k*m-d*k)*exp(b/m))/(k*pow(m,2.0)*exp(d/m)-k*pow(m,2.0)*exp(b/m)));
    
    dldb =(-k)*( (exp(b/m)*N-k*exp(d/m))/(k*m*exp(d/m)-k*m*exp(b/m)));
  
    d2ldm2=(-k)*(-((((2*d-2*b)*m-pow(d,2.0)+2*b*d-pow(b,2.0))*exp(d/m+b/m)+(2*b-2*d)*m*exp((2*b)/m))*N+(4*m*exp(d/m+b/m)-2*m*exp((2*d)/m)-2*m*exp((2*b)/m))*sum+(-2*k*pow(m,2.0)+(-2*d-2*b)*k*m+(pow(d,2.0)-2*b*d+pow(b,2.0))*k)*exp(d/m+b/m)+(k*pow(m,2.0)+2*b*k*m)*exp((2*d)/m)+(k*pow(m,2.0)+2*d*k*m)*exp((2*b)/m))/(-2*k*pow(m,4.0)*exp(d/m+b/m)+k*pow(m,4.0)*exp((2*d)/m)+k*pow(m,4.0)*exp((2*b)/m)));
    
    
    
    d2ldmdb=(-k)*(-(((m-d+b)*exp(d/m+b/m)-m*exp((2*b)/m))*N+(k*m+(d-b)*k)*exp(d/m+b/m)-k*m*exp((2*d)/m))/(-2*k*pow(m,3)*exp(d/m+b/m)+k*pow(m,3)*exp((2*d)/m)+k*pow(m,3)*exp((2*b)/m)));
    d2ldb2=(-k)*((exp(d/m+b/m)*N-k*exp(d/m+b/m))/(-2*k*pow(m,2.0)*exp(d/m+b/m)+k*pow(m,2.0)*exp((2*d)/m)+k*pow(m,2.0)*exp((2*b)/m)));
    
    break;    
  case 2:
    /* GSL allocation of memory for the covar matrix */
    covar = gsl_matrix_alloc (1,1);
    
    dldm    = -(N*ZN-ZNk*N+k*ZNk-sum+k*m)/pow(m,2.0);
    d2ldm2  = (2*N*ZN-2*ZNk*N+2*k*ZNk-2*sum+k*m)/pow(m,3.0);

    break;
  case 3:

    covar = gsl_matrix_alloc (1,1);
    
    dldm    = -(N*ZN-d*N-sum+k*m+d*k)/pow(m,2.0); 
    d2ldm2  = (2*N*ZN-2*d*N-2*sum+k*m+2*d*k)/pow(m,3.0);
 
    break;
  }
  switch(o_varcovar){
  case 0:   
    if (method < 2 ){
      gsl_permutation * P = gsl_permutation_alloc (2);
      int signum;
      gsl_matrix *J = gsl_matrix_calloc (2,2);

      gsl_matrix_set (J,0,0,dldm*dldm);
      gsl_matrix_set (J,0,1,dldm*dldb);
      gsl_matrix_set (J,1,0,dldb*dldm);
      gsl_matrix_set (J,1,1,dldb*dldb);
      gsl_linalg_LU_decomp (J,P,&signum);
      gsl_linalg_LU_invert (J,P,covar);
      gsl_matrix_free(J);
      gsl_permutation_free(P);
    }else{
      gsl_permutation * P = gsl_permutation_alloc (1);
      int signum;
      gsl_matrix *J = gsl_matrix_calloc (1,1);
      gsl_matrix_set(J,0,0,dldm*dldm);    
      gsl_linalg_LU_decomp (J,P,&signum);
      gsl_linalg_LU_invert (J,P,covar);
      gsl_matrix_free(J);
      gsl_permutation_free(P);
    }
    break;
  case 1:
    if (method < 2 ){
      gsl_permutation * P = gsl_permutation_alloc (2);
      int signum;
      gsl_matrix *H = gsl_matrix_calloc (2,2);
      gsl_matrix_set(H,0,0,d2ldm2);
      gsl_matrix_set(H,0,1,d2ldmdb);
      gsl_matrix_set(H,1,0,d2ldmdb);
      gsl_matrix_set(H,1,1,d2ldb2);
      gsl_matrix_scale (H,-1.);
      gsl_linalg_LU_decomp (H,P,&signum);
      gsl_linalg_LU_invert (H,P,covar);
      
      gsl_matrix_free(H);
      gsl_permutation_free(P);
    }else{
      gsl_permutation * P = gsl_permutation_alloc (1);
      int signum;
      gsl_matrix *H = gsl_matrix_calloc (1,1);
      gsl_matrix_set(H,0,0,d2ldm2);   
      gsl_matrix_scale (H,-1.);
      gsl_linalg_LU_decomp (H,P,&signum);
      gsl_linalg_LU_invert (H,P,covar);
      gsl_matrix_free(H);
      gsl_permutation_free(P);
    }
    break;
  case 2:
    if (method < 2 ){
      gsl_permutation * P = gsl_permutation_alloc (2);
      int signum;
      gsl_matrix *H   = gsl_matrix_calloc (2,2);
      gsl_matrix *J   = gsl_matrix_calloc (2,2);
      gsl_matrix *tmp = gsl_matrix_calloc (2,2);
      gsl_matrix_set(H,0,0,d2ldm2);
      gsl_matrix_set(H,0,1,d2ldmdb);
      gsl_matrix_set(H,1,0,d2ldmdb);
      gsl_matrix_set(H,1,1,d2ldb2);
      gsl_matrix_set(J,0,0,dldm*dldm);
      gsl_matrix_set(J,0,1,dldm*dldb);
      gsl_matrix_set(J,1,0,dldb*dldm);
      gsl_matrix_set(J,1,1,dldb*dldb);

      gsl_linalg_LU_decomp (H,P,&signum);
      gsl_linalg_LU_invert (H,P,tmp);

      gsl_blas_dgemm (CblasNoTrans,
		      CblasNoTrans,1.0,tmp,J,0.0,H);
      gsl_blas_dgemm (CblasNoTrans,
		      CblasNoTrans,1.0,H,tmp,0.0,covar);
      gsl_matrix_free(H);
      gsl_matrix_free(J);
      gsl_matrix_free(tmp);
      gsl_permutation_free(P);
      
    }else{
      gsl_permutation * P = gsl_permutation_alloc (1);
      int signum;
      gsl_matrix *H   = gsl_matrix_calloc (1,1);
      gsl_matrix *J   = gsl_matrix_calloc (1,1);
      gsl_matrix *tmp = gsl_matrix_calloc (1,1);
      gsl_matrix_set(H,0,0,d2ldm2);
      gsl_matrix_set(J,0,0,dldm*dldm);
      gsl_linalg_LU_decomp (H,P,&signum);
      gsl_linalg_LU_invert (H,P,tmp);

      gsl_blas_dgemm (CblasNoTrans,
		      CblasNoTrans,1.0,tmp,J,0.0,H);
      gsl_blas_dgemm (CblasNoTrans,
		      CblasNoTrans,1.0,H,tmp,0.0,covar);
      gsl_matrix_free(H);
      gsl_matrix_free(J);
      gsl_matrix_free(tmp);
      gsl_permutation_free(P);
    }
    break;
  }

  /* scale the covariance matrix to the appropriate dimension */
  if(covar->size1 == 1)
    {
      
      const double dtmp1 = gsl_matrix_get(covar,0,0);
      gsl_matrix_free (covar);
      covar = gsl_matrix_alloc (2,2);
      
      gsl_matrix_set(covar,0,0,dtmp1);
      gsl_matrix_set(covar,0,1,NAN);
      gsl_matrix_set(covar,1,0,NAN);
      gsl_matrix_set(covar,1,1,NAN);
      
    }

  return covar;
}
