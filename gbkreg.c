/*
  gbkreg  (ver. 5.6) -- Kernel non linear regression function
  Copyright (C) 2003-2018 Giulio Bottazzi

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  (version 2) as published by the Free Software Foundation;
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/


#include "tools.h"

#if defined HAVE_LIBGSL

#include <gsl/gsl_fft_real.h>
#include <gsl/gsl_fft_halfcomplex.h>

#endif

/* required for sorting */

/* handy structure used in computation */
typedef struct couple {
  double x,y;
} Couple;

/* compare the couple according to first value */
int couplecompare(Couple *a, Couple *b){

  if(a->x > b->x){
    return 1;
  }
  else if(a->x < b->x){
    return -1;
  }
  else{
    return 0;
  }

}


int main(int argc,char* argv[]){

    double **data=NULL;
    size_t size=0;

    char *splitstring = strdup(" \t");

    /* OPTIONS */
    int o_method=1;
    int o_kerneltype=0;
    int o_setbandwidth=0;
    int o_verbose=0;
    int o_mean=1;
    int o_sdev=0;
    int o_skew=0;
    int o_kurt=0;
    

    double ave,sdev,min,max; /* data statistics */
    
    size_t M=64; /* number of bins */

    double scale=1; /* optional scaling of the smoothing parameter */
    double h=1;/* the smoothing parameter */

    double (*K) (double) ; /* the kernel to use */


    /* COMMAND LINE PROCESSING */
    
    /* variables for reading command line options */
    /* ------------------------------------------ */
    int opt;
    /* ------------------------------------------ */
    
    
    /* read the command line */    
    while((opt=getopt_long(argc,argv,"H:K:n:S:hvM:O:F:",gb_long_options, &gb_option_index))!=EOF){
      if(opt==0){
      gbutils_header(argv[0],stdout);
      exit(0);
    }
    else if(opt=='?'){
	fprintf(stderr,"option %c not recognized\n",optopt);
	exit(-1);
      }
      else if(opt=='S'){
	/*set the scale parameter for smoothing*/
	scale=atof(optarg);
      }
      else if(opt=='n'){
	/*the number of bins*/
	M=(size_t) atoi(optarg);
      }
      else if(opt=='K'){
	/*the Kernel to use*/
	o_kerneltype = atoi(optarg);
      }
      else if(opt=='H'){
	/*set the kernel bandwidth*/
	o_setbandwidth=1;
	h = atof(optarg);
      }
      else if(opt=='M'){
	/*set the method to use*/
	o_method= atoi(optarg);
      }
      else if(opt=='O'){
	/*set the output type*/
	char *output_opts[] = {"m","v","s","k",NULL};
	char *subopts,*subvalue=NULL;

	/* remove default */
	o_mean=0;

	/* define new output */
	subopts = optarg;
	while (*subopts != '\0')
	  switch (getsubopt(&subopts, output_opts, &subvalue))
	    {
	    case 0:
	      o_mean=1;
	      break;
	    case 1:
	      o_sdev=1;
	      break;
	    case 2:
	      o_skew=1;
	      break;
	    case 3:
	      o_kurt=1;
	      break;
	    default:/* Unknown suboption. */
	      fprintf (stderr,"Unknown suboption `%s'\n", subvalue);
	      exit(1);
	    }

      }
      else if(opt=='v'){
	/*increase verbosity*/
	o_verbose=1;
      }
      else if(opt=='F'){
	/*set the fields separator string*/
	free(splitstring);
	splitstring = strdup(optarg);
      }
      else if(opt=='h'){
	/*print short help*/
	fprintf(stdout,"Kernel estimation of conditional moments. Data are read from standard input\n");
	fprintf(stdout,"as couple (x,y). The moments of y are computed on a regular grid in x. The\n");
	fprintf(stdout,"kernel bandwidth, if not provided with the option -H, is set automatically.\n");
	fprintf(stdout,"\nUsage: %s [options]\n\n",argv[0]);
	fprintf(stdout,"Options:\n");
	fprintf(stdout," -n  number of equispaced points where moments are computed (default 64)\n");
	fprintf(stdout," -H  set the kernel bandwidth\n");
	fprintf(stdout," -S  scale the automatic kernel bandwidth\n");
	fprintf(stdout," -K  choose the kernel to use (default 0)\n");
	fprintf(stdout,"      0  Epanenchnikov\n");
	fprintf(stdout,"      1  Rectangular\n");
	fprintf(stdout,"      2  Gaussian\n");
	fprintf(stdout," -M  choose the method to compute the density (default 1)\n");
	fprintf(stdout,"      0  FFT (number of points rounded to nearest power of 2)\n");
	fprintf(stdout,"      1  discrete convolution (only with compact kernels)\n");
	fprintf(stdout,"      2  explicit summation (can be long)\n");
	fprintf(stdout," -O  set the output, comma separated list of m mean, v standard deviation,\n");
	fprintf(stdout,"     s skewness and k kurtosis (default m)\n");
	fprintf(stdout," -v  verbose mode\n");
	fprintf(stdout," -F  specify the input fields separators (default \" \\t\")\n");
	fprintf(stdout," -h  this help\n");
	fprintf(stdout,"Examples:\n");
	fprintf(stdout," gbkreg -M 2 < file  compute the kernel regression of the entries in the\n");
	fprintf(stdout,"                     second column of 'file' vs. the entries in the first\n");
	fprintf(stdout,"                     column. If more data columns exist in file they are \n");
	fprintf(stdout,"                     ignored. Explicit summation method (slower) is used. \n");
	fprintf(stdout," gbkreg -02 < file   compute the kernel regression of the standard deviation\n");
	fprintf(stdout,"                     of the entries in the second column of 'file' vs. the \n");
	fprintf(stdout,"                     entries in the first colum\n");
	exit(0);
      }
    }
    /* END OF COMMAND LINE PROCESSING */

    /* initialize global variables */
    initialize_program(argv[0]);

    /* load the data */
    load2(&data,&size,0,splitstring);

    /* sort with respect to the first array */
    sort2(data[0],data[1],size,2);

    /* compute the statistics */
    moment_short(data[0],size,&ave,&sdev,&min,&max);

    /* the parameter h is set for a Gaussian kernel [Silverman p.48] */
    /* if not provided on command line */
    if(o_setbandwidth == 0){
      const unsigned index1 = size/4;
      const unsigned index2 = 3*size/4;
      const double inter4 = fabs(data[0][index1]-data[0][index2])/1.34;
      const double A = (sdev>inter4?inter4:sdev);
      h=scale*0.9*A/pow(size,.2);
    }

    /* nearest power of two */
    if(o_method==0)
      M=(int) pow(2,nearbyint(log((double) M)/M_LN2));

    /* ++++++++++++++++++++++++++++ */
    if(o_verbose == 1){
      /* bandwidth */
      fprintf(stdout,"bandwidth  %e ",h);
      if(o_setbandwidth == 0){
	fprintf(stdout,"(automatic");
	if(scale != 1 )
	  fprintf(stdout,"scaled by %e",scale);
	fprintf(stdout,")");
      }
      else{
	fprintf(stdout,"(provided)");
      }
      fprintf(stdout,"\n");
      /* kernel type */
      fprintf(stdout,"kernel     ");
      switch(o_kerneltype){
      case 0:
	fprintf(stdout,"Epanechnikov");
	break;
      case 1:
	fprintf(stdout,"Rectangular");
	  break;
      case 2:
	fprintf(stdout,"Gaussian");
	break;
      }
      fprintf(stdout,"\n");
      fprintf(stdout,"output      ");
      if(o_mean) fprintf(stdout,"mean ");
      if(o_sdev) fprintf(stdout,"st.dev. ");
      if(o_skew) fprintf(stdout,"skewness ");
      if(o_kurt) fprintf(stdout,"kurtosis ");
      fprintf(stdout,"\n");
    }
    /* ++++++++++++++++++++++++++++ */

    if(o_method==0){/*=== FFT methods ===*/

      size_t i;
      double *f;/* the density */
      double *mf;/* the estimation of f*y */
      double *m2f=0;/* the estimation of f*y*y */
      double *m3f=0;/* the estimation of f*y**3 */
      double *m4f=0;/* the estimation of f*y**4 */

      /* the boundaries of the interval are set according... */
      const double a = min-2*h;
      const double b = max+2*h;
      const double delta = (b-a)/(M-1);

/*       const double margin=.05; */
/*       const double a = min - 4*h - (max-min+4*h)*margin; */
/*       const double b = max + 4*h + (max-min+4*h)*margin; */
/*       const double delta = (b-a)/(M-1); */


      
      /* choose the kernel to use */
      switch(o_kerneltype){
      case 0:
	K=Kepanechnikov_tilde;
	break;
      case 1:
	K=Krectangular_tilde;
	break;
      case 2:
	K=Kgauss_tilde;
	break;
      default:
	fprintf(stdout,"unknown kernel; use %s -h\n",argv[0]);
	exit(+1);
      }

      /* ++++++++++++++++++++++++++++ */
      if(o_verbose == 1){
	/* method */
	fprintf(stdout,"method     FFT\n");
	/* binning */
	fprintf(stdout,"# bins     %zd\n",M);
	fprintf(stdout,"range      [%.3e,%.3e]\n",a,b);
      }
      /* ++++++++++++++++++++++++++++ */
      
      /* allocate the bins */
      f = (double *) my_alloc(M*sizeof(double));
      mf = (double *) my_alloc(M*sizeof(double));
      if(o_sdev || o_skew || o_kurt) m2f = (double *) my_alloc(M*sizeof(double));
      if(o_skew == 1) m3f = (double *) my_alloc(M*sizeof(double));
      if(o_kurt == 1) m4f = (double *) my_alloc(M*sizeof(double));

      /* prepare the binning */
      for(i=0;i<M;i++)	mf[i]=f[i]=0.0;
      if(o_sdev || o_skew || o_kurt ) for(i=0;i<M;i++) m2f[i]=0.0;
      if(o_skew) for(i=0;i<M;i++) m3f[i]=0.0;
      if(o_kurt) for(i=0;i<M;i++) m4f[i]=0.0;

      /* fill the pre-binning */
      /* the points are in a+j*delta*/
      for(i=0;i<size;i++){
	double dtmp1 = (data[0][i]-a)/delta;
	size_t index = (size_t) floor(dtmp1);
	const double epsilon = dtmp1-index;
	const double dtmp2 = data[1][i];
	if(index>=M || dtmp1<0) continue;
	f[index]+=1-epsilon;
	mf[index]+= (dtmp1=(1-epsilon)*dtmp2);
	if(o_sdev || o_skew || o_kurt) m2f[index]+= (dtmp1*=dtmp2);
	if(o_skew) m3f[index]+=dtmp1*dtmp2;
	if(o_kurt) m4f[index]+=dtmp1*dtmp2*dtmp2;
	if(index<M-1){
	  index++;
	  f[index]+=epsilon;
	  mf[index]+= (dtmp1=epsilon*dtmp2);
	  if(o_sdev || o_skew || o_kurt) m2f[index]+= (dtmp1*=dtmp2);
	  if(o_skew) m3f[index]+=dtmp1*dtmp2;
	  if(o_kurt) m4f[index]+=dtmp1*dtmp2*dtmp2;
	}
	else {
	  index=0;
	  f[index]+=epsilon;
	  mf[index]+= (dtmp1=epsilon*dtmp2);
	  if(o_sdev || o_skew || o_kurt) m2f[index]+= (dtmp1*=dtmp2);
	  if(o_skew) m3f[index]+=dtmp1*dtmp2;
	  if(o_kurt) m4f[index]+=dtmp1*dtmp2*dtmp2;
	}
      }


      /* normalization */
      for(i=0;i<M;i++){
	f[i]/=delta*size;
	mf[i]/=delta*size;
	if(o_sdev || o_skew || o_kurt) m2f[i]/=delta*size;
	if(o_skew) m3f[i]/=delta*size;
	if(o_kurt) m4f[i]/=delta*size;
      }
      

      /* fast Fourier transform */
#if defined HAVE_LIBGSL
      gsl_fft_real_radix2_transform (f,1,M);
      gsl_fft_real_radix2_transform (mf,1,M);
      if(o_sdev || o_skew || o_kurt) gsl_fft_real_radix2_transform (m2f,1,M);
      if(o_skew) gsl_fft_real_radix2_transform (m3f,1,M);
      if(o_kurt) gsl_fft_real_radix2_transform (m4f,1,M);
#endif
     
      /* multiply for the kernel */
      for(i=0;i<=M/2;i++){
	f[i]*=K(h*2*M_PI*i/(b-a));
	mf[i]*=K(h*2*M_PI*i/(b-a));
	if(o_sdev || o_skew || o_kurt) m2f[i]*=K(h*2*M_PI*i/(b-a));
	if(o_skew) m3f[i]*=K(h*2*M_PI*i/(b-a));
	if(o_kurt) m4f[i]*=K(h*2*M_PI*i/(b-a));
      }
      for(i=1;i<M/2;i++){
	f[M-i]*=K(h*2*M_PI*i/(b-a));
	mf[M-i]*=K(h*2*M_PI*i/(b-a));
	if(o_sdev || o_skew || o_kurt) m2f[M-i]*=K(h*2*M_PI*i/(b-a));
	if(o_skew) m3f[M-i]*=K(h*2*M_PI*i/(b-a));
	if(o_kurt) m4f[M-i]*=K(h*2*M_PI*i/(b-a));
      }
      
      /* transform back */
#if defined HAVE_LIBGSL
      gsl_fft_halfcomplex_radix2_inverse (f,1,M);
      gsl_fft_halfcomplex_radix2_inverse (mf,1,M);
      if(o_sdev || o_skew || o_kurt) gsl_fft_halfcomplex_radix2_inverse (m2f,1,M);
      if(o_skew) gsl_fft_halfcomplex_radix2_inverse (m3f,1,M);
      if(o_kurt) gsl_fft_halfcomplex_radix2_inverse (m4f,1,M);
#endif

      /* output */
/*       for(i=0;i<M;i++){ */
      for(i=floor(2*h/delta);i<M-floor(2*h/delta);i++){
	printf(FLOAT_SEP,a+i*delta);

	if(f[i]>0){
	  const double m  = mf[i]/f[i];
	  const double v =( o_sdev||o_skew||o_kurt ?
			    fabs(m2f[i]/f[i]-m*m) : 0.0);
	  if(o_mean) printf(FLOAT_SEP,m);
	  if(o_sdev) printf(FLOAT_SEP,sqrt(v));
	  if(o_skew)
	    printf(FLOAT_SEP,
		   (m3f[i]/f[i]-3.*m*m2f[i]/f[i]+2.*m*m*m)/pow(v,1.5));
	  if(o_kurt)
	    printf(FLOAT_SEP,
		   (m4f[i]/f[i]-4.*m*m3f[i]/f[i]+
		    6.*m*m*m2f[i]/f[i]-3.*pow(m,4))/(v*v) -3.);
	}
	else{
	  if(o_mean) printf(FLOAT_SEP,0.0);
	  if(o_sdev) printf(FLOAT_SEP,0.0);
	  if(o_skew) printf(FLOAT_SEP,0.0);
	  if(o_kurt) printf(FLOAT_SEP,0.0);
	}	
	printf("\n");
      }
    }
    else if(o_method==2){ /*=== Exact Summation ===*/

      int i;
      
      /* the boundaries of the interval are set according... */
/*       const double delta = (max-min)/((double) M-1.5); */
/*       const double a = min-.5*delta; */

      const double delta = (max-min)/((double) M-3.0);
      const double a = min-.5*delta;
      
      /* boundaries of the sum */
      const int imin = -( (int) M)/20;
      const int imax = M-1+( (int) M)/20;
      
/*       const double a = min-2*h; */
/*       const double b = max+2*h; */
/*       const double delta = (b-a)/M; */

      /* ++++++++++++++++++++++++++++ */
      if(o_verbose == 1){
	/* method */
	fprintf(stdout,"method     exact summation\n");
	/* binning */
	fprintf(stdout,"# bins     %zd\n",M);
	fprintf(stdout,"range      [%.3e,%.3e]\n",a,a+(M-1)*delta);
      }
      /* ++++++++++++++++++++++++++++ */


      switch(o_kerneltype){
      case 0:
	K=Kepanechnikov;
	break;
      case 1:
	K=Krectangular;
	break;
      case 2:
	K=Kgauss;
	break;
      default:
	fprintf(stdout,"unknown kernel; use %s -h\n",argv[0]);
	exit(+1);
      }

      for(i=imin;i<imax;i++){
	double x=0,y=0,y2=0,y3=0,y4=0,m,v;
	double zmin=0,zmax=0;
	const double z = a+i*delta;
	size_t j;
	double dtmp1;

	switch(o_kerneltype){
	case 0:/* Epanechnikov */
	  zmin = z-sqrt(5)*h;
	  zmax = z+sqrt(5)*h;
	  break;
	case 1:/* Rectangular */
	  zmin = z-h;
	  zmax = z+h;
	  break;
	case 2:/* Gaussian */
	  zmin = -DBL_MAX;
	  zmax = DBL_MAX;
	  break;
	}

	for(j=0;j<size;j++){/*assume ordered data*/
	  if(data[0][j]<zmin)
	    continue;
	  else if(data[0][j] >zmax)
	    break;
	  dtmp1 = K((z-data[0][j])/h);
	  x+= dtmp1;
	  y+= (dtmp1*=data[1][j]);
	  if(o_sdev||o_skew||o_kurt)
	    y2+= (dtmp1*=data[1][j]);;
	  if(o_skew)
	    y3+= dtmp1*data[1][j];
	  if(o_kurt)
	    y4+= dtmp1*data[1][j]*data[1][j];
	}

	printf(FLOAT_SEP,z);

	m=(x>0 ? y/x : 0.0);
	v=((o_sdev||o_skew||o_kurt) && x>0?fabs(y2/x-m*m):0.0);
	
	if(o_mean) printf(FLOAT_SEP,m);
	
	if(o_sdev) printf(FLOAT_SEP,sqrt(v));

	if(o_skew)
	    printf(FLOAT_SEP,
		   (x>0? (y3/x-3.*m*y2/x+2.*m*m*m)/pow(v,1.5) : 0.0 ));
	if(o_kurt)
	  printf(FLOAT_SEP,
		 (x>0? (y4/x-4.*m*y3/x+6.*m*m*y2/x-3.*m*m*m*m)/(v*v)-3. : 0.0));
	
	printf("\n");
      }

    }
    else if(o_method==1){ /*=== convolution ===*/

      double *f;/* the density */
      double *mf;/* the estimation of f*y */
      double *m2f=0;/* the estimation of f*y*y */
      double *m3f=0;/* the estimation of f*y**3 */
      double *m4f=0;/* the estimation of f*y**4 */

      size_t i;

      size_t J;/* maximum index for the kernel */
      double *Kvals;/* values of the kernel */      
      double *Kj;/* pointer to values of the kernel */


      /* the boundaries of the interval are set in order to span the
	 interval [xmin-delta, xmax+delta] */
      const double delta = (max-min)/((double) M-3);
      const double a = min-.5*delta;

      /* choose the kernel to use */
      switch(o_kerneltype){
      case 0:
	K=Kepanechnikov;
	J = (size_t) floor(sqrt(5)*h/delta);
	break;
      case 1:
	K=Krectangular;
	J = (size_t) floor(h/delta);
	break;
      default:
	fprintf(stdout,"kernel unknown or not compact  ; use %s -h\n",argv[0]);
	exit(+1);
      }

      /* ++++++++++++++++++++++++++++ */
      if(o_verbose == 1){
	/* method */
	fprintf(stdout,"method     discrete convolution\n");
	/* binning */
	fprintf(stdout,"# bins     %zd\n",M);
	fprintf(stdout,"range      [%.3e,%.3e]\n",a,a+(M-1)*delta);
	fprintf(stdout,"# K vals   %zd\n",J);
      }
      /* ++++++++++++++++++++++++++++ */

      /* set the values of the kernel */
      Kvals = (double *) my_alloc((2*J+1)*sizeof(double));
      for(i=0;i<=2*J;i++){
	Kvals[i]=K((i-(double) J)*delta/h)/h;
      }
      Kj = Kvals+J;

      /* ++++++++++++++++++++++++++++ */
      if(o_verbose == 1){
	/* check */
	double sum=0;
	for(i=0;i<=2*J;i++)
	  sum+=Kvals[i];
	fprintf(stdout,"sum K vals %.3e\n",sum*delta);
      }
      /* ++++++++++++++++++++++++++++ */


      /* allocate the bins */
      f = (double *) my_alloc(M*sizeof(double));
      mf = (double *) my_alloc(M*sizeof(double));
      if(o_sdev||o_skew||o_kurt) m2f = (double *) my_alloc(M*sizeof(double));
      if(o_skew) m3f = (double *) my_alloc(M*sizeof(double));
      if(o_kurt) m4f = (double *) my_alloc(M*sizeof(double));
      
      /* prepare the binning */
      for(i=0;i<M;i++)	mf[i]=f[i]=0.0;
      if(o_sdev||o_skew||o_kurt ) for(i=0;i<M;i++) m2f[i]=0.0;
      if(o_skew) for(i=0;i<M;i++) m3f[i]=0.0;
      if(o_kurt) for(i=0;i<M;i++) m4f[i]=0.0;

      /* fill the pre-binning */
      /* the points are in a+j*delta*/
      for(i=0;i<size;i++){
	double dtmp1 = (data[0][i]-a)/delta;
	size_t index = (size_t) floor(dtmp1);
	const double epsilon = dtmp1-index;
	const double dtmp2 = data[1][i];
	if(index>=M || dtmp1<0) continue;
	f[index]+=1-epsilon;
	mf[index]+= (dtmp1=(1-epsilon)*dtmp2);
	if(o_sdev || o_skew || o_kurt) m2f[index]+= (dtmp1*=dtmp2);
	if(o_skew) m3f[index]+=dtmp1*dtmp2;
	if(o_kurt) m4f[index]+=dtmp1*dtmp2*dtmp2;
	if(index<M-1){
	  index++;
	  f[index]+=epsilon;
	  mf[index]+= (dtmp1=epsilon*dtmp2);
	  if(o_sdev || o_skew || o_kurt) m2f[index]+= (dtmp1*=dtmp2);
	  if(o_skew) m3f[index]+=dtmp1*dtmp2;
	  if(o_kurt) m4f[index]+=dtmp1*dtmp2*dtmp2;
	}
	else {
	  index=0;
	  f[index]+=epsilon;
	  mf[index]+= (dtmp1=epsilon*dtmp2);
	  if(o_sdev || o_skew || o_kurt) m2f[index]+= (dtmp1*=dtmp2);
	  if(o_skew) m3f[index]+=dtmp1*dtmp2;
	  if(o_kurt) m4f[index]+=dtmp1*dtmp2*dtmp2;
	}
      }

      /* normalization */
      for(i=0;i<M;i++){
	f[i]/=delta*size;
	mf[i]/=delta*size;
	if(o_sdev || o_skew || o_kurt) m2f[i]/=delta*size;
	if(o_skew) m3f[i]/=delta*size;
	if(o_kurt) m4f[i]/=delta*size;
      }

      /* print the result */

      for(i=0;i<M;i++){
	int j;
	double x=0,y=0,y2=0,y3=0,y4=0;
	double m,v;

	const double z = a+i*delta;

	const int jmin = (i>J?-J:-i);
	const int jmax = (i<M-1-J?J:M-1-i);

	for(j=jmin;j<=jmax;j++){

	  x+=Kj[j]*f[i+j];
	  y+=Kj[j]*mf[i+j];
	  if(o_sdev||o_skew||o_kurt) y2+=Kj[j]*m2f[i+j];
	  if(o_skew) y3+=Kj[j]*m3f[i+j];
	  if(o_kurt) y4+=Kj[j]*m4f[i+j];
	}

	printf(FLOAT_SEP,z);
	
	m=(x>0 ? y/x : 0.0);
	v=((o_sdev||o_skew||o_kurt) && x>0?fabs(y2/x-m*m):0.0);
	
	if(o_mean) printf(FLOAT_SEP,m);
	
	if(o_sdev) printf(FLOAT_SEP,sqrt(v));

	if(o_skew)
	    printf(FLOAT_SEP,
		   (x>0? (y3/x-3.*m*y2/x+2.*m*m*m)/pow(v,1.5) : 0.0 ));
	if(o_kurt)
	  printf(FLOAT_SEP,
		 (x>0? (y4/x-4.*m*y3/x+6.*m*m*y2/x-3.*m*m*m*m)/(v*v)-3. : 0.0));
	printf("\n");
      }
    }
    else{/*=== unknown method ===*/
      fprintf(stdout,"unknown method; use %s -h\n",argv[0]);
      exit(+1);
    }
    

    exit(0);
}
