/*
  gbker2d  (ver. 5.6) -- Kernel density estimate for bivariate data
  Copyright (C) 2003-2018 Giulio Bottazzi

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  (version 2) as published by the Free Software Foundation;
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/


#include "tools.h"

int main(int argc,char* argv[]){

    double **data=NULL;
    size_t size=0;

    char *splitstring = strdup(" \t");

    /* OPTIONS */
    int o_kerneltype=0;
    int o_setbandwidth=0;
    int o_verbose=0;
    int o_devariate=1;
    
    /* data statistics */
    double ave[2],sdev[2],min[2],max[2],cov; 

    /* variables for "de-covariation" */
    double corr,cphi,sphi,rho,detJ;

    /* variables for binning */
    size_t M[2]={10,10}; /* number of bins */
    double *csi; /* the bins */
    
    /* kernel variables */
    double (*K) (double);    /* kernel   */
    double Kscale=1;         /* automatic bandwidth factor */
    double scale[2]={1.,1.}; /* optional scaling of the bandwidth */
    double h[2]={1.,1.};     /* bandwidth */
    double delta[2];         /* step sizes */
    double a[2];             /* initial points */
    size_t J[2];             /* integer radius */
    double * Kvals;          /* kernel values */
    

    /* COMMAND LINE PROCESSING */
    
    /* variables for reading command line options */
    /* ------------------------------------------ */
    int opt;
    /* ------------------------------------------ */
    
    
    /* read the command line */    
    while((opt=getopt_long(argc,argv,"H:K:n:S:hvDF:",gb_long_options, &gb_option_index))!=EOF){
      if(opt==0){
      gbutils_header(argv[0],stdout);
      exit(0);
    }
    else if(opt=='?'){
	fprintf(stderr,"option %c not recognized\n",optopt);
	exit(-1);
      }
      else if(opt=='S'){
	/*set the scale parameter for smoothing*/
	if(!strchr (optarg,',')){
	  scale[0]=scale[1]=atof(optarg);
	}
	else{
	  char *stmp1=strdup (optarg);
	  scale[0] = atof(strtok (stmp1,","));
	  scale[1] = atof(strtok (NULL,","));
	  free(stmp1);
	}
      }
      else if(opt=='n'){
	/*the number of bins*/
	if(!strchr (optarg,',')){
	  M[0]=M[1]=atoi(optarg);
	}
	else{
	  char *stmp1=strdup (optarg);
	  M[0] = atoi(strtok (stmp1,","));
	  M[1] = atoi(strtok (NULL,","));
	  free(stmp1);
	}
      }
      else if(opt=='K'){
	/*the Kernel to use*/
	o_kerneltype = atoi(optarg);
      }
      else if(opt=='H'){
	/*set the kernel bandwidth*/
	o_setbandwidth=1;
	if(!strchr (optarg,',')){
	  h[0]=h[1]=atof(optarg);
	}
	else{
	  char *stmp1=strdup (optarg);
	  h[0] = atof(strtok (stmp1,","));
	  h[1] = atof(strtok (NULL,","));
	  free(stmp1);
	}
      }
      else if(opt=='v'){
	/*increase verbosity*/
	o_verbose=1;
      }
      else if(opt=='D'){
	/*skip data de-(co)varation*/
	o_devariate=0;
      }
      else if(opt=='F'){
	/*set the fields separator string*/
	free(splitstring);
	splitstring = strdup(optarg);
      }
      else if(opt=='h'){
	/*print short help*/
	fprintf(stdout,"2D kernel density estimation on an equispaced grid. Data are read from standard\n");
	fprintf(stdout,"input as couple (x,y). The kernel bandwidth if not provided with the option -H\n");
	fprintf(stdout,"is set automatically. Different bandwidths can be specified for x and y.       \n");
	fprintf(stdout,"\nUsage: %s [options]\n\n",argv[0]);
	fprintf(stdout,"Options:\n");
	fprintf(stdout," -n  number of points where the density is computed (default 10) use comma (,)\n");
	fprintf(stdout,"     to specify different values for x and y components\n");
	fprintf(stdout," -H  set the kernel bandwidth explicitly; use comma (,) to specify different\n");
	fprintf(stdout,"     values for x and y components\n");
	fprintf(stdout," -S  scale the kernel bandwidth with respect to euristic 'optimal' value; use\n");
	fprintf(stdout,"     comma (,) to specify different values for x and y components\n");
	fprintf(stdout," -K  choose the kernel to use: 0: Epanenchnikov 1: Rectangular 2: Silverman\n");
	fprintf(stdout,"     type 1 3: Silverman type 2 (default 0)\n");
	fprintf(stdout," -D  switch off data de-variation procedure\n");
	fprintf(stdout," -v  verbose mode\n");
	fprintf(stdout," -F  specify the input fields separators (default \" \\t\")\n");
	fprintf(stdout," -h  this help\n");
	fprintf(stdout,"Examples:\n");
	fprintf(stdout," gbker2d -n 10,20 -K 1 < file  compute the kernel on an equispaced grid 10x20\n");
	fprintf(stdout,"                               using a rectangular kernel\n");
	exit(0);
      }
    }
    /* END OF COMMAND LINE PROCESSING */

    /* initialize global variables */
    initialize_program(argv[0]);
    
    /* load the data */
    load2(&data,&size,0,splitstring);

    /* compute the statistics */
    moment_short2(data,size,ave,sdev,&cov,min,max);
    corr = cov/(sdev[0]*sdev[1]);
    if(corr == 1 || corr == -1){
      fprintf(stdout,"deterministic relation between x and y\n");
      exit(+1);
    }
    rho = 1./sqrt(1-corr*corr);
    cphi = cos(.5*asin(-corr));
    sphi = sin(.5*asin(-corr));
    detJ = rho/(sdev[0]*sdev[1]);


    /* ++++++++++++++++++++++++++++ */
    if(o_verbose == 1){
      fprintf(stdout,"---- Data Statistics ------------\n");
      fprintf(stdout,"   ave       sdev      min       max\n");
      fprintf(stdout,"x: %+.2e %+.2e %+.2e %+.2e\n",
	      ave[0],sdev[0],min[0],max[0]);
      fprintf(stdout,"y: %+.2e %+.2e %+.2e %+.2e\n",
	      ave[1],sdev[1],min[1],max[1]);
      fprintf(stdout,"r=%e rho=%e phi=%e\n",corr,rho,.5*asin(-corr));
      fprintf(stdout,"\n");
    }
    /* ++++++++++++++++++++++++++++ */


    /* possibly eliminate variance and covariance */
    if(o_devariate == 1){
      size_t i;

      for(i=0;i<size;i++){
	const double dtmp0 = (data[0][i]-ave[0])/sdev[0];
	const double dtmp1 = (data[1][i]-ave[1])/sdev[1];
	data[0][i] = (dtmp0*cphi+dtmp1*sphi)*rho;
	data[1][i] = (dtmp0*sphi+dtmp1*cphi)*rho;
      }

      /*find new min and max*/
      min[0] = max[0] =data[0][0];
      min[1] = max[1] =data[1][0];
      
      for (i=0;i<size;i++) {
	const double dtmp0= data[0][i];
	const double dtmp1= data[1][i];
	if(dtmp0 < min[0]){
	  min[0] = dtmp0;
	}
	else if(dtmp0 > max[0]){
	  max[0] = dtmp0;
	}
	if(dtmp1 < min[1]){
	  min[1] = dtmp1;
	}
	else if(dtmp1 > max[1]){
	  max[1] = dtmp1;
	}
      }
    }


    /* choose the kernel to use */
    switch(o_kerneltype){
    case 0:
      K=Kepanechnikov2d;
      Kscale=2.40;
      break;
    case 1:
      K=Krectangular2d;
      break;
    case 2:
      K=Ksilverman2d_1;
      Kscale=2.78;
      break;
    case 3:
      K=Ksilverman2d_2;
      Kscale=3.12;
      break;
    default:
      fprintf(stdout,"unknown kernel; use %s -h\n",argv[0]);
      exit(+1);
    }


    /* the parameter h is not provided on command line it is set
       by an automatic procedure [Silverman p.86] */
    if(o_setbandwidth == 0){
      if(o_devariate == 1){
	h[0]=scale[0]*Kscale/pow(size,1./6);
	h[1]=scale[1]*Kscale/pow(size,1./6);
      }
      else{
	h[0]=sdev[0]*scale[0]*Kscale/pow(size,1./6);
	h[1]=sdev[1]*scale[1]*Kscale/pow(size,1./6);
      }
    }

    /* ++++++++++++++++++++++++++++ */
    if(o_verbose == 1){
      fprintf(stdout,"---- Density Parameters ------------\n");
      /* data treatment */
      fprintf(stdout,"de-variation  ");
      if(o_devariate == 1)
	fprintf(stdout,"yes (detJ=%+.3e)",detJ);
      else
	fprintf(stdout,"no");
      fprintf(stdout,"\n");	
      /* kernel type */
      fprintf(stdout,"kernel type   ");
      switch(o_kerneltype){
      case 0:
	fprintf(stdout,"Epanechnikov");
	break;
      case 1:
	fprintf(stdout,"Rectangular");
	break;
      case 2:
	fprintf(stdout,"Silverman type 1");
	break;
      case 3:
	fprintf(stdout,"Silverman type 2");
	break;
      }
      fprintf(stdout,"\n");
      /* bandwidth */
      fprintf(stdout,"x bandwidth   %.3e ",h[0]);
      if(o_setbandwidth == 0){
	fprintf(stdout,"(automatic with A=%.2f ",Kscale);
	if(scale[0] != 1 )
	  fprintf(stdout,"scaled by %f",scale[0]);
	fprintf(stdout,")");
      }
      else{
	fprintf(stdout,"(provided)");
      }
      fprintf(stdout,"\n");
      fprintf(stdout,"y bandwidth   %.3e ",h[1]);
      if(o_setbandwidth == 0){
	fprintf(stdout,"(automatic with A=%.2f ",Kscale);
	if(scale[1] != 1 )
	  fprintf(stdout,"scaled by %f",scale[1]);
	fprintf(stdout,")");
      }
      else{
	fprintf(stdout,"(provided)");
      }
      fprintf(stdout,"\n");
      fprintf(stdout,"\n");
    }
    /* ++++++++++++++++++++++++++++ */


    /* the boundaries of the interval are set 
       to have a = xmin-delta b=xmax+delta */
    delta[0] = (max[0]-min[0])/(M[0] -2.0);
    delta[1] = (max[1]-min[1])/(M[1] -2.0);
    a[0] = min[0]-.5*delta[0];
    a[1] = min[1]-.5*delta[1];
    J[0] = (size_t) floor(h[0]/delta[0]);
    J[1] = (size_t) floor(h[1]/delta[1]);
 
    /* ++++++++++++++++++++++++++++ */
    if(o_verbose == 1){
      /* binning */
      fprintf(stdout,"---- Binning Structure ------------\n");
      fprintf(stdout,"   M      data range         delta    J     bins range\n");
      fprintf(stdout,"x: %zd [%+.2e,%+.2e] %+.2e %zd [%+.2e,%+.2e]\n",
	      M[0],min[0],max[0],delta[0],J[0],a[0],a[0]+(M[0]-1)*delta[0]);    
      fprintf(stdout,"y: %zd [%+.2e,%+.2e] %+.2e %zd [%+.2e,%+.2e]\n",
	      M[1],min[1],max[1],delta[1],J[1],a[1],a[1]+(M[1]-1)*delta[1]);
      fprintf(stdout,"\n");
    }
    /* ++++++++++++++++++++++++++++ */
    
    
    /* set the values of the kernel */
    {
      size_t i,j;

      Kvals = (double *) my_alloc((2*J[0]+1)*(2*J[1]+1)*sizeof(double));

      for(i=0;i<=2*J[0];i++){
	const double dtmp0 = (int) i- (int) J[0];
	for(j=0;j<=2*J[1];j++){
	  const double dtmp1 = (int) j- (int) J[1];
	  const double dtmp2 = dtmp0*dtmp0*delta[0]*delta[0]/(h[0]*h[0])+
	    dtmp1*dtmp1*delta[1]*delta[1]/(h[1]*h[1]) ;
	  Kvals[i*(2*J[1]+1)+j] = K(dtmp2)/(h[0]*h[1]);
	}
      }

    }

    /* ++++++++++++++++++++++++++++ */
    if(o_verbose == 1){
      size_t i,j;
      double sum=0.0;
      
      fprintf(stdout,"---- Final Checks -----------------\n");

      for(i=0;i<=2*J[0];i++){
	for(j=0;j<=2*J[1];j++){
	  sum += Kvals[i*(2*J[1]+1)+j];
/* 	  fprintf(stdout,"%f ",Kvals[i*(2*J[1]+1)+j]); */
	}
/* 	fprintf(stdout,"\n"); */
      }
      fprintf(stdout,"sum of kernel coeff. (~1):     %e\n",sum*delta[0]*delta[1]);
    }
    /* ++++++++++++++++++++++++++++ */

    /* binning */
    {
      size_t i,j;
/*       double sum=0.0; */

      /* allocate the bins */
      csi = (double *) my_alloc(M[0]*M[1]*sizeof(double));
      
      /* prepare the binning */
      for(i=0;i<M[0];i++){
	for(j=0;j<M[1];j++){
	  csi[i*M[1]+j]=0.0;
	}
      }
    
      /* fill the pre-binning */
      /* the points are in a+j*delta*/
      for(i=0;i<size;i++){
	const double dtmp0 = (data[0][i]-a[0])/delta[0];
	const double dtmp1 = (data[1][i]-a[1])/delta[1];
	const size_t index0 = floor(dtmp0);
	const size_t index1 = floor(dtmp1);
	const double epsilon0 = dtmp0-index0;
	const double epsilon1 = dtmp1-index1;

	if(index0 >= M[0]-1  || index1 >= M[1]-1 ) continue;

	csi[index0*M[1]+index1]+=(1.-epsilon0)*(1.-epsilon1);
	csi[index0*M[1]+index1+1]+=(1.-epsilon0)*epsilon1;
	csi[(index0+1)*M[1]+index1]+=epsilon0*(1.-epsilon1);
	csi[(index0+1)*M[1]+index1+1]+=epsilon0*epsilon1;
      }

      /* normalization */
      for(i=0;i<M[0];i++){
	for(j=0;j<M[1];j++){
 	  csi[i*(M[1])+j]/=size;
/* 	  sum += csi[i*M[1]+j]; */
	}
      }

/*       if(o_verbose == 1){ */
/* 	for(i=0;i<M[0];i++){ */
/* 	  for(j=0;j<M[1];j++){ */
/* 	    fprintf(stdout,"%f ", csi[i*M[1]+j]); */
/* 	  } */
/* 	  fprintf(stdout,"\n"); */
/* 	} */
/* 	fprintf(stdout,"csi sum %f\n",sum); */
/*       } */
    }

    /* print the result */
    {
      int i,j;
      double dtmp1=0.0;
      const int Jx=J[0];
      const int Jy=J[1];
      const int Mx=M[0];
      const int My=M[1];

      for(i=-Jx;i<Mx+Jx;i++){
	const double tmpx = a[0]+i*delta[0];
	const int min0 = (i-Jx>0?i-Jx:0);
	const int max0 = (i+Jx>Mx-1?Mx-1:i+Jx);
	for(j=-Jy;j<My+Jy;j++){
	  const double tmpy = a[1]+j*delta[1];
	  const int min1 = (j-Jy>0?j-Jy:0);
	  const int max1 = (j+Jy>My-1?My-1:j+Jy);
	  double sum=0.0;
	  int l,m;
	  for(l=min0;l<=max0;l++){
	    for(m=min1;m<=max1;m++){
	      sum+=Kvals[(i-l+Jx)*(2*Jy+1)+(j-m+Jy)]*csi[l*My+m];
	    }
	  }
	  if(o_devariate == 1){
	    const double x = (tmpx*cphi-tmpy*sphi)*sdev[0]+ave[0];
	    const double y = (tmpy*cphi-tmpx*sphi)*sdev[1]+ave[1];
	    printf(FLOAT_SEP,x);
	    printf(FLOAT_SEP,y);
	    printf(FLOAT_NL,sum*detJ);
	    dtmp1+=sum;
	  }
	  else{
	    printf(FLOAT_SEP,tmpx);
	    printf(FLOAT_SEP,tmpy);
	    printf(FLOAT_NL,sum);
	    dtmp1+=sum;
	  }
	}
	printf("\n");
      }

      /* ++++++++++++++++++++++++++++ */
      if(o_verbose == 1){
	fprintf(stdout,"sum of discrete density (~1):  %e\n",dtmp1*delta[0]*delta[1]);
      }
      /* ++++++++++++++++++++++++++++ */

    }


    
    exit(0);
}
