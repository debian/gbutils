/*
  gbtest (ver. 5.6) -- Compute statistical tests on data

  Copyright (C) 2011-2018 Giulio Bottazzi

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  (version 2) as published by the Free Software Foundation;
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#include "tools.h"
#include <gsl/gsl_sort.h>
#include <gsl/gsl_cdf.h>
#include <gsl/gsl_sf_gamma.h>

#define NAME(x) !strcmp(test_name,(x))

/* Global variable accessed by different routines */
/* ---------------------------------------------- */

/* options */
char Is_Data_Sorted = 0;
char Is_Data_Denan = 0;
char O_Verbose = 0;
char O_Significance = 0;


/* requirement of the test */

char Need_Sorted=0; 		/* data must be sorted */
char Need_Distrib=0; 		/* data are from a distribution
				   function, between 0 and 1 */

/* variables related to "ties" counting */

size_t tgroups=0;   /* number of tied groups */
size_t *tgroup=NULL;  /* size of tied groups  */

/* degree of freedoms of the T tests with hetero variances */
double TdiffV_dof;

/* Utility functions and structures */
/* ------------------------------- */


/* common ordering of joint samples  */

typedef struct indexed {
  size_t sample;
  double data;
} Indexed;

int sort_indexed_by_value(const void *a, const void *b){
  const Indexed *da = (const Indexed *) a; 
  const Indexed *db = (const Indexed *) b; 
  
  return (da->data > db->data) - (da->data < db->data); 
}


/* One sample tests */
/* ---------------- */


/* the following is special. the first column contains the
   observations in each bin and the second column the theoretical
   probability of each bin */

double
chi2_onesample(double ** const data, const size_t size)
{

  size_t i;
  double N=0.0;
  double res=0.0;
  
  /* compute total number of observations */
  for(i=0;i<size;i++) N += data[0][i];
  /* build the test */
  for(i=0;i<size;i++){
/*     printf(" %g\n",(data[0][i]-N*data[1][i])*(data[0][i]-N*data[1][i])/(N*data[1][i])); */
    if(data[1][i]>0)
      res+=(data[0][i]-N*data[1][i])*(data[0][i]-N*data[1][i])/(N*data[1][i]);
    else if(data[1][i] == 0 && data[0][i] >0 ){
      res=NAN;
      break;
    }
  }
  
  return res;
}


size_t WILCOsize=0; /* number of non-zero entries */

double WILCO_test(double * data, const size_t size){

  size_t i,j;
  double res=0.0;
  Indexed *alldata = (Indexed *) my_alloc(sizeof(Indexed)*size);

  /* zero entries are discarded */
  j=0;
  for(i=0;i<size;i++){
    if(data[i]<0){ 	/* negative entry */
      alldata[j].sample = 0;
      alldata[j].data = -data[i];
      j++;
    }
    else if (data[i]>0){ /* positive entry */
      alldata[j].sample = 1;
      alldata[j].data = data[i];
      j++;
    }
  }
  
  WILCOsize=j;
  alldata = (Indexed *) my_realloc(alldata,WILCOsize*sizeof(Indexed));
  
  /* sort pooled data */
  qsort(alldata,WILCOsize,sizeof(Indexed),sort_indexed_by_value);


/*   for(i=0;i<size;i++){ */
/*     fprintf(stderr,"%zd: %g\t%zd\n", */
/* 	    i+1, alldata[i].data, alldata[i].sample); */
/*   } */

  /* free possibly allocated structures */
  tgroups=0;
  /* free(tgroup); */

  /* define sum of ranks of the second group */
  i=0;
  while(i<WILCOsize){
    const double val = alldata[i].data;
    size_t equals=0;

    while(i+equals+1<WILCOsize && alldata[i+equals+1].data == val)
      equals++;

    if(equals>0){
      tgroups++;
      tgroup = (size_t *) my_realloc((void *) tgroup,tgroups*sizeof(size_t));
      tgroup[tgroups-1] = equals+1;
      for(j=i;j<i+equals+1;j++){
/* 	printf("%zd %g %zd %g\n",j+1,alldata[j].data, alldata[j].sample */
/* 	       ,(double) (i+1.+ equals/2.0)*alldata[j].sample); */
	res += (i+1.+equals/2.0)*alldata[j].sample;
      }
      i+=equals+1;
    }
    else{
/*       printf("%zd %g %zd %g\n",i+1,alldata[i].data, alldata[i].sample */
/* 	     ,(i+1.)*alldata[i].sample); */
      res += (i+1.)*alldata[i].sample;
      i++;
    }
  }

  /* de-allocate */
  free(alldata);

  return res;

}


double WILCO_pscore(const double statistic,  const size_t size)
{
  double stat=statistic;
  size_t i;
  double mean,stdev;

  mean= WILCOsize*(WILCOsize+1.0)/4.0;

  if(tgroups==0)
    stdev=sqrt(WILCOsize*(WILCOsize+1.0)*(2.0*WILCOsize+1.0)/24.0);
  else {
    double dtmp1=0.0;
    for(i=0;i<tgroups;i++)
      dtmp1 += tgroup[i]*(tgroup[i]*tgroup[i]-1.0);
    dtmp1*=0.5;
    stdev=sqrt((WILCOsize*(WILCOsize+1.0)*(2.0*WILCOsize+1.0)-dtmp1)/24.0) ;
  }

  stat = (stat-mean)/stdev;


  /* +++++++++++++++++++++++++++++++++++++ */
  if(O_Verbose>1){
    if(WILCOsize<size)
      fprintf(stderr,"#  Number of discarded zero entries    %zd\n",WILCOsize-size);
    if(tgroups>0)
      fprintf(stderr,"#  Number of ties group(s)             %zd\n",tgroups);
    fprintf(stderr,"#  Observed Wilcoxon W                 %g\n",
	    statistic);
    fprintf(stderr,"#  Large sample mean                   %g\n",mean);
    fprintf(stderr,"#  Large sample std. dev.              %g\n",stdev);
    fprintf(stderr,"#  Standardized W                      %g\n",stat);
    fprintf(stderr,"#  Asymptotic inference:\n");
    fprintf(stderr,"#   One-sided P-value: Pr{stat<W}      %g\n",
	    gsl_cdf_ugaussian_P (stat));
    fprintf(stderr,"#   One-sided P-value: Pr{stat>W}      %g\n",
	    gsl_cdf_ugaussian_Q (stat));
    fprintf(stderr,"#   Two-sided P-value: Pr{|stat|>|W|}  %g\n",
	    2*gsl_cdf_ugaussian_Q (fabs(stat)));

  }
  /* +++++++++++++++++++++++++++++++++++++ */


  return 2.0*gsl_cdf_ugaussian_Q (fabs(stat));

}


double TRTP_test(double * data, const size_t size){

  double res=0;
  size_t i;

  for(i=1;i<size-1;i++)
    if( (data[i-1]<data[i] && data[i]>data[i+1]) ||
	(data[i-1]>data[i] && data[i]<data[i+1]) )
      res=res+1;

  return res;

}


double TRTP_pscore(const double statistic,  const size_t size){

  const double mean=2.*(size-2.)/3.;
  const double stdev=sqrt((16.*size-29.)/90.);
  const double stat =(statistic-mean)/stdev;

  /* +++++++++++++++++++++++++++++++++++++ */
  if(O_Verbose>1){
    fprintf(stderr,"#  Observed Turning Points             %g\n",
	    statistic);
    fprintf(stderr,"#  Expected mean                       %g\n",mean);
    fprintf(stderr,"#  Expected std. dev.                  %g\n",stdev);
    fprintf(stderr,"#  Standardized score                  %g\n",stat);
    fprintf(stderr,"#  Asymptotic inference:\n");
    fprintf(stderr,"#   One-sided P-value: Pr{stat<obs.}      %g\n",
	    gsl_cdf_ugaussian_P (stat));
    fprintf(stderr,"#   One-sided P-value: Pr{stat>obs.}      %g\n",
	    gsl_cdf_ugaussian_Q (stat));
    fprintf(stderr,"#   Two-sided P-value: Pr{|stat|>|obs.|}  %g\n",
	    2*gsl_cdf_ugaussian_Q (fabs(stat)));
  }
  /* +++++++++++++++++++++++++++++++++++++ */

  return 2.0*gsl_cdf_ugaussian_Q (fabs(stat));

}


double TRDS_test(double * data, const size_t size){

  double res=0;
  size_t i;

  for(i=1;i<size;i++)
    if( data[i]>data[i-1])
      res=res+1.;

  return res;

}


double TRDS_pscore(const double statistic,  const size_t size){

  const double mean=(size-1.)/2.;
  const double stdev=sqrt((size+1.)/12.);
  const double stat =(statistic-mean)/stdev;

  /* +++++++++++++++++++++++++++++++++++++ */
  if(O_Verbose>1){
    fprintf(stderr,"#  Observed Positive Differences       %g\n",
	    statistic);
    fprintf(stderr,"#  Expected mean                       %g\n",mean);
    fprintf(stderr,"#  Expected std. dev.                  %g\n",stdev);
    fprintf(stderr,"#  Standardized score                  %g\n",stat);
    fprintf(stderr,"#  Asymptotic inference:\n");
    fprintf(stderr,"#   One-sided P-value: Pr{stat<obs.}      %g\n",
	    gsl_cdf_ugaussian_P (stat));
    fprintf(stderr,"#   One-sided P-value: Pr{stat>obs.}      %g\n",
	    gsl_cdf_ugaussian_Q (stat));
    fprintf(stderr,"#   Two-sided P-value: Pr{|stat|>|obs.|}  %g\n",
	    2*gsl_cdf_ugaussian_Q (fabs(stat)));
  }
  /* +++++++++++++++++++++++++++++++++++++ */

  return 2.0*gsl_cdf_ugaussian_Q (fabs(stat));

}

double TRRT_test(double * data, const size_t size){

  double res=0;
  size_t i,j;

  for(i=0;i<size-1;i++)
    for(j=i+1;j<size;j++)
      if( data[j]>data[i])
	res=res+1.;
  
  return res;

}


double TRRT_pscore(const double statistic,  const size_t size){

  const double mean=size*(size-1.)/4.;
/*   const double stdev=sqrt(size*(size-1.)*(2.*size+5.)/8.); */
  const double stdev=sqrt(size*(size-1.)*(2.*size+5.)/72.);
  const double stat =(statistic-mean)/stdev;

  /* +++++++++++++++++++++++++++++++++++++ */
  if(O_Verbose>1){
    fprintf(stderr,"#  Observed increasing couples         %g\n",
	    statistic);
    fprintf(stderr,"#  Expected mean                       %g\n",mean);
    fprintf(stderr,"#  Expected std. dev.                  %g\n",stdev);
    fprintf(stderr,"#  Standardized score                  %g\n",stat);
    fprintf(stderr,"#  Asymptotic inference:\n");
    fprintf(stderr,"#   One-sided P-value: Pr{stat<obs.}      %g\n",
	    gsl_cdf_ugaussian_P (stat));
    fprintf(stderr,"#   One-sided P-value: Pr{stat>obs.}      %g\n",
	    gsl_cdf_ugaussian_Q (stat));
    fprintf(stderr,"#   Two-sided P-value: Pr{|stat|>|obs.|}  %g\n",
	    2*gsl_cdf_ugaussian_Q (fabs(stat)));
  }
  /* +++++++++++++++++++++++++++++++++++++ */

  return 2.0*gsl_cdf_ugaussian_Q (fabs(stat));

}


double Dminus_test(double * data, const size_t size)
{

  size_t i;
  double result=0;

  /* sort data if needed */
  if(Is_Data_Sorted==0)
    qsort(data,size,sizeof(double),sort_by_value);

  /* check that data are from a distribution function */
  if(data[0]<0 || data[size-1]>1){
    fprintf(stderr,"WARNING (%s): data not from a distribution function, NAN is returned.\n",GB_PROGNAME);
    result=NAN;
  }
  else{
    for(i=0;i<size;i++){
      const double dtmp1 = (double) (i+1)/ (double) size - data[i];
      if(dtmp1>result) result = dtmp1;
    }
  }

  return result;
    
}

double Dminus_pscore(const double statistic,  const size_t size)
{
  const size_t maxindex = (size_t) floor(size*(1-statistic));
  const double N = (double) size;
  const double stat = statistic*sqrt(N);
  const double Gasy=1-exp(-2.*stat*stat);

  size_t i;
  double G=0;
  double G_smallcontrib=0;

  for(i=0;i<maxindex;i++){

    const double term = exp( gsl_sf_lnchoose (size,i) + 
			     (N-i)*log(1-statistic-i/N)+
			     (i-1.)*log(statistic+i/N)
			     );

    if(term<DBL_EPSILON)
      G_smallcontrib+=term;
    else
      G+=term;
  }
  G=G+G_smallcontrib;
  G=1.-statistic*G;

  /* +++++++++++++++++++++++++++++++++++++ */
  if(O_Verbose>1){
    fprintf(stderr,"#  Observed D+                         %g\n",statistic);
    fprintf(stderr,"#  Sample size                         %zd\n",size);
    fprintf(stderr,"#  Standardized score                  %g\n",stat);
    fprintf(stderr,"#  Exact inference:\n");
    fprintf(stderr,"#   One-sided P-value: Pr{stat>D-}      %g\n",1-G);
    fprintf(stderr,"#   One-sided P-value: Pr{stat<D-}      %g\n",G);
    fprintf(stderr,"#  Asymptotic inference:\n");
    fprintf(stderr,"#   One-sided P-value: Pr{stat>D-}      %g\n",1-Gasy);
    fprintf(stderr,"#   One-sided P-value: Pr{stat<D-}      %g\n",Gasy);
  }
  /* +++++++++++++++++++++++++++++++++++++ */

  return 1-G;
    
}


double Dplus_test(double * data, const size_t size)
{

  size_t i;
  double result=0;
  
  /* sort data if needed */
  if(Is_Data_Sorted==0)
    qsort(data,size,sizeof(double),sort_by_value);
  
  /* check that data are from a distribution function */
  if(data[0]<0 || data[size-1]>1){
    fprintf(stderr,"WARNING (%s): data not from a distribution function, NAN is returned.\n",GB_PROGNAME);
    result=NAN;
  }
  else{
    for(i=0;i<size;i++){
      const double dtmp1 = data[i] - (double) i/size ;
      if(dtmp1>result) result = dtmp1;
    }
  }

  return result;
    
}


double Dplus_pscore(const double statistic,  const size_t size)
{
  const size_t maxindex = (size_t) floor(size*(1-statistic));
  const double N = (double) size;
  const double stat = statistic*sqrt(N);
  const double Gasy=1-exp(-2.*stat*stat);

  size_t i;
  double G=0;
  double G_smallcontrib=0;

  for(i=0;i<maxindex;i++){

    const double term = exp( gsl_sf_lnchoose (size,i) + 
			     (N-i)*log(1-statistic-i/N)+
			     (i-1.)*log(statistic+i/N)
			     );

    if(term<DBL_EPSILON)
      G_smallcontrib+=term;
    else
      G+=term;
  }
  G=G+G_smallcontrib;
  G=1.-statistic*G;

  /* +++++++++++++++++++++++++++++++++++++ */
  if(O_Verbose>1){
    fprintf(stderr,"#  Observed D-                         %g\n",statistic);
    fprintf(stderr,"#  Sample size                         %zd\n",size);
    fprintf(stderr,"#  Standardized score                  %g\n",stat);
    fprintf(stderr,"#  Exact inference:\n");
    fprintf(stderr,"#   One-sided P-value: Pr{stat>D+}      %g\n",1-G);
    fprintf(stderr,"#   One-sided P-value: Pr{stat<D+}      %g\n",G);
    fprintf(stderr,"#  Asymptotic inference:\n");
    fprintf(stderr,"#   One-sided P-value: Pr{stat>D+}      %g\n",1-Gasy);
    fprintf(stderr,"#   One-sided P-value: Pr{stat<D+}      %g\n",Gasy);
  }
  /* +++++++++++++++++++++++++++++++++++++ */

  return 1-G;
    
}


double D_test(double * data, const size_t size)
{

  size_t i;
  double result=0,dtmp1=0;

  /* sort data if needed */
  if(Is_Data_Sorted==0)
    qsort(data,size,sizeof(double),sort_by_value);

  /* check that data are from a distribution function */
  if(data[0]<0 || data[size-1]>1){
    fprintf(stderr,"WARNING (%s): data not from a distribution function, NAN is returned.\n",GB_PROGNAME);
    result=NAN;
  }
  else{
    for(i=0;i<size;i++){
      const double dtmp2 = (double) (i+1)/ (double) size;
      const double dtmp3 = data[i];
      const double dtmp4 = fabs(dtmp2-dtmp3)>fabs(dtmp1-dtmp3) ? fabs(dtmp2-dtmp3) : fabs(dtmp1-dtmp3);
      if(dtmp4>result) result = dtmp4;
      dtmp1=dtmp2;
    }
  }
 
  return result;
}

double Qks(double x){

  /* computation of Qks(x) */
  const double x2 = -2.0*x*x;
  double const eps1=10*DBL_EPSILON;
  double const eps2=DBL_EPSILON;
  
  int i=1;
  double fac=2.0,sum=0.0,term=0.0,absterm=0.0;

  do{
    absterm=fabs(term);    /* previous term absolute value */
    term=fac*exp(x2*i*i);  /* new term */
    sum += term;           /* terms sum */
    fac = -fac;            /* change sign of the next term */
    i++;
  }
  while(fabs(term) > eps1*absterm && fabs(term) > eps2*sum);

  return sum;

}

double D_pscore(const double statistic,  const size_t size)
{

  const double eff_size =sqrt(size);
  const double stat = (eff_size+0.12+0.11/eff_size)*statistic ; 
  const double statasy = eff_size*statistic ;
  const double qks = Qks(stat);
  const double qksasy = Qks(statasy);

  /* +++++++++++++++++++++++++++++++++++++ */
  if(O_Verbose>1){
    fprintf(stderr,"#  Observed D                          %g\n",statistic);
    fprintf(stderr,"#  Sample size                         %zd\n",size);
    fprintf(stderr,"#  Asymptotic standardized score       %g\n",statasy);
    fprintf(stderr,"#  Small-sample corrected score        %g\n",stat);
    fprintf(stderr,"#  Small-sample corrected inference:\n");
    fprintf(stderr,"#   One-sided P-value: Pr{stat>D}      %g\n",qks);
    fprintf(stderr,"#   One-sided P-value: Pr{stat<D}      %g\n",1-qks);
    fprintf(stderr,"#  Asymptotic inference:\n");
    fprintf(stderr,"#   One-sided P-value: Pr{stat>D}      %g\n",qksasy);
    fprintf(stderr,"#   One-sided P-value: Pr{stat<D}      %g\n",1-qksasy);
  }
  /* +++++++++++++++++++++++++++++++++++++ */

  
  return qks;

}



double V_test(double * data, const size_t size)
{

  size_t i;
  double result=0,Dplus=0,Dminus=0;

  /* sort data if needed */
  if(Is_Data_Sorted==0)
    qsort(data,size,sizeof(double),sort_by_value);

  /* check that data are from a distribution function */
  if(data[0]<0 || data[size-1]>1){
    fprintf(stderr,"WARNING (%s): data not from a distribution function, NAN is returned.\n",GB_PROGNAME);
    result=NAN;
  }
  else{

    for(i=0;i<size;i++){
      const double dtmp1 = (double) (i+1)/ (double) size - data[i];
      if(dtmp1>Dplus) Dplus = dtmp1;
    }
    
    for(i=0;i<size;i++){
      const double dtmp1 = data[i] - (double) i/size ;
      if(dtmp1>Dminus) Dminus = dtmp1;
    }

    result=Dplus+Dminus;
  }

  return result;
    
}

double Qkp(double x){

  double sum=0.0;
  
  /* computation of Qkp(x) according to numerical recipes */
  if(x<0.4){
    sum=0.5;
  }
  else{
    const double x2 = 2.0*x*x;
    double const eps1=10*DBL_EPSILON;
    double const eps2=DBL_EPSILON;
    
    int i=1;
    double term=0.0,oldterm=0.0;
    
    do{
      oldterm=term;    /* save previous term */
      term=(2*x2*i*i-1)*exp(-x2*i*i);  /* new term */
      sum += term;          /* terms sum */
      i++;
    }
    while(fabs(term) > eps1*fabs(oldterm) && fabs(term) > eps2*fabs(sum));
  }
  
  return 2*sum;

}

double V_pscore(const double statistic,  const size_t size)
{

  const double eff_size =sqrt(size);
  const double stat = (eff_size+0.155+0.24/eff_size)*statistic ; 
  const double statasy = eff_size*statistic ;
  const double qkp = Qkp(stat);
  const double qkpasy = Qkp(statasy);

  /* +++++++++++++++++++++++++++++++++++++ */
  if(O_Verbose>1){
    fprintf(stderr,"#  Observed V                          %g\n",statistic);
    fprintf(stderr,"#  Sample size                         %zd\n",size);
    fprintf(stderr,"#  Asymptotic standardized score       %g\n",statasy);
    fprintf(stderr,"#  Small-sample corrected score        %g\n",stat);
    fprintf(stderr,"#  Small-sample corrected inference:\n");
    fprintf(stderr,"#   One-sided P-value: Pr{stat>V}      %g\n",qkp);
    fprintf(stderr,"#   One-sided P-value: Pr{stat<V}      %g\n",1-qkp);
    fprintf(stderr,"#  Asymptotic inference:\n");
    fprintf(stderr,"#   One-sided P-value: Pr{stat>V}      %g\n",qkpasy);
    fprintf(stderr,"#   One-sided P-value: Pr{stat<V}      %g\n",1-qkpasy);
  }
  /* +++++++++++++++++++++++++++++++++++++ */

  
  return qkp;

}




double W2_test(double * data, const size_t size)
{

  size_t i;
  double result=0;

  /* sort data if needed */
  if(Is_Data_Sorted==0)
    qsort(data,size,sizeof(double),sort_by_value);

  /* check that data are from a distribution function */
  if(data[0]<0 || data[size-1]>1){
    fprintf(stderr,"WARNING (%s): data not from a distribution function, NAN is returned.\n",GB_PROGNAME);
    result=NAN;
  }
  else{
    for(i=0;i<size;i++){
      const double dtmp1 = (data[i]-(2.*i+1.)/(2.*size));
      result +=dtmp1*dtmp1;
    }
    result += 1./(12.*size);
  }

  return result;

}


double U2_test(double * data, const size_t size)
{

  size_t i;
  double result=0;

  double dtmp1;

  /* sort data if needed */
  if(Is_Data_Sorted==0)
    qsort(data,size,sizeof(double),sort_by_value);

  /* check that data are from a distribution function */
  if(data[0]<0 || data[size-1]>1){
    fprintf(stderr,"WARNING (%s): data not from a distribution function, NAN is returned.\n",GB_PROGNAME);
    result=NAN;
  }
  else{
    for(i=0;i<size;i++){
      const double dtmp2 = (data[i]-(2.*i+1.)/(2.*size));
      result +=dtmp2*dtmp2;
    }
    result += 1./(12.*size);

    dtmp1=0.0;
    for(i=0;i<size;i++)
      dtmp1 += data[i];
    
    dtmp1 /= (double) size;
    dtmp1 -= .5;
    dtmp1 *= dtmp1;
    
    result -= size*dtmp1;
  }
  
  return result;
  
}


double A2_test(double * data, const size_t size)
{

  size_t i;
  double result=0.0;

  /* sort data if needed */
  if(Is_Data_Sorted==0)
    qsort(data,size,sizeof(double),sort_by_value);
  
  /* check that data are from a distribution function */
  if(data[0]<0 || data[size-1]>1){
    fprintf(stderr,"WARNING (%s): data not from a distribution function, NAN is returned.\n",GB_PROGNAME);
    result=NAN;
  }
  else{
    for(i=0;i<size;i++)
      result += (2.*i+1.)*(log(data[i])+log(1.-data[size-1-i]));
    
    result=-result/size-size;
  }

  return result;
  
}


double Tsingle_test(double * data, const size_t size)
{

  size_t i;

  double mean,var,error;
  double res;


  /* compute the statistics */
  mean=0;
  for(i=0;i<size;i++){
    mean += data[i];
  }
  mean /= size;
  var=error=0.0;
  for (i=0;i<size;i++) {
    const double dtmp1 = data[i]-mean;
    error += dtmp1;/* as suggested in Numerical Recipes */
    var += dtmp1*dtmp1;
  }
  var=(var-error*error/size)/(size-1.0);

/*   printf("m1=%g m2=%g v1=%g v2=%g\n",mean1,mean2,var1,var2); */

  res = mean/sqrt(var/size);

  return res;
  
}
#if defined HAVE_LIBGSL
double Tsingle_pscore(const double statistic,const size_t size)
{
  
  /* +++++++++++++++++++++++++++++++++++++ */
  if(O_Verbose>1){
    fprintf(stderr,"#  Observed T                          %g\n",statistic);
    fprintf(stderr,"#  Degrees of freedom                  %zd\n",size-1);
    fprintf(stderr,"#  Asymptotic inference:\n");
    fprintf(stderr,"#   One-sided P-value: Pr{stat>T}      %g\n",
	    gsl_cdf_tdist_Q (statistic,size-1.0));
    fprintf(stderr,"#   One-sided P-value: Pr{stat<T}      %g\n",
	    gsl_cdf_tdist_P (statistic,size-1.));
    fprintf(stderr,"#   Two-sided P-value: Pr{|stat|>|T|}  %g\n",
	    2*gsl_cdf_tdist_Q (fabs(statistic),size-1.));
  }
  /* +++++++++++++++++++++++++++++++++++++ */


  return 2*gsl_cdf_tdist_Q (fabs(statistic),size-1.);

}
#else
double Tsingle_pscore(const double statistic, const size_t size)
{

  return NAN;

}
#endif

/* Paired samples tests */
/* -------------------- */


double WILCO2_test(const double * const data1, const double * const data2, const size_t size)
{

  size_t i,j;
  double res=0.0;
  Indexed *alldata = (Indexed *) my_alloc(sizeof(Indexed)*size);
  
  /* zero entries are discarded */
  j=0;
  for(i=0;i<size;i++){
    double dtmp= data1[i]-data2[i];
    if(dtmp<0){ 	/* negative entry */
      alldata[j].sample = 0;
      alldata[j].data = -dtmp ;
      j++;
    }
    else if (dtmp>0){ /* positive entry */
      alldata[j].sample = 1;
      alldata[j].data = dtmp;
      j++;
    }
  }
  
  WILCOsize=j;
  alldata = (Indexed *) my_realloc(alldata,WILCOsize*sizeof(Indexed));
  
  /* sort pooled data */
  qsort(alldata,WILCOsize,sizeof(Indexed),sort_indexed_by_value);


/*   for(i=0;i<size;i++){ */
/*     fprintf(stderr,"%zd: %g\t%zd\n", */
/* 	    i+1, alldata[i].data, alldata[i].sample); */
/*   } */

  /* free possibly allocated structures */
  tgroups=0;
  /* free(tgroup); */

  /* define sum of ranks of the second group */
  i=0;
  while(i<WILCOsize){
    const double val = alldata[i].data;
    size_t equals=0;

    while(i+equals+1<WILCOsize && alldata[i+equals+1].data == val)
      equals++;

    if(equals>0){
      tgroups++;
      tgroup = (size_t *) my_realloc((void *) tgroup,tgroups*sizeof(size_t));
      tgroup[tgroups-1] = equals+1;
      for(j=i;j<i+equals+1;j++){
/* 	printf("%zd %g %zd %g\n",j+1,alldata[j].data, alldata[j].sample */
/* 	       ,(double) (i+1.+ equals/2.0)*alldata[j].sample); */
	res += (i+1.+equals/2.0)*alldata[j].sample;
      }
      i+=equals+1;
    }
    else{
/*       printf("%zd %g %zd %g\n",i+1,alldata[i].data, alldata[i].sample */
/* 	     ,(i+1.)*alldata[i].sample); */
      res += (i+1.)*alldata[i].sample;
      i++;
    }
  }

  /* de-allocate */
  free(alldata);

  return res;
  
}


double R_test(const double * const data1, const double * const data2, const size_t size)
{

  size_t i;

  double mean1,mean2,var1,var2,error1,error2,r;

  /* compute the statistics */
  mean1=mean2=0;
  for(i=0;i<size;i++){
    mean1 += data1[i];
    mean2 += data2[i];
  }
  mean1/=size;
  mean2/=size;

  var1=var2=error1=error2=r=0.0;
  for (i=0;i<size;i++) {
    const double dtmp1 = data1[i]-mean1;
    const double dtmp2 = data2[i]-mean2;
    error1 += dtmp1;
    error2 += dtmp2;
    var1 += dtmp1*dtmp1;
    var2 += dtmp2*dtmp2;
    r+=dtmp1*dtmp2;
  }

  var1-=error1*error1/size;
  var2-=error2*error2/size;

  r/=sqrt(var1)*sqrt(var2);

  return r;
  
}

#if defined HAVE_LIBGSL
double R_pscore(const double statistic, const size_t size)
{


  /* compute standardized statistics */
  double stat = 0.5*log((1+statistic)/(1-statistic))*sqrt(size-3.0);


  /* +++++++++++++++++++++++++++++++++++++ */
  if(O_Verbose>1){
    fprintf(stderr,"#  Size of the sample                  %zd\n",size);
    fprintf(stderr,"#  Observed Pearson's R                        %g\n",statistic);
    fprintf(stderr,"#  Asymptotic inference:\n");
    fprintf(stderr,"#   Negative correlation P-value: Pr{stat<R}   %g\n",
	    gsl_cdf_ugaussian_P (stat));
    fprintf(stderr,"#   Positive correlation P-value: Pr{stat>R}   %g\n",
	    gsl_cdf_ugaussian_Q (stat));
    fprintf(stderr,"#   Any correlation P-value:  Pr{|stat|>|R|}   %g\n",
	    2*gsl_cdf_ugaussian_Q (fabs(stat)));

  }
  /* +++++++++++++++++++++++++++++++++++++ */


  return 2.0*gsl_cdf_ugaussian_Q (fabs(stat));

}
#else
double R_pscore(const double statistic, const size_t size)
{
  return NAN;

}
#endif

#if defined HAVE_LIBGSL
double RHO_test(const double * const data1, const double * const data2, const size_t size)
{

  size_t i,j;
  size_t *temp = (size_t *) my_alloc(sizeof(size_t)*size);

  size_t equal=0;

  double *rank1 = (double *) my_alloc(sizeof(double)*size);
  double *rank2 = (double *) my_alloc(sizeof(double)*size);

  double s1=0.0,s2=0.0,s12=0.0;

  const double dtmp1 = size*((size+1.0)/2.0)*((size+1.0)/2.0);

  /* used to collect statistics on ties */
  tgroup = (size_t *) my_realloc((void *) tgroup,2*sizeof(size_t));

  gsl_sort_index (temp,data1,1,size);

  /* correct for ties */
  tgroup[0]=0;
  for (i=0;i<size;i++){
    if(i<size-1 && data1[temp[i]] == data1[temp[i+1]])
      equal++;
    else if(equal != 0){
      /* equal entries from present i to i-equal */
      const double meanrank = i+1-0.5*equal;
      for (j=i-equal;j<=i;j++)
	rank1[temp[j]] = meanrank;
      equal=0;
      tgroup[0]++;
    }
    else
      rank1[temp[i]] = i+1;
  }

  gsl_sort_index (temp,data2,1,size);

  /* correct for ties */
  tgroup[1]=0;
  for (i=0;i<size;i++){
    if(i<size-1 && data2[temp[i]] == data2[temp[i+1]])
      equal++;
    else
      if(equal != 0){
	/* equal entries from present i to i-equal */
	const double meanrank = i+1-0.5*equal;
	for (j=i-equal;j<=i;j++)
	  rank2[temp[j]] = meanrank;
	equal=0;
	tgroup[1]++;
      }
    else
      rank2[temp[i]] = i+1;
  }

/*   for (i=0;i<size;i++) */
/*     printf("# %f %f\n",rank1[i],rank2[i]); */


  for (i=0;i<size;i++){
    s1+=rank1[i]*rank1[i];
    s2+=rank2[i]*rank2[i];
    s12+=rank1[i]*rank2[i];
  }


  s1-=dtmp1;
  s2-=dtmp1;
  s12-=dtmp1;

  /* de-allocate */
  free(temp);
  free(rank1);
  free(rank2);

  return s12/sqrt(s1*s2);

}

double RHO_pscore(const double statistic, const size_t size)
{


  /* compute standardized statistics */
  double stat = statistic*sqrt(size-1.);


  /* +++++++++++++++++++++++++++++++++++++ */
  if(O_Verbose>1){
    fprintf(stderr,"#  Number of X ties group                      %zd\n",tgroup[0]);
    fprintf(stderr,"#  Number of Y ties group                      %zd\n",tgroup[1]);
    fprintf(stderr,"#  Observed Spearman's RHO                     %g\n",statistic);
    fprintf(stderr,"#  Standardized RHO                            %g\n",stat);
    fprintf(stderr,"#  Asymptotic inference:\n");
    fprintf(stderr,"#   Negative correlation P-value: Pr{stat<RHO}   %g\n",
	    gsl_cdf_ugaussian_P (stat));
    fprintf(stderr,"#   Positive correlation P-value: Pr{stat>RHO}   %g\n",
	    gsl_cdf_ugaussian_Q (stat));
    fprintf(stderr,"#   Any correlation P-value:  Pr{|stat|>|RHO|}   %g\n",
	    2*gsl_cdf_ugaussian_Q (fabs(stat)));

  }
  /* +++++++++++++++++++++++++++++++++++++ */


  return 2.0*gsl_cdf_ugaussian_Q (fabs(stat));

}

#else
double RHO_test(const double * const data1, const double * const data2, const size_t size)
{
  return NAN;

}
double RHO_pscore(const double statistic,const size_t size)
{
  return NAN;

}
#endif



double TAU_test(const double * const data1, const double * const data2, const size_t size)
{

  size_t i,j;
  double con=0,dis=0;

  /* used to collect statistics on ties */
  tgroup = (size_t *) my_realloc((void *) tgroup,2*sizeof(size_t));

  tgroup[0]=tgroup[1]=0;
  for (i=0;i<size;i++)
    for (j=i+1;j<size;j++){
      const double dtmp1 = (data1[i]-data1[j])*(data2[i]-data2[j]);
      if(dtmp1>0)
	con+=1;
      else if (dtmp1<0)
	dis+=1;
      else if(data2[i]-data2[j] == 0){
	con += 0.5;
	dis += 0.5;
	tgroup[1]++;
      }
      else
	tgroup[0]++;
    }
  
  /* correct for ties */

/*  printf("####%f %f\n", con,dis); */


  return (con-dis)/(con+dis);

}

#if defined HAVE_LIBGSL
double TAU_pscore(const double statistic, const size_t size)
{

  /* compute K coefficient */
/*   const double K = statistic*(size*(size-1)/2-tgroup[0])-1; */

  /* compute standardized statistics */
  const double stat = 3*statistic*sqrt( (size*(size-1.))/(2*(2*size+5.)) );
/*   const double statK = K/sqrt( size*(size-1.)*(2*size+5.)/18); */


  /* +++++++++++++++++++++++++++++++++++++ */
  if(O_Verbose>1){
    fprintf(stderr,"#  Number of X ties group                      %zd\n",tgroup[0]);
    fprintf(stderr,"#  Number of Y ties group                      %zd\n",tgroup[1]);
    fprintf(stderr,"#  Observed Kendall's TAU                      %g\n",statistic);
    fprintf(stderr,"#  Standardized TAU                            %g\n",stat);
/*     fprintf(stderr,"#  Observed Kendall's K                        %g\n",K); */
/*     fprintf(stderr,"#  Standardized K                              %g\n",statK); */
    fprintf(stderr,"#  Asymptotic inference based on TAU:\n");
    fprintf(stderr,"#   Negative correlation P-value: Pr{stat<TAU}   %g\n",
	    gsl_cdf_ugaussian_P (stat));
    fprintf(stderr,"#   Positive correlation P-value: Pr{stat>TAU}   %g\n",
	    gsl_cdf_ugaussian_Q (stat));
    fprintf(stderr,"#   Any correlation P-value:  Pr{|stat|>|TAU|}   %g\n",
	    2*gsl_cdf_ugaussian_Q (fabs(stat)));
/*     fprintf(stderr,"#  Asymptotic inference based on K:\n"); */
/*     fprintf(stderr,"#   Negative correlation P-value: Pr{stat<W}   %g\n", */
/* 	    gsl_cdf_ugaussian_P (statK)); */
/*     fprintf(stderr,"#   Positive correlation P-value: Pr{stat>W}   %g\n", */
/* 	    gsl_cdf_ugaussian_Q (statK)); */
/*     fprintf(stderr,"#   Any correlation P-value:  Pr{|stat|>|W|}   %g\n", */
/* 	    2*gsl_cdf_ugaussian_Q (fabs(statK))); */

  }
  /* +++++++++++++++++++++++++++++++++++++ */


  return 2.0*gsl_cdf_ugaussian_Q (fabs(stat));

}

#else
double TAU_pscore(const double statistic,
		  const size_t size1, const size_t size2)
{
  return NAN;

}
#endif


double CHI2_2_test(const double * const data1, const double * const data2, const size_t size)
{
  
  size_t i;

  double tot1=0.0,tot2=0.0,res=0.0,dtmp1,dtmp2;

  for(i=0;i<size;i++){
    tot1+=data1[i];
    tot2+=data2[i];
  }

  dtmp1 = sqrt(tot2/tot1);
  dtmp2 = 1./dtmp1;

  for(i=0;i<size;i++)
    if(data1[i]+data2[i]>0){
      const double dtmp3 = (dtmp1*data1[i]-dtmp2*data2[i]);
      res += dtmp3*dtmp3/(data1[i]+data2[i]);
    }

  return res;
}


#if defined HAVE_LIBGSL
double CHI2_2_pscore(const double statistic, const size_t size)
{


  /* +++++++++++++++++++++++++++++++++++++ */
  if(O_Verbose>1){
    fprintf(stderr,"#  Observed T                          %g\n",statistic);
    fprintf(stderr,"#  Degrees of freedom                  %zd\n",size-1);
    fprintf(stderr,"#  Asymptotic inference:\n");
    fprintf(stderr,"#   One-sided P-value: Pr{stat>T}      %g\n",
	     gsl_cdf_chisq_Q (statistic,size-1));

  }
  /* +++++++++++++++++++++++++++++++++++++ */

  return   gsl_cdf_chisq_Q (statistic,size-1);
}
#else
double CHI2_2_pscore(const double statistic,
		 const size_t size1, const size_t size2)
{

  return  NAN;
}
#endif


double Tpaired_test(const double * const data1, const double * const data2, const size_t size)
{
  
  size_t i;
  double mean, var, error;

  /* compute the mean and standard deviation of the difference */

  /* compute the statistics */
  mean=0;
  for(i=0;i<size;i++){
    mean += data1[i]-data2[i];
  }
  mean /= size;
  var=error=0.0;
  for (i=0;i<size;i++) {
    const double dtmp1 = data1[i]-data2[i]-mean;
    error += dtmp1;/* as suggested in Numerical Recipes */
    var += dtmp1*dtmp1;
  }
  var=var-error*error/size;
  var/=(size-1.0);

  return sqrt(size)*mean/sqrt(var);
}

#if defined HAVE_LIBGSL
double Tpaired_pscore(const double statistic, const size_t size)
{

  /* +++++++++++++++++++++++++++++++++++++ */
  if(O_Verbose>1){
    fprintf(stderr,"#  Observed T                          %g\n",statistic);
    fprintf(stderr,"#  Degrees of freedom                  %zd\n",size-1);
    fprintf(stderr,"#  Asymptotic inference:\n");
    fprintf(stderr,"#   One-sided P-value: Pr{stat>T}      %g\n",
	    gsl_cdf_tdist_Q (statistic,size-1.0));
    fprintf(stderr,"#   One-sided P-value: Pr{stat<T}      %g\n",
	    gsl_cdf_tdist_P (statistic,size-1.0));
    fprintf(stderr,"#   Two-sided P-value: Pr{|stat|>|T|}  %g\n",
	    2*gsl_cdf_tdist_Q (fabs(statistic),size-1.0));
  }
  /* +++++++++++++++++++++++++++++++++++++ */

  return 2*gsl_cdf_tdist_Q (fabs(statistic),size-1.0);
}
#else
double Tpaired_pscore(const double statistic,
		 const size_t size1, const size_t size2)
{

  return  NAN;
}
#endif


/* Two sample tests */
/* ---------------- */

double F_test(double * data1, const size_t size1, 
	      double * data2, const size_t size2)
{
  size_t i;

  double mean1,mean2,var1,var2,error;

  /* compute the statistics */
  mean1=mean2=0;
  for(i=0;i<size1;i++){
    mean1 += data1[i];
  }
  for(i=0;i<size2;i++){
    mean2 += data2[i];
  }
  mean1 /= size1;
  mean2 /= size2;
  var1=error=0.0;
  for (i=0;i<size1;i++) {
    const double dtmp1 = data1[i]-mean1;
    error += dtmp1;/* as suggested in Numerical Recipes */
    var1 += dtmp1*dtmp1;
  }
  var1=var1-error*error/size1;
  var1/=(size1-1.0);

  var2=error=0.0;
  for (i=0;i<size2;i++) {
    const double dtmp1 = data2[i]-mean2;
    error += dtmp1;/* as suggested in Numerical Recipes */
    var2 += dtmp1*dtmp1;
  }
  var2=var2-error*error/size2;
  var2/=(size2-1.0);

/*   printf("m1=%g m2=%g v1=%g v2=%g\n",mean1,mean2,var1,var2); */

  /* +++++++++++++++++++++++++++++++++++++ */
  if(O_Verbose>1){
    fprintf(stderr,"#  Degrees of freedom:                 %zd / %zd\n",size1,size2);
    fprintf(stderr,"#  Variances                           %g / %g\n",var1,var2);
  }
  /* +++++++++++++++++++++++++++++++++++++ */


  return var1/var2;

}
#if defined HAVE_LIBGSL
double F_pscore(const double statistic,
		const size_t size1, const size_t size2)
{
  
  const double twotailed = ( statistic > (size2-1.)/(size2-3.) ?
			    2.*gsl_cdf_fdist_Q (statistic,size1-1.,size2-1.):
			    2.*gsl_cdf_fdist_P (statistic,size1-1.,size2-1.));
  
  /* +++++++++++++++++++++++++++++++++++++ */
  if(O_Verbose>1){
    fprintf(stderr,"#  Observed (expected) F               %g (%g)\n",
	    statistic,(size2-1.)/(size2-3.));
    fprintf(stderr,"#  Asymptotic inference:\n");
    fprintf(stderr,"#   One-sided P-value: Pr{stat<F}      %g\n",
	    gsl_cdf_fdist_P (statistic,size1-1.,size2-1.) );
    fprintf(stderr,"#   One-sided P-value: Pr{stat>F}      %g\n",
	    gsl_cdf_fdist_Q (statistic,size1-1.,size2-1.) );
    fprintf(stderr,"#   Two-sided P-value: Pr{|stat|>|F|}  %g\n",
	    twotailed);
    
  }
  /* +++++++++++++++++++++++++++++++++++++ */

  return twotailed;
  
}
#else
double F_pscore(const double statistic,
		 const size_t size1, const size_t size2)
{

  return NAN;

}
#endif



double Thetero_test(double * data1, const size_t size1, 
		   double * data2, const size_t size2)
{
  size_t i;

  double mean1,mean2,var1,var2,error;
  double res;


  /* compute the statistics */
  mean1=mean2=0;
  for(i=0;i<size1;i++){
    mean1 += data1[i];
  }
  for(i=0;i<size2;i++){
    mean2 += data2[i];
  }
  mean1 /= size1;
  mean2 /= size2;
  var1=error=0.0;
  for (i=0;i<size1;i++) {
    const double dtmp1 = data1[i]-mean1;
    error += dtmp1;/* as suggested in Numerical Recipes */
    var1 += dtmp1*dtmp1;
  }
  var1=var1-error*error/size1;
  var1/=(size1-1.0);

  var2=error=0.0;
  for (i=0;i<size2;i++) {
    const double dtmp1 = data2[i]-mean2;
    error += dtmp1;/* as suggested in Numerical Recipes */
    var2 += dtmp1*dtmp1;
  }
  var2=var2-error*error/size2;
  var2/=(size2-1.0);

/*   printf("m1=%g m2=%g v1=%g v2=%g\n",mean1,mean2,var1,var2); */

  res = (mean1-mean2)/sqrt( var1/size1+var2/size2 );

  TdiffV_dof = pow(var1/size1+var2/size2,2)/
    (pow(var1/size1,2)/(size1-1.0)+pow(var2/size2,2)/(size2-1.0));

  return res;
}
#if defined HAVE_LIBGSL
double Thetero_pscore(const double statistic,
		     const size_t size1, const size_t size2)
{

  /* +++++++++++++++++++++++++++++++++++++ */
  if(O_Verbose>1){
    fprintf(stderr,"#  Size of the first sample            %zd\n",size1);
    fprintf(stderr,"#  Size of the second sample           %zd\n",size2);
    fprintf(stderr,"#  Observed T                          %g\n",statistic);
    fprintf(stderr,"#  Degrees of freedom                  %g\n",TdiffV_dof);
    fprintf(stderr,"#  Asymptotic inference:\n");
    fprintf(stderr,"#   One-sided P-value: Pr{stat>T}      %g\n",
	    gsl_cdf_tdist_Q (statistic,TdiffV_dof));
    fprintf(stderr,"#   One-sided P-value: Pr{stat<T}      %g\n",
	    gsl_cdf_tdist_P (statistic,TdiffV_dof));
    fprintf(stderr,"#   Two-sided P-value: Pr{|stat|>|T|}  %g\n",
	    2*gsl_cdf_tdist_Q (fabs(statistic),TdiffV_dof));
  }
  /* +++++++++++++++++++++++++++++++++++++ */


  return 2*gsl_cdf_tdist_Q (fabs(statistic),TdiffV_dof);

}
#else
double Thetero_pscore(const double statistic,
		 const size_t size1, const size_t size2)
{

  return NAN;

}
#endif


double Thomo_test(double * data1, const size_t size1, 
		   double * data2, const size_t size2)
{
  size_t i;

  double mean1,mean2,var1,var2,error;
  double res;


  /* compute the statistics */
  mean1=mean2=0;
  for(i=0;i<size1;i++){
    mean1 += data1[i];
  }
  for(i=0;i<size2;i++){
    mean2 += data2[i];
  }
  mean1 /= size1;
  mean2 /= size2;
  var1=error=0.0;
  for (i=0;i<size1;i++) {
    const double dtmp1 = data1[i]-mean1;
    error += dtmp1;/* as suggested in Numerical Recipes */
    var1 += dtmp1*dtmp1;
  }
  var1=var1-error*error/size1;
  var2=error=0.0;
  for (i=0;i<size2;i++) {
    const double dtmp1 = data2[i]-mean2;
    error += dtmp1;/* as suggested in Numerical Recipes */
    var2 += dtmp1*dtmp1;
  }
  var2=var2-error*error/size2;

/*   printf("m1=%g m2=%g v1=%g v2=%g\n",mean1,mean2,var1,var2); */

  res = (mean1-mean2)/sqrt( (var1+var2)*(size1+size2)/(size1*size2*(size1+size2-2.0)) );

  return res;
}
#if defined HAVE_LIBGSL
double Thomo_pscore(const double statistic,
		     const size_t size1, const size_t size2)
{


  /* +++++++++++++++++++++++++++++++++++++ */
  if(O_Verbose>1){
    fprintf(stderr,"#  Observed T                          %g\n",statistic);
    fprintf(stderr,"#  Degrees of freedom                  %zd\n",size1+size2-2);
    fprintf(stderr,"#  Asymptotic inference:\n");
    fprintf(stderr,"#   One-sided P-value: Pr{stat>T}      %g\n",
	    gsl_cdf_tdist_Q (statistic,size1+size2-2.0));
    fprintf(stderr,"#   One-sided P-value: Pr{stat<T}      %g\n",
	    gsl_cdf_tdist_P (statistic,size1+size2-2.0));
    fprintf(stderr,"#   Two-sided P-value: Pr{|stat|>|T|}  %g\n",
	    2*gsl_cdf_tdist_Q (fabs(statistic),size1+size2-2.0));
  }
  /* +++++++++++++++++++++++++++++++++++++ */

  return 2*gsl_cdf_tdist_Q (fabs(statistic),size1+size2-2.0);

}
#else
double Thomo_pscore(const double statistic,
		 const size_t size1, const size_t size2)
{

  return NAN;

}
#endif


/* Wilcoxon-Mann-Whitney U */

double WMW_test(double * data1, const size_t size1, 
		double * data2, const size_t size2)
{
  size_t i,j;
  double rank2=0.0;

  const size_t N = size1+size2;
  Indexed *alldata = (Indexed *) my_alloc(sizeof(Indexed)*N);  /* contains pooled data */
  

  /* set the pooled data */
  for(i=0;i<size1;i++){
    alldata[i].sample = 0;
    alldata[i].data = data1[i];
  }
  for(i=0;i<size2;i++){
    alldata[size1+i].sample = 1;
    alldata[size1+i].data = data2[i];
  }

  /* sort pooled data */
  qsort(alldata,N,sizeof(Indexed),sort_indexed_by_value);

  /* free possibly allocated structures */
  tgroups=0;
  /* free(tgroup); */

  /* define sum of ranks of the second group */
  i=0;
  while(i<N){
    const double val = alldata[i].data;
    size_t equals=0;
    while(i+equals+1<N && alldata[i+equals+1].data == val)
      equals++;
    
    if(equals>0){
      tgroups++;
       tgroup = (size_t *) my_realloc((void *) tgroup,tgroups*sizeof(size_t));  
      tgroup[tgroups-1] = equals+1;
      for(j=i;j<i+equals+1;j++)
	if( alldata[j].sample==1 ) rank2 += i+1.+equals/2.0 ;
      i+=equals+1;
    }
    else{
      if( alldata[i].sample==1 ) rank2 += i+1;
      i++;
    }
  }

/*   for(i=0;i<N;i++){ */
/*     fprintf(stderr,"%zd: %g\t%zd\n", */
/* 	    i+1, alldata[i].data, alldata[i].sample); */
/*   } */


  free(alldata);

  return rank2;
}

#if defined HAVE_LIBGSL
double WMW_pscore(const double statistic,
		 const size_t size1, const size_t size2)
{

  size_t i;
  const size_t N=size1+size2;
  const double mean=size2*(N+1.0)/2.0;
  double stdev=sqrt(size1*size2*(N+1.0)/12.0);
  double stat;
  
  /* compute correction for ties */
  if(tgroups>0){
    double dtmp1=0.0;
    for(i=0;i<tgroups;i++)
      dtmp1 += tgroup[i]*(tgroup[i]*tgroup[i]-1.0);
    dtmp1 /= N*(N*N-1.0);
    dtmp1 = 1.0-dtmp1;
    stdev = stdev*dtmp1;
  }

  /* compute standardized statistics */
  stat = (statistic-mean)/stdev;


  /* +++++++++++++++++++++++++++++++++++++ */
  if(O_Verbose>1){
    if(tgroups>0)
      fprintf(stderr,"#  Number of ties group(s)             %zd\n",tgroups);
    fprintf(stderr,"#  Observed Mann-Whitney U             %g\n",
	    statistic);
    fprintf(stderr,"#  Large sample mean                   %g\n",mean);
    fprintf(stderr,"#  Large sample std. dev.              %g\n",stdev);
    fprintf(stderr,"#  Standardized U                      %g\n",stat);
    fprintf(stderr,"#  Asymptotic inference:\n");
    fprintf(stderr,"#   One-sided P-value: Pr{stat<W}      %g\n",
	    gsl_cdf_ugaussian_P (stat));
    fprintf(stderr,"#   One-sided P-value: Pr{stat>W}      %g\n",
	    gsl_cdf_ugaussian_Q (stat));
    fprintf(stderr,"#   Two-sided P-value: Pr{|stat|>|W|}  %g\n",
	    2*gsl_cdf_ugaussian_Q (fabs(stat)));

  }
  /* +++++++++++++++++++++++++++++++++++++ */


  return 2.0*gsl_cdf_ugaussian_Q (fabs(stat));

}
#else
double WMW_pscore(const double statistic,
		 const size_t size1, const size_t size2)
{

  return NAN;

}
#endif


double FP_test(double * data1, const size_t size1, 
		  double * data2, const size_t size2)
{

  size_t i,j;

  double *rank1 = (double *) my_alloc(sizeof(double)*size1);
  double *rank2 = (double *) my_alloc(sizeof(double)*size2);

  double mean1,mean2,var1,var2,error;
  double res;

  /* sort data if needed */
  if(Is_Data_Sorted==0){
    qsort(data1,size1,sizeof(double),sort_by_value);
    qsort(data2,size2,sizeof(double),sort_by_value);
  }
  
  for(i=0;i<size1;i++){
    rank1[i]=0.0;
    for(j=0;j<size2;j++)
      if(data2[j]<data1[i])
	rank1[i] += 1.0;
      else if (data2[j]==data1[i])
	rank1[i] += 0.5;
      else
	break;
  }

  for(i=0;i<size2;i++){
    rank2[i]=0.0;
    for(j=0;j<size1;j++)
      if(data1[j]<data2[i])
	rank2[i] += 1.0;
      else if (data1[j]==data2[i])
	rank2[i] += 0.5;
      else
	break;
  }


  /* compute the statistics */
  mean1=mean2=0;
  for(i=0;i<size1;i++){
    mean1 += rank1[i];
  }
  for(i=0;i<size2;i++){
    mean2 += rank2[i];
  }
  mean1 /= size1;
  mean2 /= size2;
  var1=error=0.0;
  for (i=0;i<size1;i++) {
    const double dtmp1 = rank1[i]-mean1;
    error += dtmp1;/* as suggested in Numerical Recipes */
    var1 += dtmp1*dtmp1;
  }
  var1=var1-error*error/size1;
  var2=error=0.0;
  for (i=0;i<size2;i++) {
    const double dtmp1 = rank2[i]-mean2;
    error += dtmp1;/* as suggested in Numerical Recipes */
    var2 += dtmp1*dtmp1;
  }
  var2=var2-error*error/size2;

/*   printf("m1=%g m2=%g v1=%g v2=%g\n",mean1,mean2,var1,var2); */

  res = (mean1*size1-mean2*size2)/(2.0*sqrt(var1+var2+mean1*mean2));


  /* de-allocate */
  free(rank1);
  free(rank2);

  return res;

}

#if defined HAVE_LIBGSL
double FP_pscore(const double statistic,
		 const size_t size1, const size_t size2)
{

  /* +++++++++++++++++++++++++++++++++++++ */
  if(O_Verbose>1){
    if(tgroups>0)
      fprintf(stderr,"#  Number of ties group(s)             %zd\n",tgroups);
    fprintf(stderr,"#  Observed Standardized U             %g\n",statistic);
    fprintf(stderr,"#  Asymptotic inference:\n");
    fprintf(stderr,"#   One-sided P-value: Pr{stat<U}      %g\n",
	    gsl_cdf_ugaussian_P (statistic));
    fprintf(stderr,"#   One-sided P-value: Pr{stat>U}      %g\n",
	    gsl_cdf_ugaussian_Q (statistic));
    fprintf(stderr,"#   Two-sided P-value: Pr{|stat|>|U|}  %g\n",
	    2*gsl_cdf_ugaussian_Q (fabs(statistic)));

  }
  /* +++++++++++++++++++++++++++++++++++++ */


  return 2.0*gsl_cdf_ugaussian_Q (fabs(statistic));

}
#else
double FP_pscore(const double statistic,
		 const size_t size1, const size_t size2)
{

  return NAN;

}
#endif


double KS_test(double * data1, const size_t size1, 
	       double * data2, const size_t size2)
{
  
  size_t i1=0,i2=0;
  double d1,d2,adiff,F1=0.0,F2=0.0,res=0.0;

  /* sort data if needed */
  if(Is_Data_Sorted==0){
    qsort(data1,size1,sizeof(double),sort_by_value);
    qsort(data2,size2,sizeof(double),sort_by_value);
  }
  
  while (i1 < size1 && i2 < size2) {
    d1=data1[i1];
    d2=data2[i2];

    if (d1 <= d2) F1=i1++/((double) size1);
    if (d2 <= d1) F2=i2++/((double) size2);

    adiff = fabs(F1-F2);

    if (adiff > res) res=adiff;
  }

  return res;
}

double KS_pscore(const double statistic,
		 const size_t size1, const size_t size2)
{

  const double eff_size =sqrt(((double) size1*size2)/( (double) size1+size2));
  const double x = (eff_size+0.12+0.11/eff_size)*statistic ; 
  const double qks=Qks(x);
  

  /* +++++++++++++++++++++++++++++++++++++ */
  if(O_Verbose>1){
    fprintf(stderr,"#  Observed KS                          %g\n",statistic);
    fprintf(stderr,"#  Effective Size                       %g\n",eff_size*eff_size);
    fprintf(stderr,"#  Asymptotic inference:\n");
    fprintf(stderr,"#   One-sided P-value: Pr{stat>KS}      %g\n",qks);
  }
  /* +++++++++++++++++++++++++++++++++++++ */

  
  return qks;

}


/* Many samples tests */
/* ------------------ */

double CHI2_N_test (double ** const data,const size_t *const length, const size_t smpls){

  /* all columns have equal length */
  const size_t ctgs=length[0];

  size_t i,j;
  double C[ctgs]; /*  */
  double N[smpls];
  double Ntot;

  double res=0.0;

  for(i=0;i<ctgs;i++) C[i]=0;
  
  Ntot=0;
  for(i=0;i<smpls;i++){
    N[i]=0;
    for(j=0;j<ctgs;j++){
      N[i]+=data[i][j];
      C[j]+=data[i][j];
    }
    Ntot+=N[i];
  }

/*   for(i=0;i<ctgs;i++) */
/*     printf("C[%zd]=%g\n",i,C[i]); */

/*   for(i=0;i<smpls;i++) */
/*     printf("N[%zd]=%g\n",i,N[i]); */

  for(i=0;i<smpls;i++)
    for(j=0;j<ctgs;j++)
      res+=data[i][j]*data[i][j]*Ntot/(N[i]*C[j]);
  res-=Ntot;

  return res;
  
}

#if defined HAVE_LIBGSL
double CHI2_N_pscore(const double statistic,
		 const size_t * const length, const size_t  smpls){

  /* +++++++++++++++++++++++++++++++++++++ */
  if(O_Verbose>1){
    fprintf(stderr,"#  Observed T                          %g\n",statistic);
    fprintf(stderr,"#  Degrees of freedom                  %zd\n",(smpls-1)*(length[0]-1));
    fprintf(stderr,"#  Asymptotic inference:\n");
    fprintf(stderr,"#   One-sided P-value: Pr{stat>T}      %g\n",
	     gsl_cdf_chisq_Q (statistic,(smpls-1)*(length[0]-1)) );

  }
  /* +++++++++++++++++++++++++++++++++++++ */

  return gsl_cdf_chisq_Q (statistic,(smpls-1)*(length[0]-1));

}
#else
double CHI2_N_pscore(const double statistic,
		 const size_t * const length, const size_t  K){

  return NAN;

}
#endif

double KW_test (double ** const data,const size_t *const length, const size_t K){
  
  size_t i,j,h;

  size_t N=0;  /* total number of observations */
  Indexed *alldata;  /* contains pooled data */

  double *ranks = (double *) my_alloc(sizeof(double)*K); /* average rank of the different samples */

  double res=0;
  
  /* allocate and set the pooled data */
  for(i=0;i<K;i++) N += length[i];
  alldata = (Indexed *) my_alloc(sizeof(Indexed)*N);
  h=0;
  for(i=0;i<K;i++)
    for(j=0;j<length[i];j++){
      alldata[h].sample = i;
      alldata[h].data = data[i][j];
      h++;
    }

  /* sort pooled data */
  qsort(alldata,N,sizeof(Indexed),sort_indexed_by_value);

/*   for(i=0;i<N;i++){ */
/*     fprintf(stderr,"%zd: %g\t%zd\n", */
/* 	    i+1, alldata[i].data, alldata[i].sample+1); */
/*   } */


  /* free possibly allocated structures */
  tgroups=0;
  free(tgroup);

  /* define sum of ranks for each group */
  for(i=0;i<K;i++) ranks[i]=0;
  i=0;
  while(i<N){
    const double val = alldata[i].data;
    size_t equals=0;
    while(alldata[i+equals+1].data == val)
      equals++;

    if(equals>0){
      tgroups++;
      tgroup = (size_t *) my_realloc((void *) tgroup,tgroups*sizeof(size_t));
      tgroup[tgroups-1] = equals+1;
      for(j=i;j<i+equals+1;j++){
	ranks[alldata[j].sample] += i+1.+equals/2.0;
      }
      i+=equals+1;
    }
    else{
      ranks[alldata[i].sample] += i+1;
      i++;
    }
  }

/*   for(i=0;i<K;i++) */
/*     fprintf(stderr,"R[%zd]=%g\n",i,ranks[i]); */

  /* statistics */
  for(i=0;i<K;i++)
    res+=ranks[i]*ranks[i]/length[i];
  res = res*12.0/(N*(N+1))-3.0*(N+1);

/*   fprintf(stderr,"sum=%g\n",res); */

  /* correct for ties */
  if(tgroups>0){
    double dtmp1=0.0;
    for(i=0;i<tgroups;i++)
      dtmp1 += tgroup[i]*(tgroup[i]*tgroup[i]-1.0);
    dtmp1 /= N*(N*N-1.0);
    dtmp1 = 1.0-dtmp1;
/*     fprintf(stderr,"ties corr=%g\n",dtmp1); */
    res= res/dtmp1;
  }

  /* de-allocate */
  free(alldata);
  free(ranks);

  return res ;
}

#if defined HAVE_LIBGSL
double KW_pscore(const double statistic,
		 const size_t * const length, const size_t  K){

  /* +++++++++++++++++++++++++++++++++++++ */
  if(O_Verbose>1){
    fprintf(stderr,"#  Kruskal-Wallis multi-sample statistics\n");
    fprintf(stderr,"#  Size of the sample                  %zd\n",*length);
    if(tgroups>0)
      fprintf(stderr,"#  Number of ties group(s)             %zd\n",tgroups);
    fprintf(stderr,"#  Observed Standardized H             %g\n",statistic);
    fprintf(stderr,"#  Degrees of freedom                  %zd\n",K-1);
    fprintf(stderr,"#  Asymptotic inference:\n");
    fprintf(stderr,"#   P-value: Pr{stat>W}                %g\n",
	    gsl_cdf_chisq_Q (statistic,K-1));
  }
  /* +++++++++++++++++++++++++++++++++++++ */


  return gsl_cdf_chisq_Q (statistic,K-1);

}
#else
double KW_pscore(const double statistic,
		 const size_t * const length, const size_t  K){

  return NAN;

}
#endif


double LEVENE_test (double ** const data,const size_t *const length, const size_t K){


  size_t i,j;
  
  double numerator,denominator;

  double *means  = my_alloc(sizeof(double)*K);
  double mean;
  size_t totlen;

  /* compute the statistics */
  mean=0.0;
  totlen=0;
  for(j=0;j<K;j++){

    means[j]=0.0;
    for(i=0;i<length[j];i++){
      means[j] += data[j][i];
      mean    += data[j][i];
    }
    means[j] /= length[j];
    totlen  += length[j];

  }
  mean /= totlen;

  numerator=0.0;
  denominator=0.0;
  for(j=0;j<K;j++){
    
    numerator += length[j]*(means[j]-mean)*(means[j]-mean);
    for(i=0;i<length[j];i++)
      denominator +=  (data[j][i]-means[j])*(data[j][i]-means[j]);

  }


  /* +++++++++++++++++++++++++++++++++++++ */
  if(O_Verbose>1){
    fprintf(stderr,"#  Number of observations              %zd\n",totlen);
    fprintf(stderr,"#  Number of groups                    %zd\n",K);
  }
  /* +++++++++++++++++++++++++++++++++++++ */

  /* de-allocate */
  free(means);

  return ((totlen-K)*numerator)/((K-1)*denominator);


}

double LEVENE_pscore(const double statistic,
		     const size_t * const length, const size_t  K){

  size_t i,totlen;

  totlen=0;

  for(i=0;i<K;i++) totlen+=length[i];

  /* +++++++++++++++++++++++++++++++++++++ */
  if(O_Verbose>1){
    fprintf(stderr,"#  Observed (expected) W               %g (%g)\n",
	    statistic,(totlen-K)/(totlen-K-2.));
    fprintf(stderr,"#  Asymptotic inference:\n");
    fprintf(stderr,"#   P-value: Pr{stat>W}                %g\n",
	    gsl_cdf_fdist_Q (statistic,K-1,totlen-K));
  }
  /* +++++++++++++++++++++++++++++++++++++ */


  return gsl_cdf_fdist_Q (statistic,K-1,totlen-K);
  

}


/* Generic functions to run tests */
/* ------------------------------ */

void
run_onesample( char *testname, 
	       double (*test) (double *, const size_t),
	       double (*pscore) (const double, const size_t),
	       double ** const data,const size_t * const length, const size_t columns)
{

  double *res = (double *) my_alloc(2*columns*sizeof(double));
  size_t i;

  size_t column;
  double statistic,significance;

  /* ++++++++++++++++++++++++++++++ */
  if (O_Verbose>0){
    fprintf(stderr,"# %s\n",testname);
  }
  /* ++++++++++++++++++++++++++++++ */


  /* for all columns */
  for(i=0;i<columns;i++){
    size_t len=length[i];

    /* allocate local version of data */
    double *data1= (double *) my_alloc(sizeof(double)*len);

    /* copy data */
    memcpy(data1,data[i],sizeof(double)*len);
      
    /* denan data */	
    denan(&data1,&len);
    
    /* ++++++++++++++++++++++++++++++++++++++++++++ */
    if (O_Verbose>1 && columns > 1)
      fprintf(stderr,"# columns %zd: %zd\n",i,len);
    /* ++++++++++++++++++++++++++++++++++++++++++++ */
    
    /* compute statistics */
    res[i] = test(data1,len);
    
    /* compute p-score */
    if(O_Significance == 1){
      if(pscore == NULL)
	res[columns+i] = NAN;
      else
	res[columns+i] = pscore(res[i],len);
    }
      
    free(data1);

  }

  /* print results */
  if(O_Significance == 0){
    if(O_Verbose>0) printf("#statistic\n");

    for(i=0;i<columns-1;i++)
      printf(FLOAT_SEP,res[i]);
    printf(FLOAT_NL,res[columns-1]);
  }
  else if (O_Significance == 1){
    if(O_Verbose>0) printf("#statistic\tp-score\n");
    for(i=0;i<columns-1;i++){
      printf(FLOAT_SEP,res[i]);
      printf(FLOAT_SEP,res[columns+1]);
    }
    printf(FLOAT_SEP,res[columns-1]);
    printf(FLOAT_NL,res[2*columns-1]);
  }
  
  free(res);
  
}


void
run_pairs( char *testname, 
	   double (*test) (const double const *, const double const *, const size_t),
	   double (*pscore) (const double, const size_t),
	   double ** const data,const size_t * const length, const size_t columns)
{

  double *res = (double *) my_alloc((columns*columns)*sizeof(double));
  size_t i1,i2;

  /* ++++++++++++++++++++++++++++++ */
  if (O_Verbose>0){
    fprintf(stderr,"# %s\n",testname);
  }
  /* ++++++++++++++++++++++++++++++ */
  
  /* for the couples of columns */
  for(i1=0;i1<columns;i1++){
    res[i1*columns+i1] = 0;
    for(i2=i1+1;i2<columns;i2++){
      size_t len=length[i1];

      /* allocate local version of data */
      double *data1= (double *) my_alloc(sizeof(double)*len);
      double *data2= (double *) my_alloc(sizeof(double)*len);

      /* copy data */
      memcpy(data1,data[i1],sizeof(double)*len);
      memcpy(data2,data[i2],sizeof(double)*len);
      
      /* pairs denan, it ensures that the samples have same size */	
      denan_pairs(&data1,&data2,&len);
      
      /* ++++++++++++++++++++++++++++++++++++++++++++ */
      if (O_Verbose>1 && columns >2){
	fprintf(stderr,"#  === Columns %zd %zd\n",i1+1,i2+1);
      }
      /* ++++++++++++++++++++++++++++++++++++++++++++ */
      
      /* compute statistics */
      res[i2*columns+i1] = test(data1,data2,len);

      /* compute p-score */
      if(O_Significance == 1){
	if(pscore == NULL)
	  res[i1*columns+i2] = NAN;
	else
	  res[i1*columns+i2] = pscore(res[i2*columns+i1],len);
      }
      
      free(data1);
      free(data2);
    }
  }
    
  /* print results */
  if(O_Significance == 0 && columns==2){
    if(O_Verbose>0) printf("#statistic\n");
    printf(FLOAT_NL,res[2]);
  }
  else if(O_Significance == 0 && columns>2){

    for(i1=1;i1<columns;i1++){
      for(i2=0;i2<i1-1;i2++){
	printf(FLOAT_SEP,res[i1*columns+i2]);
      }
      i2=i1-1;
      printf(FLOAT_NL,res[i1*columns+i2]);
    }

  }
  else if (O_Significance == 1 && columns==2){
    if(O_Verbose>0) printf("#statistic\tp-score\n");
    printf(FLOAT_SEP,res[2]);
    printf(FLOAT_NL,res[1]);
  }
  else if (O_Significance == 1 && columns>2){
    for(i1=0;i1<columns;i1++){
      for(i2=0;i2<columns-1;i2++){
	if(i2 != i1) printf(FLOAT_SEP,res[i1*columns+i2]);
	else printf(EMPTY_SEP,"----");
      }
      i2=columns-1;
      if(i2 != i1) printf(FLOAT_NL,res[i1*columns+i2]);
      else printf(EMPTY_NL,"----");
    }
  }
  
  free(res);
}


void
run_twosamples( char *testname, 
	       double (*test) (double *, const size_t,
			       double *, const size_t),
	       double (*pscore) (const double, const size_t, const size_t),
	       double ** const data,const size_t * const length, const size_t columns)
{

  double *res = (double *) my_alloc((columns*columns)*sizeof(double));
  size_t i1,i2;

  /* ++++++++++++++++++++++++++++++ */
  if (O_Verbose>0){
    fprintf(stderr,"# %s\n",testname);
  }
  /* ++++++++++++++++++++++++++++++ */
  
  /* for the couples of columns */
  for(i1=0;i1<columns;i1++){
    res[i1*columns+i1] = 0;
    for(i2=i1+1;i2<columns;i2++){
      size_t len1=length[i1];
      size_t len2=length[i2];

      /* allocate local version of data */
      double *data1= (double *) my_alloc(sizeof(double)*len1);
      double *data2= (double *) my_alloc(sizeof(double)*len2);

      /* copy data */
      memcpy(data1,data[i1],sizeof(double)*len1);
      memcpy(data2,data[i2],sizeof(double)*len2);
      
      /* pairs denan */
      denan(&(data1),&(len1));
      denan(&(data2),&(len2));
      
      /* ++++++++++++++++++++++++++++++++++++++++++++ */
      if (O_Verbose>1 && columns >2)
	fprintf(stderr,"#  === Columns %zd %zd\n",i1+1,i2+1);
      /* ++++++++++++++++++++++++++++++++++++++++++++ */
      
      /* compute statistics */
      res[i2*columns+i1] = test(data1,len1,data2,len2);

      /* compute p-score */
      if(O_Significance == 1){
	if(pscore == NULL)
	  res[i1*columns+i2] = NAN;
	else
	  res[i1*columns+i2] = pscore(res[i2*columns+i1],len1,len2);
      }
      
      free(data1);
      free(data2);
    }
  }
    
  /* print results */
  if(O_Significance == 0 && columns==2){
    if(O_Verbose>0) printf("#statistic\n");
    printf(FLOAT_NL,res[2]);
  }
  else if(O_Significance == 0 && columns>2){

    for(i1=1;i1<columns;i1++){
      for(i2=0;i2<i1-1;i2++){
	printf(FLOAT_SEP,res[i1*columns+i2]);
      }
      i2=i1-1;
      printf(FLOAT_NL,res[i1*columns+i2]);
    }

  }
  else if (O_Significance == 1 && columns==2){
    if(O_Verbose>0) printf("#statistic\tp-score\n");
    printf(FLOAT_SEP,res[2]);
    printf(FLOAT_NL,res[1]);
  }
  else if (O_Significance == 1 && columns>2){
    for(i1=0;i1<columns;i1++){
      for(i2=0;i2<columns-1;i2++){
	if(i2 != i1) printf(FLOAT_SEP,res[i1*columns+i2]);
	else printf(EMPTY_SEP,"----");
      }
      i2=columns-1;
      if(i2 != i1) printf(FLOAT_NL,res[i1*columns+i2]);
      else printf(EMPTY_NL,"----");
    }
  }
  
  free(res);
}


void
run_manysamples( char *testname, 
		 double (*test) (double ** const,const size_t * const, const size_t),
		 double (*pscore) (const double, const size_t * const, const size_t),
		 double ** const data,const size_t * const length, const size_t columns)
{

  double statistic,significance;

  /* ++++++++++++++++++++++++++++++ */
  if(O_Verbose==1){
    printf("#%s\n",testname);
    printf("#statistic");
    if(O_Significance == 1)
      printf("\tp-score");
    printf("\n");
  }
  else if (O_Verbose>1){
    fprintf(stderr,"# %s\n",testname);
  }
  /* ++++++++++++++++++++++++++++++ */

  statistic = test(data,length,columns);
  if(O_Significance == 1){
    printf(FLOAT_SEP,statistic);
    if(pscore == NULL)
      significance = NAN;
    else
      significance = pscore(statistic,length,columns);
    printf(FLOAT_NL,significance);
  }
  else
    printf(FLOAT_NL,statistic);
  
}



int main(int argc,char* argv[]){

  size_t rows=0,columns=0,column;
  size_t *length;
  double **data=NULL;

  /* binary options */
  int o_binary_in=0;


  size_t stat,i;
  char *splitstring = strdup(" \t");

  /* variables for reading command line options */
  /* ------------------------------------------ */
  int opt;
  /* ------------------------------------------ */


  /* COMMAND LINE PROCESSING */
  
  while((opt=getopt_long(argc,argv,"hF:v:spb:o:",gb_long_options, &gb_option_index))!=EOF){
    if(opt==0){
      gbutils_header(argv[0],stdout);
      exit(0);
    }
    else if(opt=='?'){
      fprintf(stderr,"option %c not recognized\n",optopt);
      exit(-1);
    }
    else if(opt=='h'){
      /*print help*/
      fprintf(stdout,"Compute statistical tests on data. Each test is specified by a unique\n");
      fprintf(stdout,"name and optional parameters follows, separated by commas. Depending\n");
      fprintf(stdout,"on the test, one, two or three columns of data are expected. '1 dist'\n");
      fprintf(stdout,"tests expect data in [0,1] from a distribution function. '1 sample'\n");
      fprintf(stdout,"tests expect a single columns. Test on 'pairs' expect two columns of\n");
      fprintf(stdout,"equal length. Test on '2 samples' expect two columns of data, possibly\n");
      fprintf(stdout,"of different length. Test on 2+ and 3+ samples expect a varying number of\n");
      fprintf(stdout,"columns. If more columns are provided, the test is repeated for any\n");
      fprintf(stdout,"column in the case of 1 sample test, and any couple of columns in the case\n");
      fprintf(stdout,"of 2 sample tests. In the last case, the output is expressed in matrix\n");
      fprintf(stdout,"format, with estimated statistics on the lower triangle and p-scores\n");
      fprintf(stdout,"(if requested) on the upper. The removal of 'nan' entries is automatic,\n");
      fprintf(stdout,"and is performed consistently with the nature of the test.\n");
      fprintf(stdout,"\nUsage: %s [options] <tests list> \n\n",argv[0]);
      fprintf(stdout,"Tests typically assume 1, 2 or 3+ samples. Available tests are:\n\n");
      fprintf(stdout," D+,D-,D,V 1 dist   Kolmogorov-Smirnov tests on cumulated data \n");
      fprintf(stdout," W2,A2,U2  1 dist   Cramer-von Mises tests on cumulated data   \n");
      fprintf(stdout," CHI2-1    1 samp   Chi-Sqrd, 1 samp. 2nd column: th. prob.    \n");
      fprintf(stdout," WILCO     1 samp   Wilcoxon signed-rank test (mode=0)         \n");
      fprintf(stdout," TS        1 samp   Student's T (mean=0)                       \n");
      fprintf(stdout," TR-TP     1 samp   Test of randomness: turning points         \n");
      fprintf(stdout," TR-DS     1 samp   Test of randomness: difference sign        \n");
      fprintf(stdout," TR-RT     1 samp   Test of randomness: rank test              \n");
      fprintf(stdout," R         pairs    Pearson's correlation coefficient          \n");
      fprintf(stdout," RHO       pairs    Spearman's Rho rank correlation            \n");
      fprintf(stdout," TAU       pairs    Kendall's Tau correlation                  \n");
      fprintf(stdout," CHI2-2    pairs    Chi-Sqrd, 2 samples                        \n");
      fprintf(stdout," TP        pairs    Student's T with paired samples            \n");
      fprintf(stdout," WILCO2    pairs    Wilcoxon test on paired samples            \n");
      fprintf(stdout," KS        2 samp   Kolmogorov-Smirnov test                    \n");
      fprintf(stdout," T         2 samp   Student's T with same variances            \n");
      fprintf(stdout," TH        2 samp   Student's T with different variances       \n");
      fprintf(stdout," F         2 samp   F-Test for different variances             \n");
      fprintf(stdout," WMW       2 samp   Wilcoxon-Mann-Whitney U                    \n");
      fprintf(stdout," FP        2 samp   Fligner-Policello standardized U^          \n");
      fprintf(stdout," LEV-MEAN  2+ samp  Levene equality of variances using means   \n");
      fprintf(stdout," LEV-MED   2+ samp  Levene equality of variances using medians \n");
      fprintf(stdout," KW        3+ samp  Kruscal-Wallis test on 3+ samples          \n");
      fprintf(stdout," CHI2-N    3+ samp  Multi-columns contingency table analysis   \n");
      fprintf(stdout,"Options:\n");
      fprintf(stdout," -p  compute associated significance\n");
      fprintf(stdout," -s  input data are already sorted in ascending order\n");
      fprintf(stdout," -F  specify the input fields separators (default \" \\t\")    \n");
      fprintf(stdout," -o  set the output format (default '%%12.6e')  \n");
      fprintf(stdout," -v  verbosity: 0 none, 1 headings, 2+ description (default 0) \n");
      fprintf(stdout," -h  this help\n");
      fprintf(stdout,"Examples:\n");
      fprintf(stdout," gbtest FP < file  compute the Fligner-Policello test on the columns\n");
      fprintf(stdout,"                   of 'file', considering all possible pairings.\n");
      fprintf(stdout," gbtest KW -p < file  compute the Kruscal-Wallis test and its p-score\n");
      fprintf(stdout,"                      on the first three columns of data in 'file' \n");
      finalize_program(); exit(0);
    }
    else if(opt=='F'){
      /*set the fields separator string*/
      free(splitstring);
      splitstring = strdup(optarg);
    }
    else if(opt=='v'){
      O_Verbose=(unsigned) atoi(optarg);
    }
    else if(opt=='p'){
      O_Significance=1;
    }
    else if(opt=='s'){
      Is_Data_Sorted=1;
    }
    else if(opt=='b'){
      /*set binary input or output*/      
      switch(atoi(optarg)){
      case 0 :
	break;
      case 1 :
	o_binary_in=1;
	break;
      case 2 :
	fprintf(stderr,"no binary output implemented, option ignored.\n");
	break;
      case 3 :
	o_binary_in=1;
	fprintf(stderr,"no binary output implemented, option ignored.\n");
	break;
      default:
	fprintf(stderr,"unclear binary specification %d, option ignored.\n",atoi(optarg));
	break;
      }
    }
    else if(opt=='o'){
      /*set the ouptustring */
      FLOAT = strdup(optarg);
    }
  }
  /* END OF COMMAND LINE PROCESSING */

  /* initialize global variables */
  initialize_program(argv[0]);

  /* load data as a matrix: all column have the first rows. Missing
     data are replaced with NAN*/
  if(o_binary_in==1)
    loadtable_bin(&data,&rows,&columns,0);
  else{
    loadtable(&data,&rows,&columns,0,splitstring);
    free(splitstring);
  }

  length =(size_t *) my_alloc(sizeof(size_t)*columns);
  for(i=0;i<columns;i++)
    length[i] = rows;

  /*+++++++++++++++++++++++++++++++++++++++*/
  if(O_Verbose>1)
    fprintf(stderr,"# loaded %zdx%zd data table\n",rows,columns);
  /*+++++++++++++++++++++++++++++++++++++++*/
  

  /*+++++++++++++++++++++++++++++++++++++++*/
  /* if(O_Verbose>1){ */
  /*   for(column=0;column<columns;column++) */
  /*     if(length[column] != rows) */
  /* 	fprintf(stderr,"# %zd NAN removed from column %zd\n",rows-length[column],column+1); */
  /* } */
  /*+++++++++++++++++++++++++++++++++++++++*/


  /* parse line for test specification */
  for(stat=optind;stat<(size_t) argc;stat++){
    char *test_name=strdup (argv[stat]);

    if (NAME("D+"))
      {

	run_onesample("D+ Kolmogorov-Smirnov test",
		      Dplus_test,Dplus_pscore,data,length,columns);
      }
    else if(NAME("D-"))
      {

	run_onesample("D- Kolmogorov-Smirnov test",
		      Dminus_test,Dminus_pscore,data,length,columns);
      }
    else if(NAME("D"))
      {

	run_onesample("D Kolmogorov-Smirnov test",
		      D_test,D_pscore,data,length,columns);
      }
    else if(NAME("V"))
      {

	run_onesample("V Kuiper Test",V_test,V_pscore,data,length,columns);
      }
    else if(NAME("W2"))
      {

	run_onesample("W2 Cramer-Von Mises Test",W2_test,NULL,data,length,columns);
      }
    else if(NAME("A2"))
      {

	run_onesample("A2 Anderson-Darling Test",A2_test,NULL,data,length,columns);
      }
    else if(NAME("U2"))
      {

	run_onesample("U2 Watson Test",U2_test,NULL,data,length,columns);
      }
    else if(NAME("WILCO"))
      {

	run_onesample("Wilcoxon 1-sample Rank Sum statistics",
		      WILCO_test,WILCO_pscore,data,length,columns);
      }
    else if(NAME("TS"))
      {

	run_onesample("Student's T test; 1 sample",
		      Tsingle_test,Tsingle_pscore,data,length,columns);
      }
    else if(NAME("TR-TP"))
      {

	run_onesample("Turning Points Test of Randomness (1 sample)",
		      TRTP_test,TRTP_pscore,data,length,columns);
      }
    else if(NAME("TR-DS"))
      {

	run_onesample("Difference Sign Test of Randomness (1 sample)",
		      TRDS_test,TRDS_pscore,data,length,columns);
      }
    else if(NAME("TR-RT"))
      {

	run_onesample("Rank Test of Randomness (1 sample)",
		      TRRT_test,TRRT_pscore,data,length,columns);
      }
    else if(NAME("CHI2-1"))
      {

	double statistic,significance;
	
	/* check table's columns have homogeneous length */
	if(columns<2 || columns >2){
	  fprintf(stderr,"ERROR (%s): need exactly two columns",GB_PROGNAME);
	  exit(-1);
	}
	else if(length[0] != length[1]){
	  fprintf(stderr,"ERROR (%s): columns must contain the same number of rows",GB_PROGNAME);
	  exit(-1);
	}

	/* pairs denan */	
	denan_pairs(&(data[0]),&(data[1]),&(length[0]));
	length[1]=length[0];

	/* ++++++++++++++++++++++++++++++ */
	if(O_Verbose==1){
	  printf("#CHI^2 one-sample statistic");
	  if(O_Significance == 1)
	    printf("\tp-score");
	  printf("\n");
	}
	/* ++++++++++++++++++++++++++++++ */
	
	statistic=chi2_onesample(data,length[0]);
	if(O_Significance == 1){
	  printf(FLOAT_SEP,statistic);
#if defined HAVE_LIBGSL
	  significance = gsl_cdf_chisq_Q (statistic,length[0]-1);
#else
	  significance = NAN;
#endif
	  printf(FLOAT_NL,significance);
	}
	else
	  printf(FLOAT_NL,statistic);

      }
    else if(NAME("R")){
      
      run_pairs("Pearson's R 2-sample correlation",
		R_test,R_pscore,data,length,columns);
    }
    else if(NAME("RHO")){
      
      run_pairs("Spearman's Rho 2-sample rank correlation (with ties)",
		     RHO_test,RHO_pscore,data,length,columns);
    }
    else if(NAME("TAU")){

      run_pairs("Kendall's Tau 2-sample rank correlation (with ties)",
		     TAU_test,TAU_pscore,data,length,columns);
    }
    else if(NAME("CHI2-2")){

      run_pairs("Chi^2 Test; 2-samples",CHI2_2_test,CHI2_2_pscore,data,length,columns);
    }
    else if(NAME("TP")){

      run_pairs("Student's T test; paired observations",
		Tpaired_test,Tpaired_pscore,data,length,columns);
    }
    else if(NAME("WILCO2")){

      run_pairs("Wilcoxon signed-rank test for paired observations",
		WILCO2_test,WILCO_pscore,data,length,columns);
    }
    else if(NAME("KS"))
      {

	run_twosamples("Kolmogorov-Smirnov Test; 2-samples",
		       KS_test,KS_pscore,data,length,columns);
      }
    else if(NAME("WMW")){
      
      run_twosamples("Wilcoxon-Mann-Whitney 2-sample rank sum statistics",
		     WMW_test,WMW_pscore,data,length,columns);
    }
    else if(NAME("F")){
      
      run_twosamples("F-Test for difference in variance",F_test,F_pscore,data,length,columns);
    }
    else if(NAME("T")){

      run_twosamples("Student's T test; 2 samples with same variance",
		     Thomo_test,Thomo_pscore,data,length,columns);
    }
    else if(NAME("TH")){
      
      run_twosamples("Student's T test; 2 samples with different variance",
		     Thetero_test,Thetero_pscore,data,length,columns);
    }
    else if(NAME("FP")){

      run_twosamples("Fligner-Policello 2-sample rank sum statistics",
		     FP_test,FP_pscore,data,length,columns);
    }
    else if(NAME("KW")){
      size_t i,j;

      if(columns<3){
	fprintf(stderr,"ERROR (%s): Kruskal-Wallis test applies only to 3 or more samples.\n",
		GB_PROGNAME);
	exit(-1);
      }

      /* denan */
      for(i=0;i<columns;i++) denan(&(data[i]),&(length[i]));

      run_manysamples("Kruskal-Wallis multi-sample statistics",
		      KW_test,KW_pscore,data,length,columns);
    }
    else if(NAME("LEV-MEAN")){
      size_t i,j;

      /* denan */
      for(i=0;i<columns;i++) denan(&(data[i]),&(length[i]));

      /* transform data */
      for(j=0;j<columns;j++){
	double mean=0.0;
	for(i=0;i<length[j];i++)  mean += data[j][i];
	mean /= length[j];
	for(i=0;i<length[j];i++)  data[j][i] = fabs(data[j][i]-mean);
      }

      run_manysamples("LAVENE multi-sample test, using means",
		      LEVENE_test,LEVENE_pscore,data,length,columns);
    }
    else if(NAME("LEV-MED")){
      size_t i,j;

      /* denan */
      for(i=0;i<columns;i++) denan(&(data[i]),&(length[i]));

      /* transform data */
      for(j=0;j<columns;j++){
	double median=0.0;

	/* sort data */
	qsort(data[j],length[j],sizeof(double),sort_by_value);

	/* median */
	median=(length[j]%2 == 0 ?
		(data[j][length[j]/2-1]+data[j][length[j]/2])/2. :
		data[j][(length[j]-1)/2]);

	for(i=0;i<length[j];i++)  data[j][i] = fabs(data[j][i]-median);
      }

      run_manysamples("LAVENE multi-sample test, using medians",
		      LEVENE_test,LEVENE_pscore,data,length,columns);
    }
    else if(NAME("CHI2-N")){
      size_t i;
      size_t len = length[0];

      /* check table's columns have homogeneous length */
      for(i=1;i<columns;i++)
	if(length[i] != len){
	  fprintf(stderr,"ERROR (%s): columns must contain the same number of rows",GB_PROGNAME);
	  exit(-1);
	}

      /* denan */
      denan_data(&data,&len,&columns);
      for(i=1;i<columns;i++) length[i] = len;

      run_manysamples("Chi^2 Test; N-samples",CHI2_N_test,CHI2_N_pscore,data,length,columns);
    }
    else
      {
	fprintf(stderr,"ERROR (%s): test %s not recognized. Try option -h\n",
		GB_PROGNAME,test_name);
	exit(-1);
	
      }

    free(test_name);
  }


  /* de-allocate */
  for(i=0;i<columns;i++)
    free(data[i]);
  free(data);
  free(length);
  free(tgroup);

  finalize_program(); exit(0);
}

