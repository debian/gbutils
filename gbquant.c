/*
  gbquant  (ver. 5.6.1) -- Print quantiles of data distribution
  Copyright (C) 2004-2018 Giulio Bottazzi

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  (version 2) as published by the Free Software Foundation;
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#include "tools.h"

int main(int argc,char* argv[]){

  double *xvals=NULL,*qvals=0;;
  size_t xvalnum=0,qvalnum=0;

  size_t n=10;

  double wqmin=0,wqmax=1.;

  char *splitstring = strdup(" \t");

  /* options */
  int o_xval  = 0;
  int o_qval = 0;
  int o_err = 0;
  int o_qwindow = 0;
  int o_table = 0;
  int o_verbose=0;
  int o_position=1;
  int o_replace=0;

  /* variables for reading command line options */
  /* ------------------------------------------ */
  int opt;
  /* ------------------------------------------ */

  /* COMMAND LINE PROCESSING */    
  while((opt=getopt_long(argc,argv,"W:w:vhtx:q:en:F:Q",gb_long_options, &gb_option_index))!=EOF){
    if(opt==0){
      gbutils_header(argv[0],stdout);
      exit(0);
    }
    else if(opt=='?'){
      fprintf(stderr,"option %c not recognized\n",optopt);
      exit(-1);
    }
    else if(opt=='h'){
      /*print help*/
      fprintf(stdout,"Print distribution quantiles. Data are read from standard input. If no\n");
      fprintf(stdout,"-x or -q options are provided, a quantiles table is plotted. The number\n");
      fprintf(stdout,"of quantiles can be chosen with -n. The option -x #1 prints the quantile\n");
      fprintf(stdout,"associated with the value #1, while -q #2 print the value associate with\n");
      fprintf(stdout,"the quantile #2. Of course #2 must be between 0 and 1. Multiple values\n");
      fprintf(stdout,"can be provided to -x or -q, separated with commas. With -w #1,#2 all\n");
      fprintf(stdout,"the observations inside the quantile range [#1,#2) are printed. If more\n");
      fprintf(stdout,"columns are provided with option -t, the specified action is repeated on\n");
      fprintf(stdout,"each column. Without -t, columns are pooled unless -w is specified, in\n");
      fprintf(stdout,"which case all rows whose first element is inside the range are printed.\n");
      fprintf(stdout,"A different column for sorting can be specified with option -W. If 0 is\n");
      fprintf(stdout,"specified, the cut is applied with respect to all columns. With option -Q\n");
      fprintf(stdout,"each entry is replaced with the quantile range it belongs to, with respect\n");
      fprintf(stdout,"to the distribution obtained from the entries in its column.\n");
      fprintf(stdout,"\nUsage: %s [options]\n\n",argv[0]);
      fprintf(stdout,"Options:\n");
      fprintf(stdout," -n  number of quantiles to print (default 10)\n");
      fprintf(stdout," -x  print the quantile (interpolated) associated with the value\n");
      fprintf(stdout," -q  print the value (interpolated) associated with the quantile\n");
      fprintf(stdout," -e  print the (asymptotic) error for -x or -q; doesn't work with -t\n");
      fprintf(stdout," -Q  replace each element with the quantile range it belongs to\n");
      fprintf(stdout," -w  print observations inside a quantile windows\n");
      fprintf(stdout," -W  set the column to use (default 1)\n");
      fprintf(stdout," -t  consider separately each column of input\n");
      fprintf(stdout," -F  specify the input fields separators (default \" \\t\")\n");
      fprintf(stdout," -h  this help\n");
      fprintf(stdout," -v  verbose mode\n");
      fprintf(stdout,"Examples:\n");
      fprintf(stdout," gbquant -W 0 -w 0.1,0.9 < file  print the rows whose fields are all in\n");
      fprintf(stdout,"                                 the center deciles.\n");
      fprintf(stdout," gbquant -q 0.5 < file  print the median of the observations in 'file'.\n");
      exit(0);
    }
    else if(opt=='W'){
      o_position=atoi(optarg);
      if(o_position <0){
	fprintf(stderr,
		"ERROR (%s): Column number must be non-negative\n",
		GB_PROGNAME);
	exit(1);
      }
    }
    else if(opt=='w'){
      /*set the quantile window */

      char *stmp1=strdup (optarg);
      char *stmp2;
      char *stmp3=stmp1;

      stmp2=strsep (&stmp1,",");
      if(strlen(stmp2)>0) 
	wqmin=atof(stmp2);
      if(stmp1 != NULL && strlen(stmp1)>0)
	wqmax=atof(stmp1);
      free(stmp3);

      if(wqmax<wqmin){
	const double dtmp1 = wqmax;
	wqmax=wqmin;
	wqmin=dtmp1;
      }

      o_qwindow=1;
    }
    else if(opt=='F'){
      /*set the fields separator string*/
      free(splitstring);
      splitstring = strdup(optarg);
    }
    else if(opt=='t'){
      /*set the table form*/
      o_table=1;
    }
    else if(opt=='n'){
      /*set the number of quantiles*/
      n=atoi(optarg);
    }
    else if(opt=='x'){
      /*set the value whose quantile is required*/
      char *stmp1=strdup (optarg);
      char *stmp2;
      char *stmp3=stmp1;
      while( (stmp2=strsep (&stmp1,",")) != NULL && strlen(stmp2)>0 ){
	xvalnum++;
	xvals = (double *) my_realloc((void *) xvals,xvalnum*sizeof(double));
	xvals[xvalnum-1]=atof(stmp2);
      }
      o_xval=1;
      free(stmp3);
    }
    else if(opt=='q'){
      /*set the quantile whose value is required*/
      char *stmp1=strdup (optarg);
      char *stmp2;
      char *stmp3=stmp1;
      while( (stmp2=strsep (&stmp1,",")) != NULL && strlen(stmp2)>0 ){
	qvalnum++;
	qvals = (double *) my_realloc((void *) qvals,qvalnum*sizeof(double));
	qvals[qvalnum-1]=atof(stmp2);
      }
      o_qval=1;
      free(stmp3);
    }
    else if(opt=='Q'){
      /* replace entries*/
      o_replace=1;
    }
    else if(opt=='e'){
      /*set the verbose mode*/
      o_err=1;
    }
    else if(opt=='v'){
      /*set the verbose mode*/
      o_verbose=1;
    }
  }    
  /* END OF COMMAND LINE PROCESSING */

  /* initialize global variables */
  initialize_program(argv[0]);

  /* set the actual number of points to consider: one less the number
     of desired partition */
  n=n-1;

  if(o_xval==0 && o_qval==0 && o_qwindow==0 && o_replace==0){/* print the list of quantiles */

    if(!o_table){
      size_t size;
      double *vals=NULL;
      size_t quant;

      load(&vals,&size,0,splitstring);
      denan(&vals,&size);
      qsort(vals,size,sizeof(double),sort_by_value);

      /*+++++++++++++++++++++++++++++++++++++++*/
      if(o_verbose)
	fprintf(stderr,"loaded %zd data\n",size);
      /*+++++++++++++++++++++++++++++++++++++++*/
	
      if(n>=size) n = size-1;/* for consistency */

      /*+++++++++++++++++++++++++++++++++++++++*/
      if(o_verbose){ 
	printf(EMPTY_SEP,"#quant.");
	printf(EMPTY_NL,"val");
      }
      /*+++++++++++++++++++++++++++++++++++++++*/
      for(quant=1;quant<=n;quant++){
	double dtmp1 = quant/(n+1.);
	int index = floor((size+1.)*dtmp1);
	double delta = (size+1.)*dtmp1-index;
	printf(FLOAT_SEP,dtmp1);
	printf(FLOAT_NL,vals[index-1]*(1.-delta)+vals[index]*delta);
      }
    }
    else{
      size_t rows=0,columns=0,i,j;
      double **vals=NULL;
      double **quantiles;
      
      loadtable(&vals,&rows,&columns,0,splitstring);

      /*+++++++++++++++++++++++++++++++++++++++*/
      if(o_verbose)
	fprintf(stderr,"loaded %zdx%zd data table\n",rows,columns);
      /*+++++++++++++++++++++++++++++++++++++++*/

      if(n>=rows) n = rows-1;/* for consistency */

      /* allocate the array for the result */
      quantiles = (double **) my_alloc(n*sizeof(double*));
      for(i=0;i<n;i++)
	quantiles[i] = (double *) my_alloc(columns*sizeof(double));

      /* compute the result */
      for(i=0;i<columns;i++){
	size_t size=rows;
	denan(&(vals[i]),&size);
	qsort(vals[i],size,sizeof(double),sort_by_value);
	for(j=1;j<=n;j++){
	  double dtmp1 = j/(n+1.);
	  int index = floor((size+1.)*dtmp1);
	  double delta = (size+1.)*dtmp1-index;

	  if(dtmp1<1./(size+1.)) quantiles[j-1][i]=vals[i][0];
	  else if (dtmp1>1.-1./(size+1.)) quantiles[j-1][i]=vals[i][size-1];	  
	  else quantiles[j-1][i]=vals[i][index-1]*(1.-delta)+vals[i][index]*delta;
	}
      }

      /*+++++++++++++++++++++++++++++++++++++++*/
      if(o_verbose){ 
	printf(EMPTY_SEP,"#quant.");
	printf(EMPTY_NL,"vals->");
      }
      /*+++++++++++++++++++++++++++++++++++++++*/

      printmatrixbyrows(stdout,quantiles,n,columns);
    }
  }
  else if(o_replace==1){/* replace values with their quantiles */

    if(!o_table){
      size_t size,tmpsize;
      double *vals=NULL,*quantiles,*adtmp;
      size_t i;

      load(&vals,&size,0,splitstring);

      /*+++++++++++++++++++++++++++++++++++++++*/
      if(o_verbose)
	fprintf(stderr,"loaded %zd data\n",size);
      /*+++++++++++++++++++++++++++++++++++++++*/

      /* duplicate the arrays */
      adtmp = (double *)  my_alloc(size*sizeof(double));
      memcpy(adtmp,vals,size*sizeof(double));
      tmpsize = size;
      denan(&adtmp,&tmpsize);
      qsort(adtmp,tmpsize,sizeof(double),sort_by_value);

      if(n>=tmpsize) n = tmpsize-1;/* for consistency */

      /* allocate the array for the quantiles */
      quantiles = (double *) my_alloc(n*sizeof(double*));

      /* compute the quntiles */
      for(i=1;i<=n;i++){
	double dtmp1 = i/(n+1.);
	int index = floor((tmpsize+1.)*dtmp1);
	double delta = (tmpsize+1.)*dtmp1-index;
	quantiles[i-1]= adtmp[index-1]*(1.-delta)+adtmp[index]*delta;
      }

      /* free temporary duplication */
      free(adtmp);

      /* compute and print the result */
      for(i=0;i<size;i++){
	double dtmp1=vals[i];
	if(finite(dtmp1)){
	  size_t h;
	  for(h=0;h<n;h++){
	    if(quantiles[h] > dtmp1) break;
	  }
	  printf(INT_NL, h+1);
	}
	else{
	  printf(FLOAT_NL,dtmp1);
	}
      }

    }
    else{
      size_t rows=0,columns=0,i,j;
      double **vals=NULL;
      double **quantiles;
    
      loadtable(&vals,&rows,&columns,0,splitstring);
    
      /*+++++++++++++++++++++++++++++++++++++++*/
      if(o_verbose)
	fprintf(stderr,"loaded %zdx%zd data table\n",rows,columns);
      /*+++++++++++++++++++++++++++++++++++++++*/

      if(n>=rows) n = rows-1;/* for consistency */
      
      /* allocate the array for the quantiles */
      quantiles = (double **) my_alloc(n*sizeof(double*));
      for(i=0;i<n;i++)
	quantiles[i] = (double *) my_alloc(columns*sizeof(double));
      
      /* compute the quantiles */
      for(i=0;i<columns;i++){
	size_t size=rows;
	
	/* duplicate the arrays */
	double *adtmp = (double *)  my_alloc(rows*sizeof(double*));
	memcpy(adtmp,vals[i],rows*sizeof(double*));      
	
	denan(&adtmp,&size);
	qsort(adtmp,size,sizeof(double),sort_by_value);
	for(j=1;j<=n;j++){
	  double dtmp1 = j/(n+1.);
	  int index = floor((size+1.)*dtmp1);
	  double delta = (size+1.)*dtmp1-index;
	  
	  if(dtmp1<1./(size+1.)) quantiles[j-1][i]=adtmp[0];
	  else if (dtmp1>1.-1./(size+1.)) quantiles[j-1][i]=adtmp[size-1];	  
	  else quantiles[j-1][i]=adtmp[index-1]*(1.-delta)+adtmp[index]*delta;
	}
	free(adtmp);
      }
      
      /* compute and print the result */
      for(i=0;i<rows;i++){
	for(j=0;j<columns;j++){
	  double dtmp1=vals[j][i];
	  if(finite(dtmp1)){
	    size_t h;
	    for(h=0;h<n;h++){
	      if(quantiles[h][j] > dtmp1) break;
	    }
	    printf(INT_SEP, h+1);
	  }
	  else{
	    printf(FLOAT_SEP,dtmp1);
	  }
	}
	printf("\n");
      }
    }

  }
  else if(o_qwindow==1){ /* print the data inside the quantile windows */
    if(!o_table){
      size_t rows=0,columns=0,i,j;
      double **vals=NULL;

      size_t indexmin,indexmax;

      /* load data: data[column][row] */
      loadtable(&vals,&rows,&columns,0,splitstring);
      
      if(o_position > columns){
	fprintf(stderr,
		"ERROR (%s): Column position larger than columns number.\n",
		GB_PROGNAME);
	exit(1);
      }

      /* remove NAN entries */
      denan_data(&vals,&rows,&columns);

      /* define the indexes boundaries */
      indexmin = floor(rows*wqmin);
      indexmax = ceil(rows*wqmax);
      
      if(columns==1){
	/*+++++++++++++++++++++++++++++++++++++++*/
	if(o_verbose)
	  fprintf(stderr,"loaded %zd data\n",rows);
	/*+++++++++++++++++++++++++++++++++++++++*/

	/* sort data */
	qsort(vals[0],rows,sizeof(double),sort_by_value);

	for(j=0;j<indexmin;j++)
	  vals[0][j]=NAN;

	for(j=indexmax;j<rows;j++)
	  vals[0][j]=NAN;

      }
      else if(columns>1){
	/*+++++++++++++++++++++++++++++++++++++++*/
	if(o_verbose)
	  fprintf(stderr,"loaded %zdx%zd data table\n",rows,columns);
	/*+++++++++++++++++++++++++++++++++++++++*/


	if(o_position>0){/* sort data accordong to given position*/
	  sortn(vals,columns,rows,o_position-1,2);

	  for(j=0;j<indexmin;j++)
	    vals[o_position-1][j]=NAN;

	  for(j=indexmax;j<rows;j++)
	    vals[o_position-1][j]=NAN;

	}
	else if(o_position==0){/* sort data according to each position */

	  for(i=0;i<columns;i++){
	    sortn(vals,columns,rows,i,2);
	    
	    for(j=0;j<indexmin;j++)
	      vals[i][j]=NAN;
	    
	    for(j=indexmax;j<rows;j++)
	      vals[i][j]=NAN;
	    
	  }
	}
      }
      else{
	fprintf(stderr,
		"ERROR (%s): Provide at least one column of data\n",
		GB_PROGNAME);
	exit(1);
      }

      /* remove NAN entries generated by cropping*/
      denan_data(&vals,&rows,&columns); 

      
      /*+++++++++++++++++++++++++++++++++++++++*/
      if(o_verbose) 
	printf("#data in quantile range [%f,%f]\n",wqmin,wqmax);
      /*+++++++++++++++++++++++++++++++++++++++*/

      for(i=0;i<rows;i++){
	for(j=0;j<columns-1;j++) printf(FLOAT_SEP,vals[j][i]);
	printf(FLOAT_NL,vals[columns-1][i]);
      }

    }
    else{

      size_t rows=0,columns=0,i,j;
      double **vals=NULL;
      double **result=NULL;
      size_t *resnum=NULL;
      size_t maxresnum=0;
      size_t indexmin,indexmax;

      loadtable(&vals,&rows,&columns,0,splitstring);

      /* allocate the array for the result */
      result = (double **) my_alloc(columns*sizeof(double *));
      resnum = (size_t *) my_alloc(columns*sizeof(size_t));
      

      /*+++++++++++++++++++++++++++++++++++++++*/
      if(o_verbose)
	fprintf(stderr,"loaded %zdx%zd data table\n",rows,columns);
      /*+++++++++++++++++++++++++++++++++++++++*/

      /* compute the result */
      for(i=0;i<columns;i++){
	size_t size=rows;
	denan(&(vals[i]),&size);
	qsort(vals[i],size,sizeof(double),sort_by_value);

	indexmin = floor(size*wqmin);
	indexmax = ceil(size*wqmax);

	result[i] = (double *) my_alloc((indexmax-indexmin)*sizeof(double));
	resnum[i] = indexmax-indexmin;
	if(resnum[i]> maxresnum) maxresnum = resnum[i];
	for(j=0;j<indexmax-indexmin;j++)
	  result[i][j] = vals[i][j+indexmin];
      }

      /*+++++++++++++++++++++++++++++++++++++++*/
      if(o_verbose) 
	printf("#data in quantile range [%f,%f]\n",wqmin,wqmax);
      /*+++++++++++++++++++++++++++++++++++++++*/
      for(i=0;i<maxresnum;i++){
	for(j=0;j<columns;j++){
	  if(i<resnum[j])
	    printf(FLOAT_SEP,result[j][i]);
	  else
	    printf(FLOAT_SEP,NAN);
	}
	printf("\n");
      }
    }
  }
  else if(o_xval==1){ /* print the quantile associated with the value */
    if(!o_table){
      size_t size,index;
      double *vals=NULL;
      
      load(&vals,&size,0,splitstring);

      /*+++++++++++++++++++++++++++++++++++++++*/
      if(o_verbose)
	fprintf(stderr,"loaded %zd data\n",size);
      /*+++++++++++++++++++++++++++++++++++++++*/

      denan(&vals,&size);
      qsort(vals,size,sizeof(double),sort_by_value);

      /*+++++++++++++++++++++++++++++++++++++++*/
      if(o_verbose){
	if(o_err==0)
	  printf(EMPTY_NL,"#cumulated");
	else{
	  printf(EMPTY_SEP,"#cumulated");
	  printf(EMPTY_NL,"std.err.");
	}
      }
      /*+++++++++++++++++++++++++++++++++++++++*/
      for(index=0;index<xvalnum;index++){
	const double xval = xvals[index];
	if(xval<vals[0]) printf(FLOAT_NL,0.0);
	else if (xval>vals[size-1]) printf(FLOAT_NL,1.0);
	else {
	  int i=0;
	  double dtmp1,dtmp2;
	  while(vals[i]<xval) i++;
	  dtmp1=xval-vals[i-1];
	  dtmp2=vals[i]-xval;
	  if(o_err==0)
	    printf(FLOAT_NL,(i*dtmp2+(i+1.)*dtmp1)/((size+1.)*(dtmp1+dtmp2)));
	  else{
	    printf(FLOAT_SEP,(i*dtmp2+(i+1.)*dtmp1)/((size+1.)*(dtmp1+dtmp2)));
	    printf(FLOAT_NL,
		   sqrt(((size+1-i)*i*dtmp2+(size-i)*(i+1.)*dtmp1)/((size+1.)*(size+1.)*(size+2.)*(dtmp1+dtmp2))));
	  }
	}
      }
    }
    else{
      size_t rows=0,columns=0,i,j;
      double **vals=NULL;
      double **results=NULL;

      loadtable(&vals,&rows,&columns,0,splitstring);

      /* allocate the array for the result */
      results = (double **) my_alloc(xvalnum*sizeof(double *));
      for(j=0;j<xvalnum;j++)
	results[j] = (double *) my_alloc(columns*sizeof(double));

      /*+++++++++++++++++++++++++++++++++++++++*/
      if(o_verbose)
	fprintf(stderr,"loaded %zdx%zd data table\n",rows,columns);
      /*+++++++++++++++++++++++++++++++++++++++*/

      /* compute the result */
      for(i=0;i<columns;i++){
	size_t size=rows;
	denan(&(vals[i]),&size);
	qsort(vals[i],size,sizeof(double),sort_by_value);

	for(j=0;j<xvalnum;j++){
	  const double xval = xvals[j];

/* 	  printf("%d %f [%f,%f] \n",i,xval,vals[i][0],vals[i][size-1]); */
	  
	  if(xval<vals[i][0]) results[j][i]=0.0;
	  else if (xval>vals[i][size-1]) results[j][i]=1.0;
	  else {
	    int index=0;
	    double dtmp1,dtmp2;
	    while(vals[i][index]<xval) index++;
	    dtmp1=xval-vals[i][index-1];
	    dtmp2=vals[i][index]-xval;
	    results[j][i]=(index*dtmp2+(index+1.)*dtmp1)/((size+1.)*(dtmp1+dtmp2));
	  }
	}
      }

      /*+++++++++++++++++++++++++++++++++++++++*/
      if(o_verbose) 
	printf(EMPTY_NL,"#cumulated->");
      /*+++++++++++++++++++++++++++++++++++++++*/
      printmatrixbyrows(stdout,results,xvalnum,columns);

    }
  }
  else if(o_qval==1) { /* print the values associated with the quantile */
    if(!o_table){
      size_t size,index;
      double *vals=NULL;
      
     load(&vals,&size,0,splitstring);

      /*+++++++++++++++++++++++++++++++++++++++*/
      if(o_verbose)
	fprintf(stderr,"loaded %zd data\n",size);
      /*+++++++++++++++++++++++++++++++++++++++*/

      denan(&vals,&size);
      qsort(vals,size,sizeof(double),sort_by_value);

      /*+++++++++++++++++++++++++++++++++++++++*/
      if(o_verbose){
	if(o_err==0)
	  printf(EMPTY_NL,"#quantiles");
	else{
	  printf(EMPTY_SEP,"#quantiles");
	  printf(EMPTY_NL,"std.err.");
	}
      }
      /*+++++++++++++++++++++++++++++++++++++++*/
      for(index=0;index<qvalnum;index++){
	const double qval = qvals[index];
	if(qval<1./(size+1.)) printf(FLOAT_NL,vals[0]);
	else if (qval>1.-1./(size+1.)) printf(FLOAT_NL,vals[size-1]);
	else {
	  int index = floor((size+1.)*qval);
	  double delta =  (size+1.)*qval-index;
	  double quantile = vals[index-1]*(1.-delta)+vals[index]*delta;
	  if(o_err==0)
	    printf(FLOAT_NL,quantile);
	  else{
	    int supindex = ceil((size+1.)*qval+sqrt(qval*(1-qval)*(size+2.)));
	    int infindex = floor((size+1.)*qval-sqrt(qval*(1-qval)*(size+2.)));
	    double supdist= vals[supindex-1]-quantile;
	    double infdist = quantile-vals[infindex-1];
	    printf(FLOAT_SEP,vals[index-1]*(1.-delta)+vals[index]*delta);
	    printf(FLOAT_NL,(supdist>infdist?supdist:infdist));
	  }
	}
      }
    }
    else{
      size_t rows=0,columns=0,i,j;
      double **vals=NULL;
      double **results;
      
      loadtable(&vals,&rows,&columns,0,splitstring);

      /*+++++++++++++++++++++++++++++++++++++++*/
      if(o_verbose)
	fprintf(stderr,"loaded %zdx%zd data table\n",rows,columns);
      /*+++++++++++++++++++++++++++++++++++++++*/

      /* allocate the array for the result */
      results = (double **) my_alloc(qvalnum*sizeof(double *));
      for(j=0;j<qvalnum;j++)
	results[j] = (double *) my_alloc(columns*sizeof(double));
      
      for(i=0;i<columns;i++){
	size_t size=rows;
	denan(&(vals[i]),&size);
	qsort(vals[i],size,sizeof(double),sort_by_value);

	for(j=0;j<qvalnum;j++){
	  const double qval = qvals[j];
	  
	  if(qval<1./(size+1.)) results[j][i]=vals[i][0];
	  else if (qval>1.-1./(size+1.)) results[j][i]=vals[i][size-1];
	  else {
	    int index = floor((size+1.)*qval);
	    double delta =  (size+1.)*qval-index;
	    results[j][i]=vals[i][index-1]*(1.-delta)+vals[i][index]*delta;
	  }
	}
      }

      /*+++++++++++++++++++++++++++++++++++++++*/
      if(o_verbose) 
	printf(EMPTY_NL,"#quantiles->");
      /*+++++++++++++++++++++++++++++++++++++++*/
      printmatrixbyrows(stdout,results,qvalnum,columns);

    }
  }
  else{
    fprintf(stderr,"you asked for something impossible! check %s -h\n",argv[0]);      
  }
  
  exit(0);
}
