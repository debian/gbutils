/*
  gbstat  (ver. 5.6) -- Basic descriptive statistics of data
  Copyright (C) 2000-2018 Giulio Bottazzi

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  (version 2) as published by the Free Software Foundation;
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#include "tools.h"

double HSM(double *vals,size_t size){

  double mode;
  size_t n=size;
  size_t i=0;
  
  while(n>3){
    size_t h;
    size_t j=i;
    size_t N=(size_t) ceil(n/2.0);
    double w=vals[i+n-1]-vals[i];
    
    /* printf("from %d to %d\n",i,i+n-N);  */    
    for(h=i; h < i+n-N+1; h++){
      double dtmp1=vals[h+N-1]-vals[h];
      
      /* printf("w[%d]=%f ",h,dtmp1);  */      
      if(dtmp1<w){
	w=dtmp1;
	j=h;
      }
    }
    
    n=N;
    i=j;    
    /* printf("\n[%+f,%+f] [%d,%d] %d\n",vals[i],vals[i+n-1],i,i+n-1,n);  */
  }
  
  
  if(n==3){
    if(vals[i+2]-vals[i+1] < vals[i+1]-vals[i])
      mode = 0.5*(vals[i+2]+vals[i+1]);
    else if (vals[i+2]-vals[i+1] == vals[i+1]-vals[i])
      mode = vals[i+1];
    else
      mode=0.5*(vals[i+1]+vals[i]);
  }
  else if(n==2){
    mode=0.5*(vals[i]+vals[i+1]);
  }
  else if (n==1){
    mode=vals[i];
  }
  
  return mode;
}


int main(int argc,char* argv[]){

  /* output variables */
  char o_median=0;
  char o_mode=0;
  char o_central=0;
  double result[11];
  double variance;

  char *splitstring = strdup(" \t");

  /* options options */
  char * const output_opts[] = {"mean","std","skew","kurt","adev","min","max","num","median","mode","var",NULL};
  char * const output_description[] = {"mean","stdev","skewness","kurtosis","average dev.","min","max","valid","median","mode","variance",NULL};
  size_t outnum;
  size_t *outtype;

  /* options */
  int o_short = 0;
  int o_table = 0;

  /* variables for reading command line options */
  /* ------------------------------------------ */
  int opt;
  /* ------------------------------------------ */

  /* set default output */
  outnum=8;
  outtype = (size_t *) my_alloc(outnum*sizeof(size_t));
  outtype[0]=0;
  outtype[1]=1;
  outtype[2]=2;
  outtype[3]=3;
  outtype[4]=4;
  outtype[5]=5;
  outtype[6]=6;
  outtype[7]=7;
  
  /* COMMAND LINE PROCESSING */

  while((opt=getopt_long(argc,argv,"hstmF:O:o:",gb_long_options, &gb_option_index))!=EOF){
    if(opt==0){
      gbutils_header(argv[0],stdout);
      exit(0);
    }
    else if(opt=='?'){
      fprintf(stderr,"option %c not recognized\n",optopt);
      exit(-1);
    }
    else if(opt=='h'){
      /*print help*/
      fprintf(stdout,"Sample statistics. Compute mean, standard deviation, skewness, kurtosis,\n");
      fprintf(stdout,"average deviation, minimum, maximum, median (if option -m is specified)\n");
      fprintf(stdout,"and number of valid observations. Option -O allows one to select specific\n");
      fprintf(stdout,"statistics. With option -t statistics are printed for each column of data\n");
      fprintf(stdout,"separately. A header line is added with the meaning of the different columns.\n");
      fprintf(stdout,"The mode is computed using the Half-Sample method.\n");
      fprintf(stdout,"\nUsage: %s [options]\n\n",argv[0]);
      fprintf(stdout,"Options:                                                                 \n");
      fprintf(stdout," -m  add the sample median to the estimated statistics                   \n");
      fprintf(stdout," -t  print statistics for each column of input                           \n");
      fprintf(stdout," -s  short: mean std skew kurt adev min max (median) num                 \n");
      fprintf(stdout," -O  select output: mean,std,var, skew,kurt,adev,min,max,median,num,mode \n");
      fprintf(stdout," -o  set the output format (default '%%12.6e')                           \n");
      fprintf(stdout," -F  specify the input fields separators (default \" \\t\")              \n");
      fprintf(stdout," -h this help\n");
      fprintf(stdout,"Examples:\n");
      fprintf(stdout," gbstat -O median,kurt < file  compute the median and the kurtosis\n");
      fprintf(stdout,"                               of all the data in 'file'\n");
      fprintf(stdout," gbstat -ts < file  compute statistics for each columns separately.\n");
      fprintf(stdout,"                    Since option -s is provided, the header is omitted.\n");
      finalize_program();exit(0);
    }
    else if(opt=='m'){
      /*add the median as output before last*/
      outnum=9;
      outtype = (size_t *) my_realloc((void *)outtype,outnum*sizeof(size_t));
      outtype[7]=8;
      outtype[8]=7;
    }
    else if(opt=='s'){
      /*set the short form*/
      o_short=1;
    }
    else if(opt=='t'){
      /*set the table form*/
      o_table=1;
    }
    else if(opt=='F'){
      /*set the fields separator string*/
      free(splitstring);
      splitstring = strdup(optarg);
    }
    else if(opt=='O'){
      /* set the output type */
      char *subopts,*subvalue=NULL;
      /* clear default settings */
      outnum=0;
      free(outtype); outtype=NULL;

      /* define new output */
      subopts = optarg;
      while (*subopts != '\0'){
	int  itmp1 =  getsubopt(&subopts, output_opts, &subvalue);
	outnum++;
	outtype = (size_t *) my_realloc((void *)outtype,outnum*sizeof(size_t));
	
	if(itmp1 == -1){/* Unknown suboption. */
	  fprintf (stderr,"ERROR (%s): Unknown output type `%s'\n", 
		   GB_PROGNAME,subvalue);
	  exit(1);
	}
	else
	  outtype[outnum-1]=(size_t) itmp1;
      }
    }
    else if(opt=='o'){
      /*set the ouptustring */
      FLOAT = strdup(optarg);
    }
  }
  /* END OF COMMAND LINE PROCESSING */

  /* initialize global variables */
  initialize_program(argv[0]);
  
  /* check if the median, the mode or some central statistics should
     be computed */
  {
    size_t i;

    for(i=0;i<outnum;i++)
      if(outtype[i]==8)
	o_median=1;
      else if (outtype[i]==9)
	o_mode=1;
      else if  (outtype[i]<7 || outtype[i]==10)
	o_central=1;
  }

  if(!o_table){
    size_t size,i;
    double *vals=NULL;

    load(&vals,&size,0,splitstring);

    denan(&vals,&size);

    result[7]=size;
    if(o_central)
      moment(vals,size,
	     &(result[0]),&(result[4]),&(result[1]),&(result[10]),&(result[2]),&(result[3]),&(result[5]),&(result[6]));
    
    /* sort the data */
    if(o_median || o_mode)
      qsort(vals,size,sizeof(double),sort_by_value);
    
    /* compute the median */
    if(o_median)
      if(size>1)
	result[8] = (size%2 ==0 ? .5*(vals[size/2-1]+vals[size/2]) : vals[(size-1)/2]);
      else if(size ==1)
	result[8] = vals[0];
      else if(size <1)
	result[8] = NAN;
    
    /* compute the mode */
    if(o_mode)
      if(size>1)
	result[9]=HSM(vals,size);
      else if(size ==1)
	result[9] = vals[0];
      else if(size <1)
	result[9] = NAN;
    
    if(o_short){
      for(i=0;i<outnum-1;i++)
	printf(FLOAT_SEP,result[outtype[i]]);
      printf(FLOAT_NL,result[outtype[outnum-1]]);
    }
    else{
      for(i=0;i<outnum;i++){
	printf("%*s ",12,output_description[outtype[i]]);
	printf(FLOAT_NL,result[outtype[i]]);
      }
    }

    /* free data */
    free(vals);
  }
  else {/*TABLE MODE*/
    size_t rows=0,columns=0,i;
    double **vals=NULL;
    /*double *set;*/

    loadtable(&vals,&rows,&columns,0,splitstring);
    
    if(!o_short){
      printf("#");
      for(i=0;i<outnum-1;i++)
	printf(EMPTY_SEP,output_description[outtype[i]]);
      printf(EMPTY_NL,output_description[outtype[outnum-1]]);
    }

    for(i=0;i<columns;i++){
      size_t itmp1=rows;
      size_t j;

      denan(&(vals[i]),&itmp1);

      result[7]=itmp1;
      if(o_central)
	moment(vals[i],itmp1,
	       &(result[0]),&(result[4]),&(result[1]),&(result[10]),&(result[2]),&(result[3]),&(result[5]),&(result[6]));

      /* sort the data */
      if(o_median || o_mode)
	qsort(vals[i],itmp1,sizeof(double),sort_by_value);

      /* compute the median */
      if(o_median)
	if(itmp1 > 1)
	  result[8] = (itmp1%2 == 0 ? .5*(vals[i][itmp1/2-1]+vals[i][itmp1/2]) : vals[i][(itmp1-1)/2]);
	else if(itmp1 == 1)
	  result[8] = vals[i][0];
	else if(itmp1 < 1)
	  result[8] = NAN;

      /* compute the mode */
      if(o_mode)
	if(itmp1 > 1)
	  result[9]=HSM(vals[i],itmp1);
	else if(itmp1 == 1)
	  result[9] = vals[i][0];
	else if(itmp1 < 1)
	  result[9] = NAN;

      for(j=0;j<outnum-1;j++)
	printf(FLOAT_SEP,result[outtype[j]]);
      printf(FLOAT_NL,result[outtype[outnum-1]]);
    }

    /* free data */
    for(i=0;i<columns;i++) free(vals[i]); free(vals);

  }

  free(splitstring);
  free(outtype);
  finalize_program();exit(0);
}
