/*
  gbboot  (ver. 5.6) -- Bootstrap user provided data
  Copyright (C) 2009-2018 Giulio Bottazzi

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  (version 2) as published by the Free Software Foundation;
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#include "tools.h"

#if defined HAVE_LIBGSL
#include <gsl/gsl_interp.h>
#endif


int main(int argc,char* argv[]){



  /* default options */
  unsigned o_verbose=0;
  unsigned o_output=0;
  unsigned o_setn=0;

  /* default parameters values */
  char *splitstring = strdup(" \t");
  unsigned n=0; /* number of extractions */
  unsigned seed=0;
  double noise=0.5;

  
  /* variables for reading command line options */
  /* ------------------------------------------ */
  int opt;
  /* ------------------------------------------ */
    
  while((opt=getopt_long(argc,argv,"vhF:M:O:R:n:e:",gb_long_options, &gb_option_index))!=EOF){
    if(opt==0){
      gbutils_header(argv[0],stdout);
      exit(0);
    }
    else if(opt=='?'){
      fprintf(stderr,"option %c not recognized\n",optopt);
      exit(-1);
    }
    else if(opt=='h'){
      /*print help*/
      /* print short help */
      fprintf(stdout,"Data bootstrap. Random sampling with replacement or addition\n");
      fprintf(stdout,"of noise. In the last case the option -n set the number of\n");
      fprintf(stdout,"times the entire sample is printed with a random noise added.\n");
      fprintf(stdout,"The noise is uniformly distributed around zero with semi-width\n");
      fprintf(stdout,"equal to the minimal positive distance between consecutives\n");
      fprintf(stdout,"observations times a fraction set with option \n");
      fprintf(stdout,"\nUsage: %s [options]\n\n",argv[0]);
      fprintf(stdout,"Options:\n");
      fprintf(stdout," -n  set the number of deviates (default =sample size) \n");
      fprintf(stdout," -O  type of output (default 0):                              \n");
      fprintf(stdout,"      0  flatten input in one distribution                    \n");
      fprintf(stdout,"      1  multivariate output: rows with independent columns   \n");
      fprintf(stdout,"      2  multivariate output: rows with fixed columns         \n");
      fprintf(stdout,"      3  flatten and smoothed (EDF linear interpolation)      \n");
      fprintf(stdout,"      4  add noise to data                                    \n");
      fprintf(stdout,"      5  multivariate output: rows with fixed blocks          \n");
      fprintf(stdout," -e  noise term ( default .5) \n");
      fprintf(stdout," -R  set the rng seed  (default 0) \n");
      fprintf(stdout," -F  specify the input fields separators (default \" \\t\")\n");
      fprintf(stdout," -h  print this help\n");
      fprintf(stdout," -v  verbose mode\n");
      exit(0);
    }
    else if(opt=='F'){
      /*set the fields separator string*/
      free(splitstring);
      splitstring = strdup(optarg);
    }
    else if(opt=='R'){
      /*RNG seed*/
      seed = (unsigned) atol(optarg);
    }
    else if(opt=='e'){
      /*displacement length*/
      noise = atof(optarg);
    }
    else if(opt=='O'){
      /* set the type of output */
      o_output = atoi(optarg);
    }
    else if(opt=='n'){
      /*set the number of generated deviates*/
      n=atoi(optarg);
      o_setn=1;
    }
    else if(opt=='v'){
      /*set the verbose mode*/
      o_verbose=1;
    }
  }    
  /* END OF COMMAND LINE PROCESSING */

  /* initialize global variables */
  initialize_program(argv[0]);

  /* set the RNG */
  srandom(seed);


  switch(o_output){
  case 0:
    {
      unsigned j;
      
      double *data=NULL;
      size_t size;
  
      load(&data,&size,0,splitstring);

      /*+++++++++++++++++++++++++++++++++++++++*/
      if(o_verbose)
	fprintf(stdout,"loaded %zd data\n",size);
      /*+++++++++++++++++++++++++++++++++++++++*/

      if(!o_setn) n=size;

      for(j=0;j<n;j++)
	printf(FLOAT_NL,data[(size_t) (size*( (double) random())/RAND_MAX ) ]);
    }
    break;
  case 1:
    {
      unsigned i,j;

      size_t rows=0,columns=0;
      double **data=NULL;
      
      loadtable(&data,&rows,&columns,0,splitstring);

      /*+++++++++++++++++++++++++++++++++++++++*/
      if(o_verbose)
	fprintf(stdout,"loaded table %zdx%zd\n",rows,columns);
      /*+++++++++++++++++++++++++++++++++++++++*/

      if(!o_setn) n=rows;

      for(i=0;i<n;i++){
	for(j=0;j<columns;j++)
	  printf(FLOAT_SEP,data[j][(size_t) (rows*( (double) random())/RAND_MAX ) ]);
	printf("\n");
      }
    }
    break;
  case 2:
    {
      unsigned i,j;

      size_t rows=0,columns=0;
      double **data=NULL;
      
      loadtable(&data,&rows,&columns,0,splitstring);

      /*+++++++++++++++++++++++++++++++++++++++*/
      if(o_verbose)
	fprintf(stdout,"loaded table %zdx%zd\n",rows,columns);
      /*+++++++++++++++++++++++++++++++++++++++*/

      if(!o_setn) n=rows;

      for(i=0;i<n;i++){
	const size_t index = rows*( (double) random())/RAND_MAX;
	for(j=0;j<columns;j++)
	  printf(FLOAT_SEP,data[j][index]);
	printf("\n");
      }
    }
    break;
  case 3:
#if defined HAVE_LIBGSL
    {
      unsigned j;
      
      double *data=NULL;
      size_t size;

      double *x,*y;
      size_t length;


      gsl_interp_accel *accelerator;
      gsl_interp * interpolator;


      load(&data,&size,0,splitstring);

      /*+++++++++++++++++++++++++++++++++++++++*/
      if(o_verbose)
	fprintf(stdout,"loaded %zd data\n",size);
      /*+++++++++++++++++++++++++++++++++++++++*/

      qsort(data,size,sizeof(double),sort_by_value);

      x = (double *) my_alloc(size*sizeof(double));
      y = (double *) my_alloc(size*sizeof(double));

      length=0;
      for(j=0;j<size;j++){
	if(j<size-1 && data[j+1]==data[j]) 
	  continue;
	else{
	  y[length]=data[j];
	  x[length]=((double) j+1)/(size+1);
	  length++;
	}
      }
      /*+++++++++++++++++++++++++++++++++++++++*/
      if(o_verbose)
	fprintf(stdout,"interpolated point %zd; %zd duplicated entries \n",
		length,size-length);
      /*+++++++++++++++++++++++++++++++++++++++*/

      x = (double *) my_realloc((void *) x,length*sizeof(double));
      y = (double *) my_realloc((void *) y,length*sizeof(double));

      /* allocate interpolator */
      interpolator = gsl_interp_alloc (gsl_interp_linear,length);

      /* initialize the interpolator */
      gsl_interp_init(interpolator,x,y,length);

      /* allocate the lookup accelerator */
      accelerator=gsl_interp_accel_alloc();

      if(!o_setn) n=size;

      for(j=0;j<n;j++)
	printf(FLOAT_NL,gsl_interp_eval(interpolator,x,y,
					((double) random())/RAND_MAX,accelerator));
      
    }
#else
    fprintf(stdout,"Smoothed bootstrap is based on GSL which was not found on you system\n");
#endif
    break;
  case 4:
    {
      unsigned i,j,h;

      size_t rows=0,columns=0;
      double **data=NULL;
      
      double *minskip;


      loadtable(&data,&rows,&columns,0,splitstring);

      /*+++++++++++++++++++++++++++++++++++++++*/
      if(o_verbose)
	fprintf(stdout,"loaded table %zdx%zd\n",rows,columns);
      /*+++++++++++++++++++++++++++++++++++++++*/

      if(rows<2){
	fprintf(stderr,"ERROR (%s): provide at least two observations per column\n",GB_PROGNAME);
	exit(-1);
      }

      /* allocate minimal skip for columns */
      minskip = (double *) my_alloc(columns*sizeof(double));
      for(j=0;j<columns;j++) minskip[j]=DBL_MAX;

      /* compute minimal distance for each column */
      for(j=0;j<columns;j++){
	double *x = (double *) my_alloc(rows*sizeof(double));
	
	memcpy(x,data[j],rows*sizeof(double));

	qsort(x,rows,sizeof(double),sort_by_value);

	for(h=0;h<rows-1;h++){
	  const double dtmp1 = x[h+1]-x[h];

	  if(dtmp1>0 && dtmp1 < minskip[j] )
	    minskip[j] = dtmp1;
	}

	free(x);
      }

      
      if(!o_setn) n=1;
      
      for(i=0;i<n;i++){
	for(h=0;h<rows;h++){
	  for(j=0;j<columns;j++)
	    printf(FLOAT_SEP,data[j][h]+noise*minskip[j]*(2.0*random()/RAND_MAX-1.0));
	  printf("\n");
	}
      }
    }
    break;
  case 5:
    {

      unsigned i;
      size_t b,c;

      /* input data storage */
      size_t rows=0,columns=0,blocks=0;
      double ***vals=NULL;

      /* output data storage */
      double ***res=NULL;

      /* load data vals[block][column][rows] */
      loadtableblocks(&vals,&blocks,&rows,&columns,0,splitstring);


      /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
      if(o_verbose)
	fprintf(stdout,"loaded %zd blocks of %zd rows\n",blocks,rows);
      /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

      if(!o_setn) n= (unsigned) rows;

      /* allocate output */
      res = (double ***) my_alloc(blocks*sizeof(double**));
      for(b=0;b<blocks;b++){
	res[b] = (double **) my_alloc(columns*sizeof(double*));
	for(c=0;c<columns;c++){
	  res[b][c] = (double *) my_alloc(n*sizeof(double));
	}
      }

      /* fill output */
      for(i=0;i<n;i++){
	const size_t index= (size_t) (rows*( (double) random())/RAND_MAX );

	for(b=0;b<blocks;b++)
	  for(c=0;c<columns;c++)
	    res[b][c][i] = vals[b][c][index];
      }

      /* print output */
      for(b=0;b<blocks;b++){
	printmatrixbycols(stdout,res[b],n,columns);
	if(b<blocks-1) printf("\n\n");
      }
    }
    break;


  }

  return 0;
}
