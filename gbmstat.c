/*
  gbmstat (ver. 5.6) -- Computing statistics in a  moving windows
  Copyright (C) 1998-2018 Giulio Bottazzi

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  (version 2) as published by the Free Software Foundation;
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#include "tools.h"

int main(int argc,char* argv[]){
  
  size_t lag=10;
  
  char *splitstring = strdup(" \t");
  
  /* options */
  int o_binary_in=0;
  int o_table = 0;
  int o_denan=0;
  char * const output_opts[] = {"mean","adev","std","var","skew","kurt","min","max","nonan",NULL};
  size_t outnum;
  size_t *outtype;


  /* set default output */
  outnum=1;
  outtype = (size_t *) my_alloc(1*sizeof(size_t));
  outtype[0]=0; 		/* mean */
  

  /* COMMAND LINE PROCESSING */
  
  /* variables for reading command line options */
  /* ------------------------------------------ */
  int opt;
  /* ------------------------------------------ */
  
  
  /* read the command line */    
  while((opt=getopt_long(argc,argv,"s:hF:tO:Db:",gb_long_options, &gb_option_index))!=EOF){
    if(opt==0){
      gbutils_header(argv[0],stdout);
      exit(0);
    }
    else if(opt=='?'){
      fprintf(stderr,"option %c not recognized\n",optopt);
      exit(-1);
    }
    else if(opt=='F'){
      /*set the fields separator string*/
      free(splitstring);
      splitstring = strdup(optarg);
    }
    else if(opt=='D'){
      /*ignore NAN data*/
      o_denan=1;
    }
    else if(opt=='s'){
      /*set the lag*/
      lag=(size_t) atoi(optarg);
    }
    else if(opt=='t'){
      /*set the table form*/
      o_table=1;
    }
    else if(opt=='b'){
      /*set binary input or output*/

      switch(atoi(optarg)){
      case 0 :
	break;
      case 1 :
	o_binary_in=1;
	break;
      case 2 :
	PRINTMATRIXBYCOLS=printmatrixbycols_bin;
	break;
      case 3 :
	o_binary_in=1;
	PRINTMATRIXBYCOLS=printmatrixbycols_bin;
	break;
      default:
	fprintf(stderr,"unclear binary specification %d, option ignored.\n",atoi(optarg));
	break;
      }
    }
    else if(opt=='O'){
      /* set the output type */
      char *subopts,*subvalue=NULL;
      /* clear default settings */
      outnum=0;
      free(outtype); outtype=NULL;

      /* define new output */
      subopts = optarg;
      while (*subopts != '\0'){
	int  itmp1 =  getsubopt(&subopts, output_opts, &subvalue);
	outnum++;
	outtype = (size_t *) my_realloc((void *)outtype,outnum*sizeof(size_t));

	if(itmp1 == -1){/* Unknown suboption. */
	  fprintf (stderr,"ERROR (%s): Unknown output type `%s'\n", 
		   GB_PROGNAME,subvalue);
	  exit(1);
	}
	else
	  outtype[outnum-1]=(size_t) itmp1;
      }
    }
    else if(opt=='h'){
      /*print short help*/
      fprintf(stdout,
	      "Compute moving average. Data are read from standard input.\n");
      fprintf(stdout,"\nUsage: %s [options]\n\n",argv[0]);
      fprintf(stdout,"Options:\n");
      fprintf(stdout," -s  number of steps to average (default 10)\n");
      fprintf(stdout," -t  compute average for each column of input       \n");
      fprintf(stdout," -D  ignore NAN entries in computation              \n");
      fprintf(stdout," -O  set the output, comma separated list of        \n");
      fprintf(stdout,"     mean, adev, std, var, skew, kurt, min ,max     \n");
      fprintf(stdout,"     and nonan (default mean). nonan works only with\n");
      fprintf(stdout,"     option -D     \n");
      fprintf(stdout," -F  specify the input fields separators (default \" \\t\")\n");
      fprintf(stdout," -h  this help\n");
      exit(0);
    }
  }
  /* END OF COMMAND LINE PROCESSING */

  /* initialize global variables */
  initialize_program(argv[0]);

  if(!o_table){

    double *data=NULL;
    size_t size,i,j;
    double **out;

    /*   1    2   3   4    5   6    7   8    9   */
    /* mean adev std var skew kurt min max nonan */

    double * stats = (double *) my_alloc(9*sizeof(double));

    /* load data */
    if(o_binary_in==1)
      load_bin(&data,&size,0);
    else
      load(&data,&size,0,splitstring);

    /* check there are enough entries */
    if(size<lag) {
      fprintf(stderr,"WARNING (%s): not enough data for lag %zd\n",
	      GB_PROGNAME,lag);
      exit(-1);
    }

    /* allocate output */
    out = (double **) my_alloc(outnum*sizeof(double *));
    for(i=0;i<outnum;i++)
      out[i] = (double *) my_alloc((size-lag+1)*sizeof(double));
    
    /* compute output */
    for(i=0;i<=size-lag;i++){

      /* compute statistics */
      if(o_denan)
	moment_nonan(data+i,lag,
		     stats,stats+1,stats+2,stats+3,
		     stats+4,stats+5,stats+6,stats+7,stats+8);
      else
	moment(data+i,lag,
	       stats,stats+1,stats+2,stats+3,
	       stats+4,stats+5,stats+6,stats+7);

      /* store results */
      for(j=0;j<outnum;j++)
	out[j][i]=stats[outtype[j]];
    }

    /* print output */

    PRINTMATRIXBYCOLS(stdout,out,size-lag+1,outnum);

     free(stats); 
     for(i=0;i<outnum;i++) 
       free(out[i]); 
     free(out); 
     free(data); 
  }
  else {

    double **data=NULL; /* array of values */
    size_t rows=0,columns=0;

    size_t i,j,h;
    double **stats, **out;

    /* load data: data[column][row] */
    if(o_binary_in==1)
      loadtable_bin(&data,&rows,&columns,0);
    else
      loadtable(&data,&rows,&columns,0,splitstring);

    /* allocate results of stat */

    /*   1    2   3   4    5   6    7   8    9   */
    /* mean adev std var skew kurt min max nonan */
    stats = (double **) my_alloc(columns*sizeof(double *));
    for(h=0;h<columns;h++)
      stats[h] = (double *) my_alloc(9*sizeof(double));

    /* allocate output */
    out = (double **) my_alloc(outnum*columns*sizeof(double *));
    for(i=0;i<outnum*columns;i++)
      out[i] = (double *) my_alloc((rows-lag+1)*sizeof(double));

    for(i=0;i<=rows-lag;i++){

      /* compute statistics */
      if(o_denan)
	for(h=0;h<columns;h++)
	  moment_nonan(data[h]+i,lag,
		       stats[h],stats[h]+1,stats[h]+2,stats[h]+3,
		       stats[h]+4,stats[h]+5,stats[h]+6,stats[h]+7,stats[h]+8);
      else
	for(h=0;h<columns;h++)
	  moment(data[h]+i,lag,
		 stats[h],stats[h]+1,stats[h]+2,stats[h]+3,
		 stats[h]+4,stats[h]+5,stats[h]+6,stats[h]+7);
	
      /* store and print results */
      for(h=0;h<columns;h++)
	for(j=0;j<outnum;j++)
	  out[h*outnum+j][i]=stats[h][outtype[j]];
    }


    PRINTMATRIXBYCOLS(stdout,out,rows-lag+1,outnum*columns);

    for(i=0;i<columns;i++)
      free(stats[i]);
    free(stats);
    for(i=0;i<outnum*columns;i++)
      free(out[i]);
    free(out);
    for(i=0;i<columns;i++)
      free(data[i]);
    free(data);

  }

  
  return 0;

}
