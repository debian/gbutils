#!/bin/bash

# created:  2021-08-05 giulio
# Time-stamp: <2022-04-11 14:03:03 giulio>

res=`echo "
10 20 30
11 21 31
12 22 32
13 23 33
14 24 34


15 25 35
16 26 36
17 27 37
18 28 38
19 29 39


" | ./gbget '[1](2)t' -o %d`

status=$?

[[ $status == 0 && $res  == "20 21 22 23 24" ]]
