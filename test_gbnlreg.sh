#!/bin/bash

# created:  2021-08-05 giulio
# Time-stamp: <2022-04-11 14:04:41 giulio>

res=`echo "
10 20
11 21
12 22
13 23
14 24
" | ./gbnlreg 'x2-a*x1-b,a=0,b=0' | ./gbget '()' -o %d`

status=$?

[[ $status == 0 && $res == "1 10" ]]
