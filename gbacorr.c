/*
  gbacorr (ver. 5.6) -- Compute auto/cross-correlation coefficients
  Copyright (C) 2014-2018 Giulio Bottazzi

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  (version 2) as published by the Free Software Foundation;
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#include "tools.h"
#include <gsl/gsl_cdf.h>


int main(int argc,char* argv[]){

  char *splitstring = strdup(" \t");

  size_t rows=0,columns=0;
  double **vals=NULL,*res=NULL;

  size_t i;

  int tini=0,tfin=10;
  int t;

  /* OPTIONS */
  int o_method=0;
  int o_confidence=0;
  double confidence=0.01;

  /* variables for reading command line options */
  /* ------------------------------------------ */
  int opt;
  /* ------------------------------------------ */

  
  /* COMMAND LINE PROCESSING */

  while((opt=getopt_long(argc,argv,"hF:M:t:p:",gb_long_options, &gb_option_index))!=EOF){
    if(opt==0){
      gbutils_header(argv[0],stdout);
      exit(0);
    }
    else if(opt=='?'){
      fprintf(stderr,"option %c not recognized\n",optopt);
      exit(-1);
    }
    else if(opt=='h'){
      /*print help*/
      fprintf(stdout,"Compute auto/cross-correlation coefficients\n\n");
      fprintf(stdout,"Usage: %s [options]\n",argv[0]);
      fprintf(stdout,"                                                                           \n");
      fprintf(stdout,"If the input is a single columns x_1...X_T, the autocorrelation function   \n");
      fprintf(stdout,"c(t) is printed, defined as                          \n");
      fprintf(stdout,"  c_{t} = 1/(T-t-1) \\sum_i (x_i-m) (x_{i+t}-m) /s^2          \n");
      fprintf(stdout,"where m is the sample average and s the standard deviation.   \n");
      fprintf(stdout,"With a second column y_1...y_T, the cross-correlation          \n");
      fprintf(stdout,"  c_{t} = 1/(T-t-1) \\sum_i (x_i-mx) (y_{i+t}-my) /(sx sy)    \n");
      fprintf(stdout,"is printed where mx and my are the average values of the two columns and sx\n");
      fprintf(stdout,"and sy their standard deviations. With -M 1 it is mx=my=0, the st.dev. is\n");
      fprintf(stdout,"computed accordingly and in the previous formula T-t-1 is replaced by T-t. \n");
      fprintf(stdout,"The range of t is set by option -t. \n");
      fprintf(stdout,"Options\n");
      fprintf(stdout," -M  choose the method (default '0'):                         \n");
      fprintf(stdout,"      0  auto/cross-correlation with mean removal,             \n");
      fprintf(stdout,"      1  auto/cross-correlation without mean removal           \n");
      fprintf(stdout," -t  set range of t (default '0,10'), accept negative integers\n");
      fprintf(stdout," -p  specify the confidence level in (0,1). Interval ac_low,ac_hi has a  \n");
      fprintf(stdout,"     probability 1-confidence to contain the true value. With this option\n");
      fprintf(stdout,"     the output becomes: lag ac ac_low ac_hi.\n");
      fprintf(stdout," -F  specify the input fields separators (default \" \\t\")   \n");
      fprintf(stdout," -h  this help\n");
      fprintf(stdout,"Examples:\n");
      fprintf(stdout," gbacorr -t 0,2 'file(1)'    first three a.c. coeff. of the first data column\n");
      fprintf(stdout," gbacorr -p 0.05 'file(1:2)' x-corr of the first two columns together with\n");
      fprintf(stdout,"                             their 5%% confidence intervals\n");
      exit(0);
    }
    else if(opt=='t'){
      /*set the lead-lag range */

      char *stmp1=strdup (optarg);
      char *stmp2;
      char *stmp3=stmp1;
      
      stmp2=strsep (&stmp1,",");
      if(strlen(stmp2)>0) 
	tini=atoi(stmp2);
      if(stmp1 != NULL && strlen(stmp1)>0)
	tfin=atof(stmp1);
      free(stmp3);

      if(tini>tfin){
	const int dtmp1 = tini;
	tini=tfin;
	tfin=dtmp1;
      }

    }
    else if(opt=='M'){
      /*set the method to use*/
      o_method = atoi(optarg);
    }
    else if(opt=='p'){
      /*set the method to use*/
      o_confidence =1;
      confidence = atof(optarg);
    }
    else if(opt=='F'){
      /*set the fields separator string*/
      free(splitstring);
      splitstring = strdup(optarg);
    }
  }
  /* END OF COMMAND LINE PROCESSING */

  /* initialize global variables */
  initialize_program(argv[0]);

  /* load data */
  loadtable(&vals,&rows,&columns,0,splitstring);

  /* allocate space for the result */
  res = (double *) my_alloc((tfin-tini+1)*sizeof(double));

  /* compute the result */
  if(o_method==0){/* with mean removal */

    if(columns==1){

      double ave,sdev,min,max;

      moment_short(vals[0],rows,&ave,&sdev,&min,&max);

      for(t=tini;t<=tfin;t++){
	size_t startindex = (t<0 ? -t : 0);
	size_t endindex = (t>0 ? rows-t : rows);
	res[t-tini]=0.0;
	for(i=startindex;i < endindex ;i++)
	  res[t-tini] += (vals[0][i]-ave)*(vals[0][i+t]-ave);
	res[t-tini] /= (endindex-startindex-1)*sdev*sdev;
      }
    }
    else{

      double ave1,sdev1,min1,max1;
      double ave2,sdev2,min2,max2;

      moment_short(vals[0],rows,&ave1,&sdev1,&min1,&max1); 
      moment_short(vals[1],rows,&ave2,&sdev2,&min2,&max2); 

      for(t=tini;t<=tfin;t++){
	size_t startindex = (t<0 ? -t : 0);
	size_t endindex = (t>0 ? rows-t : rows);
	res[t-tini]=0.0;
	for(i=startindex;i < endindex ;i++)
	  res[t-tini] += (vals[0][i]-ave1)*(vals[1][i+t]-ave2);
	res[t-tini] /= (endindex-startindex-1)*sdev1*sdev2;
      }
    }
  }
  else if(o_method==1){/* without mean removal */

    /* the mean is assumed known and the denominator of the unbiased
       estimator of standard deviation is #obs. instead of #obs. -1 */

    if(columns==1){

      double sdev=0;
      for(i=0;i < rows ;i++)
	sdev += vals[0][i]*vals[0][i];
      sdev /= rows;

      for(t=tini;t<=tfin;t++){
	size_t startindex = (t<0 ? -t : 0);
	size_t endindex = (t>0 ? rows-t : rows);
	res[t-tini]=0.0;
	for(i=startindex;i < endindex ;i++)
	  res[t-tini] += vals[0][i]*vals[0][i+t];
	res[t-tini] /= (endindex-startindex)*sdev*sdev;
      }

    }
    else{

      double sdev1=0, sdev2=0;

      for(i=0;i < rows ;i++){
	sdev1 += vals[0][i]*vals[0][i];
	sdev2 += vals[1][i]*vals[1][i];
      }

      sdev1 /= rows;
      sdev2 /= rows;

      for(t=tini;t<=tfin;t++){
	size_t startindex = (t<0 ? -t : 0);
	size_t endindex = (t>0 ? rows-t : rows);
	res[t-tini]=0.0;
	for(i=startindex;i < endindex ;i++)
	  res[t-tini] += vals[0][i]*vals[1][i+t];
	res[t-tini] /= (endindex-startindex)*sdev1*sdev2;
      }

    }

  }
  else{/* unknown method  */
    fprintf(stderr,"ERROR (%s): unknown method; use option -h\n",
	    GB_PROGNAME);
    exit(+1);
  }
  
    
  /* print the result */
  if(o_confidence==0){
    for(t=tini;t<=tfin;t++){
      printf(INT_SEP,t);
      printf(FLOAT_NL,res[t-tini]);
    }
  }
  else if(o_confidence==1){
    for(t=tini;t<=tfin;t++){
      const double r = res[t-tini];
      if(-1 < r && r <1 ){
	const double z = 0.5*log((1+r)/(1-r));       /* Fisher transformation */      
	const double zlow=z-gsl_cdf_ugaussian_Qinv(confidence/2)/sqrt(rows-fabs(t)-4);
	const double zhi=z+gsl_cdf_ugaussian_Qinv(confidence/2)/sqrt(rows-fabs(t)-4);
	printf(INT_SEP,t);
	printf(FLOAT_SEP,(exp(2*zlow)-1)/(exp(2*zlow)+1));
	printf(FLOAT_SEP,r);
	printf(FLOAT_NL,(exp(2*zhi)-1)/(exp(2*zhi)+1));
      }
      else{
	printf(INT_SEP,t);
	printf(FLOAT_SEP,r);
	printf(FLOAT_SEP,r);
	printf(FLOAT_NL,r);
      }
    }
  }

  exit(0);
}
