/*
  gbrand (ver. 6.0) -- Sampling from random distributions
  Copyright (C) 2008-2018 Giulio Bottazzi

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  (version 2) as published by the Free Software Foundation;
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#include "tools.h"

#include <gsl/gsl_randist.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_test.h>
#include <gsl/gsl_sf_lambert.h>
#include <gsl/gsl_sf_gamma.h>

const double pareto3(gsl_rng *r, double a, double b, double s){

  const double q = gsl_rng_uniform (r);

  const double dtmp1= b/a;

  return s*(gsl_sf_lambert_W0 (dtmp1*exp(dtmp1)*pow(1-q,-1/a))/dtmp1-1);

}


const double aexppow(gsl_rng *r, const double m, const double Aleft, const double Aright,
		     const double bleft, const double bright, const double probleft){

  if(gsl_rng_uniform(r)<probleft)
    return m-fabs(gsl_ran_exppow (r,Aleft,bleft));
  else
    return m+fabs(gsl_ran_exppow (r,Aright,bright));
}




int main(int argc,char* argv[]){

  size_t columns=1,rows=1;

  const gsl_rng_type * r_T;
  gsl_rng * r;

  char o_verbose=0;
  char o_seed =0;

  char *name;
  unsigned long int seed=0;

  /* variables for reading command line options */
  /* ------------------------------------------ */
  int opt;
  /* ------------------------------------------ */

  /* initialize random number generator */
  gsl_rng_env_setup();
  
  r_T = gsl_rng_default;
  r = gsl_rng_alloc (r_T);

  /* initialize global variables */
  initialize_program(argv[0]);

  /* COMMAND LINE PROCESSING */
  while((opt=getopt_long(argc,argv,"hvc:r:R:b:",gb_long_options, &gb_option_index))!=EOF){
    if(opt==0){
      gbutils_header(argv[0],stdout);
      exit(0);
    }
    else if(opt=='?'){
      fprintf(stderr,"option %c not recognized\n",optopt);
      exit(-1);
    }
    else if(opt=='h'){
      /*print help*/
      fprintf(stdout,"Generates a grid of i.i.d. samples from a distribution. The name of the\n");
      fprintf(stdout,"distribution and its parameters are specified on the command line.\n");
      fprintf(stdout,"the column index. Indeces start from 1. If more than one function are\n");
      fprintf(stdout,"provided they are printed one after the other. The type of random number\n");
      fprintf(stdout,"generator and the default seed can be set with the environment variables\n");
      fprintf(stdout,"GSL_RNG_TYPE and GSL_RNG_SEED, respectively, as documented in GSL manual.\n\n");
      fprintf(stdout,"Usage: %s [options] NAME PARAMETERS... \n\n",argv[0]);
      fprintf(stdout,"Options:\n");
      fprintf(stdout," -r  specify number of rows (default 1)\n");
      fprintf(stdout," -c  specify number of columns (default 1)\n");
      fprintf(stdout," -R  set the seed (default 1)\n");
      fprintf(stdout," -v  verbose mode\n");
      fprintf(stdout," -h  this help\n\n");
      fprintf(stdout,"List of valid distributions (with parameters):\n");
      fprintf(stdout,"  beta  a,b = shape parameters\n");
      fprintf(stdout,"  binomial  p = probability, N = number of trials\n");
      fprintf(stdout,"  bivariate-gaussian  sigmax = x std.dev., sigmay = y std.dev., rho = correlation\n");
      fprintf(stdout,"  cauchy  a = scale parameter\n");
      fprintf(stdout,"  chisq  nu = degrees of freedom\n");
      fprintf(stdout,"  dir-2d  none\n");
      fprintf(stdout,"  dir-3d  none\n");
      fprintf(stdout,"  dir-nd  n1 = number of dimensions of hypersphere\n");
      fprintf(stdout,"  erlang  a = scale parameter, b = order\n");
      fprintf(stdout,"  exponential  mu = mean value\n");
      fprintf(stdout,"  exppow  a = scale parameter, b = power (1=exponential, 2=gaussian)\n");
      fprintf(stdout,"  aexppow  m = mode, al = left scale, ar = right scale, bl = left shape, br = right shape\n");
      fprintf(stdout,"  fdist  nu1, nu2 = degrees of freedom parameters\n");
      fprintf(stdout,"  flat  a = lower limit, b = upper limit\n");
      fprintf(stdout,"  gamma  a = order, b = scale\n");
      fprintf(stdout,"  gaussian-tail  a = lower limit, sigma = standard deviation\n");
      fprintf(stdout,"  gaussian  sigma = standard deviation\n");
      fprintf(stdout,"  geometric  p = bernoulli trial probability of success\n");
      fprintf(stdout,"  gumbel1  a = order, b = scale parameter\n");
      fprintf(stdout,"  gumbel2  a = order, b = scale parameter\n");
      fprintf(stdout,"  hypergeometric  n1 = # tagged, n2 = # untagged, t = number of trials\n");
      fprintf(stdout,"  laplace  a = scale parameter\n");
      fprintf(stdout,"  landau  none\n");
      fprintf(stdout,"  levy  c = scale, a = power (1=cauchy, 2=gaussian)\n");
      fprintf(stdout,"  levy-skew  c = scale, a = power (1=cauchy, 2=gaussian), b = skew\n");
      fprintf(stdout,"  logarithmic  p = probability\n");
      fprintf(stdout,"  logistic  a = scale parameter\n");
      fprintf(stdout,"  lognormal  zeta = location parameter, sigma = scale parameter\n");
      fprintf(stdout,"  negative-binomial  p = probability, a = order\n");
      fprintf(stdout,"  pareto  a = power, b = scale parameter\n");
      fprintf(stdout,"  pascal  p = probability, n = order (integer)\n");
      fprintf(stdout,"  poisson  mu = scale parameter\n");
      fprintf(stdout,"  rayleigh-tail  a = lower limit, sigma = scale parameter\n");
      fprintf(stdout,"  rayleigh  sigma = scale parameter\n");
      fprintf(stdout,"  tdist  nu = degrees of freedom\n");
      fprintf(stdout,"  ugaussian-tail  a = lower limit\n");
      fprintf(stdout,"  ugaussian  none\n");
      fprintf(stdout,"  weibull  a = scale parameter, b = exponent\n");
      fprintf(stdout,"  pareto3  a = power, b = exponential parameter, s = scale parameter\n");      
      fprintf(stdout,"Examples:\n");
      fprintf(stdout," gbrand -c 2 -r 100 gaussian 2.5  generate two columns of 100 rows each sampling from a\n");
      fprintf(stdout,"                                  Gaussian distribution with mean 0 and standard deviation 2.5\n");
      fprintf(stdout," gbrand -R 145 -r 1000 exponential 1  one hundredth exponantial variates with parameters 1, the\n");
      fprintf(stdout,"                                      the seed of the RNG is set to 145\n");
      fprintf(stdout," gbrand -r 10 flat 3 4  ten realizations of the r.v. uniformly distributed in [3,4]\n");
      finalize_program(); return 0;
    }
    else if(opt=='R'){
      /*RNG seed*/
      o_seed=1;
      seed=atol(optarg);
    }
    else if(opt=='v'){
      o_verbose=1;
    }
    else if(opt=='c'){
      columns=atoi(optarg);
    }
    else if(opt=='r'){
      rows=atoi(optarg);
    }
    else if(opt=='b'){
      /*set binary output*/
      switch(atoi(optarg)){
      case 0 :
	break;
      case 1 :
	fprintf(stdout,"no binary input implemented, option ignored.\n");
	break;
      case 2 :
	PRINTMATRIXBYCOLS=printmatrixbycols_bin;
	break;
      case 3 :
	fprintf(stdout,"no binary input implemented, option ignored.\n");
	PRINTMATRIXBYCOLS=printmatrixbycols_bin;
	break;
      default:
	fprintf(stdout,"unclear binary specification %d, option ignored.\n",atoi(optarg));
	break;
      }
    }

  }


  if(optind<argc){
    name = argv[optind]; 
    optind++;
  }
  else{
    fprintf(stderr,"ERROR (%s): provide a distribution\n",GB_PROGNAME);
    exit(-1);
  }

  /* set the seed on the RNG */
  if(o_seed)
    gsl_rng_set (r,seed);

  /* ++++++++++++++++++++++++++++++++++++++++++++++++ */
  if(o_verbose){
    fprintf(stdout,"distribution: %s\n",name);
    fprintf(stdout,"cols: %zd rows: %zd\n",columns,rows);
    fprintf(stdout,"generator: %s\n",gsl_rng_name (r));
    fprintf(stdout,"seed: %ld\n",seed);

  }
  /* ++++++++++++++++++++++++++++++++++++++++++++++++ */

  {
    /* MAIN PART: a cut and paste exercise from  */
    
    /* randist/gsl-randist.c
     * 
     * Copyright (C) 1996, 1997, 1998, 1999, 2000 James Theiler, Brian Gough
     * 
     */

    size_t i,j;

    double mu = 0, nu = 0, nu1 = 0, nu2 = 0, sigma = 0, a = 0, b = 0, c = 0,s=1;
    double zeta = 0, sigmax = 0, sigmay = 0, rho = 0;
    double p = 0;
    double x = 0, y =0, z=0  ;
    unsigned int N = 0, t = 0, n1 = 0, n2 = 0 ;

    double m=0, al=1, ar=1, bl=2, br = 2;
    
#define NAME(x) !strcmp(name,(x))
#define PERROR(x) {fprintf(stderr,"ERROR (%s): parameters needed: %s\n",GB_PROGNAME,x); exit(-1);}

    
#define OUTPUT(x) {double **output= (double **) my_alloc(sizeof(double *)*columns); for (i=0;i<columns;i++) output[i]= (double *) my_alloc(sizeof(double)*rows);  for (i=0;i<rows;i++){ for(j=0;j<columns;j++) output[j][i]= (x); }; PRINTMATRIXBYCOLS(stdout,output,rows,columns); for (i=0;i<columns;i++) free(output[i]); free(output);}

#define INTOUTPUT(x) for (i=0;i<rows;i++){for(j=0;j<columns-1;j++) printf(INT_SEP, (x)); printf(INT_NL, (x));}

#define OUTPUT2(a,x,y) for (i=0;i<rows;i++){for(j=0;j<columns-1;j++) { a; printf(FLOAT_SEP, (x)); printf(FLOAT_SEP, (y)) ;} a; printf(FLOAT_SEP, (x)); printf(FLOAT_NL, (y));}

#define OUTPUT3(a,x,y,z) for (i=0;i<rows;i++){for(j=0;j<columns-1;j++) { a; printf(FLOAT_SEP, (x)); printf(FLOAT_SEP, (y)); printf(FLOAT_SEP, (z));} a; printf(FLOAT_SEP, (x)); printf(FLOAT_SEP, (y));  printf(FLOAT_NL, (z));}

#define INT_OUTPUT(x) for (i=0;i<rows;i++){for(j=0;j<columns;j++) printf("%d ", (x)); printf("\n");}

#define ARGS(x,y) if (argc < optind+x) PERROR(y);

#define DBL_ARG(x) if (argc-optind) { x=atof(argv[optind]);optind++;} else {PERROR( #x);};

#define INT_ARG(x) if (argc-optind) { x=atoi(argv[optind]);optind++;} else {PERROR( #x);};

    if (NAME("bernoulli"))
      {
	ARGS(1, "p = probability of success");
	DBL_ARG(p)
	INT_OUTPUT(gsl_ran_bernoulli (r, p));
      }
    else if (NAME("beta"))
      {
	ARGS(2, "a,b = shape parameters");
	DBL_ARG(a)
	DBL_ARG(b)
	OUTPUT(gsl_ran_beta (r, a, b));
      }
    else if (NAME("binomial"))
      {
	ARGS(2, "p = probability, N = number of trials");
	DBL_ARG(p)
	  INT_ARG(N)
	  INT_OUTPUT(gsl_ran_binomial (r, p, N));
      }
    else if (NAME("cauchy"))
      {
	ARGS(1, "a = scale parameter");
	DBL_ARG(a)
	  OUTPUT(gsl_ran_cauchy (r, a));
      }
    else if (NAME("chisq"))
      {
	ARGS(1, "nu = degrees of freedom");
	DBL_ARG(nu)
	  OUTPUT(gsl_ran_chisq (r, nu));
      }
    else if (NAME("erlang"))
      {
	ARGS(2, "a = scale parameter, b = order");
	DBL_ARG(a)
	  DBL_ARG(b)
	  OUTPUT(gsl_ran_erlang (r, a, b));
      }
    else if (NAME("exponential"))
      {
	ARGS(1, "mu = mean value");
	DBL_ARG(mu) ;
	OUTPUT(gsl_ran_exponential (r, mu));
      }
    else if (NAME("exppow"))
      {
	ARGS(2, "a = scale parameter, b = power (1=exponential, 2=gaussian)");
	DBL_ARG(a) ;
	DBL_ARG(b) ;
	OUTPUT(gsl_ran_exppow (r, a, b));
      }
    else if (NAME("aexppow"))
      {
	ARGS(5, "m = mode, al = left scale, ar = right scale, bl = left shape, br = right shape");
	DBL_ARG(m) ;
	DBL_ARG(al) ;
	DBL_ARG(ar) ;
	DBL_ARG(bl) ;
	DBL_ARG(br) ;

	const double Aleft=al*pow(bl,1./bl);
	const double Aright=ar*pow(br,1./br);
	
	const double probleft= Aleft*gsl_sf_gamma(1./bl+1.)/
	  (Aleft*gsl_sf_gamma(1./bl+1.)+Aright*gsl_sf_gamma(1./br+1.));

	OUTPUT(aexppow (r, m, Aleft, Aright,bl,br,probleft));

      }
    else if (NAME("fdist"))
      {
	ARGS(2, "nu1, nu2 = degrees of freedom parameters");
	DBL_ARG(nu1) ;
	DBL_ARG(nu2) ;
	OUTPUT(gsl_ran_fdist (r, nu1, nu2));
      }
    else if (NAME("flat"))
      {
	ARGS(2, "a = lower limit, b = upper limit");
	DBL_ARG(a) ;
	DBL_ARG(b) ;
	OUTPUT(gsl_ran_flat (r, a, b));
      }
    else if (NAME("gamma"))
      {
	ARGS(2, "a = order, b = scale");
	DBL_ARG(a) ;
	DBL_ARG(b) ;
	OUTPUT(gsl_ran_gamma (r, a, b));
      }
    else if (NAME("gaussian"))
      {
	ARGS(1, "sigma = standard deviation");
	DBL_ARG(sigma) ;
	OUTPUT(gsl_ran_gaussian (r, sigma));
      }
    else if (NAME("gaussian-tail"))
      {
	ARGS(2, "a = lower limit, sigma = standard deviation");
	DBL_ARG(a) ;
	DBL_ARG(sigma) ;
	OUTPUT(gsl_ran_gaussian_tail (r, a, sigma));
      }
    else if (NAME("ugaussian"))
      {
	ARGS(0, "unit gaussian, no parameters required");
	OUTPUT(gsl_ran_ugaussian (r));
      }
    else if (NAME("ugaussian-tail"))
      {
	ARGS(1, "a = lower limit");
	DBL_ARG(a) ;
	OUTPUT(gsl_ran_ugaussian_tail (r, a));
      }
    else if (NAME("bivariate-gaussian"))
      {
	ARGS(3, "sigmax = x std.dev., sigmay = y std.dev., rho = correlation");
	DBL_ARG(sigmax) ;
	DBL_ARG(sigmay) ;
	DBL_ARG(rho) ;
	OUTPUT2(gsl_ran_bivariate_gaussian (r, sigmax, sigmay, rho, &x, &y), 
		x, y);
      }
    else if (NAME("dir-2d"))
      {
	OUTPUT2(gsl_ran_dir_2d (r, &x, &y), x, y);
      }
    else if (NAME("dir-3d"))
      {
	OUTPUT3(gsl_ran_dir_3d (r, &x, &y, &z), x, y, z);
      }
    else if (NAME("dir-nd"))
      {
	size_t h;
	double *xarr;
	ARGS(1, "n1 = number of dimensions of hypersphere"); 
	INT_ARG(n1) ;
	xarr = (double *)malloc(n1*sizeof(double));

	for (i=0;i<rows;i++){
	  for(j=0;j<columns-1;j++){
	    gsl_ran_dir_nd (r, n1, xarr);
	    for (h = 0; h < n1; h++)
	      printf(FLOAT_SEP, xarr[h]);
	  }
	  j=columns-1;
	  gsl_ran_dir_nd (r, n1, xarr);
	  for (h = 0; h < n1-1; h++)
	    printf(FLOAT_SEP, xarr[h]);
	  printf(FLOAT_NL, xarr[n1-1]);
	}

	free(xarr);
      }  
    else if (NAME("geometric"))
      {
	ARGS(1, "p = bernoulli trial probability of success");
	DBL_ARG(p) ;
	INT_OUTPUT(gsl_ran_geometric (r, p));
      }
    else if (NAME("gumbel1"))
      {
	ARGS(2, "a = order, b = scale parameter");
	DBL_ARG(a) ;
	DBL_ARG(b) ;
	OUTPUT(gsl_ran_gumbel1 (r, a, b));
      }
    else if (NAME("gumbel2"))
      {
	ARGS(2, "a = order, b = scale parameter");
	DBL_ARG(a) ;
	DBL_ARG(b) ;
	OUTPUT(gsl_ran_gumbel2 (r, a, b));
      }
    else if (NAME("hypergeometric"))
      {
	ARGS(3, "n1 = tagged population, n2 = untagged population, t = number of trials");
	INT_ARG(n1) ;
	INT_ARG(n2) ;
	INT_ARG(t) ;
	INT_OUTPUT(gsl_ran_hypergeometric (r, n1, n2, t));
      }
    else if (NAME("laplace"))
      {
	ARGS(1, "a = scale parameter");
	DBL_ARG(a) ;
	OUTPUT(gsl_ran_laplace (r, a));
      }
    else if (NAME("landau"))
      {
	ARGS(0, "no arguments required");
	OUTPUT(gsl_ran_landau (r));
      }
    else if (NAME("levy"))
      {
	ARGS(2, "c = scale, a = power (1=cauchy, 2=gaussian)");
	DBL_ARG(c) ;
	DBL_ARG(a) ;
	OUTPUT(gsl_ran_levy (r, c, a));
      }
    else if (NAME("levy-skew"))
      {
	ARGS(3, "c = scale, a = power (1=cauchy, 2=gaussian), b = skew");
	DBL_ARG(c) ;
	DBL_ARG(a) ;
	DBL_ARG(b) ;
	OUTPUT(gsl_ran_levy_skew (r, c, a, b));
      }
    else if (NAME("logarithmic"))
      {
	ARGS(1, "p = probability");
	DBL_ARG(p) ;
	INT_OUTPUT(gsl_ran_logarithmic (r, p));
      }
    else if (NAME("logistic"))
      {
	ARGS(1, "a = scale parameter");
	DBL_ARG(a) ;
	OUTPUT(gsl_ran_logistic (r, a));
      }
    else if (NAME("lognormal"))
      {
	ARGS(2, "zeta = location parameter, sigma = scale parameter");
	DBL_ARG(zeta) ;
	DBL_ARG(sigma) ;
	OUTPUT(gsl_ran_lognormal (r, zeta, sigma));
      }
    else if (NAME("negative-binomial"))
      {
	ARGS(2, "p = probability, a = order");
	DBL_ARG(p) ;
	DBL_ARG(a) ;
	INT_OUTPUT(gsl_ran_negative_binomial (r, p, a));
      }
    else if (NAME("pareto"))
      {
	ARGS(2, "a = power, b = scale parameter");
	DBL_ARG(a) ;
	DBL_ARG(b) ;
	OUTPUT(gsl_ran_pareto (r, a, b));
      }
    else if (NAME("pareto3"))
      {
	ARGS(3, "a = power, b = exponential parameter , s = scale parameter");
	DBL_ARG(a) ;
	DBL_ARG(b) ;
	DBL_ARG(s) ;
	OUTPUT(pareto3(r, a, b,s));
      }
    else if (NAME("pascal"))
      {
	ARGS(2, "p = probability, n = order (integer)");
	DBL_ARG(p) ;
	INT_ARG(N) ;
	INT_OUTPUT(gsl_ran_pascal (r, p, N));
      }
    else if (NAME("poisson"))
      {
	ARGS(1, "mu = scale parameter");
	DBL_ARG(mu) ;
	INT_OUTPUT(gsl_ran_poisson (r, mu));
      }
    else if (NAME("rayleigh"))
      {
	ARGS(1, "sigma = scale parameter");
	DBL_ARG(sigma) ;
	OUTPUT(gsl_ran_rayleigh (r, sigma));
      }
    else if (NAME("rayleigh-tail"))
      {
	ARGS(2, "a = lower limit, sigma = scale parameter");
	DBL_ARG(a) ;
	DBL_ARG(sigma) ;
	OUTPUT(gsl_ran_rayleigh_tail (r, a, sigma));
      }
    else if (NAME("tdist"))
      {
	ARGS(1, "nu = degrees of freedom");
	DBL_ARG(nu) ;
	OUTPUT(gsl_ran_tdist (r, nu));
      }
    else if (NAME("weibull"))
      {
	ARGS(2, "a = scale parameter, b = exponent");
	DBL_ARG(a) ;
	DBL_ARG(b) ;
	OUTPUT(gsl_ran_weibull (r, a, b));
      }
    else if (NAME("discrete"))
      {

	double *plist=NULL;
	size_t pnum=0;

	gsl_ran_discrete_t *table;
	
	ARGS(1, "p1,p2,...,pN list of probabilities ");
	
	if (argc-optind) { 
	  char *stmp1=strdup (argv[optind]);
	  char *stmp2;
	  char *stmp3=stmp1;
	  size_t i;
	  double dtmp1;

	  optind++;

	  while( (stmp2=strsep (&stmp1,",")) != NULL && strlen(stmp2)>0 ){
	    pnum++;
	    plist = (double *) my_realloc((void *) plist,pnum*sizeof(double));
	    plist[pnum-1]=atof(stmp2);
	  }
	  free(stmp3);

	  /* check */
	  dtmp1=0.0;
	  for(i=0;i<pnum;i++)
	    dtmp1 += plist[i];
	  if(fabs(dtmp1-1.0) > 1e-06)
	    fprintf(stderr,"WARNING: probabilities do not sum to one!\n");
	} 
	else {
	  PERROR("plist");
	}

	table=gsl_ran_discrete_preproc (pnum,plist);

	INTOUTPUT( (gsl_ran_discrete (r,table)+1) );

	free(plist);
      }
    else
      {
	fprintf(stderr,"ERROR (%s): unrecognized distribution: %s\n",GB_PROGNAME,name);
	exit(-1);
      }

  /* END OF MAIN PART */
  }

  gsl_rng_free (r);

  finalize_program(); return 0;
}
