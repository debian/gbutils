#include "gbhill.h"


/* Pareto 1 distribution */
/* ------------------------ */

/* x[0]=g x[1]=b */

/* F[j] = 1-pow(b*x[j],-1./g)} */
/* -log(f[j]) = -log(1./g) + (1./g)*log(b) + (1./g+1)log(x[j]) */

/* N: sample size k: number of observations used */

void
pareto1_nll (const size_t n, const double *x,void *params,double *fval){

  size_t i;

  /* log-likelihood parameters */
  const size_t min    = ((struct loglike_params *) params)->min;
  const size_t max    = ((struct loglike_params *) params)->max;
  const double N      = (double) ((struct loglike_params *) params)->size;
  const double d      = (double) ((struct loglike_params *) params)->d;
  const int    method = ((struct loglike_params *) params)->method;
  const double *data  = ((struct loglike_params *) params)->data;

  /* distribution parameters */
  const double g=x[0];
  const double b=x[1];

  const double k = ((double) max-min+1);

  *fval=0.0;
  for(i=min;i<=max;i++) *fval += log(data[i]);
  *fval = (1./g+1.0)*(*fval)/k -log(1./g) + (1./g)*log(b);

  switch(method){
  case 0:
    if(N>k) *fval += -(N/k-1.0)*log(1.0-pow(b*data[min],-1./g));
    break;
  case 1:
    if(N>k) *fval += -(N/k-1.0)*log(1.0-pow(b*d,-1./g));
    break;  
  case 2:
    if(N>k) *fval += (N/k-1.0)*((1./g)*log(b) + (1./g)*log(data[max]));
    break;
  case 3:
    if(N>k) *fval += (N/k-1.0)*((1./g)*log(b) + (1./g)*log(d));
    break;
  }

/*   fprintf(stderr,"[ f ] x1=%g x2=%g f=%g\n",x[0],x[1],*fval); */

}

void
pareto1_dnll (const size_t n, const double *x,void *params,double *grad){

  size_t i;

  /* log-likelihood parameters */
  const size_t min    = ((struct loglike_params *) params)->min;
  const size_t max    = ((struct loglike_params *) params)->max;
  const double N      = (double) ((struct loglike_params *) params)->size;
  const double d      = (double) ((struct loglike_params *) params)->d;
  const int    method = ((struct loglike_params *) params)->method;
  const double *data  = ((struct loglike_params *) params)->data;

  /* distribution parameters */
  const double g=x[0];
  const double b=x[1];

  const double k = ((double) max-min+1);


  grad[0]=0.0;
  for(i=min;i<=max;i++) grad[0] += log(data[i]);
  grad[0]=(grad[0]/k -g +log(b))*(-1./pow(g,2));

  grad[1]=1./(b*g);

  switch(method){

  case 0:
    if(N>k) {
      const double F = 1.0- pow(b*data[min],-1./g);
      const double dFdg = log(b*data[min])*pow(b*data[min],-1./g);
      const double dFdb = (1./g)*pow(b*(data[min]),-1./g)/b;

      grad[0] += (-(N/k-1.0)*dFdg/F)*(-1./pow(g,2));
      grad[1] += -(N/k-1.0)*dFdb/F;
      
    }
    break;
  case 1:
    if(N>k){
      const double F = 1.0- pow(b*d,-1./g);
      const double dFdg = log(b*d)*pow(b*d,-1./g);
      const double dFdb = (1./g)*pow(b*d,-1./g)/b;

      grad[0] += (-(N/k-1.0)*dFdg/F)*(-1./pow(g,2));
      grad[1] += -(N/k-1.0)*dFdb/F;
      
    }
    break;
  case 2:
    if(N>k){
      grad[0] += ((N/k-1.0)*(log(b)+log(data[max])))*(-1./pow(g,2));
      grad[1] += (N/k-1.0)*(1./(b*g));
    }
    break;
  case 3:
    if(N>k){
      grad[0] += ((N/k-1.0)*(log(b)+log(d)))*(-1./pow(g,2));
      grad[1] += (N/k-1.0)*(1./(g*b));
    }
    break;
  }
  

/*   fprintf(stderr,"[ df] x1=%g x2=%g df/dx1=%g df/dx2=%g\n", */
/* 	  x[0],x[1],grad[0],grad[1]); */

}

void
pareto1_nlldnll (const size_t n, const double *x,void *params,double *fval,double *grad){


  size_t i;

  /* log-likelihood parameters */
  const size_t min    = ((struct loglike_params *) params)->min;
  const size_t max    = ((struct loglike_params *) params)->max;
  const double N      = (double) ((struct loglike_params *) params)->size;
  const double d      = (double) ((struct loglike_params *) params)->d;
  const int    method = ((struct loglike_params *) params)->method;
  const double *data  = ((struct loglike_params *) params)->data;

  /* distribution parameters */
  const double g=x[0];
  const double b=x[1];

  const double k = ((double) max-min+1);

  *fval=0.0;
  for(i=min;i<=max;i++) *fval += log(data[i]);
  grad[0]=((*fval)/k -g +log(b))*(-1./pow(g,2));
  *fval = ((1./g)+1.0)*(*fval)/k -log(1./g) + (1./g)*log(b);
  grad[1]=1./(b*g);

  switch(method){

  case 0:
    if(N>k) {

      const double F = 1.0- pow(b*data[min],-1./g);
      const double dFdg = log(b*data[min])*pow(b*data[min],-1./g);
      const double dFdb = (1./g)*pow(b*(data[min]),-1./g)/b;

      *fval += -(N/k-1.0)*log(F);			    
      grad[0] += (-(N/k-1.0)*dFdg/F)*(-1./pow(g,2));
      grad[1] += -(N/k-1.0)*dFdb/F;      
      
    }
    break;
  case 1:
    if(N>k) {

      const double F = 1.0- pow(b*d,-1./g);
      const double dFdg = log(b*d)*pow(b*d,-1./g);
      const double dFdb = (1./g)*pow(b*d,-1./g)/b;

      *fval += -(N/k-1.0)*log(F);			    
      grad[0] += (-(N/k-1.0)*dFdg/F)*(-1./pow(g,2));
      grad[1] += -(N/k-1.0)*dFdb/F;      
      
    }
    break;
  case 2:
    if(N>k){
      *fval   += (N/k-1.0)*((1./g)*log(b) + (1./g)*log(data[max]));
      grad[0] += ((N/k-1.0)*(log(b)+log(data[max])))*(-1./pow(g,2));
      grad[1] += (N/k-1.0)*(1./(b*g));
    }
    break;
  case 3:
    if(N>k){

      *fval += (N/k-1.0)*((1./g)*log(b) + (1./g)*log(d));
      grad[0] += ((N/k-1.0)*(log(b)+log(d)))*(-1./pow(g,2));
      grad[1] += (N/k-1.0)*(1./(b*g));
    }
    break;  
  }


/*   fprintf(stderr,"[fdf] x1=%g x2=%g f=%g df/dx1=%g df/dx2=%g\n", */
/* 	  x[0],x[1],*fval,grad[0],grad[1]); */

}

double
pareto1_f (const double x,const size_t n, const double *par){

  return gsl_ran_pareto_pdf(x,1./par[0],1./par[1]);

}

double
pareto1_F (const double x,const size_t n, const double *par){

  return gsl_cdf_pareto_P (x,1./par[0],1./par[1]);

}


gsl_matrix *pareto1_varcovar(const int o_varcovar, double *x, void *params){

  size_t i;

  /* log-likelihood parameters */
  const size_t min    = ((struct loglike_params *) params)->min;
  const size_t max    = ((struct loglike_params *) params)->max;
  const double N      = (double) ((struct loglike_params *) params)->size;
  const double d      = (double) ((struct loglike_params *) params)->d;
  const int    method = ((struct loglike_params *) params)->method;
  const double *data  = ((struct loglike_params *) params)->data;

  /* distribution parameters */
  const double g=x[0];
  const double b=x[1];

  const double k = ((double) max-min+1);
 
  double dldg    = 0.0;
  double d2ldg2  = 0.0;
  double d2ldgdb = 0.0;
  double dldb    = 0.0;
  double d2ldb2  = 0.0;
  gsl_matrix *covar;
  
  double sum   = 0.0;
  for(i=min;i<=max;i++) sum += log(data[i]);


  double ZN = data[0];
  double Zk = data[min];
  double ZNk  = data[max];
  
  switch(method){
  case 0:
  
    /* GSL allocation of memory for the covar matrix */
    covar = gsl_matrix_alloc (2,2);
    
    /* Compute ll partial derivatives. */

    dldg    = -(log(b*Zk)*N-k*log(b*Zk)+((g-log(b))*k-sum)*pow(b*Zk,1./g)+sum+(log(b)-g)*k)/(pow(g,2.)*pow(b*Zk,1./g)-pow(g,2.));
    d2ldg2  = -((pow(b*Zk,1./g)*pow(log(b*Zk),2.)+(2.*g-2.*g*pow(b*Zk,1./g))*log(b*Zk))*N-k*pow(b*Zk,1./g)*pow(log(b*Zk),2.)+(2.*g*k*pow(b*Zk,1./g)-2.*g*k)*log(b*Zk)+(2.*g*sum+(2.*log(b)*g-pow(g,2.))*k)*pow(b*Zk,2./g)+((2.*pow(g,2.)-4*log(b)*g)*k-4*g*sum)*pow(b*Zk,1./g)+2.*g*sum+(2.*log(b)*g-pow(g,2.))*k)/(pow(g,4.)*pow(b*Zk,2./g)-2.*pow(g,4.)*pow(b*Zk,1./g)+pow(g,4.));
    
    
    d2ldgdb = (g*pow(b*Zk,1./g)*(log(b*Zk)*N-k*log(b*Zk)+((g-log(b))*k-sum)*pow(b*Zk,1./g)+sum+(log(b)-g)*k))/(b*pow(pow(g,2.)*pow(b*Zk,1./g)-pow(g,2.),2.))-(N/b+(((g-log(b))*k-sum)*pow(b*Zk,1./g))/(b*g)-(k*pow(b*Zk,1./g))/b)/(pow(g,2.)*pow(b*Zk,1./g)-pow(g,2.));

    dldb = (N-k)/(b*g*pow(b*Zk,1./g)*(1-1/pow(b*Zk,1./g)))-k/(b*g);
    
    d2ldb2=-(N-k)/(pow(b,2.)*g*pow(b*Zk,1./g)*(1-1/pow(b*Zk,1./g)))-(N-k)/(pow(b,2.)*pow(g,2.)*pow(b*Zk,1./g)*(1-1/pow(b*Zk,1./g)))-(N-k)/(pow(b,2.)*pow(g,2.)*pow(b*Zk,2./g)*pow(1-1/pow(b*Zk,1./g),2.))+k/(pow(b,2.)*g);
  

    break;
  case 1:
    /* GSL allocation of memory for the covar matrix */
    covar = gsl_matrix_alloc (2,2);
   
    
    /* Compute ll partial derivatives (Wolfram Mathematica or equivalent).  */
    
    dldg   = -(log(b*d)*N+(1-pow(b*d,1./g))*sum+((pow(b*d,1./g)-1)*g-log(b)*pow(b*d,1./g)-log(b*d)+log(b))*k)/((pow(b*d,1./g)-1)*pow(g,2.0));
    d2ldg2 = (((2*pow(b*d,1./g)*log(b*d)-2*log(b*d))*g-pow(b*d,1./g)*pow(log(b*d),2.0))*N+(-2*pow(b*d,2.0/g)+4*pow(b*d,1./g)-2)*g*sum+((pow(b*d,2.0/g)-2*pow(b*d,1./g)+1)*pow(g,2.0)+(-2*log(b)*pow(b*d,2.0/g)+2*log(b*d)+pow(b*d,1./g)*(4*log(b)-2*log(b*d))-2*log(b))*g+pow(b*d,1./g)*pow(log(b*d),2.0))*k)/((pow(b*d,2.0/g)-2*pow(b*d,1./g)+1)*pow(g,4.0));
    d2ldgdb = -(((pow(b*d,1./g)-1)*g-pow(b*d,1./g)*log(b*d))*N+((pow(b*d,1./g)-pow(b*d,2.0/g))*g+pow(b*d,1./g)*log(b*d))*k)/((b*pow(b*d,2.0/g)-2*b*pow(b*d,1./g)+b)*pow(g,3.0));

    dldb = (N-pow(b*d,1./g)*k)/((b*pow(b*d,1./g)-b)*g);
    d2ldb2 = -(((pow(b*d,1./g)-1)*g+pow(b*d,1./g))*N+((pow(b*d,1./g)-pow(b*d,2.0/g))*g-pow(b*d,1./g))*k)/((pow(b,2.0)*pow(b*d,2.0/g)-2*pow(b,2.0)*pow(b*d,1./g)+pow(b,2.0))*pow(g,2.0));

    break;
  case 2:
    /* GSL allocation of memory for the covar matrix */
    /* Nota that the ll does not depend on b in this case.*/
    /* As a consequence the covar matrix is 1x1 . */
    covar = gsl_matrix_alloc (1,1);
    
    /* Compute ll partial derivatives. */
    dldg   =  -(N*log(ZN)-log(ZNk)*N+k*log(ZNk)-sum+g*k)/pow(g,2.0);
    d2ldg2 =  (2*N*log(ZN)-2*log(ZNk)*N+2*k*log(ZNk)-2*sum+g*k)/pow(g,3.0);
  
    break;
  case 3:
    /* GSL allocation of memory for the covar matrix */
    /* Nota that the ll does not depend on b in this case.*/
    /* As a consequence the covar matrix is 1x1 . */
    covar = gsl_matrix_alloc (1,1);
    
    /* Compute ll partial derivatives. */
  
    dldg = ((log(d)+log(1/ZN))*N+sum+(-g-log(d))*k)/pow(g,2.0);
    
    d2ldg2 = -((2*log(d)+2*log(1/ZN))*N+2*sum+(-g-2*log(d))*k)/pow(g,3.0);
    
   
    break;


  }
  switch(o_varcovar){
  case 0:   
    if (method < 2 ){
      gsl_permutation * P = gsl_permutation_alloc (2);
      int signum;
      gsl_matrix *J = gsl_matrix_calloc (2,2);
    
      gsl_matrix_set (J,0,0,dldg*dldg);
      gsl_matrix_set (J,0,1,dldg*dldb);
      gsl_matrix_set (J,1,0,dldb*dldg);
      gsl_matrix_set (J,1,1,dldg*dldb);

      gsl_linalg_LU_decomp (J,P,&signum);
      gsl_linalg_LU_invert (J,P,covar);
      gsl_matrix_free(J);
      gsl_permutation_free(P);
    }else{
      gsl_permutation * P = gsl_permutation_alloc (1);
      int signum;
      gsl_matrix *J = gsl_matrix_calloc (1,1);
      gsl_matrix_set(J,0,0,dldg*dldg);    
      gsl_linalg_LU_decomp (J,P,&signum);
      gsl_linalg_LU_invert (J,P,covar);
      gsl_matrix_free(J);
      gsl_permutation_free(P);
    }
    break;
  case 1:
    if (method < 2){
      gsl_permutation * P = gsl_permutation_alloc (2);
      int signum;
      gsl_matrix *H = gsl_matrix_calloc (2,2);

      gsl_matrix_set(H,0,0,d2ldg2);
      gsl_matrix_set(H,0,1,d2ldgdb);
      gsl_matrix_set(H,1,0,d2ldgdb);
      gsl_matrix_set(H,1,1,d2ldb2);
      gsl_matrix_scale (H,-1.);
      gsl_linalg_LU_decomp (H,P,&signum);
      gsl_linalg_LU_invert (H,P,covar);

      gsl_matrix_free(H);
      gsl_permutation_free(P);
    }else{
      gsl_permutation * P = gsl_permutation_alloc (1);
      int signum;
      gsl_matrix *H = gsl_matrix_calloc (1,1);
      gsl_matrix_set(H,0,0,d2ldg2);   
      gsl_matrix_scale (H,-1.);
      gsl_linalg_LU_decomp (H,P,&signum);
      gsl_linalg_LU_invert (H,P,covar);
      gsl_matrix_free(H);
      gsl_permutation_free(P);
    }
    break;
  case 2:
    if (method < 2){
      gsl_permutation * P = gsl_permutation_alloc (2);
      int signum;
      gsl_matrix *H   = gsl_matrix_calloc (2,2);
      gsl_matrix *J   = gsl_matrix_calloc (2,2);
      gsl_matrix *tmp = gsl_matrix_calloc (2,2);
      gsl_matrix_set(H,0,0,d2ldg2);
      gsl_matrix_set(H,0,1,d2ldgdb);
      gsl_matrix_set(H,1,0,d2ldgdb);
      gsl_matrix_set(H,1,1,d2ldb2);
      gsl_matrix_set(J,0,0,dldg*dldg);
      gsl_matrix_set(J,0,1,dldg*dldb);
      gsl_matrix_set(J,1,0,dldb*dldg);
      gsl_matrix_set(J,1,1,dldb*dldb);

      gsl_linalg_LU_decomp (H,P,&signum);
      gsl_linalg_LU_invert (H,P,tmp);

      gsl_blas_dgemm (CblasNoTrans,
		      CblasNoTrans,1.0,tmp,J,0.0,H);
      gsl_blas_dgemm (CblasNoTrans,
		      CblasNoTrans,1.0,H,tmp,0.0,covar);
      gsl_matrix_free(H);
      gsl_matrix_free(J);
      gsl_matrix_free(tmp);
      gsl_permutation_free(P);
      
    }else{
      gsl_permutation * P = gsl_permutation_alloc (1);
      int signum;
      gsl_matrix *H   = gsl_matrix_calloc (1,1);
      gsl_matrix *J   = gsl_matrix_calloc (1,1);
      gsl_matrix *tmp = gsl_matrix_calloc (1,1);
      gsl_matrix_set(H,0,0,d2ldg2);
      gsl_matrix_set(J,0,0,dldg*dldg);
      gsl_linalg_LU_decomp (H,P,&signum);
      gsl_linalg_LU_invert (H,P,tmp);

      gsl_blas_dgemm (CblasNoTrans,
		      CblasNoTrans,1.0,tmp,J,0.0,H);
      gsl_blas_dgemm (CblasNoTrans,
		      CblasNoTrans,1.0,H,tmp,0.0,covar);
      gsl_matrix_free(H);
      gsl_matrix_free(J);
      gsl_matrix_free(tmp);
      gsl_permutation_free(P);
    }
    break;
  }

  /* scale the covariance matrix to the appropriate dimension */
  if(covar->size1 == 1)
    {

      const double dtmp1 = gsl_matrix_get(covar,0,0);
      gsl_matrix_free (covar);
      covar = gsl_matrix_alloc (2,2);
      
      gsl_matrix_set(covar,0,0,dtmp1);
      gsl_matrix_set(covar,0,1,NAN);
      gsl_matrix_set(covar,1,0,NAN);
      gsl_matrix_set(covar,1,1,NAN);
      
    }


  return covar;
}
