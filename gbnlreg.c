/*
  gbnlreg (ver. 5.6) -- Non linear regression
  Copyright (C) 2005-2018 Giulio Bottazzi

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  (version 2) as published by the Free Software Foundation;
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#include "tools.h"
#include "matheval.h"
#include "assert.h"

#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_multifit_nlin.h>
#include <gsl/gsl_cdf.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_multimin.h>


/* handy structure used in computation */
typedef struct function {
  void *f;
  size_t argnum;
  char **argname;
  void **df;
  void ***ddf;
} Function;


struct objdata {
  Function * F;
  size_t rows;
  size_t columns;
  double **data;
};


/* Ordinary Least Squares estimation */
/* --------------------------------- */

/* 

   The objective function sumsq= \sum_{i=1}^n f(x_i,\theta )^2, the
   sum of the squared residuals, is minimized. Under the hypothesis of
   normally distributed errors the log-likelihood is 
   LL=-n/2-n*log(sumsq/n)-n*log(sqrt(2*pi))

*/

int
ols_obj_f (const gsl_vector * x, void *params,
	   gsl_vector * f){

  const Function *F = ((struct objdata *)params)->F;
  const size_t rows=((struct objdata *)params)->rows;
  const size_t columns=((struct objdata *)params)->columns;
  double **data=((struct objdata *)params)->data;
  const size_t pnum = x->size;
  double values[pnum+columns];
  size_t i,j;
  
  /* set the parameter values */
  for(i=columns;i<columns+pnum;i++)
    values[i] = gsl_vector_get(x,i-columns);

  for(i=0;i<rows;i++){

    /* set the variables values */
    for(j=0;j<columns;j++)
      values[j] = data[j][i];

    gsl_vector_set (f, i,
		    evaluator_evaluate (F->f,columns+pnum,F->argname,values));
  }

  return GSL_SUCCESS;
}



int
ols_obj_df (const gsl_vector * x, void *params,
       gsl_matrix * J){

  const Function *F = ((struct objdata *)params)->F;
  const size_t rows=((struct objdata *)params)->rows;
  const size_t columns=((struct objdata *)params)->columns;
  double **data=((struct objdata *)params)->data;
  const size_t pnum = x->size;
  double values[pnum+columns];
  size_t i,j;

  for(i=columns;i<columns+pnum;i++)
    values[i] = gsl_vector_get(x,i-columns);

  for(i=0;i<rows;i++){
    
    for(j=0;j<columns;j++)
      values[j] = data[j][i];

    for(j=0;j<pnum;j++){
      gsl_matrix_set (J, i, j,
		      evaluator_evaluate ((F->df)[j],columns+pnum,F->argname,values));
    }
  }

  return GSL_SUCCESS;
}



int
ols_obj_fdf (const gsl_vector * x, void *params,
	  gsl_vector * f, gsl_matrix * J){


  const Function *F = ((struct objdata *)params)->F;
  const size_t rows=((struct objdata *)params)->rows;
  const size_t columns=((struct objdata *)params)->columns;
  double **data=((struct objdata *)params)->data;
  const size_t pnum = x->size;
  double values[pnum+columns];
  size_t i,j;

  for(i=columns;i<columns+pnum;i++)
    values[i] = gsl_vector_get(x,i-columns);

  for(i=0;i<rows;i++){
    
    for(j=0;j<columns;j++)
      values[j] = data[j][i];

    gsl_vector_set (f, i,
		    evaluator_evaluate (F->f,columns+pnum,F->argname,values));

/*     for(j=0;j<columns+pnum;j++) */
/*       printf("%s=%f ",F->argname[j],values[j]); */
/*     printf("-> %f\n",evaluator_evaluate (F->f,columns+pnum,F->argname,values)); */

    for(j=0;j<pnum;j++){
      gsl_matrix_set (J, i, j,evaluator_evaluate ((F->df)[j],columns+pnum,F->argname,values));
    }

  }

  return GSL_SUCCESS;

}


/* compute variance-covariance matrix */
gsl_matrix * ols_varcovar(const int o_varcovar,
			  gsl_multifit_fdfsolver *s,
			  const struct objdata obj,
			  const size_t Pnum){

  size_t i,j,h;

  const size_t rows = obj.rows;
  const size_t columns = obj.columns;
  double **data = obj.data;
  const Function *F = obj.F;
  const double sumsq = pow(gsl_blas_dnrm2(s->f),2.);
  const double sigma2 = sumsq/(rows-Pnum);

  gsl_matrix *covar = gsl_matrix_alloc (Pnum,Pnum);

  switch(o_varcovar){
  case 0: /* inverse "reduced Hessian", as in Numerical Recipes */
    {

#ifdef GSL_VER_2
      gsl_matrix *J = gsl_matrix_alloc(rows, Pnum); 
      
      gsl_multifit_fdfsolver_jac(s, J); 
#else
      gsl_matrix *J = s->J;
#endif

      gsl_multifit_covar (J, 1e-6, covar);

      gsl_matrix_scale (covar,sigma2);

#ifdef GSL_VER_2
      gsl_matrix_free (J); 
#endif
      
    }
    break;
  case 1: /* J^{-1} */
    {
      
      gsl_permutation * P = gsl_permutation_alloc (Pnum);
      int signum;
      
      gsl_matrix *J = gsl_matrix_calloc (Pnum,Pnum);
      gsl_matrix *dJ = gsl_matrix_calloc (Pnum,Pnum);
      
      double f,df1,df2;
      
      double values[columns+Pnum];
      
      /* fill with optimal parameter */
      for(i=columns;i<columns+Pnum;i++)
	values[i] = gsl_vector_get(s->x,i-columns);
      
      /* compute information matrix */
      for(i=0;i<rows;i++){
	
	for(j=0;j<columns;j++)
	  values[j] = data[j][i];
	
	f = evaluator_evaluate (F->f,columns+Pnum,F->argname,values);
	
	for(j=0;j<Pnum;j++){
	  df1 = evaluator_evaluate ((F->df)[j],columns+Pnum,F->argname,values);
	  for(h=0;h<Pnum;h++){
	    df2 = evaluator_evaluate ((F->df)[h],columns+Pnum,F->argname,values);
	    gsl_matrix_set (dJ,j,h,df1*df2*f*f);
	  }
	}
      
	gsl_matrix_add (J,dJ);
      }
      /* invert information matrix; dJ store temporary LU decomp. */
      gsl_matrix_memcpy (dJ,J);
      gsl_linalg_LU_decomp (dJ,P,&signum);
      gsl_linalg_LU_invert (dJ,P,covar);
      
      
      gsl_matrix_free(J);
      gsl_matrix_free(dJ);
      gsl_permutation_free(P);

      gsl_matrix_scale (covar,sigma2*sigma2);
      
    }
    break;
    
  case 2: /* H^{-1} */
    {
      gsl_permutation * P = gsl_permutation_alloc (Pnum);
      int signum;
      
      gsl_matrix *H = gsl_matrix_calloc (Pnum,Pnum);
      gsl_matrix *dH = gsl_matrix_calloc (Pnum,Pnum);
      
      double f,df1,df2,ddf;

      double values[columns+Pnum];
      
      /* fill with optimal parameter */
      for(i=columns;i<columns+Pnum;i++)
	values[i] = gsl_vector_get(s->x,i-columns);
      
      /* compute Hessian */
      for(i=0;i<rows;i++){
	
	for(j=0;j<columns;j++)
	  values[j] = data[j][i];
	
	f = evaluator_evaluate (F->f,columns+Pnum,F->argname,values);
	
	for(j=0;j<Pnum;j++){
	  df1 = evaluator_evaluate ((F->df)[j],columns+Pnum,F->argname,values);
	  for(h=0;h<Pnum;h++){
	    df2 = evaluator_evaluate ((F->df)[h],columns+Pnum,F->argname,values);
	    ddf = evaluator_evaluate ((F->ddf)[j][h],columns+Pnum,F->argname,values);
	    gsl_matrix_set (dH,j,h,df1*df2+f*ddf);
	  }
	}
	gsl_matrix_add (H,dH);
      }
	
      /* invert Hessian; dH store temporary LU decomp. */
      gsl_matrix_memcpy (dH,H);
      gsl_linalg_LU_decomp (dH,P,&signum);
      gsl_linalg_LU_invert (dH,P,covar);
      
      
      gsl_matrix_free(H);
      gsl_matrix_free(dH);
      gsl_permutation_free(P);

      gsl_matrix_scale (covar,sigma2);

    }
    break;
    
  case 3: /* H^{-1} J H^{-1} */
    {
      
      gsl_permutation * P = gsl_permutation_alloc (Pnum);
      int signum;
      
      gsl_matrix *H = gsl_matrix_calloc (Pnum,Pnum);
      gsl_matrix *dH = gsl_matrix_calloc (Pnum,Pnum);
      
      gsl_matrix *J = gsl_matrix_calloc (Pnum,Pnum);
      gsl_matrix *dJ = gsl_matrix_calloc (Pnum,Pnum);
      
      double f,df1,df2,ddf;
      
      double values[columns+Pnum];
      
      /* fill with optimal parameter */
      for(i=columns;i<columns+Pnum;i++)
	values[i] = gsl_vector_get(s->x,i-columns);
      
      /* compute Hessian and information matrix */
      for(i=0;i<rows;i++){
	
	for(j=0;j<columns;j++)
	  values[j] = data[j][i];
	
	f = evaluator_evaluate (F->f,columns+Pnum,F->argname,values);
	
	for(j=0;j<Pnum;j++){
	  df1 = evaluator_evaluate ((F->df)[j],columns+Pnum,F->argname,values);
	  for(h=0;h<Pnum;h++){
	    df2 = evaluator_evaluate ((F->df)[h],columns+Pnum,F->argname,values);
	    ddf = evaluator_evaluate ((F->ddf)[j][h],columns+Pnum,F->argname,values);
	    gsl_matrix_set (dH,j,h,df1*df2+f*ddf);
	    gsl_matrix_set (dJ,j,h,df1*df2*f*f);
	  }
	}
	gsl_matrix_add (H,dH);
	gsl_matrix_add (J,dJ);
      }
      
      /* invert Hessian; dH store temporary LU decomp. */
      gsl_matrix_memcpy (dH,H);
      gsl_linalg_LU_decomp (dH,P,&signum);
      gsl_linalg_LU_invert (dH,P,H);
      
      /* dJ = H^{-1} J ; covar = dJ H^{-1} */
      gsl_blas_dgemm (CblasNoTrans,
		      CblasNoTrans,1.0,H,J,0.0,dJ);
      gsl_blas_dgemm (CblasNoTrans,
		      CblasNoTrans,1.0,dJ,H,0.0,covar);
      
      
      gsl_matrix_free(H);
      gsl_matrix_free(dH);
      gsl_matrix_free(J);
      gsl_matrix_free(dJ);
      gsl_permutation_free(P);
      
    }
    break;
    
  }

  return covar;
}


/* Minimum Absolute Deviation (MAD) estimation */
/* ------------------------------------------- */

/*

  The distribution considered is
   
   f(x) = 1/a exp( (x-m)/a )
   
   the objective function becomes obj = 1/n \sum_i |x_i-m| .  The
   log-likelihood reads LL= -n*( 1+ log(2*obj) ).

*/


double
mad_obj_f (const gsl_vector * x, void *params){

  const Function *F = ((struct objdata *)params)->F;
  const size_t rows=((struct objdata *)params)->rows;
  const size_t columns=((struct objdata *)params)->columns;
  double **data=((struct objdata *)params)->data;
  const size_t pnum = x->size;
  double values[pnum+columns];
  size_t i,j;
  double res=0;

  /* set the parameter values */
  for(i=columns;i<columns+pnum;i++)
    values[i] = gsl_vector_get(x,i-columns);

  for(i=0;i<rows;i++){

    /* set the variables values */
    for(j=0;j<columns;j++)
      values[j] = data[j][i];

    res += fabs(evaluator_evaluate (F->f,columns+pnum,F->argname,values));
  }

  res/=rows;

  return res;
}


/* compute variance-covariance matrix */
gsl_matrix *mad_varcovar(const int o_varcovar,
			 const gsl_multimin_fminimizer *s,
			 const struct objdata params,
			 const size_t Pnum)
{

  size_t i,j,h;
  
  const size_t rows=((struct objdata )params).rows;
  const size_t columns=((struct objdata )params).columns;
  double **data=((struct objdata )params).data;
  const Function *F = ((struct objdata )params).F;
  const double a = s->fval;

  gsl_matrix *covar = gsl_matrix_alloc (Pnum,Pnum);

  switch(o_varcovar){
  case 0: /* inverse "reduced Hessian", as in Numerical Recipes */
  case 1: /* J^{-1} */
    {

      gsl_permutation * P = gsl_permutation_alloc (Pnum);
      int signum;

      gsl_matrix *J = gsl_matrix_calloc (Pnum,Pnum);
      gsl_matrix *dJ = gsl_matrix_calloc (Pnum,Pnum);

      double df1,df2;

      double values[columns+Pnum];

      /* fill with optimal parameter */
      for(i=columns;i<columns+Pnum;i++)
	values[i] = gsl_vector_get(s->x,i-columns);
	
      /* compute information matrix */
      for(i=0;i<rows;i++){
	  
	for(j=0;j<columns;j++)
	  values[j] = data[j][i];

	for(j=0;j<Pnum;j++){
	  df1 = evaluator_evaluate ((F->df)[j],columns+Pnum,F->argname,values);
	  for(h=0;h<Pnum;h++){
	    df2 = evaluator_evaluate ((F->df)[h],columns+Pnum,F->argname,values);
	    gsl_matrix_set (dJ,j,h,df1*df2);
	  }
	}
	gsl_matrix_add (J,dJ);
      }
	

      /* invert information matrix; dJ store temporary LU decomp. */
      gsl_matrix_memcpy (dJ,J);
      gsl_linalg_LU_decomp (dJ,P,&signum);
      gsl_linalg_LU_invert (dJ,P,covar);



      gsl_matrix_scale (covar,a*a);

      gsl_matrix_free(J);
      gsl_matrix_free(dJ);
      gsl_permutation_free(P);

    }
    break;

  case 2: /* H^{-1} */
    {
      gsl_permutation * P = gsl_permutation_alloc (Pnum);
      int signum;

      gsl_matrix *H = gsl_matrix_calloc (Pnum,Pnum);
      gsl_matrix *dH = gsl_matrix_calloc (Pnum,Pnum);

      double f,df1,df2,ddf;

      double values[columns+Pnum];

      /* fill with optimal parameter */
      for(i=columns;i<columns+Pnum;i++)
	values[i] = gsl_vector_get(s->x,i-columns);
	
      /* compute Hessian */
      for(i=0;i<rows;i++){
	  
	for(j=0;j<columns;j++)
	  values[j] = data[j][i];

	f = evaluator_evaluate (F->f,columns+Pnum,F->argname,values);
	  
	for(j=0;j<Pnum;j++){
	  df1 = evaluator_evaluate ((F->df)[j],columns+Pnum,F->argname,values);
	  for(h=0;h<Pnum;h++){
	    df2 = evaluator_evaluate ((F->df)[h],columns+Pnum,F->argname,values);
	    ddf = evaluator_evaluate ((F->ddf)[j][h],columns+Pnum,F->argname,values);
	    gsl_matrix_set (dH,j,h,df1*df2+(f>0 ? 1. : -1.)*a*ddf);
	  }
	}
	gsl_matrix_add (H,dH);
      }

	
      /* invert Hessian; dH store temporary LU decomp. */
      gsl_matrix_memcpy (dH,H);
      gsl_linalg_LU_decomp (dH,P,&signum);
      gsl_linalg_LU_invert (dH,P,covar);


      gsl_matrix_scale (covar,a*a);


      gsl_matrix_free(H);
      gsl_matrix_free(dH);
      gsl_permutation_free(P);
    }
    break;

  case 3: /* H^{-1} J H^{-1} */
    {

      gsl_permutation * P = gsl_permutation_alloc (Pnum);
      int signum;

      gsl_matrix *H = gsl_matrix_calloc (Pnum,Pnum);
      gsl_matrix *dH = gsl_matrix_calloc (Pnum,Pnum);

      gsl_matrix *J = gsl_matrix_calloc (Pnum,Pnum);
      gsl_matrix *dJ = gsl_matrix_calloc (Pnum,Pnum);

      double f,df1,df2,ddf;

      double values[columns+Pnum];

      /* fill with optimal parameter */
      for(i=columns;i<columns+Pnum;i++)
	values[i] = gsl_vector_get(s->x,i-columns);
	
      /* compute Hessian and information matrix */
      for(i=0;i<rows;i++){
	  
	for(j=0;j<columns;j++)
	  values[j] = data[j][i];

	f = evaluator_evaluate (F->f,columns+Pnum,F->argname,values);
	  
	for(j=0;j<Pnum;j++){
	  df1 = evaluator_evaluate ((F->df)[j],columns+Pnum,F->argname,values);
	  for(h=0;h<Pnum;h++){
	    df2 = evaluator_evaluate ((F->df)[h],columns+Pnum,F->argname,values);
	    ddf = evaluator_evaluate ((F->ddf)[j][h],columns+Pnum,F->argname,values);
	    gsl_matrix_set (dH,j,h,df1*df2+(f>0 ? 1. : -1.)*a*ddf);
	    gsl_matrix_set (dJ,j,h,df1*df2);
	  }
	}
	gsl_matrix_add (H,dH);
	gsl_matrix_add (J,dJ);
      }
	
      /* invert Hessian; dH store temporary LU decomp. */
      gsl_matrix_memcpy (dH,H);
      gsl_linalg_LU_decomp (dH,P,&signum);
      gsl_linalg_LU_invert (dH,P,H);

      /* dJ = H^{-1} J ; covar = dJ H^{-1} */
      gsl_blas_dgemm (CblasNoTrans,
		      CblasNoTrans,1.0,H,J,0.0,dJ);
      gsl_blas_dgemm (CblasNoTrans,
		      CblasNoTrans,1.0,dJ,H,0.0,covar);

      gsl_matrix_scale (covar,a*a);

      gsl_matrix_free(H);
      gsl_matrix_free(dH);
      gsl_matrix_free(J);
      gsl_matrix_free(dJ);
      gsl_permutation_free(P);

    }
    break;

  }

  return covar;

}


/* Asymmetric Minimum Absolute Deviation (AMAD) estimation */
/* ------------------------------------------------------- */


/*

  The distribution considered is
   
                     /  exp( (m-x)/al)  if x<0
   f(x) = 1/(al+ar) |
                     \  exp( (x-m)/ar ) if x>0
   
   the objective function becomes obj = \sqrt{sumL/nL}+\sqrt{sumR/nR} with

   sumL = \sum{x_i<0} -x_i ; nL = \sum{x_i<0} 1
   sumL = \sum{x_i>0} x_i ; nL = \sum{x_i>0} 1

   The log-likelihood reads LL= -n*( 1+2 log(obj) ).

*/

double
amad_obj_f (const gsl_vector * x, void *params){

  const Function *F = ((struct objdata *)params)->F;
  const size_t rows=((struct objdata *)params)->rows;
  const size_t columns=((struct objdata *)params)->columns;
  double **data=((struct objdata *)params)->data;
  const size_t pnum = x->size;
  double values[pnum+columns];
  size_t i,j;
  double sumL=0,sumR=0;

  /* set the parameter values */
  for(i=columns;i<columns+pnum;i++)
    values[i] = gsl_vector_get(x,i-columns);

  for(i=0;i<rows;i++){

    /* set the variables values */
    for(j=0;j<columns;j++)
      values[j] = data[j][i];

    const double dtmp1 = evaluator_evaluate (F->f,columns+pnum,F->argname,values);
    if(dtmp1>0) sumR += dtmp1;
    else sumL += -dtmp1;
  }
  sumL/=rows;
  sumR/=rows;

  return sqrt(sumL)+sqrt(sumR);
}


/* compute variance-covariance matrix */
gsl_matrix *amad_varcovar(const int o_varcovar,
			  const gsl_multimin_fminimizer *s,
			  const struct objdata params,
			  const size_t Pnum)
{

  size_t i,j,h;

  const size_t rows=((struct objdata )params).rows;
  const size_t columns=((struct objdata )params).columns;
  double **data=((struct objdata )params).data;
  const Function *F = ((struct objdata )params).F;
  double values[columns+Pnum];
      

  double al,ar;

  gsl_matrix *covar = gsl_matrix_alloc (Pnum,Pnum);

  /* compute al,ar at the minimum */
  {
    double sumL=0,sumR=0;

    /* set the parameter values */
    for(i=columns;i<columns+Pnum;i++)
      values[i] = gsl_vector_get(s->x,i-columns);

    /* set the variables values */
    for(i=0;i<rows;i++){    
      for(j=0;j<columns;j++)
	values[j] = data[j][i];
    
      const double dtmp1 = evaluator_evaluate (F->f,columns+Pnum,F->argname,values);
      if(dtmp1>0) sumR += dtmp1;
      else sumL += -dtmp1;
    }

    sumL/=rows;
    sumR/=rows;
  
    al = sumL+sqrt(sumL*sumR);
    ar = sumR+sqrt(sumL*sumR);
  }

  switch(o_varcovar){
  case 0: /* inverse "reduced Hessian", adapted following Numerical Recipes */
    {
      gsl_permutation * P = gsl_permutation_alloc (Pnum);
      int signum;

      gsl_matrix *H = gsl_matrix_calloc (Pnum,Pnum);
      gsl_matrix *dH = gsl_matrix_calloc (Pnum,Pnum);

      double df1,df2;

      /* fill with optimal parameter */
      for(i=columns;i<columns+Pnum;i++)
	values[i] = gsl_vector_get(s->x,i-columns);
	
      /* compute Hessian */
      for(i=0;i<rows;i++){
	  
	for(j=0;j<columns;j++)
	  values[j] = data[j][i];

	for(j=0;j<Pnum;j++){
	  df1 = evaluator_evaluate ((F->df)[j],columns+Pnum,F->argname,values);
	  for(h=0;h<Pnum;h++){
	    df2 = evaluator_evaluate ((F->df)[h],columns+Pnum,F->argname,values);
	    gsl_matrix_set (dH,j,h,df1*df2);
	  }
	}
	gsl_matrix_add (H,dH);
      }

	
      /* invert Hessian; dH store temporary LU decomp. */
      gsl_matrix_memcpy (dH,H);
      gsl_linalg_LU_decomp (dH,P,&signum);
      gsl_linalg_LU_invert (dH,P,covar);


      gsl_matrix_scale (covar,al*ar);


      gsl_matrix_free(H);
      gsl_matrix_free(dH);
      gsl_permutation_free(P);
    }
    break;
  case 1: /* J^{-1} */
    {

      gsl_permutation * P = gsl_permutation_alloc (Pnum);
      int signum;

      gsl_matrix *J = gsl_matrix_calloc (Pnum,Pnum);
      gsl_matrix *dJ = gsl_matrix_calloc (Pnum,Pnum);

      double f,df1,df2;

      /* fill with optimal parameter */
      for(i=columns;i<columns+Pnum;i++)
	values[i] = gsl_vector_get(s->x,i-columns);
	
      /* compute information matrix */
      for(i=0;i<rows;i++){
	  
	for(j=0;j<columns;j++)
	  values[j] = data[j][i];

	f = evaluator_evaluate (F->f,columns+Pnum,F->argname,values);

	for(j=0;j<Pnum;j++){
	  df1 = evaluator_evaluate ((F->df)[j],columns+Pnum,F->argname,values);
	  for(h=0;h<Pnum;h++){
	    df2 = evaluator_evaluate ((F->df)[h],columns+Pnum,F->argname,values);
	    gsl_matrix_set (dJ,j,h,df1*df2/(f>0 ? ar*ar : al*al));
	  }
	}
	gsl_matrix_add (J,dJ);
      }
	

      /* invert information matrix; dJ store temporary LU decomp. */
      gsl_matrix_memcpy (dJ,J);
      gsl_linalg_LU_decomp (dJ,P,&signum);
      gsl_linalg_LU_invert (dJ,P,covar);


      gsl_matrix_free(J);
      gsl_matrix_free(dJ);
      gsl_permutation_free(P);

    }
    break;

  case 2: /* H^{-1} */
    {
      gsl_permutation * P = gsl_permutation_alloc (Pnum);
      int signum;

      gsl_matrix *H = gsl_matrix_calloc (Pnum,Pnum);
      gsl_matrix *dH = gsl_matrix_calloc (Pnum,Pnum);

      double f,df1,df2,ddf;

      /* fill with optimal parameter */
      for(i=columns;i<columns+Pnum;i++)
	values[i] = gsl_vector_get(s->x,i-columns);
	
      /* compute Hessian */
      for(i=0;i<rows;i++){
	  
	for(j=0;j<columns;j++)
	  values[j] = data[j][i];

	f = evaluator_evaluate (F->f,columns+Pnum,F->argname,values);
	  
	for(j=0;j<Pnum;j++){
	  df1 = evaluator_evaluate ((F->df)[j],columns+Pnum,F->argname,values);
	  for(h=0;h<Pnum;h++){
	    df2 = evaluator_evaluate ((F->df)[h],columns+Pnum,F->argname,values);
	    ddf = evaluator_evaluate ((F->ddf)[j][h],columns+Pnum,F->argname,values);
	    gsl_matrix_set (dH,j,h,df1*df2+(f>0 ? al : -ar)*ddf);
	  }
	}
	gsl_matrix_add (H,dH);
      }

	
      /* invert Hessian; dH store temporary LU decomp. */
      gsl_matrix_memcpy (dH,H);
      gsl_linalg_LU_decomp (dH,P,&signum);
      gsl_linalg_LU_invert (dH,P,covar);


      gsl_matrix_scale (covar,al*ar);


      gsl_matrix_free(H);
      gsl_matrix_free(dH);
      gsl_permutation_free(P);
    }
    break;

  case 3: /* H^{-1} J H^{-1} */
    {

      gsl_permutation * P = gsl_permutation_alloc (Pnum);
      int signum;

      gsl_matrix *H = gsl_matrix_calloc (Pnum,Pnum);
      gsl_matrix *dH = gsl_matrix_calloc (Pnum,Pnum);

      gsl_matrix *J = gsl_matrix_calloc (Pnum,Pnum);
      gsl_matrix *dJ = gsl_matrix_calloc (Pnum,Pnum);

      double f,df1,df2,ddf;

      /* fill with optimal parameter */
      for(i=columns;i<columns+Pnum;i++)
	values[i] = gsl_vector_get(s->x,i-columns);
	
      /* compute Hessian and information matrix */
      for(i=0;i<rows;i++){
	  
	for(j=0;j<columns;j++)
	  values[j] = data[j][i];

	f = evaluator_evaluate (F->f,columns+Pnum,F->argname,values);
	  
	for(j=0;j<Pnum;j++){
	  df1 = evaluator_evaluate ((F->df)[j],columns+Pnum,F->argname,values);
	  for(h=0;h<Pnum;h++){
	    df2 = evaluator_evaluate ((F->df)[h],columns+Pnum,F->argname,values);
	    ddf = evaluator_evaluate ((F->ddf)[j][h],columns+Pnum,F->argname,values);
	    gsl_matrix_set (dH,j,h,df1*df2/(al*ar)+ddf/(f>0 ? ar : -al));
	    gsl_matrix_set (dJ,j,h,df1*df2/(f>0 ? ar*ar : al*al));
	  }
	}
	gsl_matrix_add (H,dH);
	gsl_matrix_add (J,dJ);
      }

	
      /* invert Hessian; dH store temporary LU decomp. */
      gsl_matrix_memcpy (dH,H);
      gsl_linalg_LU_decomp (dH,P,&signum);
      gsl_linalg_LU_invert (dH,P,H);

      /* dJ = H^{-1} J ; covar = dJ H^{-1} */
      gsl_blas_dgemm (CblasNoTrans,
		      CblasNoTrans,1.0,H,J,0.0,dJ);
      gsl_blas_dgemm (CblasNoTrans,
		      CblasNoTrans,1.0,dJ,H,0.0,covar);


      gsl_matrix_free(H);
      gsl_matrix_free(dH);
      gsl_matrix_free(J);
      gsl_matrix_free(dJ);
      gsl_permutation_free(P);

    }
    break;

  }

  return covar;

}



int main(int argc,char* argv[]){

  int i;

  char *splitstring = strdup(" \t");
  int o_verbose=0;
  int o_output=0;
  int o_varcovar=0;
  int o_method=0;

  /* data from sdtin */
  size_t rows=0,columns=0;
  double **data=NULL;

  /* definition of the function */
  Function F;
  char **Param=NULL;
  double *Pval=NULL;
  gsl_matrix *Pcovar=NULL;  /* covariance matrix */
  size_t Pnum=0;

  /* minimization tolerance  */
  double eps=1e-5;
  size_t maxiter=500;
  double restart_accuracy=1e-5;
  size_t restart_maxiter=2000;

  /* variables for reading command line options */
  /* ------------------------------------------ */
  int opt;
  /* ------------------------------------------ */

  /* initialize global variables */
  initialize_program(argv[0]);

  /* COMMAND LINE PROCESSING */
  while((opt=getopt_long(argc,argv,"v:hF:O:V:M:e:E:I:i:",gb_long_options, &gb_option_index))!=EOF){
    if(opt==0){
      gbutils_header(argv[0],stdout);
      exit(0);
    }
    else if(opt=='?'){
      fprintf(stderr,"option %c not recognized\n",optopt);
      exit(-1);
    }
    else if(opt=='h'){
      /*print help*/
      fprintf(stdout,"Non linear least square regression. Read data in columns (X_1 .. X_N).\n");
      fprintf(stdout,"The model is specified by a function f(x1,x2...) using variables names\n");
      fprintf(stdout,"x1,x2,..,xN for the first, second .. N-th column of data. f(x1,x2,...)\n");
      fprintf(stdout,"are assumed i.i.d.\n");
      fprintf(stdout,"\nUsage: %s [options] <function definition> \n\n",argv[0]);
      fprintf(stdout,"Options:\n");
      fprintf(stdout," -O  type of output (default 0)\n");
      fprintf(stdout,"      0  parameters                     \n");
      fprintf(stdout,"      1  parameters and errors          \n");
      fprintf(stdout,"      2  <variables> and residuals      \n");
      fprintf(stdout,"      3  parameters and variance matrix \n");
      fprintf(stdout,"      4  parameters, errors and s-scores\n");
      fprintf(stdout," -V  variance matrix estimation (default 0)\n");
      fprintf(stdout,"      0  <gradF gradF^t>                \n");
      fprintf(stdout,"      1  < J^{-1} >                    \n");
      fprintf(stdout,"      2  < H^{-1} >                     \n");
      fprintf(stdout,"      3  < H^{-1} J H^{-1} >           \n");
      fprintf(stdout," -v  verbosity level (default 0)\n");
      fprintf(stdout,"      0  just results                   \n");
      fprintf(stdout,"      1  comment headers                \n");
      fprintf(stdout,"      2  summary statistics             \n");
      fprintf(stdout,"      3  covariance matrix              \n");
      fprintf(stdout,"      4  minimization steps             \n");
      fprintf(stdout,"      5  model definition               \n");
      fprintf(stdout," -M  estimation method (default 0)\n");
      fprintf(stdout,"      0  ordinary least square  (OLS)   \n");
      fprintf(stdout,"      1  minimum absolute deviation (MAD)\n");
      fprintf(stdout,"      2  asymmetric MAD                  \n");
      fprintf(stdout," -e  minimization tolerance (default 1e-5)\n");
      fprintf(stdout," -i  minimization max iterations (default 500) \n");
      fprintf(stdout," -E  restart accuracy (default 1e-5, ignored with option -M 0)\n");
      fprintf(stdout," -I  restart max iterations (default 2000, ignored with option -M 0)\n");
      fprintf(stdout," -F  input fields separators (default \" \\t\")\n");
      fprintf(stdout," -h  this help               \n");
      exit(0);
    }
    else if(opt=='e'){
      /* set the minimization tolerance */
      eps = fabs(atof(optarg));
    }
    else if(opt=='E'){
      /* set the accuracy of the simplex (method 1 and 2) */
      restart_accuracy = fabs(atof(optarg));
    }
    else if(opt=='i'){
      /* maximum number of iterations for the optimizer */
      maxiter = atoi(optarg);
    }
    else if(opt=='I'){
      /* maximum number of iterations for restarting */
      restart_maxiter = atoi(optarg);
    }
    else if(opt=='F'){
      /*set the fields separator string*/
      free(splitstring);
      splitstring = strdup(optarg);
    }
    else if(opt=='M'){
      /*set the estimation method*/
      o_method = atoi(optarg);
      if(o_method<0 || o_method>2){
	fprintf(stderr,"ERROR (%s): method option '%d' not recognized. Try option -h.\n",
		GB_PROGNAME,o_method);
	exit(-1);
      }
    }
    else if(opt=='O'){
      /* set the type of output */
      o_output = atoi(optarg);
      if(o_output<0 || o_output>4){
	fprintf(stderr,"ERROR (%s): output option '%d' not recognized. Try option -h.\n",
		GB_PROGNAME,o_output);
	exit(-1);
      }
    }
    else if(opt=='V'){
      /* set the type of covariance */
      o_varcovar = atoi(optarg);
      if(o_varcovar<0 || o_varcovar>3){
	fprintf(stderr,"ERROR (%s): variance option '%d' not recognized. Try option -h.\n",
		GB_PROGNAME,o_varcovar);
	exit(-1);
      }
    }
    else if(opt=='v'){
      o_verbose = atoi(optarg);
    }
  }
  /* END OF COMMAND LINE PROCESSING */


  /* load data */
  loadtable(&data,&rows,&columns,0,splitstring);
  

  /* parse line for functions and variables specification */
  if(optind == argc){
    fprintf(stderr,"ERROR (%s): please provide a function to fit.\n",
	    GB_PROGNAME);
    exit(-1);
  }
    
  for(i=optind;i<argc;i++){
    char *piece=strdup (argv[i]);
    char *stmp1=piece;
    char *stmp2;
    
    while( (stmp2=strsep (&stmp1,",")) != NULL ){/*take a piece*/
      char *stmp3 = strdup(stmp2);
      char *stmp4 = stmp3;
      char *stmp5;
      /* 	fprintf(stderr,"token:%s\n",stmp3); */

      /* initial condition */
      if( (stmp5=strsep(&stmp4,"=")) != NULL && stmp4 != NULL ){
	if( strlen(stmp5)>0 && strlen(stmp4)>0){
	  Pnum++;
	  Param=(char **) my_realloc((void *)  Param,Pnum*sizeof(char *));
	  Pval=(double *) my_realloc((void *) Pval,Pnum*sizeof(double));
	  
	  Param[Pnum-1] = strdup(stmp5);
	  Pval[Pnum-1] = atof(stmp4);
	}
      }
      else{ /* allocate new function */
	F.f = evaluator_create (stmp3);
	assert(F.f);
      }
      free(stmp3);
    }
    free(piece);
  }


  /* check that everything is correct */
  if(Pnum==0){
    fprintf(stderr,"ERROR (%s): please provide a parameter to estimate.\n",
	    GB_PROGNAME);
    exit(-1);
  }


  {
    size_t i,j;
    char **NewParam=NULL;
    double *NewPval=NULL;
    size_t NewPnum=0;

    char **storedname=NULL;
    size_t storednum=0;

    /* retrive list of arguments and their number */
    /* notice that storedname is not allocated */
    {
      int argnum;
      evaluator_get_variables (F.f,&storedname,&argnum);
      storednum = (size_t) argnum;
    }

    /* check the definition of the function */
    for(i=0;i<storednum;i++){
      char *stmp1 = storedname[i];
      if(*stmp1 == 'x'){
	size_t index = (atoi(stmp1+1)>0?atoi(stmp1+1):0);
	if(index>columns){
	  fprintf(stderr,"ERROR (%s): column %zd not present in data\n",
		  GB_PROGNAME,index);
	  exit(-1);
	}
      }
      else {
	for(j=0;j<Pnum;j++)
	  if( strcmp(Param[j],stmp1)==0 ) break;
	if(j==Pnum){
	  fprintf(stderr,"ERROR (%s): parameter %s without initial value\n",
		  GB_PROGNAME,stmp1);
	  exit(-1);
	}
      }
    }

    /* remove unnecessary parameters */
    for(i=0;i<Pnum;i++){
      for(j=0;j<storednum;j++)
	if( strcmp(Param[i],storedname[j])==0 ) break;
      if(j==storednum){
	fprintf(stderr,"WARNING (%s): irrelevant parameter %s removed\n",
		GB_PROGNAME,Param[i]);
	continue;
      }
      NewPnum++;
      NewParam=(char **) my_realloc((void *)  NewParam,NewPnum*sizeof(char *));
      NewPval=(double *) my_realloc((void *) NewPval,NewPnum*sizeof(double));
      NewParam[NewPnum-1] = strdup(Param[i]);
      NewPval[NewPnum-1] = Pval[i];
    }

    for(i=0;i<Pnum;i++)
      free(Param[i]);
    free(Param);
    free(Pval);
    
    Param = NewParam;
    Pval = NewPval;
    Pnum= NewPnum;


    /* prepare the new list of argument names */
    F.argnum=columns+Pnum;
    F.argname = (char **) my_alloc((columns+Pnum)*sizeof(char *));
    
    for(i=0;i<columns;i++){

      int length;
      length = snprintf(NULL,0,"x%zd",i+1);
      F.argname[i] = (char *) my_alloc((length+1)*sizeof(char));
      snprintf(F.argname[i],length+1,"x%zd",i+1);

    }
    for(i=columns;i<columns+Pnum;i++){

      int length;
      length = snprintf(NULL,0,"%s",Param[i-columns]);
      F.argname[i] = (char *) my_alloc((length+1)*sizeof(char));
      snprintf(F.argname[i],length+1,"%s",Param[i-columns]);

    }

    /* define first order derivatives */
    F.df = (void **) my_alloc(Pnum*sizeof(void *));
    for(i=0;i<Pnum;i++){
      F.df[i] = evaluator_derivative (F.f,Param[i]);
      assert(F.df[i]);
    }

    /* define second order derivatives */
    if(o_varcovar>1){
      F.ddf = (void ***) my_alloc(Pnum*sizeof(void **));
      for(i=0;i<Pnum;i++)
	F.ddf[i] = (void **) my_alloc(Pnum*sizeof(void *));
      
      for(i=0;i<Pnum;i++)
	for(j=0;j<Pnum;j++){
	  F.ddf[i][j] = evaluator_derivative ((F.df)[i],Param[j]);
	  assert((F.ddf)[i][j]);
	}
    }

  }
  
  /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
  /* print builded functions and variables */
  if(o_verbose>4){
    size_t i,j;

    fprintf(stderr," ------------------------------------------------------------\n");

    fprintf(stderr,"  Model [xi: i-th data column]:\n");
    switch(o_method){
    case 0:
      fprintf(stderr,"     %s ~ i.i.d. N(0,stdev) \n", evaluator_get_string (F.f) );
      break;
    case 1:
      fprintf(stderr,"     %s ~ i.i.d. L(0,sdev) \n", evaluator_get_string (F.f) );
      break;
    case 2:
      fprintf(stderr,"     %s ~ i.i.d. L(0,a_l,a_r) \n", evaluator_get_string (F.f) );
      break;
    }
    fprintf (stderr,"\n  Parameters and initial conditions:\n");
    for(i=0;i<Pnum;i++)
      fprintf (stderr,"      %s = %f\n", Param[i],Pval[i]);

    fprintf (stderr,"\n  Model first derivatives:\n");
    for(i=0;i<Pnum;i++)
      fprintf (stderr,"     d f(x) / d%s = %s\n",Param[i],evaluator_get_string ((F.df)[i]));

    if(o_varcovar>1){
      fprintf (stderr,"\n  Model second derivatives:\n");
      for(i=0;i<Pnum;i++)
	for(j=0;j<Pnum;j++)
	  fprintf (stderr,"     d^2 f(x) / d%s d%s = %s\n",Param[i],Param[j],
		   evaluator_get_string ((F.ddf)[i][j]));
    }
  }
  /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

  

  /* PARAMETERS ESTIMATION */
  /* --------------------- */

  if(o_method == 0){/* non-linear regression */
    
    const gsl_multifit_fdfsolver_type *T = gsl_multifit_fdfsolver_lmsder;
    gsl_multifit_fdfsolver *s = gsl_multifit_fdfsolver_alloc (T, rows, Pnum);
    gsl_multifit_function_fdf obj;

    size_t i;

    /* iteration controls */
    size_t iter=0;
    int solver_status,object_status=GSL_CONTINUE,estimate_status=GSL_CONTINUE;
    double oldf;
    
    /* set initial condition */
    gsl_vector_view x = gsl_vector_view_array (Pval, Pnum);
    struct objdata param;

    /* initial value of the square root of the sum of squares */
    double sumrootsq0;

    /* set the parameter for the function */
    param.F = &F;
    param.rows = rows;
    param.columns = columns;
    param.data = data ;

    /* set the object structure */
    obj.f = ols_obj_f;
    obj.df = ols_obj_df;
    obj.fdf = ols_obj_fdf;
    obj.n = rows;
    obj.p = Pnum;
    obj.params = &param;
    
    /* initialize the solver */
    gsl_multifit_fdfsolver_set (s, &obj,&x.vector);


    /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
    if(o_verbose>3){
      fprintf(stderr," ------------------------------------------------------------\n");
      fprintf(stderr," Iter  ");
	for(i=0;i<Pnum;i++)
	  fprintf(stderr,"%-9s ",Param[i]);
      fprintf(stderr,"|f(x)|      |df(x)|       |x|         |dx|       status\n");
    }
    /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

    /* set the initial value of the object function and store it */
    sumrootsq0=oldf=gsl_blas_dnrm2 (s->f);
    do
      {
	double f,g,df,dg;
	solver_status = gsl_multifit_fdfsolver_iterate (s);

	/* compute increment in object and estimates */
	f=gsl_blas_dnrm2 (s->f);
	df=fabs(oldf-f);
	oldf=f;
	g=gsl_blas_dnrm2 (s->x);
	dg=gsl_blas_dnrm2 (s->dx);
	
	iter++;

	/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
	/* print status */
	if(o_verbose>3){
	  fprintf (stderr,"%3zd  ",iter);
	  for(i=0;i<Pnum;i++)
	    fprintf (stderr,"%8.5f ",gsl_vector_get (s->x, i));
	  fprintf (stderr," %8.5e  %8.5e  %8.5e  %8.5e %s\n",
		   f,df,g,dg,gsl_strerror (solver_status));
	}
	/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

/* 	if (status) */
/* 	  break; */

        /* check condition on estimates precision */
	if( estimate_status == GSL_CONTINUE && 
	    dg < eps && dg < eps*g )
	  estimate_status = GSL_SUCCESS;

	/* check condition of object convergence */
	if( object_status == GSL_CONTINUE &&
	    df < eps && df < eps*fabs(f) )
	  object_status = GSL_SUCCESS;

      }
    while ( (estimate_status == GSL_CONTINUE || object_status == GSL_CONTINUE)
	    && iter < maxiter);

    if(iter == maxiter && (estimate_status == GSL_CONTINUE || object_status == GSL_CONTINUE) )
      fprintf(stderr,"WARNING (%s): maximum number of iterations without convergence.\n", GB_PROGNAME);

    /* store final values */
    for(i=0;i<Pnum;i++)
      Pval[i] = gsl_vector_get(s->x,i);

    /* build the covariance matrix */
    if(o_output>0 || o_verbose>2)
      Pcovar= ols_varcovar(o_varcovar,s,param,Pnum);

    /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
    if(o_verbose>1){
      const double sumsq = pow(gsl_blas_dnrm2(s->f),2);
      const double sigma = sqrt(sumsq/(rows-Pnum));
      const double LL= -(1+log(sumsq/rows)+log(2*M_PI) )*0.5*rows;

      /* n.b.: the baseline model is NOT params=0 BUT params=initial condition */
      const double R2=1-sumsq/pow(sumrootsq0,2);
      const double R2adj=1-(sumsq/(rows-Pnum-1))/(pow(sumrootsq0,2.)/(rows-1));
      const double F=((rows-Pnum-1)/Pnum)*R2/(1-R2);


      fprintf(stderr," ------------------------------------------------------------\n");
      fprintf(stderr,"                        number of observations = %zd\n",rows);
      fprintf(stderr,"                 sum of squared residual (SSR) = %g\n",sumsq);
      fprintf(stderr,"               number degrees of freedom (ndf) = %zd\n",rows-Pnum);
      fprintf(stderr,"                 residuals stdev sqrt(SSR/ndf) = %g\n",sigma);
      fprintf(stderr,"      R^2 (sum sq. initial /sum sq. estimated) = %g\n",R2);
      fprintf(stderr,"                                  adjusted R^2 = %g\n",R2adj);
      fprintf(stderr,"                                  F statistics = %g\n",F);
      fprintf(stderr,"              One-sided P-value: Pr{stat>obs.} = %g\n",gsl_cdf_chisq_Q(Pnum*F,Pnum));
      fprintf(stderr,"                 chi-square test P(>SSR | ndf) = %g\n",
	      gsl_cdf_chisq_Q (sumsq,rows-Pnum));
      fprintf(stderr,"                         log-likelihood log(L) = %g\n",LL);
      fprintf(stderr,"              Akaike information criterion AIC = %g\n",2*Pnum-2*LL  );
      fprintf(stderr,"               corrected Akaike criterion AICc = %g\n",
	      (2*Pnum*(Pnum+1))/(rows-Pnum-1)+2*Pnum-2*LL);
      fprintf(stderr,"            Bayesian information criterion BIC = %g\n",Pnum*log(rows)-2*LL);
      fprintf(stderr,"                    Hannan–Quinn criterion HQC = %g\n",
	      2*Pnum*log(log(rows))+rows*log(sumsq/rows) );
    }
    /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */


    /* free space */
    gsl_multifit_fdfsolver_free (s);

  }
  else if (o_method == 1){

    const gsl_multimin_fminimizer_type *T = gsl_multimin_fminimizer_nmsimplex2;
    gsl_multimin_fminimizer *s = NULL;
    gsl_vector *x = gsl_vector_alloc (Pnum);
    gsl_vector *x0 = gsl_vector_alloc (Pnum);
    gsl_vector *ss = gsl_vector_calloc(Pnum);
    int converged;
    size_t totiter=0;
    gsl_multimin_function obj;

    /* set minimizer initial condition */
    struct objdata param;

    /* set the parameter for the function */
    param.F = &F;
    param.rows = rows;
    param.columns = columns;
    param.data =data ;

    /* set the object structure */
    obj.f = &mad_obj_f;
    obj.n = Pnum;
    obj.params = &param;
    
    /* allocate the solver */
    s = gsl_multimin_fminimizer_alloc (T, Pnum);

    /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
    if(o_verbose>3){
      size_t i;

      fprintf(stderr," ------------------------------------------------------------\n");
      fprintf(stderr,"      Stopping criteria: max{simplex size} < %g\n\n",eps);
      fprintf(stderr," Iter  ");
	for(i=0;i<Pnum;i++)
	  fprintf(stderr,"%-9s",Param[i]);
	fprintf(stderr,"f            ss          status\n");
    }
    /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

    /* set x to initial condition */
    {
      size_t i;
      for(i=0;i<Pnum;i++){
	gsl_vector_set(x,i,Pval[i]);
	gsl_vector_set(x0,i,Pval[i]);
      }
    }

    do {
      /* iteration controls */
      size_t iter=0;
      int status;
      double norm_diff;

      size_t i;

      /* set initial steps size, Pfeffer's method  */
      for(i=0;i<Pnum;i++)
	(ss->data)[i] = 0.05*fabs(gsl_vector_get(x,i))+0.0075;

      /* initialize the solver */
      gsl_multimin_fminimizer_set (s, &obj,x, ss);

      /* not converged yet */
      converged=0;
      do
	{
	  	  
	  status = gsl_multimin_fminimizer_iterate(s);
	  const double size = gsl_multimin_fminimizer_size (s);
	  iter++;
	  
	  /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
	  /* print status */
	  if(o_verbose>3){
	    fprintf (stderr,"%3zd  ",iter);
	    for(i=0;i<Pnum;i++)
	      fprintf (stderr,"%8.5f ",gsl_vector_get (s->x, i));
	    fprintf (stderr," %8.5e  %8.5e %s\n",
		     s->fval,size,gsl_strerror (status));
	  }
	  /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
	  
	  status=gsl_multimin_test_size (size,eps);
	}
      while (status == GSL_CONTINUE && iter < maxiter);

      /* x = x-s->x */
      gsl_blas_daxpy (-1,s->x,x);
      /* norm_diff = ||x-s->x|| */
      norm_diff=gsl_blas_dnrm2 (x);
      
      if( norm_diff < restart_accuracy ){
	  converged = 1;
	/* store final values */
	for(i=0;i<Pnum;i++)
	  Pval[i] = gsl_vector_get(s->x,i);
      }

      /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* print status */
      if(o_verbose>3){
	fprintf (stderr,"   min=%8.10e ||x-x_old||=%8.5e =>",s->fval,norm_diff);
	if(converged==0)
	  fprintf (stderr," restart\n");
	else
	  fprintf (stderr," converged\n");	
      }
      /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

      /* update the initial point: x = s->x */
      if(converged==0){
	gsl_vector_memcpy (x,s->x);
	totiter += iter;
      }
      
      if(totiter >= restart_maxiter)
	fprintf(stderr,"WARNING (%s): maximum number of iterations without convergence. final tolerance = %f\n",
		GB_PROGNAME,norm_diff);
    }
    while (converged == 0 && totiter < restart_maxiter);


    /* store final values */
    for(i=0;i<Pnum;i++)
      Pval[i] = gsl_vector_get(s->x,i);

    /* build the covariance matrix */
    if(o_output>0 || o_verbose>2)
      Pcovar = mad_varcovar(o_varcovar,s,param,Pnum);

    /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
    if(o_verbose>1){
      double values[Pnum+columns];
      size_t i,j;
      double residuals[rows];
      double ll0,D,SC95;
 
      const double LL= -(1+log(2*s->fval))*rows;

      /* compute the absolute deviation with initial parameters;
	 remember that the objective function is already divided by
	 the number of observations */
      ll0=obj.f(x0,&param);
      
      /* compute the residual with estimated parameters */
      for(i=columns;i<columns+Pnum;i++) /* set the parameter values */
	values[i] = Pval[i-columns];
      for(i=0;i<rows;i++){
	for(j=0;j<columns;j++)/* set the variables values */
	  values[j] = data[j][i];
	
	residuals[i] = evaluator_evaluate (F.f,columns+Pnum,F.argname,values);
      }
      qsort(residuals,rows,sizeof(double),sort_by_value);

      /* distribution free confidence interval with p=0.95 for the
	 mode of the residuals, computed according to Hollander and
	 Wolfe "Nonparametric statistical methods" */
      SC95=sqrt(rows)*(residuals[ (size_t)( (rows-1)/2+1.96*sqrt(rows/4)) ] - residuals[ (size_t) ( (rows-1)/2-1.96*sqrt(rows/4))  ])/1.96;


      /* D statistics asymptotically distributed under the null
	 according to a chi^2(num. of parameters). See "Coefficient of
	 determination for least absolute deviation analysys" McKean
	 and Sievers */
      D=2*(ll0-s->fval)*rows/SC95;

      fprintf(stderr," ------------------------------------------------------------\n");
      fprintf(stderr,"                        number of observations = %zd\n",rows);
      fprintf(stderr,"                  average absolute residuals a = %g\n",s->fval);
      /* fprintf(stderr," conf. int. median of res. (p=0.95)  SC95 = %f\n",SC95); */
      fprintf(stderr,"                   Shrader-McKean statistics D = %g\n",D);
      fprintf(stderr,"              One-sided P-value: Pr{stat>obs.} = %g\n",gsl_cdf_chisq_Q(D,Pnum));
      fprintf(stderr,"     McKean-Sievers coeff. of determination R2 = %g\n",(ll0-s->fval)/(ll0-s->fval+(rows-Pnum-1)*SC95/(2*rows)));
      fprintf(stderr,"                         log-likelihood log(L) = %g\n",LL);
      fprintf(stderr,"              Akaike information criterion AIC = %g\n",2*Pnum-2*LL  );
      fprintf(stderr,"               corrected Akaike criterion AICc = %g\n",
	      (2*Pnum*(Pnum+1))/(rows-Pnum-1)+2*Pnum-2*LL);
      fprintf(stderr,"            Bayesian information criterion BIC = %g\n",Pnum*log(rows)-2*LL);
    }
    /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

    /* free space */
    gsl_vector_free(ss);
    gsl_multimin_fminimizer_free (s);

  }
  else if (o_method == 2){

    const gsl_multimin_fminimizer_type *T = gsl_multimin_fminimizer_nmsimplex2;
    gsl_multimin_fminimizer *s = NULL;
    gsl_vector *x = gsl_vector_alloc (Pnum);
    gsl_vector *ss = gsl_vector_calloc(Pnum);
    int converged;
    size_t totiter=0;
    gsl_multimin_function obj;

    /* set minimizer initial condition */
    struct objdata param;

    /* set the parameter for the function */
    param.F = &F;
    param.rows = rows;
    param.columns = columns;
    param.data =data ;

    /* set the object structure */
    obj.f = &amad_obj_f;
    obj.n = Pnum;
    obj.params = &param;
    

    /* allocate the solver */
    s = gsl_multimin_fminimizer_alloc (T, Pnum);

    /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
    if(o_verbose>3){
      size_t i;

      fprintf(stderr," ------------------------------------------------------------\n");
      fprintf(stderr,"      Stopping criteria: max{simplex size} < %g\n\n",eps);
      fprintf(stderr," Iter  ");
	for(i=0;i<Pnum;i++)
	  fprintf(stderr,"%-9s",Param[i]);
	fprintf(stderr,"f            ss          status\n");
    }
    /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

    /* set x to initial condition */
    {
      size_t i;
      for(i=0;i<Pnum;i++)
	gsl_vector_set(x,i,Pval[i]);
    }

    do {
      /* iteration controls */
      size_t iter=0;
      int status;
      double norm_diff;

      size_t i;

      /* set initial steps size, Pfeffer's method  */
      for(i=0;i<Pnum;i++)
	(ss->data)[i] = 0.05*fabs(gsl_vector_get(x,i))+0.0075;

      /* initialize the solver */
      gsl_multimin_fminimizer_set (s, &obj,x, ss);

      /* not converged yet */
      converged=0;
      do
	{
	  	  
	  status = gsl_multimin_fminimizer_iterate(s);
	  const double size = gsl_multimin_fminimizer_size (s);
	  iter++;
	  
	  /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
	  /* print status */
	  if(o_verbose>3){
	    fprintf (stderr,"%3zd  ",iter);
	    for(i=0;i<Pnum;i++)
	      fprintf (stderr,"%8.5f ",gsl_vector_get (s->x, i));
	    fprintf (stderr," %8.5e  %8.5e %s\n",
		     s->fval,size,gsl_strerror (status));
	  }
	  /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
	  
	  status=gsl_multimin_test_size (size,eps);
	}
      while (status == GSL_CONTINUE && iter < maxiter);

      /* x = x-s->x */
      gsl_blas_daxpy (-1,s->x,x);
      /* norm_diff = ||x-s->x|| */
      norm_diff=gsl_blas_dnrm2 (x);
      
      if( norm_diff < restart_accuracy ){
	  converged = 1;
	/* store final values */
	for(i=0;i<Pnum;i++)
	  Pval[i] = gsl_vector_get(s->x,i);
      }

      /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* print status */
      if(o_verbose>3){
	fprintf (stderr,"   min=%8.10e ||x-x_old||=%8.5e =>",s->fval,norm_diff);
	if(converged==0)
	  fprintf (stderr," restart\n");
	else
	  fprintf (stderr," converged\n");	
      }
      /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

      /* update the initial point: x = s->x */
      if(converged==0){
	gsl_vector_memcpy (x,s->x);
	totiter += iter;
      }
      

      if(totiter >= restart_maxiter)
	fprintf(stderr,"WARNING (%s): maximum number of iterations without convergence. final tolerance = %f\n",
		GB_PROGNAME,norm_diff);
    }
    while (converged == 0 && totiter < restart_maxiter);


    /* build the covariance matrix, if needed */
    if(o_output>0 || o_verbose>2)
      Pcovar = amad_varcovar(o_varcovar,s,param,Pnum);

    /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
    if(o_verbose>1){
      double values[Pnum+columns];
      size_t i,j;
      double sumL=0,sumR=0,al,ar,dtmp1;

      const double LL= -(1+2*log(s->fval))*rows;

      fprintf(stderr," ------------------------------------------------------------\n");
      
      /* compute sumL and sumR at the minimum */
      for(i=columns;i<columns+Pnum;i++) /* set the parameter values */
	values[i] = Pval[i-columns];
      for(i=0;i<rows;i++){
	for(j=0;j<columns;j++)/* set the variables values */
	  values[j] = data[j][i];
	
	dtmp1 = evaluator_evaluate (F.f,columns+Pnum,F.argname,values);
	if(dtmp1>0) sumR += dtmp1;
	else sumL += -dtmp1;
      }
      sumL/=rows;
      sumR/=rows;
      al = sumL+sqrt(sumL*sumR);
      ar = sumR+sqrt(sumL*sumR);
      fprintf(stderr,"                        number of observations = %zd\n",rows);
      fprintf(stderr,"  square root sum of left residuals srqrt(S_L) = %g\n",sqrt(sumL));
      fprintf(stderr," square root sum of right residuals srqrt(S_R) = %g\n",sqrt(sumR));
      fprintf(stderr,"                         sqrt(S_L)+ srqrt(S_R) = %g\n",s->fval);
      fprintf(stderr,"                                           a_L = %g\n",al);
      fprintf(stderr,"                                           a_R = %g\n",ar);
      fprintf(stderr,"                         log-likelihood log(L) = %g\n",LL);
      fprintf(stderr,"              Akaike information criterion AIC = %g\n",2*Pnum-2*LL  );
      fprintf(stderr,"               corrected Akaike criterion AICc = %g\n",
	      (2*Pnum*(Pnum+1))/(rows-Pnum-1)+2*Pnum-2*LL);
      fprintf(stderr,"            Bayesian information criterion BIC = %g\n",Pnum*log(rows)-2*LL);
    }
    /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

    /* free space */
    gsl_multimin_fminimizer_free (s);
    gsl_vector_free(ss);
    gsl_vector_free(x);

  }

  /* output */
  /* ------ */

  /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
  if(o_verbose>2){
    size_t i,j;

    fprintf(stderr," ------------------------------------------------------------\n");
    fprintf(stderr,"  variance matrix                         = ");
    switch(o_varcovar){
    case 0: fprintf(stderr,"<gradF gradF^t>\n"); break;
    case 1: fprintf(stderr,"J^{-1}\n"); break;
    case 2: fprintf(stderr,"H^{-1}\n"); break;
    case 3: fprintf(stderr,"H^{-1} J H^{-1}\n"); break;
    }
    
    fprintf(stderr,"\n");
    for(i=0;i<Pnum;i++){
      fprintf(stderr," %s = %+f +/- %f (%5.1f%%) | ",
	      Param[i],Pval[i],
	      sqrt(gsl_matrix_get(Pcovar,i,i)),
	      100.*sqrt(gsl_matrix_get(Pcovar,i,i))/fabs(Pval[i]));
      for(j=0;j<Pnum;j++)
	if(j != i)
	  fprintf(stderr,"%+f ",
		  gsl_matrix_get(Pcovar,i,j)/sqrt(gsl_matrix_get(Pcovar,i,i)*gsl_matrix_get(Pcovar,j,j)));
	else
	  fprintf(stderr,"%+f ",1.0);
	fprintf(stderr,"|\n");
    }    
  }

  if(o_verbose>1)
    fprintf(stderr," ------------------------------------------------------------\n");
  /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
  
  switch(o_output){
  case 0:
    {
      size_t i;
      /* +++++++++++++++++++++++++++++++++++ */
      if(o_verbose>0){
	fprintf(stdout,"#");
	for(i=0;i<Pnum-1;i++)
	  fprintf(stdout,EMPTY_SEP,Param[i]);
	i=Pnum-1;
	fprintf(stdout,EMPTY_NL,Param[i]);
      }
      /* +++++++++++++++++++++++++++++++++++ */

      printline(stdout,Pval,Pnum);
    }
    break;
  case 1:
    {
      size_t i;

      /* +++++++++++++++++++++++++++++++++++ */
      if(o_verbose>0){
	fprintf(stdout,"#");
	for(i=0;i<Pnum-1;i++){
	  fprintf(stdout,EMPTY_SEP,Param[i]);
	  fprintf(stdout,EMPTY_SEP,"+/-");
	}
	i=Pnum-1;
	fprintf(stdout,EMPTY_SEP,Param[i]);
	fprintf(stdout,EMPTY_NL,"+/-");
      }
      /* +++++++++++++++++++++++++++++++++++ */
      
      for(i=0;i<Pnum-1;i++){
	fprintf(stdout,FLOAT_SEP,Pval[i]);
	fprintf(stdout,FLOAT_SEP,sqrt(gsl_matrix_get(Pcovar,i,i)));
      }
      i=Pnum-1;
      fprintf(stdout,FLOAT_SEP,Pval[i]);
      fprintf(stdout,FLOAT_NL,sqrt(gsl_matrix_get(Pcovar,i,i)));
    }
    break;
  case 2:
    {
      
      size_t i,j;
      double values[Pnum+columns];
      
      /* set the parameter values */
      for(i=columns;i<columns+Pnum;i++)
	values[i] = Pval[i-columns];
      
      /* +++++++++++++++++++++++++++++++++++ */
      if(o_verbose>0){
	fprintf(stderr,"#");
	for(i=0;i<Pnum-1;i++){
	  fprintf(stderr,EMPTY_SEP,Param[i]);
	  fprintf(stderr,EMPTY_SEP,"+/-");
	}
	i=Pnum-1;
	fprintf(stderr,EMPTY_SEP,Param[i]);
	fprintf(stderr,EMPTY_NL,"+/-");
	
	for(i=0;i<Pnum-1;i++){
	  fprintf(stderr,FLOAT_SEP,Pval[i]);
	  fprintf(stderr,FLOAT_SEP,sqrt(gsl_matrix_get(Pcovar,i,i)));
	}
	i=Pnum-1;
	fprintf(stderr,FLOAT_SEP,Pval[i]);
	fprintf(stderr,FLOAT_NL,sqrt(gsl_matrix_get(Pcovar,i,i)));
      }
      /* +++++++++++++++++++++++++++++++++++ */


      /* +++++++++++++++++++++++++++++++++++ */
      if(o_verbose>0){
	fprintf(stdout,"#");
	for(i=0;i<columns;i++){
	  char *string= (char *) my_alloc(sizeof(char)*4);
	  snprintf(string,4,"x%zd",i+1);
	  fprintf(stdout,EMPTY_SEP,string);
	  free(string);
	}
	fprintf(stdout,"%s\n",evaluator_get_string (F.f));
      }
      /* +++++++++++++++++++++++++++++++++++ */

      for(i=0;i<rows;i++){
	
	/* print columns and set the variables values */
	for(j=0;j<columns;j++){
	  fprintf(stdout,FLOAT_SEP,data[j][i]);
	  values[j] = data[j][i];
	}
	/* print residual */
	fprintf(stdout,FLOAT_NL,evaluator_evaluate (F.f,columns+Pnum,F.argname,values));
      }
    }
    break;
  case 3:
    {
      size_t i;

      /* +++++++++++++++++++++++++++++++++++ */
      if(o_verbose>0){
	fprintf(stdout,"#");
	for(i=0;i<Pnum-1;i++)
	  fprintf(stdout,EMPTY_SEP,Param[i]);
	i=Pnum-1;
	fprintf(stdout,EMPTY_NL,Param[i]);
      }
      /* +++++++++++++++++++++++++++++++++++ */
      
      printline(stdout,Pval,Pnum);
      
      fprintf(stdout,"\n\n");
      
      for(i=0;i<Pnum;i++){
	size_t j;
	for(j=0;j<Pnum-1;j++)
	  fprintf(stdout,FLOAT_SEP,gsl_matrix_get(Pcovar,i,j));
	j=Pnum-1;
	fprintf(stdout,FLOAT_NL,gsl_matrix_get(Pcovar,i,j));
      }
    }
    break;
  case 4:
    {
      size_t i,j;
      
      double dtmp1=0,dtmp2=0;
      for(i=0;i<rows;i++){
	dtmp1+=data[0][i];
	dtmp2+=pow(data[0][i],2.0);
      }
      dtmp1/=rows;
      dtmp2/=rows;
      double std_y=pow(dtmp2-dtmp1*dtmp1,0.5);
      
      double sscore[Pnum];
      for(j=0;j<Pnum;j++){
	dtmp1=0;
	dtmp2=0;
	for(i=0;i<rows;i++){
	  dtmp1+=data[j+1][i];
	  dtmp2+=pow(data[j+1][i],2.0);
	}
	dtmp1/=rows;
	dtmp2/=rows;
	double std_x=pow(dtmp2-dtmp1*dtmp1,0.5);
	sscore[j] = pow(Pval[j]*std_x/std_y,2.0);
      }


      /* +++++++++++++++++++++++++++++++++++ */
      if(o_verbose>0){
	fprintf(stdout,"#");
	for(i=0;i<Pnum-1;i++){
	  fprintf(stdout,EMPTY_SEP,Param[i]);
	  fprintf(stdout,EMPTY_SEP,"+/-");
	  fprintf(stdout,EMPTY_SEP,"s-score");
	}
	i=Pnum-1;
	fprintf(stdout,EMPTY_SEP,Param[i]);
	fprintf(stdout,EMPTY_SEP,"+/-");
	fprintf(stdout,EMPTY_NL,"s-score");
      }
      /* +++++++++++++++++++++++++++++++++++ */
      
      for(i=0;i<Pnum-1;i++){
	fprintf(stdout,FLOAT_SEP,Pval[i]);
	fprintf(stdout,FLOAT_SEP,sqrt(gsl_matrix_get(Pcovar,i,i)));
	fprintf(stdout,FLOAT_SEP,sscore[i]);
      }
      i=Pnum-1;
      fprintf(stdout,FLOAT_SEP,Pval[i]);
      fprintf(stdout,FLOAT_SEP,sqrt(gsl_matrix_get(Pcovar,i,i)));
      fprintf(stdout,FLOAT_NL,sscore[i]);
    }
    break;

  }
  

  /* deallocate function and data  */
  {
    size_t i;

    evaluator_destroy(F.f);

    for(i=0;i<Pnum;i++)
      evaluator_destroy(F.df[i]);

    free(F.df);

    for(i=0;i<F.argnum;i++)
      free(F.argname[i]);
    free(F.argname);
    
    for(i=0;i<columns;i++) 
      free(data[i]);
    free(data);
  }

  return 0;

}
