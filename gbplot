#!/bin/sh
# gbplot ver. 1.1   Copyright (C) 2009-2010 Giulio Bottazzi

#read command line options; the position of the last option is saved
#in OPTIND
while getopts "T:o:t:p:ihl:C:v-:" opt
do
    case $opt in
	-)
	    case "${OPTARG}" in
		help) help=yes;;
		version) version=yes;;
	    esac;;
	T) terminal=$OPTARG;;
	o) outfile=$OPTARG;;
	t) title=$OPTARG;;
	p) prestring=$OPTARG;;
	i) interactive=yes;;
	h) help=yes;;
	l) setlog=$OPTARG;;
	C) precommand="${precommand}; $OPTARG";;
	v) verbose=yes;;
	\?) help=yes;;
    esac
done

if [ "$version" = "yes" ]; then
    
    cat - <<EOF
gbplot ver. 5.6

Copyright (C) 2009-2014 Giulio Bottazzi

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
(version 2) as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

Written by Giulio Bottazzi
Report bugs to <gbutils@googlegroups.com>
Package home page <http://cafim.sssup.it/~giulio/software/gbutils/index.html>
EOF
    exit

fi


if [ "$help" = "yes" ]; then
    
    cat - <<EOF

Usage:  gbplot [options] [command] [specstring] < datafile

This command produce simple plots from the command line using the
'gnuplot' program. 'comand' is one of the gnuplot commands: 'plot' or
'splot' while 'specstring' is a list of gnuplot specifications. In
order to escape shell interpretation place 'specstring' between double
quotes. It is possible to specify the type of terminal and output file
name as command line options. It is also possible to start an
interactive session, useful to adjust plot parameters by hand. The
program can be used in a shell pipe, as in

cat datafile | gbplot [options] [command] [specstring]

Options:
 -h print this help
 -i start interactive session after the plot
 -T set a different terminal
 -o set an ouptut file
 -t assign a title to the plot
 -p prepend expression to plot command
 -l set log; possible values x,y and xy
 -C command to be run before the plot
 -v verbose output: print commands to standard error
 --help  same as '-h'
 --version print the program version


Examples:

Two plots: one using line and one using points.
 echo "1\n2\n3" | gbplot plot "u 0:1 w l title 'line', '' u 0:1 w p title 'point'"

Save the plot in PDF format in a file named 'test.pdf'
 echo "1\n2\n3" | gbplot -T pdf -o test.pdf plot "u 0:1 w l title 'line'"

Set log on the y axis
 echo "1\n2\n3" | gbplot -l y plot "u 0:1 w l title 'line', '' u 0:1 w p title 'point'"

Set log on the x axis and a grid
 echo "1\n2\n3" | gbplot -l x -C "set grid" plot "u 0:1 w l title 'line', '' u 0:1 w p title 'point'"

Set a grid and the position of the legend
 echo "1\n2\n3" | gbplot -C "set grid" -C "set key bottom right" plot "u 0:1 w l title 'line', '' u 0:1 w p title 'point'"

Compute the value of the gamma function on a set of points; gawk is used to filer the relevant numbers
 echo "1\n2\n3" | gbplot -C "set table" plot 'u (gamma(\$1))' | gawk '/ i/{print \$2}'

EOF

    exit

fi

#store provided data
cat - > .gb.data

#check the syntax and define plotting parameters
#plotcmd=${@:$OPTIND:1} <- bashism

if [ $OPTIND -gt 1 ]; then
    shift `expr $OPTIND - 1`
fi
plotcmd=$1

if [ "$plotcmd" != "plot" ] && [ "$plotcmd" != "splot" ]; then
    echo "provide a plot or splot command"
    exit
fi

#firstpar=${@:$(( OPTIND+1 )):1}  <- bashism
#plotpar=${@:$(( OPTIND+2 ))}     <- bashism

shift
firstpar=$1

if [ "$#" -gt 0 ]; then
    shift
    plotpar=$@
fi

#identify range specification
if echo "$firstpar"  | grep '\[*\]'  > /dev/null; then
    plotcmd="$plotcmd $firstpar"
else
    plotpar="$firstpar $plotpar"
fi 

#set the default terminal
if echo "set term"  | gnuplot 2>&1 | grep wx > /dev/null; then
    DEFTERM=wxt
else
    DEFTERM=x11
fi

#-----------------------------------------------


if [ "$interactive" = "yes" ]; then #interactive session
    #create temporary files
    CMDFILE=`tempfile`
    ENDFILE=`tempfile`
    
    # --- initial gnuplot commands

    #set the default terminal
    echo "set term $DEFTERM noraise" >> $CMDFILE

    #next line useful for screen to start in present directory
    echo "cd \"$PWD\"" >> $CMDFILE
    #no need of irrelevant titles
    echo "set key noautotitle" >> $CMDFILE
    #prepend expression
    if [ "$prestring" != "" ]; then
	echo "$prestring" >> $CMDFILE
    fi
    #add title
    if [ "$title" != "" ]; then
	echo "set title \"$title\"" >> $CMDFILE
    fi
    #set log scale
    if [ "$setlog" != "" ]; then
	echo "set log $setlog" >> $CMDFILE
    fi
    #add pre-plot command
    if [ "$precommand" != "" ]; then
	echo "$precommand" >> $CMDFILE
    fi
    echo "$plotcmd \".gb.data\" $plotpar" >> $CMDFILE
    echo "show plot" >> $CMDFILE

    # ----------------------------
    
    # --- final gnuplot commands
    
    #prepare for final output if 'terminal' or 'outfile' is specified
    if [ "$terminal" != "" ]; then
	echo "set term $terminal" >> $ENDFILE
    fi
    
    if [ "$outfile" != "" ]; then
	echo "set out \"$outfile\"" >> $ENDFILE
	echo "replot" >> $ENDFILE
	echo "set out" >> $ENDFILE
    fi
    
    #remove temporary files
    echo "!rm .gb.data" >> $ENDFILE
    echo "!rm $CMDFILE $ENDFILE" >>  $ENDFILE

    # ----------------------------

    if [ "$verbose" = "yes" ]; then
	cat $CMDFILE > /dev/stderr
	echo "-- session -- "  > /dev/stderr
	cat $ENDFILE > /dev/stderr
    fi

    if [ "$TERM" = "screen" ]; then
        #start gnuplot in a new screen window
	screen -X screen gnuplot $CMDFILE - $ENDFILE
    elif [ "$TERM" = "aterm" ]; then
        #start gnuplot in a new terminal
	aterm -e gnuplot $CMDFILE - $ENDFILE
    else
        #start gnuplot in a new terminal
	$TERMCMD -- gnuplot $CMDFILE - $ENDFILE
    fi
    
else   #non-interactive session
    #create temporary files
    CMDFILE=`tempfile`
    
    #next line useful for screen to start in present directory
    echo "cd \"$PWD\"" >> $CMDFILE
    #no need of irrelevant titles
    echo "set key noautotitle" >> $CMDFILE
    if [ "$terminal" != "" ]; then
	echo "set term $terminal" >> $CMDFILE
    else
	echo "set term $DEFTERM noraise" >> $CMDFILE
    fi
    
    if [ "$outfile" != "" ]; then
	echo "set out \"$outfile\"" >> $CMDFILE
    fi
    #prepend expression
    if [ "$prestring" != "" ]; then
	echo "$prestring" >> $CMDFILE
	echo "$prestring"	
    fi
    #add title
    if [ "$title" != "" ]; then
	echo "set title \"$title\"" >> $CMDFILE
    fi
    #set log scale
    if [ "$setlog" != "" ]; then
	echo "set log $setlog" >> $CMDFILE
    fi
    #add pre-plot command
    if [ "$precommand" != "" ]; then
	echo "$precommand" >> $CMDFILE
    fi
    
    echo "$plotcmd \".gb.data\" $plotpar" >> $CMDFILE

    if [ "$verbose" = "yes" ]; then
	cat $CMDFILE > /dev/stderr
    fi
    
    gnuplot -persist ${CMDFILE}
    
    rm .gb.data
    rm "$CMDFILE"
    
fi
