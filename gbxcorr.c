/*
  gbxcorr  (ver. 5.6) -- Compute cross correlation matrix
  Copyright (C) 2011-2018 Giulio Bottazzi

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  (version 2) as published by the Free Software Foundation;
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#include "tools.h"


int main(int argc,char* argv[]){

  char *splitstring = strdup(" \t");

  size_t rows=0,columns=0;
  double **vals=NULL,**res=NULL;

  size_t i,j,k;

  /* OPTIONS */
  int o_method=0;

  /* variables for reading command line options */
  /* ------------------------------------------ */
  int opt;
  /* ------------------------------------------ */

  
  /* COMMAND LINE PROCESSING */

  while((opt=getopt_long(argc,argv,"hF:M:",gb_long_options, &gb_option_index))!=EOF){
    if(opt==0){
      gbutils_header(argv[0],stdout);
      exit(0);
    }
    else if(opt=='?'){
      fprintf(stderr,"option %c not recognized\n",optopt);
      exit(-1);
    }
    else if(opt=='h'){
      /*print help*/
      fprintf(stdout,"Take as input a data matrix A with N rows and T columns         \n");
      fprintf(stdout,"  A = [ a_{t,i} ]   with t=1,..,T  i=1,...,N                    \n");
      fprintf(stdout,"and compute the NxN correlation matrix C [ c_{i,j} ] following  \n");
      fprintf(stdout,"the method specified by option '-M': with method 0 it is        \n");
      fprintf(stdout,"  c_{i,j} = 1/(T-1) \\sum_t (a_{t,i}-m_i) (a_{t,j}-m_j)         \n");
      fprintf(stdout,"where m_i is the i-th mean and with method 1 it is              \n");
      fprintf(stdout,"  c_{i,j} = 1/T \\sum_t a_{t,i} a_{t,j} .                       \n");
      fprintf(stdout,"Covariance is stored in the lower triangle while correlation    \n");
      fprintf(stdout,"coefficients are stored in the upper triangle.                  \n\n");
      fprintf(stdout,"WARNING: previous implementations were row-wise instead of column-wise\n");
      fprintf(stdout,"\nUsage: %s [options]\n\n",argv[0]);
      fprintf(stdout,"Options:                                                        \n");
      fprintf(stdout," -M  choose the method (default 0)                              \n");
      fprintf(stdout,"      0  covariance/correlation with mean removal               \n");
      fprintf(stdout,"      1  covariance/correlation without mean removal            \n");
      fprintf(stdout," -F specify the input fields separators (default \" \\t\")      \n");
      fprintf(stdout," -h this help\n");
      exit(0);
    }
    else if(opt=='M'){
      /*set the method to use*/
      o_method= atoi(optarg);
    }
    else if(opt=='F'){
      /*set the fields separator string*/
      free(splitstring);
      splitstring = strdup(optarg);
    }
  }
  /* END OF COMMAND LINE PROCESSING */

  /* initialize global variables */
  initialize_program(argv[0]);

  /* load data */
  loadtable(&vals,&rows,&columns,0,splitstring);

  /* allocate space for the result */
  res = (double **) my_alloc(columns*sizeof(double *));
  for(i=0;i<columns;i++)
    res[i] = (double *) my_alloc(columns*sizeof(double));

  /* compute the result */
  if(o_method==0){/* with mean removal */

    double *ave = (double *) my_alloc(rows*sizeof(double));

    for(i=0;i<columns;i++){
      ave[i]=0;
      for(k=0;k<rows;k++)
	ave[i] += vals[i][k];
      ave[i]/=rows;
    }

    for(i=0;i<columns;i++)
      for(j=0;j<=i;j++){
	res[i][j]=0.0;
	for(k=0;k<rows;k++)
	  res[i][j] += vals[i][k]*vals[j][k];
	res[j][i] = res[i][j] = res[i][j]/(rows-1) - ave[i]*ave[j]*rows/(rows-1);
      }

    for(i=0;i<columns;i++)
      for(j=0;j<i;j++)
	res[j][i] = res[i][j]/sqrt(res[i][i]*res[j][j]);

    free(ave);

  }
  else if(o_method==1){/* without mean removal */

    for(i=0;i<columns;i++)
      for(j=0;j<=i;j++){
	res[i][j]=0.0;
	for(k=0;k<rows;k++)
	  res[i][j] += vals[i][k]*vals[j][k];
	res[i][j] /= rows;
      }

    for(i=0;i<columns;i++)
      for(j=0;j<i;j++)
	res[j][i] = res[i][j]/sqrt(res[i][i]*res[j][j]);

  }
  else{/* unknown method  */
    fprintf(stderr,"ERROR (%s): unknown method; use option -h\n",
	    GB_PROGNAME);
    exit(+1);
  }


  /* print the result */
  for(i=0;i<columns;i++){
    for(j=0;j<columns-1;j++)
      printf(FLOAT_SEP,res[i][j]);
    printf(FLOAT_NL,res[i][columns-1]);
  }


  exit(0);
}
