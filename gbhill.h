#ifndef GBHILL_H
#define GBHILL_H 1

#include "tools.h"

#include <gsl/gsl_cdf.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_blas.h>

/* log-likelihood params */
struct loglike_params {
  size_t size;
  double *data;
  size_t used;
  size_t min;
  size_t max;
  int method;
  double d;
};

/* Exponential distribution in exponential_gbhill.c */
void exponential_nll (const size_t, const double *,void *,double *);
void exponential_dnll (const size_t, const double *,void *,double *);
void exponential_nlldnll (const size_t, const double *,void *,double *,double *);
double exponential_f (const double,const size_t, const double *);
double exponential_F (const double,const size_t, const double *);
gsl_matrix *exponential_varcovar(const int, double *, void *);

/* Pareto Type I distribution in paretoI_gbhill.c */
void pareto1_nll (const size_t, const double *,void *,double *);
void pareto1_dnll (const size_t, const double *,void *,double *);
void pareto1_nlldnll (const size_t, const double *,void *,double *,double *);
double pareto1_f (const double,const size_t, const double *);
double pareto1_F (const double,const size_t, const double *);
gsl_matrix *pareto1_varcovar(const int, double *, void *);

/* Pareto Type III distribution in paretoIII_gbhill.c */
void pareto3_nll (const size_t, const double *,void *,double *);
void pareto3_dnll (const size_t, const double *,void *,double *);
void pareto3_nlldnll (const size_t, const double *,void *,double *,double *);
double pareto3_f (const double,const size_t, const double *);
double pareto3_F (const double,const size_t, const double *);
gsl_matrix *pareto3_varcovar(const int, double *, void *);

/* Gaussian distribution in gaussian_gbhill.c */
void gaussian_nll (const size_t, const double *,void *,double *);
void gaussian_dnll (const size_t, const double *,void *,double *);
void gaussian_nlldnll (const size_t, const double *,void *,double *,double *);
double gaussian_f (const double,const size_t, const double *);
double gaussian_F (const double,const size_t, const double *);
gsl_matrix *gaussian_varcovar(const int, double *, void *);

#endif
