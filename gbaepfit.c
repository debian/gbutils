/*
  gbaepfit (ver. 6.0) -- Fit an asymmetric power exp. density via
  likelihood maximization

  Copyright (C) 2018 Giulio Bottazzi

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  (version 2) as published by the Free Software Foundation;
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/


/*
  Verbosity levels:
  0 just the final ouput
  1 the results of intermediate steps
  2 internal information on intermediate steps
  3 gory details on intermediate steps
*/

#include "tools.h"
#include "multimin.h"
#include "assert.h"

#include <gsl/gsl_linalg.h>
#include <gsl/gsl_cdf.h>
#include <gsl/gsl_sf_gamma.h>
#include <gsl/gsl_sf_psi.h>
#include <gsl/gsl_roots.h>


/* Global Variables */
/* ---------------- */

/* store data */
double *Data; /*the array of data*/
size_t Size;/*the number of data*/

/* output management */
FILE *Fmessages;/*stream for computation messages*/
FILE *Fparams;/*strem for final parameters*/
FILE *Ffitted;/*strem for distribution or density*/

/*------------------*/


/* functions for the information matrix of asymmetric subbotin  */
/* ----------------------------------------------------------- */

double B0(double x){
  return pow(x,1./x)*gsl_sf_gamma(1.+1./x);
}


double B1(double x){
  const double dt1 = 1.+1./x;
  return pow(x,1./x-1.)*gsl_sf_gamma(dt1)*(log(x)+gsl_sf_psi(dt1));
}


double B2(double x){
  const double dt1 = 1.+1./x;
  const double dt2 = log(x);
  const double dt3 = gsl_sf_psi(dt1);
  const double dt4 = gsl_sf_psi_1(dt1);
  return pow(x,1./x-2.)*gsl_sf_gamma(dt1)*(dt2*dt2+2*dt2*dt3+dt3*dt3+dt4);
}


double dB0dx(double x){
  return B0(x)/(x*x)-B1(x)/x;
}

double dB0dx2(double x){
  const double dt1=x*x;
  return -B0(x)*(3.*x-1.)/(dt1*dt1) + 2.*B1(x)*(x-1.)/(dt1*x) + B2(x)/dt1;
}

/* Output Functions */
/*----------------- */

const void printcumul(const double x[]){

  int i;
  double dtmp1; 
  const double bl=x[0];
  const double br=x[1];
  const double al=x[2];
  const double ar=x[3];
  const double m=x[4];

  const double Al=al*gsl_sf_gamma(1./bl+1.)*pow(bl,1./bl);
  const double Ar=ar*gsl_sf_gamma(1./br+1.)*pow(br,1./br);
  const double Asum=Al+Ar;

  for(i=0;i<Size;i++){
    if(Data[i]>m){
      dtmp1=pow((Data[i]-m)/ar,br)/br;
      dtmp1=(Al+Ar*gsl_sf_gamma_inc_P(1./br,dtmp1))/Asum;
     }
    else{
      dtmp1=pow((m-Data[i])/al,bl)/bl;
      dtmp1=Al*gsl_sf_gamma_inc_Q(1./bl,dtmp1)/Asum;
    }
    fprintf(Ffitted,FLOAT_SEP,Data[i]);
    fprintf(Ffitted,FLOAT_NL,dtmp1);
  }

}

const void printdensity(const double x[]){

  int i;
  const double bl=x[0];
  const double br=x[1];
  const double al=x[2];
  const double ar=x[3];
  const double m=x[4];

  const double norm=al*pow(bl,1/bl)*gsl_sf_gamma(1./bl+1.)
    +ar*pow(br,1/br)*gsl_sf_gamma(1./br+1);

  for(i=0;i<Size;i++){
    double dtmp1=Data[i];
    fprintf(Ffitted,FLOAT_SEP,dtmp1);
    dtmp1-=m;
    if(dtmp1>=0){
      fprintf(Ffitted,FLOAT_NL,exp(-pow(dtmp1/ar,br)/br)/norm);
    }
    else{
      fprintf(Ffitted,FLOAT_NL,exp(-pow(-dtmp1/al,bl)/bl)/norm);
    }
  }
  
}
/*----------------- */



/* 
   x   array of paramaters
   N     number of observations
   dim   dimension of the matrix: 2 m known; 3 m unknown
   I     the variance-covarance matrxi
*/

void varcovar(const double x[], const size_t N, const size_t dim, gsl_matrix *I){

  const double bl=x[0];
  const double br=x[1];
  const double al=x[2];
  const double ar=x[3];
  const double m=x[4];

  const double A = al*B0(bl)+ar*B0(br);
  
  const double B0l = B0(bl);
  const double B0r = B0(br);
  const double B1l = B1(bl);
  const double B1r = B1(br);
  const double B2l = B2(bl);
  const double B2r = B2(br);
  const double dB0ldx = dB0dx(bl);
  const double dB0rdx = dB0dx(br);
  const double dB0ldx2 = dB0dx2(bl);
  const double dB0rdx2 = dB0dx2(br);

  size_t i,j;

  /* needed for inversion */
  gsl_matrix *J = gsl_matrix_calloc (dim,dim);
  gsl_permutation *P = gsl_permutation_alloc (dim);
  int signum;



  /* define the matrix, the order is bl,br,al,ar,m */

  /* bl - bl */
  gsl_matrix_set (I,0,0,al*(dB0ldx2-al*dB0ldx*dB0ldx/A + 
			    B2l/bl -2*B1l/(bl*bl)+2*B0l/pow(bl,3))/A );
  
  /* bl - br */
  gsl_matrix_set (I,0,1,-al*ar*dB0ldx*dB0rdx/(A*A));
  gsl_matrix_set (I,1,0,-al*ar*dB0ldx*dB0rdx/(A*A));
  
  /* bl - al */
  gsl_matrix_set (I,0,2,dB0ldx/A-al*B0l*dB0ldx/(A*A)-B1l/A);
  gsl_matrix_set (I,2,0,dB0ldx/A-al*B0l*dB0ldx/(A*A)-B1l/A);
  
  /* bl - ar */
  gsl_matrix_set (I,0,3,-al*B0r*dB0ldx/(A*A));
  gsl_matrix_set (I,3,0,-al*B0r*dB0ldx/(A*A));
  
  /* br - br */
  gsl_matrix_set (I,1,1,ar*(dB0rdx2-ar*dB0rdx*dB0rdx/A + 
			    B2r/br -2*B1r/(br*br)+2*B0r/pow(br,3))/A );
  
  /* br - al */
  gsl_matrix_set (I,1,2,-ar*B0l*dB0rdx/(A*A));
  gsl_matrix_set (I,2,1,-ar*B0l*dB0rdx/(A*A));
  
  /* br - ar */
  gsl_matrix_set (I,1,3,dB0rdx/A-ar*B0r*dB0rdx/(A*A)-B1r/A);
  gsl_matrix_set (I,3,1,dB0rdx/A-ar*B0r*dB0rdx/(A*A)-B1r/A);

  /* al - al */
  gsl_matrix_set (I,2,2,-B0l*B0l/(A*A)+(bl+1)*B0l/(al*A));
  
  /* al - ar */
  gsl_matrix_set (I,2,3,-B0l*B0r/(A*A));
  gsl_matrix_set (I,3,2,-B0l*B0r/(A*A));
      
  /* ar - ar */
  gsl_matrix_set (I,3,3,-B0r*B0r/(A*A)+(br+1)*B0r/(ar*A));


  if(dim == 5){
    
    const double dt1l = gsl_sf_gamma (2.-1/bl);
    const double dt1r = gsl_sf_gamma (2.-1/br);
    const double dt2l = pow(bl,1.-1./bl);
    const double dt2r = pow(br,1.-1./br);

    /* bl - m */
    gsl_matrix_set (I,0,4,(log(bl)-M_EULER)/(bl*A));
    gsl_matrix_set (I,4,0,(log(bl)-M_EULER)/(bl*A));

    /* br - m */
    gsl_matrix_set (I,1,4,-(log(br)-M_EULER)/(br*A));
    gsl_matrix_set (I,4,1,-(log(br)-M_EULER)/(br*A));

    /* al - m */
    gsl_matrix_set (I,2,4,-bl/(al*A));
    gsl_matrix_set (I,4,2,-bl/(al*A));

    /* ar - m */
    gsl_matrix_set (I,3,4,br/(ar*A));
    gsl_matrix_set (I,4,3,br/(ar*A));

    /* m - m */
    gsl_matrix_set (I,4,4,( dt1l*dt2l/al+dt1r*dt2r/ar)/A );

  }

  /* invert I; in J store temporary LU decomp. */
  gsl_matrix_memcpy (J,I);
  gsl_linalg_LU_decomp (J,P,&signum);
  gsl_linalg_LU_invert (J,P,I);

  /* free allocated memory */
  gsl_matrix_free(J);
  gsl_permutation_free(P);


  /* set the var-covar matrix */
  for(i=0;i<dim;i++)
    for(j=0;j<i;j++)
      gsl_matrix_set (I,i,j,gsl_matrix_get (I,i,j)
		      /sqrt(gsl_matrix_get (I,i,i)*gsl_matrix_get (I,j,j)));

  for(i=0;i<dim;i++)
    for(j=i;j<dim;j++)
      gsl_matrix_set (I,i,j,
		      gsl_matrix_get (I,i,j)/N);

}



/* Object Function */
/*---------------- */

void objf(const size_t n,const double *x,void *params,double *f){

  double dtmp1;
  unsigned utmp1;

  double sumL=0.0;
  double sumR=0.0;

  const double bl=x[0];
  const double br=x[1];
  const double al=x[2];
  const double ar=x[3];
  const double mu=x[4];

/*   fprintf(Fmessages,"#objf bl=%.3e br=%.3e al=%.3e ar=%.3e m=%.3e\n", */
/* 	  x[0],x[1],x[2],x[3],x[4]); */

  for(utmp1=0;utmp1<Size;utmp1++){
    if(Data[utmp1]<mu){
      sumL+=pow(mu-Data[utmp1],bl);
    }
    else if (Data[utmp1]>mu){
      sumR+=pow(Data[utmp1]-mu,br);
    }
  }

/*   for(utmp1=0;utmp1<Size;utmp1++){ */
/*     if(Data[utmp1]>mu) break;     */
/*     sumL+=pow(mu-Data[utmp1],bl); */
/*   } */
/*   for(;utmp1<Size;utmp1++){ */
/*     sumR+=pow(Data[utmp1]-mu,br); */
/*   } */

  sumL /= Size;
  sumR /= Size;

  dtmp1 = al*pow(bl,1./bl)*gsl_sf_gamma(1./bl+1.)
    +ar*pow(br,1./br)*gsl_sf_gamma(1./br+1.);

  *f = log(dtmp1)
    +sumL/(pow(al,bl)*bl)+sumR/(pow(ar,br)*br);

}


void objdf(const size_t n,const double *x,void *params,double *df){

  double dtmp1;
  
  const double bl=x[0];
  const double br=x[1];
  const double al=x[2];
  const double ar=x[3];
  const double mu=x[4];

  unsigned utmp1;
  double sumL=0.0;
  double sumR=0.0;
  double sumL1=0.0;
  double sumR1=0.0;
  double sumLl=0.0;
  double sumRl=0.0;
/*   fprintf(Fmessages,"#objdf bl=%.3e br=%.3e al=%.3e ar=%.3e m=%.3e\n", */
/* 	  x[0],x[1],x[2],x[3],x[4]); */



  for(utmp1=0;utmp1<Size;utmp1++){
    if(Data[utmp1]<mu){
      sumL1+= (dtmp1 = pow(mu-Data[utmp1],bl-1.) );
      dtmp1*= mu-Data[utmp1];
      sumL+= dtmp1;
      sumLl+=log(mu-Data[utmp1])*dtmp1;
    }
    else if (Data[utmp1]>mu){
      sumR1+=(dtmp1 = pow(Data[utmp1]-mu,br-1.));
      dtmp1*= Data[utmp1]-mu;
      sumR+=dtmp1;
      sumRl+=log(Data[utmp1]-mu)*dtmp1;
    }
  }

  sumL /= Size;
  sumR /= Size;
  sumL1 /= Size;
  sumR1 /= Size;
  sumLl /= Size;
  sumRl /= Size;

  dtmp1 = al*pow(bl,1./bl)*gsl_sf_gamma(1./bl+1.)
    +ar*pow(br,1./br)*gsl_sf_gamma(1./br+1.);

  df[0] = (1.-log(bl)-gsl_sf_psi(1./bl+1.))*gsl_sf_gamma(1./bl+1.)*pow(bl,1./bl-2.)*al/dtmp1
    -(1./(bl*bl) + log(al)/bl)*sumL/pow(al,bl) + sumLl/(pow(al,bl)*bl);
  
  df[1] = (1.-log(br)-gsl_sf_psi(1./br+1.))*gsl_sf_gamma(1./br+1.)*pow(br,1./br-2.)*ar/dtmp1
    -(1./(br*br) + log(ar)/br)*sumR/pow(ar,br) + sumRl/(pow(ar,br)*br);
  
  df[2] = pow(bl,1./bl)*gsl_sf_gamma(1./bl+1.)/dtmp1
    - sumL/pow(al,bl+1.);
  
  df[3] = pow(br,1./br)*gsl_sf_gamma(1./br+1.)/dtmp1
    - sumR/pow(ar,br+1.);
  
  df[4] = sumL1/pow(al,bl) - sumR1/pow(ar,br);


}

void objfdf(const size_t n,const double *x,void *params,double *f,double *df){


  double dtmp1;

  const double bl=x[0];
  const double br=x[1];
  const double al=x[2];
  const double ar=x[3];
  const double mu=x[4];

  unsigned utmp1;
  double sumL=0.0;
  double sumR=0.0;
  double sumL1=0.0;
  double sumR1=0.0;
  double sumLl=0.0;
  double sumRl=0.0;

/*   fprintf(Fmessages,"#objfdf bl=%.3e br=%.3e al=%.3e ar=%.3e m=%.3e\n", */
/* 	  x[0],x[1],x[2],x[3],x[4]); */

  for(utmp1=0;utmp1<Size;utmp1++){
    if(Data[utmp1]<mu){
      sumL1+= (dtmp1 = pow(mu-Data[utmp1],bl-1.) );
      dtmp1*= mu-Data[utmp1];
      sumL+= dtmp1;
      sumLl+=log(mu-Data[utmp1])*dtmp1;
    }
    else if (Data[utmp1]>mu){
      sumR1+=(dtmp1 = pow(Data[utmp1]-mu,br-1.));
      dtmp1*= Data[utmp1]-mu;
      sumR+=dtmp1;
      sumRl+=log(Data[utmp1]-mu)*dtmp1;
    }
  }

  sumL /= Size;
  sumR /= Size;
  sumL1 /= Size;
  sumR1 /= Size;
  sumLl /= Size;
  sumRl /= Size;

  dtmp1 = al*pow(bl,1./bl)*gsl_sf_gamma(1./bl+1.)
    +ar*pow(br,1./br)*gsl_sf_gamma(1./br+1.);

  *f = log(dtmp1)
    +sumL/(pow(al,bl)*bl)+sumR/(pow(ar,br)*br);

  df[0] = (1.-log(bl)-gsl_sf_psi(1./bl+1.))*gsl_sf_gamma(1./bl+1.)*pow(bl,1./bl-2.)*al/dtmp1
    -(1./(bl*bl) + log(al)/bl)*sumL/pow(al,bl) + sumLl/(pow(al,bl)*bl);
  
  df[1] = (1.-log(br)-gsl_sf_psi(1./br+1.))*gsl_sf_gamma(1./br+1.)*pow(br,1./br-2.)*ar/dtmp1
    -(1./(br*br) + log(ar)/br)*sumR/pow(ar,br) + sumRl/(pow(ar,br)*br);
  
  df[2] = pow(bl,1./bl)*gsl_sf_gamma(1./bl+1.)/dtmp1
    - sumL/pow(al,bl+1.);
  
  df[3] = pow(br,1./br)*gsl_sf_gamma(1./br+1.)/dtmp1
    - sumR/pow(ar,br+1.);
  
  df[4] = sumL1/pow(al,bl) - sumR1/pow(ar,br);

}
/*---------------- */



int main(int argc,char* argv[]){
  
  /* store guess */
  double x[5]={2.,2.,1.,1.,0}; /* x[0]=bl x[1]=br x[2]=al x[3]=ar x[4]=mu */
  double fmin;  
  unsigned type[5];
  double xmin[5];
  double xmax[5];

  /* store possibly provided values for parameters */
  double provided_m=0;
  unsigned is_m_provided=0;

  /* set various options */
  unsigned O_verbose=0;
  unsigned O_output=0;
  unsigned O_method=6;
  enum{M_moments=1,M_global=2,M_intervals=4};

  /* global optimization parameters */
  struct multimin_params global_oparams={.1,1e-2,100,1e-3,1e-5,2,0};
  /* interval optimization parameters */
  struct multimin_params interv_oparams={.01,1e-3,200,1e-3,1e-5,5,0};
  /* increment in the number of intervals to expore */
  size_t interv_step=10;


  char *splitstring = strdup(" \t");

  /* COMMAND LINE PROCESSING */
  /* ----------------------- */ 
  int opt;

  /* read the command line */    
  while((opt=getopt_long(argc,argv,"O:s:d:hV:I:G:s:m:M:x:F:",gb_long_options, &gb_option_index))!=EOF){
    if(opt==0){
      gbutils_header(argv[0],stdout);
      exit(0);
    }
    else if(opt=='?'){
      fprintf(stderr,"option %c not recognized\n",optopt);
      exit(-1);
    }
    else if(opt=='h'){
      /*print short help*/
      fprintf(stdout,"Fit asymmetric power exponential density using maximum-likelihood.\n");
      fprintf(stdout,"Fit asymmetric power exponential density. Read from files of from standard input\n\n");
      fprintf(stdout,"Usage: %s [options] [files]\n\n",argv[0]);
      fprintf(stdout," -O  output type (default 0)                \n");
      fprintf(stdout,"      0  parameter bl br al ar m and log-likelihood   \n");
      fprintf(stdout,"      1  the estimated distribution function computed on the provided points     \n");
      fprintf(stdout,"      2  the estimated density function computed on the provided points \n");
      fprintf(stdout,"      3  parameters bl br al ar m and their standard errors \n");
      fprintf(stdout," -x  set initial conditions bl,br,al,ar,m  (default 2,2,1,1,0)\n");
      fprintf(stdout," -m  the mode is not estimated but is set to the value provided\n");
      fprintf(stdout," -s  number of intervals to explore at each iteration (default 10)\n");
      fprintf(stdout," -V  verbosity level (default 0)           \n");
      fprintf(stdout,"      0  just the final result        \n");
      fprintf(stdout,"      1  headings and summary table   \n");
      fprintf(stdout,"      2  intermediate steps results   \n");
      fprintf(stdout,"      3  intermediate steps internals \n");
      fprintf(stdout,"      4+  details of optim. routine   \n");
      fprintf(stdout," -M  active estimation steps. The value is the sum of (default 6)\n");
      fprintf(stdout,"      2  global optimization not considering lack of smoothness in m\n");
      fprintf(stdout,"      4  local optimization taking non-smoothness in m into consideration \n");
      fprintf(stdout," -G  set global optimization options. Fields are step,tol,iter,eps,msize,algo.\n");
      fprintf(stdout,"     Empty field implies default (default .1,1e-2,100,1e-3,1e-5,2,0)\n");
      fprintf(stdout," -I  set local optimization options. Fields are step,tol,iter,eps,msize,algo.\n");
      fprintf(stdout,"     Empty field implies default (default .01,1e-3,200,1e-3,1e-5,5,0)\n");
      fprintf(stdout,"The optimization parameters are");
      fprintf(stdout," step  initial step size of the searching algorithm                  \n");
      fprintf(stdout," tol  line search tolerance iter: maximum number of iterations      \n");
      fprintf(stdout," eps  gradient tolerance : stopping criteria ||gradient||<eps       \n");
      fprintf(stdout," msize  simplex max size : stopping criteria ||max edge||<msize     \n");
      fprintf(stdout," algo  optimization methods: 0 Fletcher-Reeves, 1 Polak-Ribiere,     \n");
      fprintf(stdout,"       2 Broyden-Fletcher-Goldfarb-Shanno, 3 Steepest descent,           \n");
      fprintf(stdout,"       4 Nelder-Mead simplex, 5 Broyden-Fletcher-Goldfarb-Shanno ver.2   \n");
      fprintf(stdout,"       6 Nelder-Mead simplex ver. 2, 7 Nelder-Mead simplex rnd init.  \n");
      fprintf(stdout,"Examples:\n");
      fprintf(stdout," gbaepfit -m 1 -M 4 <file  estimate bl,br,al,ar with m=1 and skipping initial  \n");
      fprintf(stdout,"                           global optimization\n");
      exit(0);
    }
    else if(opt=='F'){
      /*set the fields separator string*/
      free(splitstring);
      splitstring = strdup(optarg);
    }
    else if(opt=='V'){
      O_verbose=atoi(optarg);
      global_oparams.verbosity = (O_verbose>3?O_verbose-3:0);
      interv_oparams.verbosity = (O_verbose>3?O_verbose-3:0);
    }
    else if(opt=='M'){
      O_method = (unsigned) atoi(optarg);
    }
    else if(opt=='s'){
      interv_step=(size_t) atoi(optarg);
    }
    else if(opt=='G'){
      char *stmp1=strdup (optarg);
      char *stmp2;
      char *stmp3=stmp1;
      if( (stmp2=strsep (&stmp1,",")) != NULL){
	if(strlen(stmp2)>0) 
	  global_oparams.step_size=atof(stmp2);
      }
      if( (stmp2=strsep (&stmp1,",")) != NULL){
	if(strlen(stmp2)>0) 
	  global_oparams.tol=atof(stmp2);
      }
      if( (stmp2=strsep (&stmp1,",")) != NULL){
	if(strlen(stmp2)>0) 
	  global_oparams.maxiter=(unsigned) atoi(stmp2);
      }
      if( (stmp2=strsep (&stmp1,",")) != NULL){
	if(strlen(stmp2)>0) 
	  global_oparams.epsabs=atof(stmp2);
      }
      if( (stmp2=strsep (&stmp1,",")) != NULL){
	if(strlen(stmp2)>0)
	  global_oparams.maxsize=atof(stmp2);
      }
      if( (stmp2=strsep (&stmp1,",")) != NULL){
	if(strlen(stmp2)>0) 
	  global_oparams.method= (unsigned) atoi(stmp2);
      }
      free(stmp3);
    }
    else if(opt=='I'){
      char *stmp1=strdup (optarg);
      char *stmp2;
      char *stmp3=stmp1;
      if( (stmp2=strsep (&stmp1,",")) != NULL){
	if(strlen(stmp2)>0) 
	  interv_oparams.step_size=atof(stmp2);
      }
      if( (stmp2=strsep (&stmp1,",")) != NULL){
	if(strlen(stmp2)>0) 
	  interv_oparams.tol=atof(stmp2);
      }
      if( (stmp2=strsep (&stmp1,",")) != NULL){
	if(strlen(stmp2)>0) 
	  interv_oparams.maxiter=(unsigned) atoi(stmp2);
      }
      if( (stmp2=strsep (&stmp1,",")) != NULL){
	if(strlen(stmp2)>0) 
	  interv_oparams.epsabs=atof(stmp2);
      }
      if( (stmp2=strsep (&stmp1,",")) != NULL){
	if(strlen(stmp2)>0)
	  interv_oparams.maxsize=atof(stmp2);
      }
      if( (stmp2=strsep (&stmp1,",")) != NULL){
	if(strlen(stmp2)>0) 
	  interv_oparams.method= (unsigned) atoi(stmp2);
      }
      free(stmp3);
    }
    else if(opt=='O'){
      O_output=atoi(optarg);
    }
    else if(opt=='m'){
      provided_m=atof(optarg);
      is_m_provided=1;
    }
    else if(opt=='x'){
      char *stmp1=strdup (optarg);
      char *stmp2;
      char *stmp3=stmp1;
      if( (stmp2=strsep (&stmp1,",")) != NULL){
	if(strlen(stmp2)>0)
	  x[0]=atof(stmp2);
      }
      if( (stmp2=strsep (&stmp1,",")) != NULL){
	if(strlen(stmp2)>0) 
	  x[1]=atof(stmp2);
      }
      if( (stmp2=strsep (&stmp1,",")) != NULL){
	if(strlen(stmp2)>0) 
	  x[2]=atof(stmp2);
      }
      if( (stmp2=strsep (&stmp1,",")) != NULL){
	if(strlen(stmp2)>0) 
	  x[3]=atof(stmp2);
      }
      if( (stmp2=strsep (&stmp1,",")) != NULL){
	if(strlen(stmp2)>0) 
	  x[4]=atof(stmp2);
      }
      free(stmp3);
      
      if(x[0]<=0 || x[1]<=0 || x[2]<=0 || x[3]<=0){
	fprintf(Fmessages,"initial values for bl, br, al and ar should be positive\n");
	exit(-1);
      }      
    }
  }

  /* initialize global variables */
  initialize_program(argv[0]);

  /* customized admin. of errors */
  /* --------------------------- */
  gsl_set_error_handler_off ();
  
  
  /* set the various streams */
  /* ----------------------- */
  if(O_output==0 || O_output==3){
    Fmessages=stderr;
    Fparams=stdout;
    Ffitted=stderr;
  }
  else{
    Fmessages=stderr;
    Fparams=stderr;
    Ffitted=stdout;
  }

  /* load Data */
  /* --------- */

  if(O_verbose>=2)
    fprintf(Fmessages,"#--- START LOADING DATA\n");

  load(&Data,&Size,0,splitstring);

  if (Size <= 1) {
    fprintf(Fmessages,"the number of onservations must be at least 2\n");
    exit(-1);
  }

  /* sort data */
  /* --------- */
  qsort(Data,Size,sizeof(double),sort_by_value);


  /* initial values */
  /* -------------- */
  if (is_m_provided)
    x[4] = provided_m;

  objf(5,x,NULL,&fmin);

  /* output of initial values */
  /* ------------------------ */
  if(O_verbose>=2){
    fprintf(Fmessages,"#--- INITIAL VALUES\n");
    fprintf(Fmessages,"#>>> bl=%.3e br=%.3e al=%.3e ar=%.3e m=%.3e ll=%.3e\n",
	    x[0],x[1],x[2],x[3],x[4],fmin);
  }


  /* no minimization */
  /* --------------- */
  if(!( ( O_method & M_global)|(O_method & M_intervals) ))
    goto END;


  /* ML estimation: global maximization */
  /* ---------------------------------- */

  if(is_m_provided && (O_method & M_global)){

    if(O_verbose>=2)
      fprintf(Fmessages,"#--- UNCONSTRAINED OPTIMIZATION\n");

    /* set initial minimization boundaries */
    /* ----------------------------------- */
    type[0] = 4; xmin[0]=0; xmax[0]=0;
    type[1] = 4; xmin[1]=0; xmax[1]=0;
    type[2] = 4; xmin[2]=0; xmax[2]=0;
    type[3] = 4; xmin[3]=0; xmax[3]=0;
    type[4] = 3; xmin[4]=provided_m; xmax[4]=provided_m;

    /* perform global minimization */
    /* --------------------------- */
    multimin(5,x,&fmin,type,xmin,xmax,objf,objdf,objfdf,NULL,global_oparams);
    
    } 
  else if(!is_m_provided) {

    double dtmp1;    
    double xtmp[5]; /* store the temporary minimum  */
    unsigned oldindex,utmp1,index,max,min;
    
    if( O_method & M_global ){
      
      if(O_verbose>=2)
	fprintf(Fmessages,"#--- UNCONSTRAINED OPTIMIZATION\n");
      
      /* set initial minimization boundaries */
      /* ----------------------------------- */
      type[0] = 4; xmin[0]=0; xmax[0]=0;
      type[1] = 4; xmin[1]=0; xmax[1]=0;
      type[2] = 4; xmin[2]=0; xmax[2]=0;
      type[3] = 4; xmin[3]=0; xmax[3]=0;
      type[4] = 0; xmin[4]=0; xmax[4]=0;
      
      /* perform global minimization */
      /* --------------------------- */
      multimin(5,x,&fmin,type,xmin,xmax,objf,objdf,objfdf,NULL,global_oparams);
      
      if(O_verbose>=2)
	fprintf(Fmessages,"#>>> bl=%.3e br=%.3e al=%.3e ar=%.3e m=%.3e ll=%.3e\n",
		x[0],x[1],x[2],x[3],x[4],fmin);
    }
    
    if(!(O_method & M_intervals)) goto END;
    
    /* perform interval minimization */
    /* ----------------------------- */

    if(O_verbose>=2)
      fprintf(Fmessages,"#--- INTERVAL-WISE OPTIMIZATION\n");

    /* check plausibility of specified number of intervals */
    if(interv_step>Size/2){
      fprintf(Fmessages,
	      "#WARNING: Too much intervals specified. Number of intervals set to %zd\n",
	      Size/2);
      interv_step==Size/2;
    }
    
    type[4] = 3;/* the value of m is bounded on compact intervals */
    
    /* find initial index s.t. m \in [Data[index],Data[index+1]] */
    for(utmp1=0;Data[utmp1]<=x[4] && utmp1<Size;utmp1++);
    if(utmp1 == 0)
      index = 0;
    else if (utmp1 == Size)
      index= Size-2;
    else index = utmp1-1;
    
    xmin[4]=Data[index]; xmax[4]=Data[index+1]; x[4]=.5*(xmin[4]+xmax[4]);
    
    multimin(5,x,&fmin,type,xmin,xmax,objf,objdf,objfdf,NULL,interv_oparams);
    
    max=min=index;
    
    if(O_verbose>=3)
      fprintf(Fmessages,"#>>> [%+.3e:%+.3e] ll=%e\n",Data[index],Data[index+1],fmin);

    /* set the value to best estimation */
    xtmp[0]=x[0]; xtmp[1]=x[1]; xtmp[2]=x[2]; xtmp[3]=x[3]; xtmp[4]=x[4];

    /* move to the right, compute new local minima and compare with
       global minimum */
    do {
      oldindex=index;

      /* move to the right */
      for(utmp1=max+1;
	  utmp1<=max+interv_step && utmp1<Size-1;
	  utmp1++){
	
	/* set boundaries on m */
	xmin[4]=Data[utmp1]; xmax[4]=Data[utmp1+1];
	/* set initial condition */
	xtmp[4]=.5*(xmin[4]+xmax[4]);
	
	multimin(5,xtmp,&dtmp1,type,xmin,xmax,objf,objdf,objfdf,NULL,interv_oparams);
	
	if(dtmp1<fmin){/* found new minimum */
	  index=utmp1;
	  x[0]=xtmp[0]; x[1]=xtmp[1]; x[2]=xtmp[2]; x[3]=xtmp[3]; x[4]=xtmp[4];
	  fmin=dtmp1;
	  
	  if(O_verbose>=3)
	    fprintf(Fmessages,"#>>> [%+.3e:%+.3e] ll=%e\n",
		    Data[utmp1],Data[utmp1+1],dtmp1);
	}
	else {/* NOT found new minimum */
	  if(O_verbose>=3)
	    fprintf(Fmessages,"#    [%+.3e:%+.3e] ll=%e\n",
		    Data[utmp1],Data[utmp1+1],dtmp1);
	}
      }
      max=utmp1-1;
    }
    while(index!=oldindex);

    /* set the value to best estimation */
    xtmp[0]=x[0];xtmp[1]=x[1];xtmp[2]=x[2];
    
    /* move to the left, compute new local minima and compare with
       global minimum */

    /* compute new local minima and compare with global minimum */    
    do {

      oldindex=index;
      /* move to the left */
      for(utmp1=min-1;
	  (int) utmp1 >= (int) min-interv_step && (int) utmp1 >= 0;
	  utmp1--){
	
	/* set boundaries on m */
	xmin[4]=Data[utmp1]; xmax[4]=Data[utmp1+1];
	/* set initial condition */
	xtmp[4]=.5*(xmin[4]+xmax[4]);
	
	multimin(5,xtmp,&dtmp1,type,xmin,xmax,objf,objdf,objfdf,NULL,interv_oparams);
	
	if(dtmp1<fmin){/* found new minimum */
	  index=utmp1;
	  x[0]=xtmp[0]; x[1]=xtmp[1]; x[2]=xtmp[2]; x[3]=xtmp[3]; x[4]=xtmp[4];
	  fmin=dtmp1;
	  
	  if(O_verbose>=3)
	    fprintf(Fmessages,"#>>> [%+.3e:%+.3e] ll=%e\n",
		    Data[utmp1],Data[utmp1+1],dtmp1);
	}
	else {/* NOT found new minimum */
	  if(O_verbose>=3)
	    fprintf(Fmessages,"#    [%+.3e:%+.3e] ll=%e\n",
		    Data[utmp1],Data[utmp1+1],dtmp1);
	}
	
      }
      
      min=utmp1+1;
    }
    while(index!=oldindex);

    if(O_verbose>=2)
      fprintf(Fmessages,"#>>> bl=%.3e br=%.3e al=%.3e ar=%.3e m=%.3e ll=%.3e\n",
	      x[0],x[1],x[2],x[3],x[4],fmin);
    
    if(O_verbose>=3)
      fprintf(Fmessages,"#--- intervals explored: %d\n",max-min);

  }

 END:

  /* output */
  /* ------ */
  { 

    size_t i;

    /* define the var-covar matrix */
    gsl_matrix *V;

    /* set the size of the var-covar matrix */
    const size_t dim=(is_m_provided?4:5);

    if(O_output == 3 || O_verbose >=1 ){

      /* allocate var-covar matrix */
      V = gsl_matrix_calloc (dim,dim);
      
      /* compute var-covar matrix */
      varcovar(x,Size,dim,V);
      
      if(O_verbose>=1){
	fprintf(Fmessages,"#\n");
	fprintf(Fmessages,"#--- FINAL RESULT --------------------------------------------------\n");
	fprintf(Fmessages,"#                           | correlation matrix\n");
	if(!is_m_provided)
	  fprintf(Fmessages,"#     value     std.err     |  bl      br      al      ar      m\n");
	else
	  fprintf(Fmessages,"#     value     std.err     |  bl      br      al      ar\n");

	fprintf(Fmessages,"# bl= %- 10.4g %-10.4g |",x[0],sqrt(gsl_matrix_get(V,0,0)));
	for(i=0;i<dim;i++)
	  if(i==0)
	    fprintf(Fmessages,"    -- ");
	  else
	    fprintf(Fmessages," % .4f",gsl_matrix_get(V,0,i));
	fprintf(Fmessages,"\n");
	
	fprintf(Fmessages,"# br= %- 10.4g %-10.4g |",x[1],sqrt(gsl_matrix_get(V,1,1)));
	for(i=0;i<dim;i++)
	  if(i==1)
	    fprintf(Fmessages,"    -- ");
	  else
	    fprintf(Fmessages," % .4f",gsl_matrix_get(V,1,i));
	fprintf(Fmessages,"\n");
	
	fprintf(Fmessages,"# al= %- 10.4g %-10.4g |",x[2],sqrt(gsl_matrix_get(V,2,2)));
	for(i=0;i<dim;i++)
	  if(i==2)
	    fprintf(Fmessages,"    -- ");
	  else
	    fprintf(Fmessages," % .4f",gsl_matrix_get(V,2,i));
	fprintf(Fmessages,"\n");

	fprintf(Fmessages,"# ar= %- 10.4g %-10.4g |",x[3],sqrt(gsl_matrix_get(V,3,3)));
	for(i=0;i<dim;i++)
	  if(i==3)
	    fprintf(Fmessages,"    -- ");
	  else
	    fprintf(Fmessages," % .4f",gsl_matrix_get(V,3,i));
	fprintf(Fmessages,"\n");

	if(!is_m_provided){
	  fprintf(Fmessages,"# m = %- 10.4g %-10.4g |",x[4],sqrt(gsl_matrix_get(V,4,4)));
	  for(i=0;i<dim;i++)
	    if(i==4)
	      fprintf(Fmessages,"    -- ");
	    else
	      fprintf(Fmessages," % .4f",gsl_matrix_get(V,4,i));
	  fprintf(Fmessages,"\n");
	}
	else
	  fprintf(Fmessages,"#m = %- 10.4g  ---       |\n",x[4]);

	fprintf(Fmessages,"#\n");
	fprintf(Fmessages,"#                           Upper triangle: covariances\n");
	fprintf(Fmessages,"#                           Lower triangle: correlation coefficients\n");
	fprintf(Fmessages,"#-------------------------------------------------------------------\n");
	fprintf(Fmessages,"#\n");
      }
      
    }

    switch(O_output){
    case 0: /* print parameters b,a,m, and log-likelihood */
      if(O_verbose>=1){
	fprintf(Fmessages,"#");
	fprintf(Fmessages,EMPTY_SEP,"bl");
	fprintf(Fmessages,EMPTY_SEP,"br");
	fprintf(Fmessages,EMPTY_SEP,"al");
	fprintf(Fmessages,EMPTY_SEP,"ar");
	fprintf(Fmessages,EMPTY_SEP,"m");
	fprintf(Fmessages,EMPTY_NL,"log-like");
      }
      fprintf(Fparams,FLOAT_SEP,x[0]);
      fprintf(Fparams,FLOAT_SEP,x[1]);
      fprintf(Fparams,FLOAT_SEP,x[2]);
      fprintf(Fparams,FLOAT_SEP,x[3]);
      fprintf(Fparams,FLOAT_SEP,x[4]);
      fprintf(Fparams,FLOAT_NL,fmin);
      break;
    case 1: /* print distribution function */
      if(O_verbose==0){
	fprintf(Fparams,FLOAT_SEP,x[0]);
	fprintf(Fparams,FLOAT_SEP,x[1]);
	fprintf(Fparams,FLOAT_SEP,x[2]);
	fprintf(Fparams,FLOAT_SEP,x[3]);
	fprintf(Fparams,FLOAT_SEP,x[4]);
	fprintf(Fparams,FLOAT_NL,fmin);
      }
      printcumul(x);
      break;
    case 2: /* print density */
      if(O_verbose==0){
	fprintf(Fparams,FLOAT_SEP,x[0]);
	fprintf(Fparams,FLOAT_SEP,x[1]);
	fprintf(Fparams,FLOAT_SEP,x[2]);
	fprintf(Fparams,FLOAT_SEP,x[3]);
	fprintf(Fparams,FLOAT_SEP,x[4]);
	fprintf(Fparams,FLOAT_NL,fmin);
      }
      printdensity(x);
      break;
    case 3: /* print parameters final estimates and standard errors*/
      if(O_verbose>=1){
	fprintf(Fmessages,"#");
	fprintf(Fmessages,EMPTY_SEP,"bl");
	fprintf(Fmessages,EMPTY_SEP,"br");
	fprintf(Fmessages,EMPTY_SEP,"al");
	fprintf(Fmessages,EMPTY_SEP,"ar");
	fprintf(Fmessages,EMPTY_SEP,"m");
	fprintf(Fmessages,EMPTY_NL,"log-like");
	if(!is_m_provided)
	  fprintf(Fmessages,EMPTY_NL,"sigma_m\n");
	else 
	  fprintf(Fmessages,"\n");

      }
      fprintf(Fparams,FLOAT_SEP,x[0]);
      fprintf(Fparams,FLOAT_SEP,x[1]);
      fprintf(Fparams,FLOAT_SEP,x[2]);
      fprintf(Fparams,FLOAT_SEP,x[3]);
      fprintf(Fparams,FLOAT_SEP,x[4]);
      for(i=0;i<dim;i++) fprintf(Fparams,FLOAT_SEP,sqrt(gsl_matrix_get(V,i,i)));
      fprintf(Fparams,"\n");
      break;
    }

    /* free allocated space */
    if(O_output == 3 || O_verbose >=1 ) gsl_matrix_free(V);
    
  }

  free(Data);
  free(splitstring);
  return 0;

}
