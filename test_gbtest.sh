#!/bin/bash

# created: 2021-08-06 giulio
# Time-stamp: <2022-04-11 14:02:28 giulio>

res=`echo "
10 20
11 21
12 22
13 23
14 24
" | ./gbtest R | ./gbget '()' -o %d`

status=$?

[[ $status == 0 && $res == 1 ]]

