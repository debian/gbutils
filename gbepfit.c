/*
  gbepfit (ver. 6.0) -- Fit a power exponential density via maximum
  likelihood Copyright
  
  (C) 2018 Giulio Bottazzi

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  (version 2) as published by the Free Software Foundation;
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/


/*
  Verbosity levels:
  0 just the final ouput
  1 the results of intermediate steps
  2 internal information on intermediate steps
  3 gory details on intermediate steps
*/


#include "tools.h"
#include "multimin.h"
#include "assert.h"

#include <gsl/gsl_sf_gamma.h>
#include <gsl/gsl_sf_psi.h>
#include <gsl/gsl_roots.h>


/* Global Variables */
/* ---------------- */

/* store data */
double *Data; /*the array of data*/
size_t Size;/*the number of data*/

/* output management */
FILE *Fmessages;/*stream for computation messages*/
FILE *Fparams;/*strem for final parameters*/
FILE *Ffitted;/*strem for distribution or density*/

/*------------------*/


/* Compute moments */
/* --------------- */
void moment_local(double *ave, double *adev, double *sdev, double *var){
  unsigned j;
  double ep=0.0,s,p;
  
  s=0.0;
  for (j=0;j<Size;j++) s += Data[j];
  *ave=s/Size;
  *adev=(*var)=0.0;
  for (j=0;j<Size;j++) {
    *adev += fabs(s=Data[j]-(*ave));
    ep += s;
    *var += (p=s*s);
   }
  *adev /= Size;
  *var=(*var-ep*ep/Size)/(Size-1);
  *sdev=sqrt(*var);
}
/* --------------- */

/* Output Functions */
/*----------------- */

double geta(const double b,const double mu){

  double sum=0.0;
  unsigned utmp1;

  for(utmp1=0;utmp1<Size;utmp1++){
    sum+=pow(fabs(mu-Data[utmp1]),b);
  }

  return(pow(sum/Size,1./b));
}

void printcumul(double x[]){

  unsigned i;
  double dtmp1; 
  const double b=x[0];
  const double a=x[1];
  const double m=x[2];

  for(i=0;i<Size;i++){
    dtmp1=pow(fabs(Data[i]-m)/a,b)/b;
    dtmp1= .5+.5*(Data[i]>m?1.:-1.)*gsl_sf_gamma_inc_P(1./b,dtmp1);
    fprintf(Ffitted,FLOAT_SEP,Data[i]);
    fprintf(Ffitted,FLOAT_NL,dtmp1);
  }

}

void printdensity(double x[]){

  unsigned i;
  const double b=x[0];
  const double a=x[1];
  const double m=x[2];

  const double norm=2*a*gsl_sf_gamma(1./b+1.)*pow(b,1./b);

  for(i=0;i<Size;i++){
    const double dtmp1=Data[i];
    fprintf(Ffitted,FLOAT_SEP,dtmp1);
    fprintf(Ffitted,FLOAT_NL,exp(-pow(fabs(dtmp1-m)/a,b)/b)/norm);
  }

}
/*----------------- */

/* 
   par   array of paramaters
   N     number of observations
   dim   dimension of the matrix: 2 m known; 3 m unknown
   I     the variance-covarance matrxi
*/

void varcovar(const double par[], const size_t N, const size_t dim, gsl_matrix *I){

  const double b=par[0];
  const double a=par[1];
  const double m=par[2];
  const double dtmp1= log(b)+gsl_sf_psi(1+1./b);

  size_t i,j;

  /* needed for inversion */
  gsl_matrix *J = gsl_matrix_calloc (dim,dim);
  gsl_permutation *P = gsl_permutation_alloc (dim);
  int signum;


  /* define the matrix */
  
  /* b - b */
  gsl_matrix_set (I,0,0,
		  (dtmp1*dtmp1+gsl_sf_psi_1(1+1/b)*(1+1/b)-1)/pow(b,3));
  
  /* b - a */
  gsl_matrix_set (I,0,1,-dtmp1/(a*b));
  gsl_matrix_set (I,1,0,-dtmp1/(a*b));
  

  /* a - a */
  gsl_matrix_set (I,1,1,b/(a*a));


  if(dim == 3){

    /* b - m */
    gsl_matrix_set (I,0,2,0);
    gsl_matrix_set (I,2,0,0);

    /* a - m */
    gsl_matrix_set (I,1,2,0);
    gsl_matrix_set (I,2,1,0);

    /* m - m */
    gsl_matrix_set (I,2,2,
		    (pow(b,-2/b+1)*gsl_sf_gamma(2-1/b))/
		    (gsl_sf_gamma(1+1/b)*a*a));
  }

  /* invert I; in J store temporary LU decomp. */
  gsl_matrix_memcpy (J,I);
  gsl_linalg_LU_decomp (J,P,&signum);
  gsl_linalg_LU_invert (J,P,I);

  /* free allocated memory */
  gsl_matrix_free(J);
  gsl_permutation_free(P);


  /* set the var-covar matrix */
  for(i=0;i<dim;i++)
    for(j=0;j<i;j++)
      gsl_matrix_set (I,i,j,gsl_matrix_get (I,i,j)
		      /sqrt(gsl_matrix_get (I,i,i)*gsl_matrix_get (I,j,j)));

  for(i=0;i<dim;i++)
    for(j=i;j<dim;j++)
      gsl_matrix_set (I,i,j,
		      gsl_matrix_get (I,i,j)/N);

}



/* Method of Moments */
/*------------------ */


/*

The b parameter is determined by the equation:

sqrt( gamma[3/b]*gamma[1/b] )/gamma[2/b] = stdev/adev

taking the logarithm l=log(stdev/adev) and with x= -log(b)
one obtains the equation:

.5*lgamma(3*exp(x))+.5*lgamma(exp(x))-lgamma(2*exp(x))-l ==0

*/

/* this store the stdev on the average absolute deviation */
struct mm_params {
  double l;  /* l=log(stdev/adev) */
};



/* x = -log(b) */
double mm_f(double x,void *params){

  struct mm_params *p= (struct mm_params *) params;
  const double dtmp1=exp(x);
  const double y = .5*gsl_sf_lngamma(3.*dtmp1)+.5*gsl_sf_lngamma(dtmp1)-
    gsl_sf_lngamma(2.*dtmp1)-(p->l);

  return(y);
}

double mm_df(double x,void *params){

  const double dtmp1=exp(x);
  const double dy = dtmp1*(1.5*gsl_sf_psi(3.*dtmp1)+.5*gsl_sf_psi(dtmp1)-
			   2.*gsl_sf_psi(2.*dtmp1));

  return(dy);
}

void mm_fdf(double x,void *params, double *f, double *df){


  struct mm_params *p= (struct mm_params *) params;
  const double dtmp1=exp(x);

  *f = .5*gsl_sf_lngamma(3.*dtmp1)+.5*gsl_sf_lngamma(dtmp1)-
    gsl_sf_lngamma(2.*dtmp1)-(p->l);

  *df=dtmp1*(1.5*gsl_sf_psi(3.*dtmp1)+.5*gsl_sf_psi(dtmp1)-
	     2.*gsl_sf_psi(2.*dtmp1));
}


/* determination of b parameter from the ratio stdev/ave */
/* a_on_a = stdev/adev */
/* returns the parameter b */
double mm(const double s_on_a,const unsigned O_verbose){

  /* default initial value for -log(b)*/
  double mlogb=0.0;

  gsl_root_fdfsolver *d1solver;
  gsl_function_fdf FdF;

  struct mm_params params;

  unsigned utmp1;
  double dtmp1;
  int status;
  int max_iter=500;

  /* initialize the parameter */
  params.l = log(s_on_a);

  /* allocate the solver */
  d1solver=gsl_root_fdfsolver_alloc (gsl_root_fdfsolver_steffenson);

  /* set the function to solve */
  FdF.f = &mm_f;
  FdF.df = &mm_df;  
  FdF.fdf = &mm_fdf;
  FdF.params=&params;

  /* initial point */
  gsl_root_fdfsolver_set (d1solver,&FdF,mlogb);

  utmp1=0;
  do{
    utmp1++;
    status = gsl_root_fdfsolver_iterate (d1solver);

    if(status){
      fprintf(Fmessages,"#WARNING in 1d solver: %s\n", gsl_strerror (status));
      return(exp(-mlogb));
/*       exit(-1); */
    }
    dtmp1=mlogb;
    mlogb = gsl_root_fdfsolver_root (d1solver);
    status = gsl_root_test_delta (dtmp1,mlogb, 0.0, 1e-4);
    
    if(O_verbose>=3){
      if(status == GSL_SUCCESS)
	fprintf(Fmessages,"#    Converged after %d iterations lb=%f:\n",
		utmp1,-mlogb);
    }

    if(utmp1>=max_iter){
      if(O_verbose>=3){
	fprintf(Fmessages,"#WARNING in 1d solver  : exceeded max. num. of iterations %d\n",
		max_iter);
      }
      break;
    }
  }
  while (status == GSL_CONTINUE);
  gsl_root_fdfsolver_free (d1solver); 

  return(exp(-mlogb));
}


/*------------------ */



/* Object Functions */
/* ================ */


/* Functions of a and m */
/* -------------------- */

/* reduced log likelyhood x[0]=b x[1]=mu */
void objf(const size_t n,const double *x,void *params,double *f){

  const double b=x[0];
  const double mu=x[1];

  double sum=0.0;
  unsigned utmp1;
  int status;

  gsl_sf_result result;

  for(utmp1=0;utmp1<Size;utmp1++)
    sum+=pow(fabs(mu-Data[utmp1]),b);

  if( (status = gsl_sf_lngamma_e(1./b+1.,&result)) ){
    fprintf(Fmessages,"objf: lngamma(%e)=%e status  = %s \n", 1./b+1,result.val,gsl_strerror(status));
    fprintf(Fmessages,"b=%e mu=%e\n",b,mu);
/*     exit(-1); */
  }

  *f = log(2.)+log(sum/Size)/b + log(b)/b+result.val+1./b;
}


/* derivative of the reduced log likelyhood x[0]=b x[1]=mu */
void objdf(const size_t n,const double *x,void *params,double *df){

  const double b=x[0];
  const double mu=x[1];

  double sum1=0.0;
  double sum2=0.0;
  double sum3=0.0;
  unsigned utmp1;

  int status;
  gsl_sf_result result;

  for(utmp1=0;utmp1<Size;utmp1++){
    const double dtmp1=fabs(mu-Data[utmp1]);
    sum1+=pow(dtmp1,b);
    if(dtmp1!=0.0){
      sum2+=pow(dtmp1,b-1.)*(mu>Data[utmp1]?1.:-1.);
      sum3+=pow(dtmp1,b)*log(dtmp1);
    }
  }

  status = gsl_sf_psi_e(1./b+1.,&result);

  if(status){
    fprintf(Fmessages,"objdf [psi] status  = %s\n", gsl_strerror(status));
    fprintf(Fmessages,"b=%e mu=%e\n",b,mu);
    exit(-1);
  }

  df[0] = sum3/(b*sum1)-(log(sum1/Size)+log(b)+result.val)/(b*b);
  df[1] = sum2/sum1;

}



/* reduced likelyhood and derivatives x[0]=b x[1]=mu */
void objfdf(const size_t n,const double *x,void *params,double *f,double *df){

  const double b=x[0];
  const double mu=x[1];

  double sum1=0.0;
  double sum2=0.0;
  double sum3=0.0;
  unsigned utmp1;

  int status;
  gsl_sf_result result;

  for(utmp1=0;utmp1<Size;utmp1++){
    const double dtmp1=fabs(mu-Data[utmp1]);
    sum1+=pow(dtmp1,b);
    if(dtmp1!=0.0){
      sum2+=pow(dtmp1,b-1.)*(mu>Data[utmp1]?1.:-1.);
      sum3+=pow(dtmp1,b)*log(dtmp1);
    }
  }

  
  if( (status = gsl_sf_lngamma_e(1./b+1.,&result)) ){
    fprintf(Fmessages,"objfdf [lngamma] status  = %s\n", gsl_strerror(status));
    fprintf(Fmessages,"b=%e mu=%e\n",b,mu);
/*     exit(-1); */
  }


  *f=log(2.)+log(sum1/Size)/b + log(b)/b+result.val+1./b;

  if( (status = gsl_sf_psi_e(1./b+1.,&result)) ){
    fprintf(Fmessages,"objfdf [psi] status  = %s\n", gsl_strerror(status));
    fprintf(Fmessages,"b=%e mu=%e\n",b,mu);
    exit(-1);
  }

  df[0] = sum3/(b*sum1)-(log(sum1/Size)+log(b)+result.val)/(b*b);
  df[1] = sum2/sum1;

}



int main(int argc,char* argv[]){
  
  /* store guess */
  double par[3]={2.,1.,0.}; /* par[0]=b par[1]=a par[2]=m */
  double fmin;  

  /* store possibly provided values for parameters */
  double provided_m=0;
  unsigned is_m_provided=0;

  /* set various options */
  unsigned O_verbose=0;
  unsigned O_output=0;
  char O_method=7;
  enum{M_moments=1,M_global=2,M_intervals=4};


  /* global optimization parameters */
  struct multimin_params global_oparams={.1,1e-2,100,1e-3,1e-5,3,0};
  /* interval optimization parameters */
  struct multimin_params interv_oparams={.01,1e-3,200,1e-3,1e-5,5,0};
  /* increment in the number of intervals to expore */
  unsigned interv_step=10;


  char *splitstring = strdup(" \t");

  /* COMMAND LINE PROCESSING */
  /* ----------------------- */ 
  int opt;

  /* read the command line */    
  while((opt=getopt_long(argc,argv,"O:s:d:hV:I:G:s:m:M:x:F:",gb_long_options, &gb_option_index))!=EOF){
    if(opt==0){
      gbutils_header(argv[0],stdout);
      exit(0);
    }
    else if(opt=='?'){
      fprintf(stderr,"option %c not recognized\n",optopt);
      exit(-1);
    }
    else if(opt=='h'){
      /*print short help*/
      fprintf(stdout,"Fit symmetric power exponential density using maximum-likelihood or the method\n");
      fprintf(stdout,"of moments.\n");
      fprintf(stdout,"\nUsage: %s [options]\n\n",argv[0]);
      fprintf(stdout,"Options:\n");
      fprintf(stdout," -O  output type (default 0)                \n");
      fprintf(stdout,"      0  parameter b a m and log-likelihood   \n");
      fprintf(stdout,"      1  the estimated distribution function computed on the provided points     \n");
      fprintf(stdout,"      2  the estimated density function computed on the provided points \n");
      fprintf(stdout,"      3  parameters b a m and their standard errors \n");
      fprintf(stdout," -x  set initial conditions b,a,m  (default 2,1,0)\n");
      fprintf(stdout," -m  the mode is not estimated but is set to the value provided\n");
      fprintf(stdout," -s  number of intervals to explore at each iteration (default 10)\n");
      fprintf(stdout," -V  verbosity level (default 0)           \n");
      fprintf(stdout,"      0  just the final result        \n");
      fprintf(stdout,"      1  headings and summary table   \n");
      fprintf(stdout,"      2  intermediate steps results   \n");
      fprintf(stdout,"      3  intermediate steps internals \n");
      fprintf(stdout,"      4+  details of optim. routine   \n");
      fprintf(stdout," -M  active estimation steps. The value is the sum of (default 7)\n");
      fprintf(stdout,"      1  initial estimation based on method of moments\n");
      fprintf(stdout,"      2  global optimization not considering lack of smoothness in m\n");
      fprintf(stdout,"      4  local optimization taking non-smoothness in m into consideration \n");
      fprintf(stdout," -G  set global optimization options. Fields are step,tol,iter,eps,msize,algo.\n");
      fprintf(stdout,"     Empty field implies default (default .1,1e-2,100,1e-3,1e-5,3)\n");
      fprintf(stdout," -I  set local optimization options. Fields are step,tol,iter,eps,msize,algo.\n");
      fprintf(stdout,"     Empty field implies default (default .01,1e-4,200,1e-4,1e-5,5)\n");
      fprintf(stdout," -F  input fields separators (default \" \\t\")\n");
      fprintf(stdout," -h  this help                      \n");
      fprintf(stdout,"The optimization parameters are");
      fprintf(stdout," step  initial step size of the searching algorithm                   \n");
      fprintf(stdout," tol  line search tolerance iter: maximum number of iterations        \n");
      fprintf(stdout," eps  gradient tolerance : stopping criteria ||gradient||<eps         \n");
      fprintf(stdout," msize  simplex max size : stopping criteria ||max edge||<msize       \n");
      fprintf(stdout," algo  optimization methods: 0 Fletcher-Reeves, 1 Polak-Ribiere,      \n");
      fprintf(stdout,"       2 Broyden-Fletcher-Goldfarb-Shanno, 3 Steepest descent,        \n");
      fprintf(stdout,"       4 Nelder-Mead simplex, 5 Broyden-Fletcher-Goldfarb-Shanno v.2, \n");
      fprintf(stdout,"       6 Nelder-Mead simplex ver. 2, 7 Nelder-Mead simplex rnd init.  \n");
      fprintf(stdout,"Examples:\n");
      fprintf(stdout," gbepfit -m 1 -M 6 <file  estimate a and b with m=1 and skipping initial  \n");
      fprintf(stdout,"                          method of moments estimation\n");
      exit(0);
    }
    else if(opt=='F'){
      /*set the fields separator string*/
      free(splitstring);
      splitstring = strdup(optarg);
    }
    else if(opt=='V'){
      O_verbose=(unsigned) atoi(optarg);
      global_oparams.verbosity = (O_verbose>3?O_verbose-3:0);
      interv_oparams.verbosity = (O_verbose>3?O_verbose-3:0);
    }
    else if(opt=='M'){
      O_method = (char) atoi(optarg);
    }
    else if(opt=='s'){
      interv_step=(unsigned) atoi(optarg);
    }
    else if(opt=='G'){
      char *stmp1=strdup (optarg);
      char *stmp2;
      char *stmp3=stmp1;
      if( (stmp2=strsep (&stmp1,",")) != NULL){
	if(strlen(stmp2)>0) 
	  global_oparams.step_size=atof(stmp2);
      }
      if( (stmp2=strsep (&stmp1,",")) != NULL){
	if(strlen(stmp2)>0) 
	  global_oparams.tol=atof(stmp2);
      }
      if( (stmp2=strsep (&stmp1,",")) != NULL){
	if(strlen(stmp2)>0) 
	  global_oparams.maxiter=(unsigned) atoi(stmp2);
      }
      if( (stmp2=strsep (&stmp1,",")) != NULL){
	if(strlen(stmp2)>0) 
	  global_oparams.epsabs=atof(stmp2);
      }
      if( (stmp2=strsep (&stmp1,",")) != NULL){
	if(strlen(stmp2)>0)
	  global_oparams.maxsize=atof(stmp2);
      }
      if( (stmp2=strsep (&stmp1,",")) != NULL){
	if(strlen(stmp2)>0) 
	  global_oparams.method= (unsigned) atoi(stmp2);
      }
      free(stmp3);
    }
    else if(opt=='I'){
      char *stmp1=strdup (optarg);
      char *stmp2;
      char *stmp3=stmp1;
      if( (stmp2=strsep (&stmp1,",")) != NULL){
	if(strlen(stmp2)>0) 
	  interv_oparams.step_size=atof(stmp2);
      }
      if( (stmp2=strsep (&stmp1,",")) != NULL){
	if(strlen(stmp2)>0) 
	  interv_oparams.tol=atof(stmp2);
      }
	  if( (stmp2=strsep (&stmp1,",")) != NULL){
	    if(strlen(stmp2)>0) 
	      interv_oparams.maxiter=(unsigned) atoi(stmp2);
	  }
	  if( (stmp2=strsep (&stmp1,",")) != NULL){
	    if(strlen(stmp2)>0) 
	      interv_oparams.epsabs=atof(stmp2);
	  }
	  if( (stmp2=strsep (&stmp1,",")) != NULL){
	    if(strlen(stmp2)>0)
	      interv_oparams.maxsize=atof(stmp2);
	  }
	  if( (stmp2=strsep (&stmp1,",")) != NULL){
	    if(strlen(stmp2)>0) 
	      interv_oparams.method= (unsigned) atoi(stmp2);
	  }
	  free(stmp3);
	}
	else if(opt=='O'){
	  O_output=atoi(optarg);
	}
	else if(opt=='m'){
	  provided_m=atof(optarg);
	  is_m_provided=1;
	}
	else if(opt=='x'){
	  char *stmp1=strdup (optarg);
	  char *stmp2;
	  char *stmp3=stmp1;
	  if( (stmp2=strsep (&stmp1,",")) != NULL){
	    if(strlen(stmp2)>0)
	      par[0]=atof(stmp2);
	  }
	  if( (stmp2=strsep (&stmp1,",")) != NULL){
	    if(strlen(stmp2)>0) 
	      par[1]=atof(stmp2);
	  }
	  if( (stmp2=strsep (&stmp1,",")) != NULL){
	    if(strlen(stmp2)>0) 
	      par[2]=atof(stmp2);
	  }
	  free(stmp3);
	}
  }

  /* initialize global variables */
  initialize_program(argv[0]);

  /* customized admin. of errors */
  /* --------------------------- */
  gsl_set_error_handler_off ();
  

  /* set the various streams */
  /* ----------------------- */
  if(O_output==0 || O_output==3){
    Fmessages=stderr;
    Fparams=stdout;
    Ffitted=stderr;
  }
  else{
    Fmessages=stderr;
    Fparams=stderr;
    Ffitted=stdout;
  }

  /* load Data */
  /* --------- */

  if(O_verbose>=2)
    fprintf(Fmessages,"#--- LOADING DATA\n");
  
  load(&Data,&Size,0,splitstring);

  if (Size <= 1) {
    fprintf(Fmessages,"the number of onservations must be at least 2\n");
    exit(-1);
  }
  
  /* sort data */
  /* --------- */
  qsort(Data,Size,sizeof(double),sort_by_value);

  /* initial values */
  /* -------------- */
  if (is_m_provided)
    par[2] = provided_m;

  {
    const double x[2]={par[0],par[2]};
    objf(2,x,NULL,&fmin);    
  }
  

  /* output of initial values */
  /* ------------------------ */
  if(O_verbose>=2){
    fprintf(Fmessages,"#--- INITIAL VALUES\n");
    fprintf(Fmessages,"#>>> b=%.3e a=%.3e m=%.3e  ll=%.3e\n",par[0],par[1],par[2],fmin);
  }


  /* no minimization */
  /* --------------- */
  if(!( (O_method & M_moments)|( O_method & M_global)|(O_method & M_intervals)))
    goto END;


  /* method of moments estimation */
  /* ---------------------------- */

  if(O_method & M_moments)
    {
    double ave,adev,sdev,var;
    
    /* compute statistics */
    moment_local(&ave, &adev, &sdev,&var);
    
    /* method of moments estimation for mu */
    if(!is_m_provided)
      par[2] = ave;
    
    if(O_verbose>=2)
      fprintf(Fmessages,"#--- METHOD OF MOMENTS\n");
    
    /* method of moments estimation for b */
    par[0]=mm(sdev/adev,O_verbose);
    
    /*  method of moment estimation for a */
    par[1] = sdev*pow(par[0],-1./par[0])*
      exp(.5*(gsl_sf_lngamma(1./par[0])-gsl_sf_lngamma(3./par[0])));
    
    {
      const double x[2]={par[0],par[2]};
      objf(2,x,NULL,&fmin);
    }

    if(O_verbose>=2)
      fprintf(Fmessages,"#>>> b=%.3e a=%.3e m=%.3e ll=%.3e\n",par[0],par[1],par[2],fmin);

  }
  
  /* ML estimation: global maximization */
  /* ---------------------------------- */
  
  if(is_m_provided && (O_method & M_global)){
    unsigned type[2];
    double xmin[2];
    double xmax[2];
    double x[2]; /* x[0]=b x[1]=m */

    if(O_verbose>=2)
      fprintf(Fmessages,"#--- UNCONSTRAINED OPTIMIZATION\n");

    /* set initial minimization boundaries */
    /* ----------------------------------- */

    type[0] = 4; xmin[0]=0;          xmax[0]=0;          x[0]=par[0];
    type[1] = 3; xmin[1]=provided_m; xmax[1]=provided_m; x[1]=provided_m;

    /* perform global minimization */
    /* --------------------------- */
    
    multimin(2,x,&fmin,type,xmin,xmax,objf,objdf,objfdf,NULL,global_oparams);

    /* store final values */
    /* ------------------ */
    par[0]=x[0];
    par[1]=geta(x[0],x[1]);    
    par[2]=x[1];

  } 
  else if(!is_m_provided) {

    unsigned type[2];
    double xmin[2];
    double xmax[2];
    double x[2]; /* x[0]=b x[1]=m */
    double dtmp1;    
    double xtmp[2]; /* store the temporary minimum  */
    unsigned oldindex,utmp1,index,max,min;

    if( O_method & M_global ){

      if(O_verbose>=2)
	fprintf(Fmessages,"#--- UNCONSTRAINED OPTIMIZATION\n");
    
      /* set initial minimization boundaries */
      /* ----------------------------------- */

      type[0] = 4; xmin[0]=0; xmax[0]=0; x[0]=par[0];
      type[1] = 0; xmin[1]=0; xmax[1]=0; x[1]=par[2];
      
      /* perform global minimization */
      /* --------------------------- */

      multimin(2,x,&fmin,type,xmin,xmax,objf,objdf,objfdf,NULL,global_oparams);
    
      if(O_verbose>=2){      
	fprintf(Fmessages,"#>>> b=%.3e a=%.3e m=%.3e ll=%.3e\n",x[0],geta(x[0],x[1]),x[1],fmin);
      }
    }


    if(!(O_method & M_intervals)) goto END;

    /* perform interval minimization */
    /* ----------------------------- */

    if(O_verbose>=2)
      fprintf(Fmessages,"#--- INTERVAL-WISE OPTIMIZATION\n");

    type[1] = 3;

    /* find initial index s.t. m \in [Data[index],Data[index+1]] */
    for(utmp1=0;Data[utmp1]<=x[1] && utmp1<Size;utmp1++);
    if(utmp1 == 0)
      index = 0;
    else if (utmp1 == Size)
      index= Size-2;
    else index = utmp1-1;
    
    xmin[1]=Data[index]; xmax[1]=Data[index+1]; x[1]=.5*(xmin[1]+xmax[1]);

    multimin(2,x,&fmin,type,xmin,xmax,objf,objdf,objfdf,NULL,interv_oparams);

    /* set the value of b to initial minimum */
    xtmp[0]=x[0];

    max=min=index;
    
    if(O_verbose>=3)
      fprintf(Fmessages,"#>>> [%+.3e:%+.3e] b=%e m=%e ll=%e\n",Data[index],Data[index+1],
	      x[0],x[1],fmin);

    /* move to the right compute new local minima and compare with
       global minimum */    
    do {
      oldindex=index;

      for(utmp1=max+1;
	  utmp1<=max+interv_step && utmp1<Size-1;
	  utmp1++){
	
	/* set boundaries on m */
	xmin[1]=Data[utmp1]; xmax[1]=Data[utmp1+1];
	/* set initial condition */
	xtmp[0]=x[0];		/* set to the last best value */
	xtmp[1]=.5*(xmin[1]+xmax[1]); /* set to the mid interval */


	multimin(2,xtmp,&dtmp1,type,xmin,xmax,objf,objdf,objfdf,NULL,interv_oparams);


	if(dtmp1<fmin){/* found new minimum */
	  index=utmp1;
	  x[0]=xtmp[0]; x[1]=xtmp[1];
	  fmin=dtmp1;

	  if(O_verbose>=3)
	    fprintf(Fmessages,"#>>> [%+.3e:%+.3e] ll=%e\n",
		    Data[utmp1],Data[utmp1+1],dtmp1);
	}
	else {/* NOT found new minimum */
	    if(O_verbose>=3)
	      fprintf(Fmessages,"#    [%+.3e:%+.3e] ll=%e\n",
		    Data[utmp1],Data[utmp1+1],dtmp1);
	}

      }
      max=utmp1-1;
    }
    while(index!=oldindex);

    /* set the value of b to initial minimum */
    xtmp[0]=x[0];


    /* move to the left compute new local minima and compare with
       global minimum */    

    do {
      oldindex=index;

      for(utmp1=min-1;
	  (int) utmp1 >= (int) min-interv_step && (int) utmp1 >= 0;
	  utmp1--){

	/* set boundaries on m */
	xmin[1]=Data[utmp1]; xmax[1]=Data[utmp1+1];
	/* set initial condition */
	xtmp[0]=x[0];		/* set to the last best value */
	xtmp[1]=.5*(xmin[1]+xmax[1]); /* set to the mid interval */

	multimin(2,xtmp,&dtmp1,type,xmin,xmax,objf,objdf,objfdf,NULL,interv_oparams);

	if(dtmp1<fmin){/* found new minimum */
	  index=utmp1;
	  x[0]=xtmp[0]; x[1]=xtmp[1];
	  fmin=dtmp1;

	  if(O_verbose>=3)
	    fprintf(Fmessages,"#>>> [%+.3e:%+.3e] ll=%e\n",
		    Data[utmp1],Data[utmp1+1],dtmp1);
	}
	else {/* NOT found new minimum */
	    if(O_verbose>=3)
	      fprintf(Fmessages,"#    [%+.3e:%+.3e] ll=%e\n",
		    Data[utmp1],Data[utmp1+1],dtmp1);
	}

      }
    min=utmp1+1;
    }
    while(index!=oldindex);

    /* store final values */
    /* ------------------ */
    par[0]=x[0];
    par[1]=geta(x[0],x[1]);    
    par[2]=x[1];

    if(O_verbose>=2)
      fprintf(Fmessages,"#>>> b=%.3e a=%.3e m=%.3e ll=%.3e\n",par[0],par[1],par[2],fmin);

    if(O_verbose>=3)
      fprintf(Fmessages,"#--- intervals explored: %d\n",max-min);

  }

 END:

  /* output */
  /* ------ */
  { 

    size_t i;

    /* define the var-covar matrix */
    gsl_matrix *V;

    /* set the size of the var-covar matrix */
    const size_t dim=(is_m_provided?2:3);

    if(O_output == 3 || O_verbose >=1 ){

      /* allocate var-covar matrix */
      V = gsl_matrix_calloc (dim,dim);
      
      /* compute var-covar matrix */
      varcovar(par,Size,dim,V);
      
      if(O_verbose>=1){
	fprintf(Fmessages,"#\n");
	fprintf(Fmessages,"#--- FINAL RESULT ----------------------------------\n");
	fprintf(Fmessages,"#\n");
	if(!is_m_provided)
	  fprintf(Fmessages,"#     value     std.err     |  b       a       m\n");
	else
	  fprintf(Fmessages,"#     value     std.err     |  b       a\n");

	fprintf(Fmessages,"# b = %- 10.4g %-10.4g |",par[0],sqrt(gsl_matrix_get(V,0,0)));
	for(i=0;i<dim;i++)
	  if(i==0)
	    fprintf(Fmessages,"    -- ");
	  else
	    fprintf(Fmessages," % .4f",gsl_matrix_get(V,0,i));
	fprintf(Fmessages,"\n");
	
	fprintf(Fmessages,"# a = %- 10.4g %-10.4g |",par[1],sqrt(gsl_matrix_get(V,1,1)));
	for(i=0;i<dim;i++)
	  if(i==1)
	    fprintf(Fmessages,"    -- ");
	  else
	    fprintf(Fmessages," % .4f",gsl_matrix_get(V,1,i));
	fprintf(Fmessages,"\n");
	
	if(!is_m_provided){
	  fprintf(Fmessages,"# m = %- 10.4g %-10.4g |",par[2],sqrt(gsl_matrix_get(V,2,2)));
	  for(i=0;i<dim;i++)
	    if(i==2)
	      fprintf(Fmessages,"    -- ");
	    else
	      fprintf(Fmessages," % .4f",gsl_matrix_get(V,2,i));
	  fprintf(Fmessages,"\n");
	}
	else
	  fprintf(Fmessages,"#m = %- 10.4g  ---        |\n",par[2]);
	
	fprintf(Fmessages,"#\n");
	fprintf(Fmessages,"#           Upper triangle: covariances\n");
	fprintf(Fmessages,"#           Lower triangle: correlation coefficients\n");
	fprintf(Fmessages,"#---------------------------------------------------\n");
	fprintf(Fmessages,"#\n");
      }
      
    }

    switch(O_output){
    case 0: /* print parameters b,a,m, and log-likelihood */
      if(O_verbose>=1){
	fprintf(Fmessages,"#");
	fprintf(Fmessages,EMPTY_SEP,"b");
	fprintf(Fmessages,EMPTY_SEP,"a");
	fprintf(Fmessages,EMPTY_SEP,"m");
	fprintf(Fmessages,EMPTY_NL,"log-like");
      }
      fprintf(Fparams,FLOAT_SEP,par[0]);
      fprintf(Fparams,FLOAT_SEP,par[1]);
      fprintf(Fparams,FLOAT_SEP,par[2]);
      fprintf(Fparams,FLOAT_NL,fmin);
      break;
    case 1: /* print distribution function */
      if(O_verbose==0)
	fprintf(Fparams,"b=%e a=%e m=%e ll=%e\n",par[0],par[1],par[2],fmin);
      printcumul(par);
      break;
    case 2: /* print density */
      if(O_verbose==0)
	fprintf(Fparams,"b=%e a=%e m=%e ll=%e\n",par[0],par[1],par[2],fmin);
      printdensity(par);
      break;
    case 3: /* print parameters final estimates and standard errors*/
      if(O_verbose>=1){
	fprintf(Fmessages,"#");
	fprintf(Fmessages,EMPTY_SEP,"b");
	fprintf(Fmessages,EMPTY_SEP,"a");
	fprintf(Fmessages,EMPTY_SEP,"m");
	fprintf(Fmessages,EMPTY_SEP,"sigma_b");
	fprintf(Fmessages,EMPTY_SEP,"sigma_a");
	if(!is_m_provided)
	  fprintf(Fmessages,EMPTY_NL,"sigma_m");
	else
	  fprintf(Fmessages,"\n");
      }
      
      fprintf(Fparams,FLOAT_SEP,par[0]);
      fprintf(Fparams,FLOAT_SEP,par[1]);
      fprintf(Fparams,FLOAT_SEP,par[2]);
      for(i=0;i<dim;i++) fprintf(Fparams,FLOAT_SEP,sqrt(gsl_matrix_get(V,i,i)));
      fprintf(Fparams,"\n");

      break;
    }

    /* free allocated space */
    if(O_output == 3 || O_verbose >=1 ) gsl_matrix_free(V);
    
  }

  free(Data);
  free(splitstring);
  exit(0);

}
