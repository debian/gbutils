/*
  gblafit (ver. 6.0) -- Fit a Laplace (double exponential) density

  Copyright (C) 2018 Giulio Bottazzi

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  (version 2) as published by the Free Software Foundation;
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/


/*
  Verbosity levels:
  0 just the final ouput
  1 the results of intermediate steps
  2 internal information on intermediate steps
  3 gory details on intermediate steps
*/


#include "tools.h"
#include "multimin.h"
#include "assert.h"


/* Global Variables */
/* ---------------- */

/* store data */
double *Data; /*the array of data*/
size_t Size;/*the number of data*/

/* output management */
FILE *Fmessages;/*stream for computation messages*/
FILE *Fparams;/*strem for final parameters*/
FILE *Ffitted;/*strem for distribution or density*/

/*------------------*/


/* Output Functions */
/*----------------- */

void printcumul(const double m, const double a){

  int i;
  double dtmp1; 

  for(i=0;i<Size;i++){
    if(Data[i]>m){
      dtmp1=1-exp( (m-Data[i])/a )/2;
     }
    else{
      dtmp1=exp( (Data[i]-m)/a )/2;
    }
    fprintf(Ffitted,FLOAT_SEP,Data[i]);
    fprintf(Ffitted,FLOAT_NL,dtmp1);
  }

}

void printdensity(const double m, const double a){

  int i;

  for(i=0;i<Size;i++){
    double dtmp1=Data[i];
    fprintf(Ffitted,FLOAT_SEP,dtmp1);
    dtmp1=dtmp1-m;
    if(dtmp1>=0){
      fprintf(Ffitted,FLOAT_NL,exp(-dtmp1/a)/(2*a));
    }
    else{
      fprintf(Ffitted,FLOAT_NL,exp( dtmp1/a)/(2*a));
    }
  }
  
}
/*----------------- */


/* Object Function */
/*---------------- */

double nll(const double m){

  unsigned utmp1;

  double sum=0.0;

  for(utmp1=0;utmp1<Size;utmp1++)
    sum += fabs(m-Data[utmp1]);

  return log(2*sum/Size)+1.;

}


int main(int argc,char* argv[]){
  
  /* store guess */
  double m,a;
  
  /* store loglike */
  double fmin;

  /* store possibly provided values for parameters */
  double provided_m=0;
  unsigned is_m_provided=0;

  /* set various options */
  unsigned O_verbose=0;
  unsigned O_output=0;

  /* range for option -O3 */
  double xmin=0.0,xmax=0.0;
  unsigned steps=10;

  char *splitstring = strdup(" \t");

  /* COMMAND LINE PROCESSING */
  /* ----------------------- */ 
  int opt;

  /* read the command line */    
  while((opt=getopt_long(argc,argv,"n:O:m:V:x:hF:",gb_long_options, &gb_option_index))!=EOF){
    if(opt==0){
      gbutils_header(argv[0],stdout);
      exit(0);
    }
    else if(opt=='?'){
      fprintf(stderr,"option %c not recognized\n",optopt);
      return(-1);
    }
    else if(opt=='h'){
      /*print short help*/
      fprintf(stdout,"Fit Laplace density. Read data from files or from standard input.  \n");
      fprintf(stdout,"With output option '4' prints the log-likelihood as a function of m   \n");
      fprintf(stdout,"Usage: %s [options] [files]\n\n",argv[0]);
      fprintf(stdout,"input.                                                             \n\n");
      fprintf(stdout," Options:                                                            \n");
      fprintf(stdout," -O  output type (default 0)                \n");
      fprintf(stdout,"      0  parameters m a and negative log-likelihood\n");
      fprintf(stdout,"      1  the estimated distribution function computed on the provided points     \n");
      fprintf(stdout,"      2  the estimated density function computed on the provided points \n");
      fprintf(stdout,"      3  parameters m, a and their standard errors and correlations \n");
      fprintf(stdout,"      4  log-likelihood profile \n");
      fprintf(stdout," -m  the mode is not estimated but is set to the value provided\n");
      fprintf(stdout," -x  initial value of m or plot range if output type is '4' (default 0)\n");
      fprintf(stdout," -n  number of plotted points if output type is '4' (default 10)\n");
      fprintf(stdout,"Examples:\n");
      fprintf(stdout," gblafit -m 1 <file  estimate a with m=1\n");
      fprintf(stdout," gblafit -O 4 -x -1,1 <file  print the log-likelihood on a regular grid\n");
      fprintf(stdout,"                             from -1 to 1. The grid has 10 points. \n");
      exit(0);
    }
    else if(opt=='V'){
      O_verbose=atoi(optarg);
    }
    else if(opt=='O'){
      O_output=atoi(optarg);
    }
    else if(opt=='m'){
      provided_m=atof(optarg);
      is_m_provided=1;
    }
    else if(opt=='x'){
      char *stmp1=strdup (optarg);
      char *stmp2;
      char *stmp3=stmp1;
      if( (stmp2=strsep (&stmp1,",")) && stmp1 != NULL ){
	if(strlen(stmp2)>0) 
	  xmin=atof(stmp2);
	xmax=atof(stmp1);
      }
      else{
	xmin=xmax=m=atof(optarg);
      }
      free(stmp3);
    }
    else if(opt=='n'){
      steps = atoi(optarg);
    }
    else if(opt=='F'){
      /*set the fields separator string*/
      free(splitstring);
      splitstring = strdup(optarg);
    }
  }

  /* initialize global variables */
  initialize_program(argv[0]);

  /* customized admin. of errors */
  /* --------------------------- */
  gsl_set_error_handler_off ();

  
  /* set the various streams */
  /* ----------------------- */
  if(O_output==0){
    Fmessages=stderr;
    Fparams=stdout;
    Ffitted=stderr;
  }
  else{
    Fmessages=stderr;
    Fparams=stderr;
    Ffitted=stdout;
  }

  /* load Data */
  /* --------- */
  if(O_verbose>=2)
    fprintf(Fmessages,"#--- LOADING DATA\n");
  
  load(&Data,&Size,0,splitstring);

  if (Size <= 1) {
    fprintf(Fmessages,"the number of onservations must be at least 2\n");
    exit(-1);
  }

  /* sort data */
  /* --------- */
  qsort(Data,Size,sizeof(double),sort_by_value);


  /* ---------------------- */
  if(O_output<4){

    /* store Fisher sqrt(variance-covariance)*/
    /* sigma[0] = std.err on m */
    /* sigma[1] = std.err on a */
    /* sigma[2] = corr.coeff m vs. a */
    double sigma[3];

    size_t i;

    /* decide the value of m  */
    if (is_m_provided){
      m = provided_m;
    }
    else{
      size_t minindex;
    
      fmin=DBL_MAX;
      for(i=1;i<Size-1;i++){
	const double dtmp1 = nll(Data[i]);
	if(dtmp1<fmin) {
	  fmin=dtmp1;
	  minindex = i;
	}
/* 	printf("%d mll[%e]=%e [%e] ",i,Data[i],dtmp1,fmin); */
/* 	if(minindex == i) */
/* 	  printf("*"); */
/* 	printf("\n"); */
      }
      m =  Data[minindex];
    }

    /* decide the value of a  */    
    a=0;
    for(i=0;i<Size;i++){
      a+=fabs(m-Data[i]);
    }
    a/=Size;

    /* compute variance-covariance  */
    sigma[0] = 2*a*a;
    sigma[1] = 1./sqrt(4.*a*a);
    sigma[2] = -1./sqrt(4.*a*a);

    if(O_verbose>=1){
      fprintf(Fmessages,"#\n");
      fprintf(Fmessages,"#--- FINAL RESULT -----------------------------------\n");
      fprintf(Fmessages,"#                           | correlation matrix\n");
      fprintf(Fmessages,"#      value     std.err    |  m       a\n");
      fprintf(Fmessages,"#m  = %- 10.4g %-10.4g | % .4f % .4f\n",
	      m,sigma[0]/sqrt(Size),1.,sigma[2]);
      fprintf(Fmessages,"#al = %- 10.4g %-10.4g | % .4f % .4f\n",
	      a,sigma[1]/sqrt(Size),sigma[2],1.);
      fprintf(Fmessages,"#----------------------------------------------------\n");
    }
    
    /* print result */
    switch(O_output){
    case 0: /* print parameters m, a and log-likelihood */
      if(O_verbose>=1){
	fprintf(Fmessages,"#");
	fprintf(Fmessages,EMPTY_SEP,"m");
	fprintf(Fmessages,EMPTY_SEP,"a");
	fprintf(Fmessages,EMPTY_NL,"log-like");
      }
      fprintf(Fparams,FLOAT_SEP,m);
      fprintf(Fparams,FLOAT_SEP,a);
      fprintf(Fparams,FLOAT_NL,fmin);
      break;
    case 1: /* print distribution function */
      if(O_verbose==0){
	fprintf(Fmessages,"#");
	fprintf(Fmessages,EMPTY_SEP,"m");
	fprintf(Fmessages,EMPTY_SEP,"a");
	fprintf(Fmessages,EMPTY_NL,"log-like");
      }
      printcumul(m,a);
    break;
    case 2: /* print density */
      if(O_verbose==0){
	fprintf(Fmessages,"#");
	fprintf(Fmessages,EMPTY_SEP,"m");
	fprintf(Fmessages,EMPTY_SEP,"a");
	fprintf(Fmessages,EMPTY_NL,"log-like");
      }
      printdensity(m,a);
      break;
    case 3: /* print parameters final estimates and standard errors*/
      if(O_verbose>=1){
	fprintf(Fmessages,"#");
	fprintf(Fmessages,EMPTY_SEP,"m");
	fprintf(Fmessages,EMPTY_SEP,"a");
	fprintf(Fmessages,EMPTY_SEP,"sigma_m");
	fprintf(Fmessages,EMPTY_SEP,"sigma_a");
	fprintf(Fmessages,EMPTY_NL,"corr_m_a");
      }
      fprintf(Fparams,FLOAT_SEP,m);
      fprintf(Fparams,FLOAT_SEP,a);
      fprintf(Fparams,FLOAT_SEP,sigma[0]/sqrt(Size));
      fprintf(Fparams,FLOAT_SEP,sigma[1]/sqrt(Size));
      fprintf(Fparams,FLOAT_NL,sigma[2]);

      break;
    }

  }
  else if(O_output==4){/* print the negative log-likelihood */

    size_t i;

    if(xmin==xmax) steps = 1;

    for(i=0;i<steps;i++){
      const double val=xmin*(1.-i/((double) steps -1))+ xmax*i/((double) steps -1);
      fprintf(Ffitted,FLOAT_SEP,val);
      fprintf(Ffitted,FLOAT_NL,nll(val));
    }

    printf("\n\n");

    for(i=0;i<Size;i++){
      const double val=Data[i];
      fprintf(Ffitted,FLOAT_SEP,val);
      fprintf(Ffitted,FLOAT_NL,nll(val));
    }

  }

  free(Data);
  free(splitstring);
  exit(0);

}
