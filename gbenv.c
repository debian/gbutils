/*
  gbenv  (ver. 5.6) -- Floating point locale, and gbutils settings
  Copyright (C) 2006-2015 Giulio Bottazzi

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  (version 2) as published by the Free Software Foundation;
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#include "tools.h"

#ifdef linux
#include <fpu_control.h>
#include <fenv.h>
#endif

#include <langinfo.h>
#include <locale.h>

int main(int argc,char* argv[]){

  /* variables for reading command line options */
  /* ------------------------------------------ */
  int opt;
  /* ------------------------------------------ */

  /* COMMAND LINE PROCESSING */
      
  while((opt=getopt_long(argc,argv,"h",gb_long_options, &gb_option_index))!=EOF){
    if(opt==0){
      gbutils_header(argv[0],stdout);
      exit(0);
    }
    else if(opt=='?'){
      fprintf(stderr,"option %c not recognized\n",optopt);
      exit(-1);
    }
    else if(opt=='h'){
      /*print help*/
      fprintf(stdout,"Show floating point environment (for x86 family processors), locale and\n");
      fprintf(stdout,"gbutils settings.\n");
      fprintf(stdout,"   \n");
      fprintf(stdout,"Information about the floating point environment includes:\n");
      fprintf(stdout,"  if the environment is the default environment of the machine;\n");
      fprintf(stdout,"  if the precision is single, double or extended;\n");
      fprintf(stdout,"  if the rounding is nearest, upward, downward or toward zero;\n");
      fprintf(stdout,"  the list of exceptions whose interrupts are ignored (masked).\n");
      fprintf(stdout,"Information about the locale includes:    \n");
      fprintf(stdout,"  decimal point;\n");
      fprintf(stdout,"  thousand separator;\n");
      fprintf(stdout,"  the relative position of the thousand separator (grouping).\n");
      fprintf(stdout,"Information about gbutils environment include:\n");
      fprintf(stdout,"  if the error handler is activated;\n");
      fprintf(stdout,"  the value of the environment variables dictating the output format;\n");
      fprintf(stdout,"  the output format of integers and floating point numbers.\n");
      fprintf(stdout,"Options:                                                               \n");
      fprintf(stdout," -h  print this help message                                           \n");
      exit(0);
    }
  }
  /* END OF COMMAND LINE PROCESSING */


/* ---- linux implementation and x86 processor ---- */
#if defined(linux) && ( defined(__x86_64__) || defined(__i386) )

  unsigned short mode = 0 ;

  int rounding =  fegetround ();

  printf(" floating point environment ------------------------\n");

#ifdef _FPU_GETCW
  _FPU_GETCW(mode);

#define PREC_MASK  _FPU_EXTENDED

  printf(" The environment is ");
  if( mode==_FPU_DEFAULT ) 
    printf("the default\n");
  else
    printf("not the default\n");

  /* precision */
  printf(" Precision is ");
  if ( (mode & PREC_MASK) == _FPU_EXTENDED)
    printf("extended");
  else if ( (mode & PREC_MASK) == _FPU_DOUBLE)
    printf("double");
  else if ( (mode & PREC_MASK) == _FPU_SINGLE)
    printf("single");
  printf("\n");
#endif
  
  /* rounding */
  printf(" Rounding is ");
  switch(rounding) {
  case FE_TONEAREST :
    printf("nearest\n");
    break;
  case FE_UPWARD :
    printf("upward\n");
    break;
  case FE_DOWNWARD :
    printf("downward\n");
    break;
  case FE_TOWARDZERO :
    printf("toward zero\n");
    break;
  }


  /* exception */
  printf(" Masked exceptions (no interrupt):\n");
#ifdef FE_INVALID
  if(!fetestexcept(FE_INVALID)) printf("    Invalid operation\n");
#endif
#ifdef FE_DIVBYZERO
  if(!fetestexcept(FE_DIVBYZERO)) printf("    Division by zero\n");
#endif
#ifdef FE_UNDERFLOW
  if(!fetestexcept(FE_UNDERFLOW)) printf("    Underflow\n");
#endif
#ifdef FE_OVERFLOW
  if(!fetestexcept(FE_OVERFLOW)) printf("    Overflow\n");
#endif
#ifdef FE_INEXACT
  if(!fetestexcept(FE_INEXACT)) printf("    Precision\n");
#endif
  printf("\n");

#endif
/* ------------------------------ */


  printf(" locale definitions --------------------------------\n");
  {

    if (setlocale(LC_NUMERIC,"") == NULL)
      fprintf(stderr,"WARNING:Cannot set LC_NUMERIC to default locale\n");

    printf(" decimal point        \"%s\"\n",nl_langinfo(RADIXCHAR));
    printf(" thousands separator  \"%s\"\n",nl_langinfo(THOUSEP));
    printf(" grouping             ");

    {/* code snippet stolen from locale.c in glibc */

      struct lconv *locale = localeconv();

      const char *val = locale->grouping;
      int cnt = val ? strlen (val) : 0;

      while (cnt > 1){
        printf ("%d;", *val == '\177' ? -1 : *val);
        --cnt;
        ++val;
      }
      
      printf ("%d\n", cnt == 0 || *val == '\177' ? -1 : *val);
    }

  }
  printf("\n");


#if defined HAVE_LIBGSL
  printf(" gsl environment -----------------------------------\n");
  printf(" GSL error handler is ");
  if(getenv("GB_ERROR_HANDLER_OFF"))
    printf(" off\n");
  else
    printf(" on\n");
  printf("\n");
#endif


  printf(" gbutils environment -------------------------------\n");

  printf(" GB_OUT_FLOAT_FORMAT is ");
  if(FLOAT==NULL && getenv("GB_OUT_FLOAT_FORMAT"))
    printf("%s\n",getenv("GB_OUT_FLOAT_FORMAT"));
  else
    printf("not set\n");

  printf(" GB_OUT_EMPTY_FORMAT is ");
  if(FLOAT==NULL && getenv("GB_OUT_EMPTY_FORMAT"))
    printf("%s\n",getenv("GB_OUT_EMPTY_FORMAT"));
  else
    printf("not set\n");

  printf(" GB_OUT_SEP is ");
  if(FLOAT==NULL && getenv("GB_OUT_SEP"))
    printf("%s\n",getenv("GB_OUT_SEP"));
  else
    printf("not set\n");

  printf(" GB_OUT_NL is ");
  if(FLOAT==NULL && getenv("GB_OUT_NL"))
    printf("%s\n",getenv("GB_OUT_NL"));
  else
    printf("not set\n");

  printf("\n");

  printf(" gbutils settings ----------------------------------\n");
  
  initialize_program(argv[0]);

  printf(" floating point output   \"%s\"\n",FLOAT);
  printf(" integer output          \"%s\"\n",INT);
  printf(" empty field             \"%s\"\n",EMPTY);
  printf(" separation string       \"%s\"\n",SEP);

  return 0;
}
