/*
  gbnlqreg (ver. 5.6) -- Non linear quantile regression
  Copyright (C) 2005-2018 Giulio Bottazzi

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  (version 2) as published by the Free Software Foundation;
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#include "tools.h"
#include "matheval.h"
#include "assert.h"

#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_multifit_nlin.h>
#include <gsl/gsl_cdf.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_multimin.h>


/* handy structure used in computation */
typedef struct function {
  void *f;
  size_t argnum;
  char **argname;
  void **df;
  void ***ddf;
} Function;


struct objdata {
  Function * F;
  size_t rows;
  size_t columns;
  double theta;
  double **data;
};



/* Quantile estimation */
/* ------------------- */

double
qreg_obj_f (const gsl_vector * x, void *params){

  const Function *F = ((struct objdata *)params)->F;
  const size_t rows=((struct objdata *)params)->rows;
  const size_t columns=((struct objdata *)params)->columns;
  double **data=((struct objdata *)params)->data;
  const double theta=((struct objdata *)params)->theta;

  const size_t pnum = x->size;
  double values[pnum+columns];
  size_t i,j;
  double sumL=0,sumR=0;

  /* set the parameter values */
  for(i=columns;i<columns+pnum;i++)
    values[i] = gsl_vector_get(x,i-columns);

  for(i=0;i<rows;i++){

    /* set the variables values */
    for(j=0;j<columns;j++)
      values[j] = data[j][i];

    const double dtmp1 = evaluator_evaluate (F->f,columns+pnum,F->argname,values);
    if(dtmp1>0) sumR += dtmp1;
    else sumL += -dtmp1;
  }
  sumL/=rows;
  sumR/=rows;

  return (1-theta)*sumL+theta*sumR;
}


/* compute variance-covariance matrix */
gsl_matrix *qreg_varcovar(const int o_varcovar,
			  const gsl_multimin_fminimizer *s,
			  const struct objdata params,
			  const size_t Pnum)
{

  size_t i,j,h;

  const size_t rows=((struct objdata )params).rows;
  const size_t columns=((struct objdata )params).columns;
  double **data=((struct objdata )params).data;
  const Function *F = ((struct objdata )params).F;
  const double theta=((struct objdata )params).theta;

  /* the minimized value */
  const double a = s->fval;

  double values[columns+Pnum];
      

  gsl_matrix *covar = gsl_matrix_alloc (Pnum,Pnum);


  switch(o_varcovar){
  case 0: /* inverse "reduced Hessian", adapted following Numerical Recipes */
    {
      gsl_permutation * P = gsl_permutation_alloc (Pnum);
      int signum;

      gsl_matrix *H = gsl_matrix_calloc (Pnum,Pnum);
      gsl_matrix *dH = gsl_matrix_calloc (Pnum,Pnum);

      double df1,df2;

      /* fill with optimal parameter */
      for(i=columns;i<columns+Pnum;i++)
	values[i] = gsl_vector_get(s->x,i-columns);
	
      /* compute Hessian */
      for(i=0;i<rows;i++){
	  
	for(j=0;j<columns;j++)
	  values[j] = data[j][i];

	for(j=0;j<Pnum;j++){
	  df1 = evaluator_evaluate ((F->df)[j],columns+Pnum,F->argname,values);
	  for(h=0;h<Pnum;h++){
	    df2 = evaluator_evaluate ((F->df)[h],columns+Pnum,F->argname,values);
	    gsl_matrix_set (dH,j,h,theta*(1-theta)*df1*df2);
	  }
	}
	gsl_matrix_add (H,dH);
      }

	
      /* invert Hessian; dH store temporary LU decomp. */
      gsl_matrix_memcpy (dH,H);
      gsl_linalg_LU_decomp (dH,P,&signum);
      gsl_linalg_LU_invert (dH,P,covar);

      gsl_matrix_scale (covar,a*a);

      gsl_matrix_free(H);
      gsl_matrix_free(dH);
      gsl_permutation_free(P);
    }
    break;
  case 1: /* J^{-1} */
    {

      gsl_permutation * P = gsl_permutation_alloc (Pnum);
      int signum;

      gsl_matrix *J = gsl_matrix_calloc (Pnum,Pnum);
      gsl_matrix *dJ = gsl_matrix_calloc (Pnum,Pnum);

      double f,df1,df2;

      /* fill with optimal parameter */
      for(i=columns;i<columns+Pnum;i++)
	values[i] = gsl_vector_get(s->x,i-columns);
	
      /* compute information matrix */
      for(i=0;i<rows;i++){
	  
	for(j=0;j<columns;j++)
	  values[j] = data[j][i];

	f = evaluator_evaluate (F->f,columns+Pnum,F->argname,values);

	for(j=0;j<Pnum;j++){
	  df1 = evaluator_evaluate ((F->df)[j],columns+Pnum,F->argname,values);
	  for(h=0;h<Pnum;h++){
	    df2 = evaluator_evaluate ((F->df)[h],columns+Pnum,F->argname,values);
	    gsl_matrix_set (dJ,j,h,
			    df1*df2*(f>0 ? theta*theta : (1-theta)*(1-theta)));
	  }
	}
	gsl_matrix_add (J,dJ);
      }
	

      /* invert information matrix; dJ store temporary LU decomp. */
      gsl_matrix_memcpy (dJ,J);
      gsl_linalg_LU_decomp (dJ,P,&signum);
      gsl_linalg_LU_invert (dJ,P,covar);

      gsl_matrix_scale (covar,a*a);

      gsl_matrix_free(J);
      gsl_matrix_free(dJ);
      gsl_permutation_free(P);

    }
    break;

  case 2: /* H^{-1} */
    {
      gsl_permutation * P = gsl_permutation_alloc (Pnum);
      int signum;

      gsl_matrix *H = gsl_matrix_calloc (Pnum,Pnum);
      gsl_matrix *dH = gsl_matrix_calloc (Pnum,Pnum);

      double f,df1,df2,ddf;

      /* fill with optimal parameter */
      for(i=columns;i<columns+Pnum;i++)
	values[i] = gsl_vector_get(s->x,i-columns);
	
      /* compute Hessian */
      for(i=0;i<rows;i++){
	  
	for(j=0;j<columns;j++)
	  values[j] = data[j][i];

	f = evaluator_evaluate (F->f,columns+Pnum,F->argname,values);
	  
	for(j=0;j<Pnum;j++){
	  df1 = evaluator_evaluate ((F->df)[j],columns+Pnum,F->argname,values);
	  for(h=0;h<Pnum;h++){
	    df2 = evaluator_evaluate ((F->df)[h],columns+Pnum,F->argname,values);
	    ddf = evaluator_evaluate ((F->ddf)[j][h],columns+Pnum,F->argname,values);
	    gsl_matrix_set (dH,j,h,(f>0 ? theta: -(1-theta))*a*ddf
			    +theta*(1-theta)*df1*df2);
	  }
	}
	gsl_matrix_add (H,dH);
      }

	
      /* invert Hessian; dH store temporary LU decomp. */
      gsl_matrix_memcpy (dH,H);
      gsl_linalg_LU_decomp (dH,P,&signum);
      gsl_linalg_LU_invert (dH,P,covar);

      gsl_matrix_scale (covar,a*a);

      gsl_matrix_free(H);
      gsl_matrix_free(dH);
      gsl_permutation_free(P);
    }
    break;

  case 3: /* H^{-1} J H^{-1} */
    {

      gsl_permutation * P = gsl_permutation_alloc (Pnum);
      int signum;

      gsl_matrix *H = gsl_matrix_calloc (Pnum,Pnum);
      gsl_matrix *dH = gsl_matrix_calloc (Pnum,Pnum);

      gsl_matrix *J = gsl_matrix_calloc (Pnum,Pnum);
      gsl_matrix *dJ = gsl_matrix_calloc (Pnum,Pnum);

      double f,df1,df2,ddf;

      /* fill with optimal parameter */
      for(i=columns;i<columns+Pnum;i++)
	values[i] = gsl_vector_get(s->x,i-columns);
	
      /* compute Hessian and information matrix */
      for(i=0;i<rows;i++){
	  
	for(j=0;j<columns;j++)
	  values[j] = data[j][i];

	f = evaluator_evaluate (F->f,columns+Pnum,F->argname,values);
	  
	for(j=0;j<Pnum;j++){
	  df1 = evaluator_evaluate ((F->df)[j],columns+Pnum,F->argname,values);
	  for(h=0;h<Pnum;h++){
	    df2 = evaluator_evaluate ((F->df)[h],columns+Pnum,F->argname,values);
	    ddf = evaluator_evaluate ((F->ddf)[j][h],columns+Pnum,F->argname,values);
	    gsl_matrix_set (dH,j,h,(f>0 ? theta: -(1-theta))*a*ddf
			    +theta*(1-theta)*df1*df2);
	    gsl_matrix_set (dJ,j,h,
			    df1*df2*(f>0 ? theta*theta : (1-theta)*(1-theta)));
	  }
	}
	gsl_matrix_add (H,dH);
	gsl_matrix_add (J,dJ);
      }

	
      /* invert Hessian; dH store temporary LU decomp. */
      gsl_matrix_memcpy (dH,H);
      gsl_linalg_LU_decomp (dH,P,&signum);
      gsl_linalg_LU_invert (dH,P,H);

      /* dJ = H^{-1} J ; covar = dJ H^{-1} */
      gsl_blas_dgemm (CblasNoTrans,
		      CblasNoTrans,1.0,H,J,0.0,dJ);
      gsl_blas_dgemm (CblasNoTrans,
		      CblasNoTrans,1.0,dJ,H,0.0,covar);

      gsl_matrix_scale (covar,a*a);

      gsl_matrix_free(H);
      gsl_matrix_free(dH);
      gsl_matrix_free(J);
      gsl_matrix_free(dJ);
      gsl_permutation_free(P);

    }
    break;

  }

  return covar;

}



int main(int argc,char* argv[]){

  int i;

  char *splitstring = strdup(" \t");
  int o_verbose=0;
  int o_output=0;
  int o_varcovar=0;
  int o_method=0;

  /* data from sdtin */
  size_t rows=0,columns=0;
  double **data=NULL;

  /* definition of the function */
  Function F;
  char **Param=NULL;
  double *Pval=NULL;
  gsl_matrix *Pcovar=NULL;  /* covariance matrix */
  size_t Pnum=0;

  /* minimization tolerance  */
  double eps=1e-5;

  /* initial scaling of simplex size */
  double init_scale =1.0;

  /* max number of steps */
  size_t maxsteps = 500;

  /* quantile  */
  double theta=0.5;


  /* variables for reading command line options */
  /* ------------------------------------------ */
  int opt;
  /* ------------------------------------------ */

  /* COMMAND LINE PROCESSING */
  while((opt=getopt_long(argc,argv,"v:hF:O:V:e:q:M:s:",gb_long_options, &gb_option_index))!=EOF){
    if(opt==0){
      gbutils_header(argv[0],stdout);
      exit(0);
    }
    else if(opt=='?'){
      fprintf(stderr,"option %c not recognized\n",optopt);
      exit(-1);
    }
    else if(opt=='h'){
      /*print help*/
      fprintf(stdout,"Non linear quantile regression. Read data in columns (X_1 .. X_N).\n");
      fprintf(stdout,"The model is specified by a function f(x1,x2...) using variables names\n");
      fprintf(stdout,"x1,x2,..,xN for the first, second .. N-th column of data. f(x1,x2,...)\n");
      fprintf(stdout,"are assumed i.i.d.\n");
      fprintf(stdout,"\nUsage: %s [options] <function definition> \n\n",argv[0]);
      fprintf(stdout,"Options:\n");
      fprintf(stdout," -O  type of output (default 0) \n");
      fprintf(stdout,"      0  parameters                     \n");
      fprintf(stdout,"      1  parameters and errors          \n");
      fprintf(stdout,"      2  <variables> and residuals      \n");
      fprintf(stdout,"      3  parameters and variance matrix \n");
      fprintf(stdout," -V  variance matrix estimation (default 0)\n");
      fprintf(stdout,"      0 <gradF gradF^t>                \n");
      fprintf(stdout,"      1  < J^{-1} >                    \n");
      fprintf(stdout,"      2 < H^{-1} >                     \n");
      fprintf(stdout,"      3  < H^{-1} J H^{-1} >           \n");
      fprintf(stdout," -q  set the quantile (default .5)\n");
      fprintf(stdout," -M  choose optimization method (default 0)   \n");
      fprintf(stdout,"      0  Nelder-Mead Simplex o(N)\n");
      fprintf(stdout,"      1  Nelder-Mead Simplex o(N^2)\n");
      fprintf(stdout," -s  initial simplex scaling (default 1)\n");
      fprintf(stdout," -e  minimization tolerance (default 1e-5)\n");
      fprintf(stdout," -F  input fields separators (default \" \\t\")\n");
      fprintf(stdout," -h  this help                      \n");
      fprintf(stdout," -v  verbosity level (default 0)\n");
      fprintf(stdout,"      0  just results                   \n");
      fprintf(stdout,"      1  comment headers                \n");
      fprintf(stdout,"      2  summary statistics             \n");
      fprintf(stdout,"      2  summary statistics             \n");
      fprintf(stdout,"      3  covariance matrix           \n");
      fprintf(stdout,"      4  minimization steps             \n");
      fprintf(stdout,"      5  model definition               \n");
      fprintf(stdout," -N  max minimization steps (default 500) \n");
      exit(0);
    }
    else if(opt=='e'){
      /* set the minimization tolerance */
      eps = fabs(atof(optarg));
    }
    else if(opt=='s'){
      /* set the minimization tolerance */
      init_scale = fabs(atof(optarg));
    }
    else if(opt=='q'){
      /* set the minimization tolerance */
      theta = fabs(atof(optarg));
      if(theta <= 0 || theta >=1){
	fprintf(stderr,"ERROR (%s): quantile '%f' must be in (0.1). Try option -h.\n",
		GB_PROGNAME,theta);
	exit(-1);

      }
    }
    else if(opt=='F'){
      /*set the fields separator string*/
      free(splitstring);
      splitstring = strdup(optarg);
    }
    else if(opt=='M'){
      /*set the estimation method*/
      o_method = atoi(optarg);
      if(o_method<0 || o_method>2){
	fprintf(stderr,"ERROR (%s): method option '%d' not recognized. Try option -h.\n",
		GB_PROGNAME,o_method);
	exit(-1);
      }
    }
    else if(opt=='O'){
      /* set the type of output */
      o_output = atoi(optarg);
      if(o_output<0 || o_output>3){
	fprintf(stderr,"ERROR (%s): output option '%d' not recognized. Try option -h.\n",
		GB_PROGNAME,o_output);
	exit(-1);
      }
    }
    else if(opt=='V'){
      /* set the type of covariance */
      o_varcovar = atoi(optarg);
      if(o_varcovar<0 || o_varcovar>3){
	fprintf(stderr,"ERROR (%s): variance option '%d' not recognized. Try option -h.\n",
		GB_PROGNAME,o_varcovar);
	exit(-1);
      }
    }
    else if(opt=='v'){
      o_verbose = atoi(optarg);
    }
  }
  /* END OF COMMAND LINE PROCESSING */

  /* initialize global variables */
  initialize_program(argv[0]);

  /* load data */
  loadtable(&data,&rows,&columns,0,splitstring);
  

  /* parse line for functions and variables specification */
  if(optind == argc){
    fprintf(stderr,"ERROR (%s): please provide a function to fit.\n",
	    GB_PROGNAME);
    exit(-1);
  }
    
  for(i=optind;i<argc;i++){
    char *piece=strdup (argv[i]);
    char *stmp1=piece;
    char *stmp2;
    
    while( (stmp2=strsep (&stmp1,",")) != NULL ){/*take a piece*/
      char *stmp3 = strdup(stmp2);
      char *stmp4 = stmp3;
      char *stmp5;
      /* 	fprintf(stderr,"token:%s\n",stmp3); */

      /* initial condition */
      if( (stmp5=strsep(&stmp4,"=")) != NULL && stmp4 != NULL ){
	if( strlen(stmp5)>0 && strlen(stmp4)>0){
	  Pnum++;
	  Param=(char **) my_realloc((void *)  Param,Pnum*sizeof(char *));
	  Pval=(double *) my_realloc((void *) Pval,Pnum*sizeof(double));
	  
	  Param[Pnum-1] = strdup(stmp5);
	  Pval[Pnum-1] = atof(stmp4);
	}
      }
      else{ /* allocate new function */
	F.f = evaluator_create (stmp3);
	assert(F.f);
      }
      free(stmp3);
    }
    free(piece);
  }


  /* check that everything is correct */
  if(Pnum==0){
    fprintf(stderr,"ERROR (%s): please provide a parameter to estimate.\n",
	    GB_PROGNAME);
    exit(-1);
  }


  {
    size_t i,j;
    char **NewParam=NULL;
    double *NewPval=NULL;
    size_t NewPnum=0;

    char **storedname=NULL;
    size_t storednum=0;

    /* retrive list of arguments and their number */
    /* notice that storedname is not allocated */
    {
      int argnum;
      evaluator_get_variables (F.f,&storedname,&argnum);
      storednum = (size_t) argnum;
    }

    /* check the definition of the function */
    for(i=0;i<storednum;i++){
      char *stmp1 = storedname[i];
      if(*stmp1 == 'x'){
	size_t index = (atoi(stmp1+1)>0?atoi(stmp1+1):0);
	if(index>columns){
	  fprintf(stderr,"ERROR (%s): column %zd not present in data\n",
		  GB_PROGNAME,index);
	  exit(-1);
	}
      }
      else {
	for(j=0;j<Pnum;j++)
	  if( strcmp(Param[j],stmp1)==0 ) break;
	if(j==Pnum){
	  fprintf(stderr,"ERROR (%s): parameter %s without initial value\n",
		  GB_PROGNAME,stmp1);
	  exit(-1);
	}
      }
    }

    /* remove unnecessary parameters */
    for(i=0;i<Pnum;i++){
      for(j=0;j<storednum;j++)
	if( strcmp(Param[i],storedname[j])==0 ) break;
      if(j==storednum){
	fprintf(stderr,"WARNING (%s): irrelevant parameter %s removed\n",
		GB_PROGNAME,Param[i]);
	continue;
      }
      NewPnum++;
      NewParam=(char **) my_realloc((void *)  NewParam,NewPnum*sizeof(char *));
      NewPval=(double *) my_realloc((void *) NewPval,NewPnum*sizeof(double));
      NewParam[NewPnum-1] = strdup(Param[i]);
      NewPval[NewPnum-1] = Pval[i];
    }

    for(i=0;i<Pnum;i++)
      free(Param[i]);
    free(Param);
    free(Pval);
    
    Param = NewParam;
    Pval = NewPval;
    Pnum= NewPnum;


    /* prepare the new list of argument names */
    F.argnum=columns+Pnum;
    F.argname = (char **) my_alloc((columns+Pnum)*sizeof(char *));
    
    for(i=0;i<columns;i++){

      int length;
      length = snprintf(NULL,0,"x%zd",i+1);
      F.argname[i] = (char *) my_alloc((length+1)*sizeof(char));
      snprintf(F.argname[i],length+1,"x%zd",i+1);

    }
    for(i=columns;i<columns+Pnum;i++){

      int length;
      length = snprintf(NULL,0,"%s",Param[i-columns]);
      F.argname[i] = (char *) my_alloc((length+1)*sizeof(char));
      snprintf(F.argname[i],length+1,"%s",Param[i-columns]);

    }

    /* define first order derivatives */
    F.df = (void **) my_alloc(Pnum*sizeof(void *));
    for(i=0;i<Pnum;i++){
      F.df[i] = evaluator_derivative (F.f,Param[i]);
      assert(F.df[i]);
    }

    /* define second order derivatives */
    if(o_varcovar>1){
      F.ddf = (void ***) my_alloc(Pnum*sizeof(void **));
      for(i=0;i<Pnum;i++)
	F.ddf[i] = (void **) my_alloc(Pnum*sizeof(void *));
      
      for(i=0;i<Pnum;i++)
	for(j=0;j<Pnum;j++){
	  F.ddf[i][j] = evaluator_derivative ((F.df)[i],Param[j]);
	  assert((F.ddf)[i][j]);
	}
    }

  }
  
  /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
  /* print builded functions and variables */
  if(o_verbose>4){
    size_t i,j;

    fprintf(stderr," ------------------------------------------------------------\n");

    fprintf(stderr,"  Model [xi: i-th data column]:\n");
    fprintf(stderr,"     %s\n", evaluator_get_string (F.f) );
    fprintf (stderr,"\n  Parameters and initial conditions:\n");
    for(i=0;i<Pnum;i++)
      fprintf (stderr,"      %s = %f\n", Param[i],Pval[i]);

    fprintf (stderr,"\n  Model first derivatives:\n");
    for(i=0;i<Pnum;i++)
      fprintf (stderr,"     d f(x) / d%s = %s\n",Param[i],evaluator_get_string ((F.df)[i]));

    if(o_varcovar>1){
      fprintf (stderr,"\n  Model second derivatives:\n");
      for(i=0;i<Pnum;i++)
	for(j=0;j<Pnum;j++)
	  fprintf (stderr,"     d^2 f(x) / d%s d%s = %s\n",Param[i],Param[j],
		   evaluator_get_string ((F.ddf)[i][j]));
    }
  }
  /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */


  /* PARAMETERS ESTIMATION */
  /* --------------------- */
  {

    /* set the method */
    const gsl_multimin_fminimizer_type *T;
    switch(o_method){
    case 0:
      T = gsl_multimin_fminimizer_nmsimplex2;
      break;
    case 1:
      T = gsl_multimin_fminimizer_nmsimplex;
      break;
    case 2:
      T = gsl_multimin_fminimizer_nmsimplex2rand;
      break;
    }

    gsl_multimin_fminimizer *s = NULL;
    gsl_vector *ss = gsl_vector_calloc(Pnum);
    gsl_vector_view x = gsl_vector_view_array (Pval, Pnum);
    gsl_multimin_function obj;

    size_t i,j;

    /* iteration controls */
    size_t iter=0;
    int status;

    /* set initial condition */
    struct objdata param;

    /* set the parameter for the function */
    param.F = &F;
    param.rows = rows;
    param.columns = columns;
    param.data = data ;
    param.theta = theta ;

    /* set the object structure */
    obj.f = &qreg_obj_f;
    obj.n = Pnum;
    obj.params = &param;

    /* set initial steps size */
    {
      double values[Pnum+columns];

      /* set the parameter values */
      for(i=columns;i<columns+Pnum;i++)
	values[i] = gsl_vector_get(&(x.vector),i-columns);

      /* absolute value of the derivatives */
      for(j=0;j<Pnum;j++) (ss->data)[j]=0.0;
      for(i=0;i<rows;i++){
	/* set the variables values */
	for(j=0;j<columns;j++)
	  values[j] = data[j][i];

	const double f = evaluator_evaluate (F.f,F.argnum,F.argname,values);
	for(j=0;j<Pnum;j++){
	  (ss->data)[j] +=
	    (f>0 ?
	     evaluator_evaluate (F.df[j],F.argnum,F.argname,values) :
	     -evaluator_evaluate (F.df[j],F.argnum,F.argname,values))*init_scale;
	}
      }

      for(j=0;j<Pnum;j++)
	 (ss->data)[j] = fabs((ss->data)[j]/rows);
    }

    /* initialize the solver */

    s = gsl_multimin_fminimizer_alloc (T, Pnum);
    gsl_multimin_fminimizer_set (s, &obj, &x.vector, ss);

    /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
    if(o_verbose>3){
      fprintf(stderr," ------------------------------------------------------------\n");
      fprintf(stderr,"      Stopping criteria: max{simplex size} < %g\n\n",eps);
      fprintf(stderr," Iter  ");
	for(i=0;i<Pnum;i++)
	  fprintf(stderr,"%-9s",Param[i]);
	fprintf(stderr,"f            ss          status\n");
    }
    /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

    do
      {

	status = gsl_multimin_fminimizer_iterate(s);
	const double size = gsl_multimin_fminimizer_size (s);
	iter++;

	/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
	/* print status */
	if(o_verbose>3){
	  fprintf (stderr,"%3zd  ",iter);
	  for(i=0;i<Pnum;i++)
	    fprintf (stderr,"%8.5f ",gsl_vector_get (s->x, i));
	  fprintf (stderr," %8.5e  %8.5e %s\n",
		   s->fval,size,gsl_strerror (status));
	}
	/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

	status=gsl_multimin_test_size (size,eps);
      }
    while (status == GSL_CONTINUE && iter < maxsteps);

    if(iter == maxsteps)
      fprintf(stderr,"WARNING (%s): failed to converge after %zd iterations\n",
	      GB_PROGNAME,maxsteps);

    /* store final values */
    for(i=0;i<Pnum;i++)
      Pval[i] = gsl_vector_get(s->x,i);

    /* build the covariance matrix */
    Pcovar = qreg_varcovar(o_varcovar,s,param,Pnum);

    /* free space */
    gsl_vector_free(ss);
    gsl_multimin_fminimizer_free (s);

  }

  /* output */
  /* ------ */

  /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
  if(o_verbose>2){
    size_t i,j;

    fprintf(stderr," ------------------------------------------------------------\n");
    fprintf(stderr,"  variance matrix                         = ");
    switch(o_varcovar){
    case 0: fprintf(stderr,"<gradF gradF^t>\n"); break;
    case 1: fprintf(stderr,"J^{-1}\n"); break;
    case 2: fprintf(stderr,"H^{-1}\n"); break;
    case 3: fprintf(stderr,"H^{-1} J H^{-1}\n"); break;
    }
    
    fprintf(stderr,"\n");
    for(i=0;i<Pnum;i++){
      fprintf(stderr," %s = %+f +/- %f (%5.1f%%) | ",
	      Param[i],Pval[i],
	      sqrt(gsl_matrix_get(Pcovar,i,i)),
	      100.*sqrt(gsl_matrix_get(Pcovar,i,i))/fabs(Pval[i]));
      for(j=0;j<Pnum;j++)
	if(j != i)
	  fprintf(stderr,"%+f ",
		  gsl_matrix_get(Pcovar,i,j)/sqrt(gsl_matrix_get(Pcovar,i,i)*gsl_matrix_get(Pcovar,j,j)));
	else
	  fprintf(stderr,"%+f ",1.0);
	fprintf(stderr,"|\n");
    }    
  }

  if(o_verbose>1)
    fprintf(stderr," ------------------------------------------------------------\n");
  /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
  
  switch(o_output){
  case 0:
    {
      size_t i;
      /* +++++++++++++++++++++++++++++++++++ */
      if(o_verbose>0){
	fprintf(stdout,"#");
	for(i=0;i<Pnum-1;i++)
	  fprintf(stdout,EMPTY_SEP,Param[i]);
	i=Pnum-1;
	fprintf(stdout,EMPTY_NL,Param[i]);
      }
      /* +++++++++++++++++++++++++++++++++++ */

      printline(stdout,Pval,Pnum);
    }
    break;
  case 1:
    {
      size_t i;

      /* +++++++++++++++++++++++++++++++++++ */
      if(o_verbose>0){
	fprintf(stdout,"#");
	for(i=0;i<Pnum-1;i++){
	  fprintf(stdout,EMPTY_SEP,Param[i]);
	  fprintf(stdout,EMPTY_SEP,"+/-");
	}
	i=Pnum-1;
	fprintf(stdout,EMPTY_SEP,Param[i]);
	fprintf(stdout,EMPTY_NL,"+/-");
      }
      /* +++++++++++++++++++++++++++++++++++ */
      
      for(i=0;i<Pnum-1;i++){
	fprintf(stdout,FLOAT_SEP,Pval[i]);
	fprintf(stdout,FLOAT_SEP,sqrt(gsl_matrix_get(Pcovar,i,i)));
      }
      i=Pnum-1;
      fprintf(stdout,FLOAT_SEP,Pval[i]);
      fprintf(stdout,FLOAT_NL,sqrt(gsl_matrix_get(Pcovar,i,i)));
    }
    break;
  case 2:
    {
      
      size_t i,j;
      double values[Pnum+columns];
      
      /* set the parameter values */
      for(i=columns;i<columns+Pnum;i++)
	values[i] = Pval[i-columns];
      
      /* +++++++++++++++++++++++++++++++++++ */
      if(o_verbose>0){
	fprintf(stderr,"#");
	for(i=0;i<Pnum-1;i++){
	  fprintf(stderr,EMPTY_SEP,Param[i]);
	  fprintf(stderr,EMPTY_SEP,"+/-");
	}
	i=Pnum-1;
	fprintf(stderr,EMPTY_SEP,Param[i]);
	fprintf(stderr,EMPTY_NL,"+/-");
	
	for(i=0;i<Pnum-1;i++){
	  fprintf(stderr,FLOAT_SEP,Pval[i]);
	  fprintf(stderr,FLOAT_SEP,sqrt(gsl_matrix_get(Pcovar,i,i)));
	}
	i=Pnum-1;
	fprintf(stderr,FLOAT_SEP,Pval[i]);
	fprintf(stderr,FLOAT_NL,sqrt(gsl_matrix_get(Pcovar,i,i)));
      }
      /* +++++++++++++++++++++++++++++++++++ */


      /* +++++++++++++++++++++++++++++++++++ */
      if(o_verbose>0){
	fprintf(stdout,"#");
	for(i=0;i<columns;i++){
	  char *string= (char *) my_alloc(sizeof(char)*4);
	  snprintf(string,4,"x%zd",i+1);
	  fprintf(stdout,EMPTY_SEP,string);
	  free(string);
	}
	fprintf(stdout,"%s\n",evaluator_get_string (F.f));
      }
      /* +++++++++++++++++++++++++++++++++++ */

      for(i=0;i<rows;i++){
	
	/* print columns and set the variables values */
	for(j=0;j<columns;j++){
	  fprintf(stdout,FLOAT_SEP,data[j][i]);
	  values[j] = data[j][i];
	}
	/* print residual */
	fprintf(stdout,FLOAT_NL,evaluator_evaluate (F.f,columns+Pnum,F.argname,values));
      }
    }
    break;
  case 3:
    {
      size_t i;

      /* +++++++++++++++++++++++++++++++++++ */
      if(o_verbose>0){
	fprintf(stdout,"#");
	for(i=0;i<Pnum-1;i++)
	  fprintf(stdout,EMPTY_SEP,Param[i]);
	i=Pnum-1;
	fprintf(stdout,EMPTY_NL,Param[i]);
      }
      /* +++++++++++++++++++++++++++++++++++ */
      
      printline(stdout,Pval,Pnum);
      
      fprintf(stdout,"\n\n");
      
      for(i=0;i<Pnum;i++){
	size_t j;
	for(j=0;j<Pnum-1;j++)
	  fprintf(stdout,FLOAT_SEP,gsl_matrix_get(Pcovar,i,j));
	j=Pnum-1;
	fprintf(stdout,FLOAT_NL,gsl_matrix_get(Pcovar,i,j));
      }
    }
    break;
  }
  

  /* deallocate function and data  */

  evaluator_destroy(F.f);
  {
    size_t i;
    for(i=0;i<F.argnum;i++)
      free(F.argname[i]);
    free(F.argname);
    
    for(i=0;i<columns;i++) 
      free(data[i]);
    free(data);
  }

  return 0;

}
